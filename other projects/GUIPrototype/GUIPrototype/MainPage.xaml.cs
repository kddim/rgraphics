﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace GUIPrototype
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }


        //Close events
        private void LeftSideAppBar_Closed(object sender, object e)
        {
            closeAllRightSideBars();
        }


        // Click events
        private void FiltersAppBarButton_Clicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            closeAllRightSideBars();

            FiltersAppBar.Visibility = Visibility.Visible;
        }

        private void EditAppBarButton_Clicked(object sender, RoutedEventArgs e)
        {
            closeAllRightSideBars();

            EditAppBar.Visibility = Visibility.Visible;
        }

        private void TransformAppBarButton_Clicked(object sender, RoutedEventArgs e)
        {
            closeAllRightSideBars();

            TransformAppBar.Visibility = Visibility.Visible;
        }

        private void SettingsAppBarButton_Clicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            closeAllRightSideBars();

            SettingsAppBar.Visibility = Visibility.Visible;
        }

        private void NewAppBarButton_Clicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        	closeAllRightSideBars();
        }

        private void LoadAppBarButton_Clicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        	closeAllRightSideBars();
        }

        private void SaveAppBarButton_Clicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        	closeAllRightSideBars();
        }

        private void ClearAppBarButton_Clicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        	closeAllRightSideBars();
        }

        // closing all right side bars
        private void closeAllRightSideBars()
        {
            FiltersAppBar.Visibility = Visibility.Collapsed;
            EditAppBar.Visibility = Visibility.Collapsed;
            TransformAppBar.Visibility = Visibility.Collapsed;
            SettingsAppBar.Visibility = Visibility.Collapsed;
        }
    }
}
