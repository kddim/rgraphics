﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Motorola.AiPOiS.ViewModels
{
    public enum ProgressState
    {
        Started,
        Completed
    }

    public class ProgressRaport
    {
        public double Value { get; set; }
        public ProgressRaport(double value)
        {
            this.Value = value * 100.0;
        }
    }
    public class ProgressRaportViewModel : PropertyChangedBase, IHandle<ProgressState>, IHandle<ProgressRaport>
    {
        public ProgressRaportViewModel()
        {
            IoC.Get<IEventAggregator>().Subscribe(this);
        }

        private int _Progress;
        public int Progress
        {
            get { return _Progress; }
            set
            {
                _Progress = value;
                NotifyOfPropertyChange(() => Progress);
                if (value > 0 && ProgressBarTextBlockVisibility == Visibility.Collapsed)
                    ProgressBarTextBlockVisibility = Visibility.Visible;
                else if (value == 0)
                    ProgressBarTextBlockVisibility = Visibility.Collapsed;
            }
        }

        private Visibility _ProgressBarVisibility = Visibility.Collapsed;
        public Visibility ProgressBarVisibility
        {
            get { return _ProgressBarVisibility; }
            set
            {
                _ProgressBarVisibility = value;
                NotifyOfPropertyChange(() => ProgressBarVisibility);
            }
        }

        private Visibility _ProgressBarTextBlockVisibility = Visibility.Collapsed;
        public Visibility ProgressBarTextBlockVisibility
        {
            get { return _ProgressBarTextBlockVisibility; }
            set
            {
                _ProgressBarTextBlockVisibility = value;
                NotifyOfPropertyChange(() => ProgressBarTextBlockVisibility);
            }
        }
        

        private ProgressState _CurrentState = ProgressState.Completed;
        public ProgressState CurrentState
        {
            get { return _CurrentState; }
            private set
            {
                _CurrentState = value;
                NotifyOfPropertyChange(() => CurrentState);
            }
        }
        
        

        public void Handle(ProgressState message)
        {
            CurrentState = message;
            Progress = 0;

            if (CurrentState == ProgressState.Started)
                ProgressBarVisibility = Visibility.Visible;
            else
                ProgressBarVisibility = Visibility.Collapsed;
        }

        public void Handle(ProgressRaport message)
        {
            Progress = (int)message.Value;
        }
    }
}
