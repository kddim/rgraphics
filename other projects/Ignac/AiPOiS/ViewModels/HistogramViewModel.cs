﻿using Motorola.AiPOiS.Models.Effects;
using Motorola.AiPOiS.Tools;
using Motorola.AiPOiS.Views;
using Caliburn.Micro;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.System.Threading;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace Motorola.AiPOiS.ViewModels
{
    public class HistogramViewModel : Screen, IHandle<ImageArray>
    {
        private readonly INavigationService navigationService;

        public HistogramViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            IoC.Get<IEventAggregator>().Subscribe(this);
        }

        //  private int[] reds { get; set; }

        protected override void OnInitialize()
        {
            // RedModel.DefaultColorAxis = new LinearColorAxis() { AxislineColor = OxyColor.FromArgb(100, 255, 255, 255) };

            PlotModel[] models = new PlotModel[] { RedModel, GreenModel, BlueModel, GrayscaleModel };

            foreach (var model in models)
            {
                model.TextColor = OxyColor.FromArgb(100, 255, 255, 255);
                model.PlotAreaBorderColor = OxyColor.FromArgb(100, 255, 255, 255);
                model.Axes.Add(new LinearAxis(AxisPosition.Bottom) { TextColor = OxyColor.FromArgb(100, 255, 255, 255), TicklineColor = OxyColor.FromArgb(100, 255, 255, 255), AxislineColor = OxyColor.FromArgb(100, 255, 255, 255) });
                model.Axes.Add(new LinearAxis(AxisPosition.Left) { TextColor = OxyColor.FromArgb(100, 255, 255, 255), TicklineColor = OxyColor.FromArgb(100, 255, 255, 255), AxislineColor = OxyColor.FromArgb(100, 255, 255, 255) });
            }
            // AreaSeries redSeries = new AreaSeries();





        }

        public async void Handle(ImageArray histogramSource)
        {
            AreaSeries redSeries = new AreaSeries();
            AreaSeries greenSeries = new AreaSeries();
            AreaSeries blueSeries = new AreaSeries();
            AreaSeries grayscaleSeries = new AreaSeries();


            await ThreadPool.RunAsync((IAsyncAction action) =>
            {

                int[] reds = new int[256];
                int[] blues = new int[256];
                int[] greens = new int[256];
                int[] grayscales = new int[256];

                for (int i = 0; i < 256; ++i)
                {
                    reds[i] = 0;
                    greens[i] = 0;
                    blues[i] = 0;
                    grayscales[i] = 0;
                }

                foreach(var pixel in histogramSource)
                {
                    reds[pixel.R]++;
                    greens[pixel.G]++;
                    blues[pixel.B]++;
                    grayscales[pixel.RGB]++;
                }

                redSeries.Color = OxyColors.Red;

                redSeries.Points.Add(new DataPoint(0, 0));

                for (int i = 0; i < 256; ++i)
                    redSeries.Points.Add(new DataPoint(i, reds[i]));
                redSeries.Points.Add(new DataPoint(255, 0));
                redSeries.Points.Add(new DataPoint(0, 0));

                greenSeries.Color = OxyColors.Green;

                greenSeries.Points.Add(new DataPoint(0, 0));

                for (int i = 0; i < 256; ++i)
                    greenSeries.Points.Add(new DataPoint(i, greens[i]));
                greenSeries.Points.Add(new DataPoint(255, 0));
                greenSeries.Points.Add(new DataPoint(0, 0));


                blueSeries.Color = OxyColors.Blue;

                blueSeries.Points.Add(new DataPoint(0, 0));

                for (int i = 0; i < 256; ++i)
                    blueSeries.Points.Add(new DataPoint(i, blues[i]));
                blueSeries.Points.Add(new DataPoint(255, 0));
                blueSeries.Points.Add(new DataPoint(0, 0));


                grayscaleSeries.Color = OxyColors.LightGray;

                grayscaleSeries.Points.Add(new DataPoint(0, 0));

                for (int i = 0; i < 256; ++i)
                    grayscaleSeries.Points.Add(new DataPoint(i, grayscales[i]));
                grayscaleSeries.Points.Add(new DataPoint(255, 0));
                grayscaleSeries.Points.Add(new DataPoint(0, 0));

            });

            RedModel.Series.Add(redSeries);
            GreenModel.Series.Add(greenSeries);
            BlueModel.Series.Add(blueSeries);
            GrayscaleModel.Series.Add(grayscaleSeries);

            RedModel.RefreshPlot(true);
            GreenModel.RefreshPlot(true);
            BlueModel.RefreshPlot(true);
            GrayscaleModel.RefreshPlot(true);


        }



        public void GoBack()
        {
            navigationService.GoBack();
        }
        public bool CanGoBack
        {
            get
            {
                return navigationService.CanGoBack;
            }
        }

        private PlotModel _RedModel = new PlotModel();
        public PlotModel RedModel
        {
            get { return _RedModel; }
            set
            {
                _RedModel = value;
                NotifyOfPropertyChange(() => RedModel);
            }
        }

        private PlotModel _GreenModel = new PlotModel();
        public PlotModel GreenModel
        {
            get { return _GreenModel; }
            set
            {
                _GreenModel = value;
                NotifyOfPropertyChange(() => GreenModel);
            }
        }

        private PlotModel _BlueModel = new PlotModel();
        public PlotModel BlueModel
        {
            get { return _BlueModel; }
            set
            {
                _BlueModel = value;
                NotifyOfPropertyChange(() => BlueModel);
            }
        }

        private PlotModel _GrayscaleModel = new PlotModel();
        public PlotModel GrayscaleModel
        {
            get { return _GrayscaleModel; }
            set
            {
                _GrayscaleModel = value;
                NotifyOfPropertyChange(() => GrayscaleModel);
            }
        }




    }
}
