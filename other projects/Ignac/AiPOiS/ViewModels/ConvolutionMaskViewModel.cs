﻿using Motorola.AiPOiS.Models.Effects;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.AiPOiS.ViewModels
{
    public class ConvolutionMaskViewModel : Screen, IHandle<Convolution>
    {
        public ConvolutionMaskViewModel()
        {
            IoC.Get<IEventAggregator>().Subscribe(this);
        }

        private Convolution _Convolution;
        public Convolution Convolution
        {
            get { return _Convolution; }
            set
            {
                _Convolution = value;
                NotifyOfPropertyChange(() => Convolution);
            }
        }

      /*  public ConvolutionMaskValue this[int index]
        {
            get
            {
                return Convolution.Mask[index % 3, index / 3];
            }
        }*/
        
        
        protected override void OnInitialize()
        {
            
            base.OnInitialize();
        }

        public void Handle(Convolution effectInstance)
        {
            Convolution = effectInstance;
          //  NotifyOfPropertyChange(() => this);
        }
    }
}
