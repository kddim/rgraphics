﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.System.Threading;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using Motorola.AiPOiS.Models.Effects;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Motorola.AiPOiS.Tools;
using System.Diagnostics;


namespace Motorola.AiPOiS.ViewModels
{
    public class MainPageViewModel : Screen, IHandle<IEffect>
    {
        private readonly INavigationService navigationService;
        public MainPageViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            IoC.Get<IEventAggregator>().Subscribe(this);
        }

        public void Histogram()
        {
            if (SourceImage == null)
                return;
            NavigationExtensions.NavigateToViewModel<HistogramViewModel>(navigationService, "test");
            byte[] image = new byte[SourceImage.Length];
            Array.Copy(SourceImage, image, SourceImage.Length);
            ImageArray img = new ImageArray(image, (int)Decoder.PixelWidth, (int)Decoder.PixelHeight);
            IoC.Get<IEventAggregator>().PublishOnCurrentThread(img);
        }

        protected override void OnInitialize()
        {
            Effects =       new ObservableCollection<IEffect>();

            Effects.Add(    new Models.Effects.Original());
            Effects.Add(    new Models.Effects.Negative());
            Effects.Add(    new Models.Effects.Grayscale());
            Effects.Add(    new Models.Effects.Grayscale_v2());
            Effects.Add(    new Models.Effects.Normalize());
            Effects.Add(    new Models.Effects.Sepia());
            Effects.Add(    new Models.Effects.RobertsCross());
            Effects.Add(    new Models.Effects.Sobel());
            Effects.Add(    new Models.Effects.GlobalThreshold());
            Effects.Add(    new Models.Effects.LocalThreshold());
            Effects.Add(    new Models.Effects.MixedThresholding());
            Effects.Add(    new Models.Effects.Bernsen());
            Effects.Add(    new Models.Effects.Otsu());
            Effects.Add(    new Models.Effects.Convolution());
            Effects.Add(    new Models.Effects.ShapeIsolation());
            Effects.Add(    new Models.Effects.Scale());
            Effects.Add(    new Models.Effects.BilinearInterpolation());
            Effects.Add(    new Models.Effects.Rotation());
            Effects.Add(    new Models.Effects.SaltAndPepper());
            Effects.Add(    new Models.Effects.UniformNoise());
            Effects.Add(    new Models.Effects.UniformNoise2());
            Effects.Add(    new Models.Effects.UnnoiseByAverage());
            Effects.Add(    new Models.Effects.MedianUnnoise());
            Effects.Add(    new Models.Effects.BetterMedianUnnoise());
            Effects.Add(    new Models.Effects.Kuwahara());
            Effects.Add(new Models.Effects.K3M());
            Effects.Add(    new Models.Effects.GaussianBlur());


        }

        public async void PickImage()
        {
            FileOpenPicker fop = new FileOpenPicker();
            fop.ViewMode = PickerViewMode.Thumbnail;
            fop.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            fop.FileTypeFilter.Add(".jpg");
            fop.FileTypeFilter.Add(".png");
            fop.FileTypeFilter.Add(".bmp");
            fop.FileTypeFilter.Add(".jpeg");
            fop.FileTypeFilter.Add(".gif");

            file = await fop.PickSingleFileAsync();
            LoadImage();
        }

        public async void SaveImage()
        {
            if (Image == null)
                return;

            FileSavePicker fsp = new FileSavePicker();
            fsp.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            fsp.FileTypeChoices.Add(new KeyValuePair<string, IList<string>>(".png", new List<String>(new string[] { ".png" })));
            fsp.FileTypeChoices.Add(new KeyValuePair<string, IList<string>>(".jpg", new List<String>(new string[] { ".jpg" })));
            fsp.FileTypeChoices.Add(new KeyValuePair<string, IList<string>>(".bmp", new List<String>(new string[] { ".bmp" })));
            fsp.DefaultFileExtension = ".png";
            var sfile = await fsp.PickSaveFileAsync();

            Guid bitmapEncoderGuid = BitmapEncoder.PngEncoderId;

            if(sfile.FileType == ".jpg")
                bitmapEncoderGuid = BitmapEncoder.JpegEncoderId;
            else if(sfile.FileType == ".bmp")
                bitmapEncoderGuid = BitmapEncoder.BmpEncoderId;

           // BitmapEncoder encoder = BitmapEncoder.CreateAsync()

            using (IRandomAccessStream stream = await sfile.OpenAsync(FileAccessMode.ReadWrite))
            {
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(bitmapEncoderGuid, stream);
                Stream pixelStream = Image.PixelBuffer.AsStream();
                byte[] pixels = new byte[pixelStream.Length];
                await pixelStream.ReadAsync(pixels, 0, pixels.Length);

                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
                                    (uint)Image.PixelWidth,
                                    (uint)Image.PixelHeight,
                                    96.0,
                                    96.0,
                                    pixels);
                await encoder.FlushAsync();
            }            
        }

        private StorageFile file { get; set; }

        public void ReloadImage()
        {
            _Effect = Effects[0];
            NotifyOfPropertyChange(() => Effect);
            LoadImage();

        }

        public void SetAsSourceImage()
        {
            SourceImage = Image.PixelBuffer.ToArray();
            Effect = Effects[0];
        }


        private async void LoadImage()
        {
            if (file == null)
                return;

            FileStream = await file.OpenAsync(FileAccessMode.Read);
            BitmapImage bimg = new BitmapImage();
            bimg.SetSource(FileStream);


            switch (file.FileType.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    DecoderId = BitmapDecoder.JpegDecoderId;
                    break;
                case ".png":
                    DecoderId = BitmapDecoder.PngDecoderId;
                    break;
                case ".bmp":
                    DecoderId = BitmapDecoder.BmpDecoderId;
                    break;
                case ".gif":
                    DecoderId = BitmapDecoder.GifDecoderId;
                    break;
            }

            Decoder = await BitmapDecoder.CreateAsync(DecoderId, FileStream);
            PixelDataProvider pixelData = await Decoder.GetPixelDataAsync(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Straight, new BitmapTransform(), ExifOrientationMode.IgnoreExifOrientation, ColorManagementMode.DoNotColorManage);

            SourceImage = pixelData.DetachPixelData();


            Storyboard sb = (GetView() as FrameworkElement).Resources["FadeIn"] as Storyboard;

            Image = null;

            if (Effect == null && Effects.Count > 0)
                Effect = Effects[0];
            else if (Effect != null)
                ApplyEffect();
        }

        private byte[] _SourceImage;
        public byte[] SourceImage
        {
            get { return _SourceImage; }
            set
            {
                _SourceImage = value;
                NotifyOfPropertyChange(() => SourceImage);
            }
        }

        private byte[] ResultPixels { get; set; }

        private WriteableBitmap _Image;
        public WriteableBitmap Image
        {
            get { return _Image; }
            set
            {
                _Image = value;
                NotifyOfPropertyChange(() => ImageContainerWidth);
                NotifyOfPropertyChange(() => ImageContainerHeight);
                NotifyOfPropertyChange(() => Image);

            }
        }

        private double _ZoomFactor = 1.0;
        public double ZoomFactor
        {
            get { return _ZoomFactor; }
            set
            {
                _ZoomFactor = value;
                NotifyOfPropertyChange(() => ZoomFactor);
                NotifyOfPropertyChange(() => ImageContainerWidth);
                NotifyOfPropertyChange(() => ImageContainerHeight);

            }
        }

        public void ZoomIn()
        {
            if (ZoomFactor + 0.2 <= 3.01)
                ZoomFactor += 0.2;
        }
        public void ZoomOut()
        {
            if (ZoomFactor - 0.2 >= 0.2)
                ZoomFactor -= 0.2;

        }

        public double ImageContainerHeight
        {
            get { return (Image != null) ? Image.PixelHeight * ZoomFactor : 0.0; }
        }

        public double ImageContainerWidth
        {
            get { return (Image != null) ? Image.PixelWidth * ZoomFactor : 0.0; }
        }

        private IAsyncAction CurrentAction { get; set; }

        private IEffect _Effect;
        public IEffect Effect
        {
            get { return _Effect; }
            set
            {
                if (value != _Effect)
                {
                    _Effect = value;
                    if (value != null)
                        ApplyEffect();
                    NotifyOfPropertyChange(() => Effect);
                }
            }
        }

        private async void ApplyEffect()
        {
            if (SourceImage == null)
                return;

            if (CurrentAction != null && CurrentAction.Status !=  AsyncStatus.Completed)
                CurrentAction.Cancel();

            ResultPixels = new byte[SourceImage.Length];
            Array.Copy(SourceImage, ResultPixels, SourceImage.Length);

            ImageArray result = new ImageArray(ResultPixels, (int)Decoder.PixelWidth, (int)Decoder.PixelHeight);


            try
            {
                await ThreadPool.RunAsync(new WorkItemHandler((IAsyncAction action) =>
                {
                    CurrentAction = action;
                    result = Effect.ApplyEffect(result, action);


                    if (action.Status != AsyncStatus.Canceled)
                        ResultPixels = result.Get1DArray();
                }));
            }
            catch(OperationCanceledException e)
            {
                Debug.WriteLine(e);
                IoC.Get<IEventAggregator>().PublishOnUIThread(ProgressState.Completed);
            }

            if (result != null)
            {


                WriteableBitmap writeableBitmap = new WriteableBitmap((int)result.Width, (int)result.Height);

                using (Stream stream = writeableBitmap.PixelBuffer.AsStream())
                {
                    await stream.WriteAsync(ResultPixels, 0, ResultPixels.Length);
                }

                Image = writeableBitmap;
            }
            NotifyOfPropertyChange(() => Effect);
        }



        private ObservableCollection<IEffect> _Effects;
        public ObservableCollection<IEffect> Effects
        {
            get { return _Effects; }
            set
            {
                _Effects = value;
                NotifyOfPropertyChange(() => Effects);
            }
        }


        public void Handle(IEffect effect)
        {
            _Effect = effect;
            ApplyEffect();
        }

        private IRandomAccessStream FileStream { get; set; }
        private Guid DecoderId { get; set; }
        private BitmapDecoder Decoder { get; set; }

    }
}
