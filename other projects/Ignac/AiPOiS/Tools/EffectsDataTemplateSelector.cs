﻿using Motorola.AiPOiS.Models.Effects;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Motorola.AiPOiS.Tools
{
    public class EffectsDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate StandardTemplate { get; set; }
        public DataTemplate SliderTemplate { get; set; }

        public DataTemplate ActionTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            
            if (item is IAdjustable)
                return SliderTemplate;
            if (item is IHasAction)
                return ActionTemplate;

            return StandardTemplate;
        }
    }

}
