﻿using Motorola.AiPOiS.Models.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Motorola.AiPOiS.Tools
{
    public class ThumbTooltipConverter : DependencyObject, IValueConverter
    {


        public Object Context
        {
            get { return (Object)GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value); }
        }

        public static readonly DependencyProperty ContextProperty =
            DependencyProperty.Register("Context", typeof(Object), typeof(ThumbTooltipConverter), new PropertyMetadata(null));

        

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (Context != null && Context is IAdjustable)
                return ((IAdjustable)Context).GetTooltip((double)value);

            double v = (double)value;
            return (v - 1).ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
