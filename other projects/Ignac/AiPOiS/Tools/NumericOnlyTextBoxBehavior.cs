﻿using Microsoft.Xaml.Interactivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Motorola.AiPOiS.Tools
{
    public class NumericOnlyTextBoxBehavior : DependencyObject, IBehavior
    {

        /// <summary>
        /// Attaches to the specified object.
        /// </summary>
        ///The  to which the  will be attached.
        public void Attach(DependencyObject associatedObject)
        {

            this.AssociatedObject = associatedObject;

            (AssociatedObject as TextBox).TextChanged += NumericOnlyTextBoxBehavior_TextChanged;

        }

        void NumericOnlyTextBoxBehavior_TextChanged(object sender, TextChangedEventArgs e)
        {
            string text = (sender as TextBox).Text;
            var match = Regex.Match(text, "[0-9,.-]*");
            (sender as TextBox).Text = match.Value.Replace(",", ".");
            if(match.Value.Length > 0)
                (sender as TextBox).Select(match.Value.Length, 0);
        }

        /// <summary>
        /// Detaches this instance from its associated object.
        /// </summary>
        public void Detach()
        {
            (AssociatedObject as TextBox).TextChanged -= NumericOnlyTextBoxBehavior_TextChanged;
            AssociatedObject = null;
        }

        /// <summary>
        /// Gets the to which the is attached.
        /// </summary>
        public DependencyObject AssociatedObject { get; private set; }
    }
}
