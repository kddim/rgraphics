﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public interface IEffect
    {
        string Name { get; }
        ImageArray ApplyEffect(ImageArray original, IAsyncAction action);
    }
}
