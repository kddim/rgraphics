﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.AiPOiS.Models.Effects
{
    public class GaussianBlur : AdjustableEffect
    {
        public GaussianBlur()
        {
            MinAdjustValue = 0.5;
            AdjustValue = 1.0;
            MaxAdjustValue = 6.0;
            AdjustStep = 0.5;
            Name = "Gaussian Blur";
        }

        protected override ImageArray Apply(ImageArray original, Windows.Foundation.IAsyncAction action)
        {
            Func<int, int, double> f = new Func<int, int, double>((x, y) =>
            {
                return 1.0 / (2 * Math.PI * AdjustValue * AdjustValue) * Math.Exp(-((x * x + y * y) / (2 * AdjustValue * AdjustValue)));
            });

            Border b = new Border();
            original = b.ApplyEffect(original, 2);

            ImageArray result = new ImageArray(original.Width - 4, original.Height - 4);

            for (int x = 2; x < original.Width - 2; ++x)
                for (int y = 2; y < original.Height - 2; ++y)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        double sum = 0;
                        double sumf = 0;

                        for (int dx = -2; dx <= 2; ++dx)
                            for (int dy = -2; dy <= 2; ++dy)
                            {
                                sum += f( Math.Abs(dx),Math.Abs(dy)) * original[x + dx, y + dy][i];
                                sumf += f(Math.Abs(dx), Math.Abs(dy));
                            }
                        sum /= sumf;
                        sum = Math.Max(0, Math.Min(255, sum));
                        result[x - 2, y - 2][i] = (byte)sum;
                    }
                }

            return result;

        }
    }
}
