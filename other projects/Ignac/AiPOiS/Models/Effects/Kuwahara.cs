﻿using Caliburn.Micro;
using Motorola.AiPOiS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Kuwahara : Effect
    {

        public Kuwahara()
        {
            Name = "Kuwahara";
        }



        private ImageArray ApplyGrayscale(ImageArray original)
        {
            int[] horizontal = new int[2];
            int[] vertical = new int[2];

            int[] W = new int[4];
            int[] avgs = new int[4];
            int sum = 0;
            int index = 0;
            int w = 0;
            int tmp = 0;

            for (int x = 3; x < original.Width - 6; ++x)
            {
                for (int y = 3; y < original.Height - 6; ++y)
                {
                    horizontal[0] = x - 3;
                    horizontal[1] = x;
                    vertical[0] = y - 3;
                    vertical[1] = y;

                    index = 0;


                    foreach (var v in vertical)
                        foreach (var h in horizontal)
                        {


                            //Srednia:
                            sum = 0;
                            for (int i = v; i <= v + 3; ++i)
                                for (int j = h; j <= h + 3; ++j)
                                    sum += original[j, i].R;

                            sum = (int)(((double)sum) / 16.0);
                            avgs[index] = sum;

                            //W:
                            w = 0;
                            for (int i = v; i <= v + 3; ++i)
                                for (int j = h; j <= h + 3; ++j)
                                {
                                    tmp = original[j, i].R;
                                    w += (sum - tmp) * (sum - tmp);
                                }
                            w = (int)(((double)w) / 16.0);

                            W[index] = w;
                            index++;

                        }

                    original[x, y].RGB = (byte)avgs[Array.IndexOf(W, W.Min())];
                }

            }
            return original;
        }

        private ImageArray ApplyColor(ImageArray original)
        {
            int[] horizontal = new int[2];
            int[] vertical = new int[2];

            int[] W = new int[4];
            int[] avgs = new int[4];
            int[] avgR = new int[4];
            int[] avgG = new int[4];
            int[] avgB = new int[4];
            int sum = 0;
            int sumR = 0;
            int sumG = 0;
            int sumB = 0;


            int index = 0;
            int w = 0;
            int tmp = 0;

            for (int x = 3; x < original.Width - 3; ++x)
            {
                for (int y = 3; y < original.Height - 3; ++y)
                {
                    horizontal[0] = x - 3;
                    horizontal[1] = x;
                    vertical[0] = y - 3;
                    vertical[1] = y;

                    index = 0;


                    foreach (var v in vertical)
                        foreach (var h in horizontal)
                        {


                            //Srednia:
                            sum = 0;
                            sumR = 0;
                            sumG = 0;
                            sumB = 0;

                            for (int i = v; i <= v + 3; ++i)
                                for (int j = h; j <= h + 3; ++j)
                                {
                                    sum += Math.Max(original[j, i].R, Math.Max(original[j, i].G, original[j, i].B));
                                    sumR += original[j, i].R;
                                    sumG += original[j, i].G;
                                    sumB += original[j, i].B;
                                }

                            sum = (int)(((double)sum) / 16.0);
                            sumR = (int)(((double)sumR) / 16.0);
                            sumG = (int)(((double)sumG) / 16.0);
                            sumB = (int)(((double)sumB) / 16.0);
                            avgs[index] = sum;
                            avgR[index] = sumR;
                            avgG[index] = sumG;
                            avgB[index] = sumB;

                            //W:
                            w = 0;
                            for (int i = v; i <= v + 3; ++i)
                                for (int j = h; j <= h + 3; ++j)
                                {
                                    tmp = Math.Max(original[j, i].R, Math.Max(original[j, i].G, original[j, i].B));
                                    w += (sum - tmp) * (sum - tmp);
                                }
                            w = (int)(((double)w) / 16.0);

                            W[index] = w;
                            index++;

                        }

                    index = Array.IndexOf(W, W.Min());
                    original[x, y].R = (byte)avgR[index];
                    original[x, y].G = (byte)avgG[index];
                    original[x, y].B = (byte)avgB[index];
                }

            }
            return original;
        }

        protected override ImageArray Apply(ImageArray original, Windows.Foundation.IAsyncAction action)
        {

            bool isGrayscale = true;
            foreach (var pixel in original)
            {
                if (pixel.R == pixel.G && pixel.R == pixel.B) { }
                else
                {
                    isGrayscale = false;
                    break;
                }
            }


            Border b = new Border();
            original = b.ApplyEffect(original, 3);

            if (isGrayscale)
                original = ApplyGrayscale(original);
            else
                original = ApplyColor(original);

            ImageArray result = new ImageArray(original.Width - 6, original.Height - 6);
            for (int x = 0; x < result.Width; ++x)
                for (int y = 0; y < result.Height; ++y)
                {
                    result[x, y].R = original[x + 3, y + 3].R;
                    result[x, y].G = original[x + 3, y + 3].G;
                    result[x, y].B = original[x + 3, y + 3].B;
                }

            return result;



        }
    }

}
