﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Border : Effect
    {

        public Border()
        {
            Name = "White border";
        }




        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            return ApplyEffect(original, 3, new Pixel(0, 0, 255, 255, 255));
        }

        public ImageArray ApplyEffect(ImageArray original, int borderThickness)
        {
            ImageArray result = new ImageArray(original.Width + 2 * borderThickness, original.Height + 2 * borderThickness);
            
            //top
            for (int x = 0; x < result.Width; ++x)
                for (int y = 0; y < borderThickness; ++y)
                    for (int i = 0; i < 3; ++i)
                        result[x, y][i] = original.Extended(x - borderThickness, y - borderThickness)[i];

            //left
            for (int x = 0; x < borderThickness; ++x)
                for (int y = borderThickness; y < result.Height; ++y)
                    for (int i = 0; i < 3; ++i)
                        result[x, y][i] = original.Extended(x - borderThickness, y - borderThickness)[i];

            //right
            for (int x = result.Width - borderThickness; x < result.Width; ++x)
                for (int y = borderThickness; y < result.Height; ++y)
                    for (int i = 0; i < 3; ++i)
                        result[x, y][i] = original.Extended(x - borderThickness, y - borderThickness)[i];

            //bottom
            for (int x = 0; x < result.Width; ++x)
                for (int y = result.Height - borderThickness; y < result.Height; ++y)
                    for (int i = 0; i < 3; ++i)
                        result[x, y][i] = original.Extended(x - borderThickness, y - borderThickness)[i];

            for (int x = 0; x < original.Width; ++x)
                for (int y = 0; y < original.Height; ++y)
                    for (int i = 0; i < 3; ++i)
                        result[x + borderThickness, y + borderThickness][i] = original[x, y][i];

            return result;
        }

        public ImageArray ApplyEffect(ImageArray original, int borderThickness, Pixel color)
        {
            ImageArray result = new ImageArray(original.Width + 2 * borderThickness, original.Height + 2 * borderThickness);

            //top
            for (int x = 0; x < result.Width; ++x)
                for (int y = 0; y < borderThickness; ++y)
                    for(int i = 0; i < 3; ++i)
                        result[x, y][i] = color[i];
           //left
            for (int x = 0; x < borderThickness; ++x)
                for (int y = borderThickness; y < result.Height; ++y)
                    for (int i = 0; i < 3; ++i)
                        result[x, y][i] = color[i];

            //right
            for (int x = result.Width-borderThickness; x < result.Width; ++x)
                for (int y = borderThickness; y < result.Height; ++y)
                    for (int i = 0; i < 3; ++i)
                        result[x, y][i] = color[i];

            //bottom
            for (int x = 0; x < result.Width; ++x)
                for (int y = result.Height-borderThickness; y < result.Height; ++y)
                    for (int i = 0; i < 3; ++i)
                        result[x, y][i] = color[i];


            //inside
            for (int x = 0; x < original.Width; ++x)
                for (int y = 0; y < original.Height; ++y)
                    for (int i = 0; i < 3; ++i)
                        result[x + borderThickness, y + borderThickness][i] = original[x, y][i];


                        return result;
        }
    }
}
