﻿using Motorola.AiPOiS.Models.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.AiPOiS.Models.Effects
{
    public class MedianUnnoise : Effect
    {
        public MedianUnnoise()
        {
            Name = "Median Unnoise";
        }

        protected override ImageArray Apply(ImageArray original, Windows.Foundation.IAsyncAction action)
        {
            ImageArray result = new ImageArray(original.Width, original.Height);
            byte[] values = new byte[25];

            for (int x = 0; x < result.Width; ++x)
                for (int y = 0; y < result.Height; ++y)
                {
                    for (int c = 0; c < 3; ++c)
                    {
                        int i = 0;
                        for (int dx = x - 2; dx <= x + 2; ++dx)
                            for (int dy = y - 2; dy <= y + 2; ++dy)
                            {
                                values[i] = original.Extended(dx, dy)[c];
                                i++;
                            }
                        Array.Sort(values);
                        result[x, y][c] = values[12];
                    }
                }


            return result;
        }
    }
}
