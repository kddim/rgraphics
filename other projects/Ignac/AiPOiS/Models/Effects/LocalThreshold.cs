﻿using Caliburn.Micro;
using Motorola.AiPOiS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class LocalThreshold : AdjustableEffect
    {
        public LocalThreshold()
        {
           
            AdjustStep = 1.0;
            MinAdjustValue = 0.0;
            AdjustValue = 0.0;
            MaxAdjustValue = 4.0;
            Name = "Local Threshold";

        }

        private readonly int[] values = new int[] { 5, 11, 15, 21, 25 };

        public override string GetTooltip(double value)
        {
            return String.Format("Neighborhood range: {0}", values[(int)value]);
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            ImageArray result = new ImageArray(original.Width, original.Height);
            int neighborhood = values[(int)AdjustValue] / 2;
            int factor = 2 * neighborhood + 1;
            factor *= factor;

            for (int x = 0; x < original.Width; ++x)
            {
                for (int y = 0; y < original.Height; ++y)
                {
                    double avg = 0;
                    for (int _x = x - neighborhood; _x <= x + neighborhood; ++_x)
                        for (int _y = y - neighborhood; _y <= y + neighborhood; ++_y)
                            avg += original.Extended(_x, _y).RGB;

                    avg /= factor;
                    result[x, y].RGB = (original[x, y].RGB >= avg) ? (byte)255 : (byte)0;
                }
                if (action.Status == AsyncStatus.Canceled)
                    return null;
                IoC.Get<IEventAggregator>().PublishOnUIThread(new ProgressRaport((double)x / ((double)original.Width)));
            }
            return result;
        }
    }
}
