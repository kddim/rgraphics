﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Sobel : Effect
    {
        public Sobel()
        {
            Name = "Sobel's filter";
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action = null)
        {
            ImageArray result = new ImageArray(original.Width, original.Height);


            for (int x = 1; x < original.Width - 1; ++x)
                for (int y = 1; y < original.Height - 1; ++y)
                {
                    for (int rgb = 0; rgb < 3; ++rgb)
                    {
                        int p0 = original[x - 1, y - 1][rgb];
                        int p1 = original[x, y - 1][rgb];
                        int p2 = original[x + 1, y - 1][rgb];
                        int p3 = original[x + 1, y][rgb];
                        int p4 = original[x + 1, y + 1][rgb];
                        int p5 = original[x, y + 1][rgb];
                        int p6 = original[x - 1, y + 1][rgb];
                        int p7 = original[x - 1, y][rgb];

                        double X = ((double)(p2 + (2 * p3) + p4)) - ((double)(p0 + (2 * p7) + p6));
                        double Y = ((double)(p6 + (2 * p5) + p4)) - ((double)(p0 + (2 * p1) + p2));
                        double PX = X * X + Y * Y;
                        PX = Math.Sqrt(PX);
                        PX = Math.Min(255.0, PX);

                        result[x, y][rgb] = (byte)PX;
                    }
                }
            return result;
        }
    }
}
