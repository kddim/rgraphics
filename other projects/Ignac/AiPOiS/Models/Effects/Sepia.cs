﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.System.Threading;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Sepia : AdjustableEffect
    {
        public Sepia()
        {  
            MinAdjustValue = double.MinValue;
            AdjustValue = 30;
            MaxAdjustValue = double.MaxValue;
            AdjustStep = 1;
            Name = "Sepia";
        }

        public override string GetTooltip(double value)
        {
            return String.Format("W: {0}", value);
        }




        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            ImageArray copy = new ImageArray(original.Width, original.Height);

            foreach(var rgba in original)
            {
                rgba.R = (byte)Math.Min(rgba.R + 2 * ((int)AdjustValue), 255);
                rgba.G = (byte)Math.Min(rgba.G + 2 * ((int)AdjustValue), 255);
                rgba.B = (byte)Math.Min(rgba.B + 2 * ((int)AdjustValue), 255);
            }
            return original;
        }
    }
}
