﻿using Motorola.AiPOiS.ViewModels;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class ConvolutionMaskValue : PropertyChangedBase
    {
        private Double _Value = 1.0;
        public Double Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                NotifyOfPropertyChange(() => Value);
            }
        }

    }
    public class Convolution : Effect, IHasAction
    {
        public Convolution()
        {
            for (int x = 0; x < 3; ++x)
                for (int y = 0; y < 3; ++y)
                    Mask[x, y] = new ConvolutionMaskValue();

            Name = "Convolution";
        }


        private ConvolutionMaskValue[,] _Mask = new ConvolutionMaskValue[3, 3];
        public ConvolutionMaskValue[,] Mask
        {
            get { return _Mask; }
            set
            {
                _Mask = value;
                NotifyOfPropertyChange(() => Mask);
                NotifyOfPropertyChange(() => ButtonContent);
            }
        }

        public ConvolutionMaskValue this[int index]
        {
            get
            {
                return Mask[index % 3, index / 3];
            }
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            double sum = 0.0;
            for(int i = 0; i < 9; i++)
                sum += this[i].Value;

            if (sum == 0)
                sum = 1;

            ImageArray result = new ImageArray(original.Width, original.Height);
            for (int x = 1; x < original.Width - 1; ++x)
                for (int y = 1; y < original.Height - 1; ++y)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        int x0 = x - 1;
                        int y0 = y - 1;

                        double value = 0.0;

                        for (int m_x = 0; m_x < 3; ++m_x)
                            for (int m_y = 0; m_y < 3; ++m_y)
                                value += Mask[m_x, m_y].Value * original[x0 + m_x, y0 + m_y][i];
                        
                        value /= sum;
                        value = Math.Min(Math.Max(value, 0), 255);
                        result[x, y][i] = (byte)value;
                    }
                }
            return result;
        }

        public void Action()
        {
            IoC.Get<ConvolutionMaskViewModel>().Convolution = this;
            IoC.Get<Caliburn.Micro.INavigationService>().NavigateToViewModel<ConvolutionMaskViewModel>();
        }

        public Object ButtonContent
        {
            get
            {
                string formatted = "{";
                for (int y = 0; y < 3; ++y)
                {
                    string v = "";
                    for (int x = 0; x < 3; ++x)
                        v += String.Format("{0},", Mask[x, y].Value);
                    v = v.TrimEnd(',');
                    formatted += String.Format("{0} | ", v);
                }
                return formatted.TrimEnd(' ', '|') + "}" + " (Tap to edit)";
            }
        }

    }
}
