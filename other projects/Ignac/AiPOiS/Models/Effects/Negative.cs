﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Negative : Effect
    {
        public Negative()
        {
            Name = "Negative";
        }


        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
           foreach(Pixel rgba in original)
           {
               for (int i = 0; i < 3; ++i)
                   rgba[i] = (byte)(255 - rgba[i]);
           }
           return original;
        }
    }
}
