﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class GlobalThreshold : Effect
    {
        public GlobalThreshold()
        {
            Name = "Global Threshold";
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            double avg = 0;
            foreach (Pixel rgba in original)
                avg += rgba.RGB;
            avg /= original.PixelsCount;

            foreach (Pixel rgba in original)
                rgba.RGB = (rgba.RGB > avg) ? (byte)255 : (byte)0;
            return original;
           
        }
    }
}
