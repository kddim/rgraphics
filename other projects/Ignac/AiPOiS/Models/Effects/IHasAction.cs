﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.AiPOiS.Models.Effects
{
    public interface IHasAction
    {
        void Action();
        Object ButtonContent { get; }
    }
}
