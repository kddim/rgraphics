﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Scale : AdjustableEffect
    {
        public Scale()
        {
            this.AdjustStep = 0.1;
            this.MinAdjustValue = 0.5;
            this.MaxAdjustValue = 5.0;
            Name = "Scale";
        }

        public override string GetTooltip(double value)
        {
            return String.Format("Scale: {0:#.##}x", value);
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            ImageArray result = new ImageArray((int)(original.Width * AdjustValue), (int)(original.Height * AdjustValue));
            for(int x = 0; x < result.Width; ++x)
                for(int y = 0; y < result.Height; ++y)
                {
                    int sx = (int)(x / AdjustValue);
                    int sy = (int)(y / AdjustValue);

                    for (int i = 0; i < 3; ++i)
                        result[x, y][i] = original[sx, sy][i];
                }
            return result;
        }
    }
}
