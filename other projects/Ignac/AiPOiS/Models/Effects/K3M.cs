﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class K3M : Effect
    {
        public K3M()
        {
            Name = "K3M";
        }

        private static readonly int[] A0 = new int[] { 3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56, 60, 62, 63, 96, 112, 120, 124, 126, 127, 129, 131, 135, 143, 159, 191, 192, 193, 195, 199, 207, 223, 224, 225, 227, 231, 239, 240, 241, 243, 247, 248, 249, 251, 252, 253, 254 };
        private static readonly int[] A1 = new int[] { 7, 14, 28, 56, 112, 131, 193, 224 };
        private static readonly int[] A2 = new int[] { 7, 14, 15, 28, 30, 56, 60, 112, 120, 131, 135, 193, 195, 224, 225, 240 };
        private static readonly int[] A3 = new int[] { 7, 14, 15, 28, 30, 31, 56, 60, 62, 112, 120, 124, 131, 135, 143, 193, 195, 199, 224, 225, 227, 240, 241, 248 };
        private static readonly int[] A4 = new int[] { 7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120, 124, 126, 131, 135, 143, 159, 193, 195, 199, 207, 224, 225, 227, 231, 240, 241, 243, 248, 249, 252 };
        private static readonly int[] A5 = new int[] { 7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120, 124, 126, 131, 135, 143, 159, 191, 193, 195, 199, 207, 224, 225, 227, 231, 239, 240, 241, 243, 248, 249, 251, 252, 254 };

        private static readonly int[] A1pix = {3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56, 60, 62, 63, 96, 112, 120, 124, 126, 127, 129, 131, 135, 143, 159, 191, 192, 193, 195, 199, 207, 223, 224, 225, 227, 231, 239, 240, 241, 243, 247, 248, 249, 251, 252, 253, 254};

        private static int[,] mask = { { 128, 64, 32 }, { 1, 0, 16 }, { 2, 4, 8 } };

        private int[] this[int index]
        {
            get
            {
                if (index == 0)
                    return A0;
                if (index == 1)
                    return A1;
                if (index == 2)
                    return A2;
                if (index == 3)
                    return A3;
                if (index == 4)
                    return A4;
                if (index == 5)
                    return A5;

                throw new ArgumentException();
            }
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            GlobalThreshold g = new GlobalThreshold();
            original = g.ApplyEffectQuietly(original, action);

            Border b = new Border();
            original = b.ApplyEffectQuietly(original, action);

            bool isModified = true;
            List<Pixel> border = new List<Pixel>();

            while(isModified)
            {
                isModified = false;
                for(int x = 1; x < original.Width-1; ++x)
                    for(int y = 1; y < original.Height-1; ++y)
                    {
                        if(original[x,y].G == 255)
                            continue;

                        int sum = 0;

                        for(int dy=-1; dy <= 1; ++dy)
                            for(int dx=-1; dx <= 1; ++dx)
                            {
                                if (original[x + dx, y + dy].G == 0)
                                    sum += mask[dy + 1, dx + 1];
                            }
                        if(this[0].Contains(sum))
                        {
                            border.Add(original[x, y]);
                            original[x, y].R = 255;
                            original[x, y].G = 0;
                            original[x, y].B = 0;

                        }
                    }

                for (int index = 1; index <= 5; ++index)
                {
                    for (int i = 0; i < border.Count; ++i)
                    {
                        int sum = 0;

                        for (int dy = -1; dy <= 1; ++dy)
                            for (int dx = -1; dx <= 1; ++dx)
                            {
                                if (original[border[i].X + dx, border[i].Y + dy].G == 0)
                                    sum += mask[dy + 1, dx + 1];
                            }
                        if(this[index].Contains(sum))
                        {
                            border[i].RGB = 255;
                            border.RemoveAt(i);
                            i--;
                            isModified = true;
                        }
                    }
                }

                foreach (var pixel in border)
                    pixel.R = 0;

                border.Clear();

            }


            for(int x = 1; x < original.Width-1; ++x)
                for (int y = 1; y < original.Height - 1; ++y)
                {
                    if (original[x, y].G == 255)
                        continue;

                    int sum = 0;

                    for (int dy = -1; dy <= 1; ++dy)
                        for (int dx = -1; dx <= 1; ++dx)
                        {
                            if (original[x + dx, y + dy].G == 0)
                                sum += mask[dy + 1, dx + 1];
                        }
                    if (A1pix.Contains(sum))
                        original[x, y].RGB = 255;
                }

            return original;
        }

    }
}
