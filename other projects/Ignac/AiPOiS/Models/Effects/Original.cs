﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Original : Effect
    {
        public Original()
        {
            Name = "Original";
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            return original;
        }
    }
}
