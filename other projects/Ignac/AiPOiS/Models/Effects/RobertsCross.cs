﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class RobertsCross : Effect
    {
        public RobertsCross()
        {
            Name = "Robert's Cross";
        }
        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            ImageArray result = new ImageArray(original.Width, original.Height);
            for(int x = 0; x < original.Width -1; ++x)
                for(int y = 0; y < original.Height -1; ++y)
                {
                    for(int i = 0; i < 3; ++i)
                    {
                        int a = original[x, y][i] - original[x+1,y+1][i];
                        int b = original[x + 1, y][i] - original[x, y + 1][i];
                        result[x, y][i] = (byte)Math.Min(Math.Abs(a + b), 255);
                    }
                }
            return result;
        }
    }
}
