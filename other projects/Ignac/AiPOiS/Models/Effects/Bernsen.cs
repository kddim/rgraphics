﻿using Caliburn.Micro;
using Motorola.AiPOiS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Bernsen : AdjustableEffect
    {
        private readonly int[] values = new int[] { 15, 25, 35 };

        public Bernsen()
        {
            MinAdjustValue = 0;
            AdjustValue = 0;
            MaxAdjustValue = 2;
            Name = "Bernsen's method";
        }

        public override string GetTooltip(double value)
        {
            return String.Format("Max deviation: {0}", values[(int)value]);
        }



        protected override  ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            int neighborhood = 7;

            byte global_min = 255;
            byte global_max = 0;
            foreach(var pixel in original)
            {
                byte v = pixel.RGB;
                if (v > global_max)
                    global_max = v;
                if (v < global_min)
                    global_min = v;
            }

            double global_avg = (global_max + global_min) / 2.0;

            ImageArray result = new ImageArray(original.Width, original.Height);
            int i = 0;
            foreach(var pixel in original)
            {
                byte min = 255;
                byte max = 0;

                for(int x = pixel.X - neighborhood; x <= pixel.X + neighborhood; ++x)
                    for (int y = pixel.Y - neighborhood; y <= pixel.Y + neighborhood; ++y)
                    {
                        byte v = original.Extended(x, y).RGB;
                        if (v > max)
                            max = v;
                        if (v < min)
                            min = v;
                    }

                double avg = (min + max) / 2.0;

                if (Math.Abs(avg - global_avg) < values[(int)AdjustValue])
                    result[pixel.X, pixel.Y].RGB = (pixel.RGB >= (byte)avg) ? (byte)255 : (byte)0;
                else
                {
                  //  if(avg < global_avg - values[(int)AdjustValue])
                  //      result[pixel.X, pixel.Y].RGB = pixel.RGB  

                    result[pixel.X, pixel.Y].RGB = (pixel.RGB >= (byte)global_avg) ? (byte)255 : (byte)0;

                    
                }
                i++;
                if (i % 100 == 0)
                    IoC.Get<IEventAggregator>().PublishOnUIThread(new ProgressRaport(((double)i) / (double)original.PixelsCount));
            }
            return result;
        }
    }
}
