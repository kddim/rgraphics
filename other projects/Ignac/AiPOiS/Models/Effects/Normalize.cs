﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Normalize : Effect
    {
        public Normalize()
        {
            Name = "Normalize";
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            byte[] min = new byte[3] { 255, 255, 255 };
            byte[] max = new byte[3] { 0, 0, 0 };

            foreach(Pixel rgba in original)
            {
                for(int i = 0; i < 3; ++i)
                {
                    if (rgba[i] > max[i])
                        max[i] = rgba[i];
                    if (rgba[i] < min[i])
                        min[i] = rgba[i];
                }
            }
            foreach(Pixel rgba in original)
                for(int i = 0; i < 3; ++i)
                    rgba[i] = (byte)Math.Min(255, Math.Max(0, (255 * ((byte)(rgba[i] - min[i]))) / (max[i] - min[i])));
            return original;
        }
    }
}
