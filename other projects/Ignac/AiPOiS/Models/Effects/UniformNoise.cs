﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class UniformNoise : AdjustableEffect
    {
        public UniformNoise()
        {
            MinAdjustValue = 0;
            AdjustValue = 0;
            MaxAdjustValue = 2;
            Name = "Uniform Noise v1";

        }

        private readonly int[] values = new int[] { 5, 10, 15 };

        public override string GetTooltip(double value)
        {
            return String.Format("Probabbility: {0}%", values[(int)value]);
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            Random generator = new Random();
            double p = AdjustValue / 100.0;

            foreach(var pixel in original)
            {
                int addValue = generator.Next(-15, 16);
                if (generator.NextDouble() >= p)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        if ((int)pixel[i] + addValue <= 255 && (int)pixel[i] + addValue >= 0)
                            pixel[i] = (byte)(addValue + pixel[i]);
                        else if ((int)pixel[i] + addValue > 255)
                            pixel[i] = 255;
                        else
                            pixel[i] = 0;
                    }
                }
            }

            return original;
        }
    }
}
