﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.AiPOiS.Models.Effects
{
    public abstract class AdjustableEffect : Effect, IAdjustable
    {
        protected double _MinAdjustValue = 1;
        public virtual double MinAdjustValue
        {
            get { return _MinAdjustValue; }
            protected set
            {
                _MinAdjustValue = value;
                NotifyOfPropertyChange(() => MinAdjustValue);
            }
        }

        protected double _MaxAdjustValue = 100;
        public virtual double MaxAdjustValue
        {
            get { return _MaxAdjustValue; }
            protected set
            {
                _MaxAdjustValue = value;
                NotifyOfPropertyChange(() => MaxAdjustValue);
            }
        }


        protected double _AdjustValue = 1;
        public virtual double AdjustValue
        {
            get
            {
                return _AdjustValue;

            }
            set
            {
                _AdjustValue = value;
                NotifyOfPropertyChange(() => AdjustValue);
            }
        }
        protected double _AdjustStep = 1;
        public virtual double AdjustStep
        {
            get { return _AdjustStep; }
            protected set
            {
                _AdjustStep = value;
                NotifyOfPropertyChange(() => AdjustStep);
            }
        }

        protected Boolean _IsThumbToolTipEnabled = true;
        public Boolean IsThumbToolTipEnabled
        {
            get { return _IsThumbToolTipEnabled; }
            set
            {
                _IsThumbToolTipEnabled = value;
                NotifyOfPropertyChange(() => IsThumbToolTipEnabled);
            }
        }

        public virtual string GetTooltip(double value)
        {
            return String.Format("{0:#.##}", value);
        }
    }
}
