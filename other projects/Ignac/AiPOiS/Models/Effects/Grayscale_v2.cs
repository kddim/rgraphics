﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Grayscale_v2 : Effect
    {
        public Grayscale_v2()
        {
            Name = "Grayscale v2";
        }


        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            foreach(Pixel rgba in original)
                rgba.RGB = (byte)(rgba.R * 0.3 + rgba.G * 0.59 + rgba.B * 0.11);
            return original;
        }
    }
}
