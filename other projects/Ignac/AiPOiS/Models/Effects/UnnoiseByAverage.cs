﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class UnnoiseByAverage : Effect
    {
        public UnnoiseByAverage()
        {
            Name = "Unnoise by average";
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {

            ImageArray result = new ImageArray(original.Width, original.Height);
             double avg = 0.0;
            foreach(var pixel in original)
            {
                for (int i = 0; i < 3; ++i)
                {
                    avg = 0.0;
                    for (int x = pixel.X - 2; x <= pixel.X + 2; ++x)
                        for (int y = pixel.Y - 2; y <= pixel.Y + 2; ++y)
                            avg += original.Extended(x, y)[i];
                    avg /= 25.0;

                    result[pixel.X, pixel.Y][i] = (byte)avg;
                }
            }

            return result;
        }
    }
}
