﻿using Caliburn.Micro;
using Motorola.AiPOiS.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public abstract class Effect : PropertyChangedBase, IEffect
    {
        private string _Name = "Name";
        public string Name
        {
            get { return _Name; }
            protected set
            {
                _Name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }
        public ImageArray ApplyEffect(ImageArray original, IAsyncAction action)
        {
            IoC.Get<IEventAggregator>().PublishOnUIThread(ProgressState.Started);
            ImageArray result = Apply(original, action);
            IoC.Get<IEventAggregator>().PublishOnUIThread(ProgressState.Completed);
              
            return result;
        }

        protected virtual ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            return original;
        }

        internal ImageArray ApplyEffectQuietly(ImageArray original, IAsyncAction action)
        {
            return ApplyQuietly(original, action);
        }
        protected virtual ImageArray ApplyQuietly(ImageArray original, IAsyncAction action)
        {
            return Apply(original, action);
        }


        
    }
}
