﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Otsu : Effect
    {
        public Otsu()
        {
            Name = "Otsu";
        }
        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            //Potrzebne są: histogram - H oraz łączna ilość pikseli - ŁP obrazu
            //Najpierw oblicza się sumę prawdopodobieństw SU wystąpienia każdej wartości histogramu (czyli sumuje się i*H[i] dla i=<0;255>).
            int[] histogram = new int[256];
            for (int i = 0; i < histogram.Length; ++i)
                histogram[i] = 0;

            foreach (var pixel in original)
                histogram[pixel.RGB]++;

            double SU = 0;
            for (int i = 0; i < histogram.Length; ++i)
                SU += i * histogram[i];
            //Następnie ustawia się początkową wagę prawdopodobieństwa W, średnią klasy wartości S, maksimum MAX i sumę pomocniczą SUP na 0.

            double W = 0.0;
            double MAX = 0.0;
            double SUP = 0.0;

            double T1 = 0.0;
            double T2 = 0.0;

            for (int i = 0; i < histogram.Length; ++i)
            {
                W += histogram[i];
                if (W <= 0) //Zwiększenie W o wartość H[i]. Jeśli W jest równe 0 można przeskoczyć w pętli do następnej iteracji
                    continue;
                double WP = original.PixelsCount - W; //Oblicza się pomocniczą wagę WP jako ŁP odjąć W.

                if (WP <= 0) //Jeśli WP jest równe 0 należy przerwać pętlę.
                    break;
                SUP += i * histogram[i]; //Dalej zwiększa się SUP o i*H[i].
                double SG = SUP / W; //Dalej oblicza się górną pomocniczą średnią SG jako SUP/W.
                double SD = (SU - SUP) / WP; //Dalej oblicza się dolną pomocniczą średnią SD jako (S-SUP)/WP.
                double R = W * WP * (Math.Pow(SG - SD, 2));//Dalej oblicza się przedział R jako W*WP*(SG-SD)^2.

                if (R >= MAX) //Jeżeli R>=MAX
                {

                    T1 = i; //Pierwszy przedział T1 ustawia się na i.
                    if (R > MAX)
                        T2 = i;  //Jeżeli R>MAX drugi przedział T2 ustawia się na i.
                    MAX = R; //MAX ustawia się na R.
                }
            }

            double threshold = (T1 + T2) / 2.0;
            byte t = (byte)threshold;

            foreach (var pixel in original)
                pixel.RGB = (pixel.RGB >= t) ? (byte)255 : (byte)0;

            return original;
        }
    }
}
