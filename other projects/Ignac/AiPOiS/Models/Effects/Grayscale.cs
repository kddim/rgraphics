﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Grayscale : Effect
    {
        public Grayscale()
        {
            Name = "Grayscale";
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            foreach (Pixel rgba in original)
                rgba.RGB = rgba.RGB;

            return original;
        }
    }
}
