﻿using Caliburn.Micro;
using Motorola.AiPOiS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class MixedThresholding : AdjustableEffect
    {
        public MixedThresholding()
        {
            AdjustStep = 1;
            MinAdjustValue = 0;
            AdjustValue = 1;
            MaxAdjustValue = 2;
            Name = "Mixed Threshold";
        }

        private readonly int[] values = new int[] { 15, 25, 35 };


        public override string GetTooltip(double value)
        {
            return String.Format("Maximum deviation: {0}", values[(int)value]);
        }

        private double ThresholdingRange = 15.0;

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            int neighborhood = ((int)ThresholdingRange % 2 == 1) ? ((int)ThresholdingRange) / 2 : ((int)ThresholdingRange - 1) / 2;
            int factor = 2 * neighborhood + 1;
            factor *= factor;

            ImageArray image = new ImageArray(original.Width, original.Height);

            double global_avg = 0.0;
            foreach (Pixel rgb in original)
                global_avg += rgb.RGB;
            global_avg /= original.PixelsCount;

            for (int x = 0; x < original.Width; ++x)
            {
                for (int y = 0; y < original.Height; ++y)
                {
                    double local_avg = 0;
                    for (int i = x - neighborhood; i <= x + neighborhood; ++i)
                        for (int j = y - neighborhood; j <= y + neighborhood; ++j)
                            local_avg += original.Extended(i, j).RGB;
                    local_avg /= factor;

                    if (Math.Abs(global_avg - local_avg) > values[(int)AdjustValue])
                    {
                        if (original[x, y].RGB >= global_avg)
                            image[x, y].RGB = 255;
                        else
                            image[x, y].RGB = 0;
                    }
                    else
                    {
                        if (original[x, y].RGB >= local_avg)
                            image[x, y].RGB = 255;
                        else
                            image[x, y].RGB = 0;

                    }
                }
                if(x % 5 == 0)
                    IoC.Get<IEventAggregator>().PublishOnUIThread(new ProgressRaport(((double)x/((double)original.Width))));
            }
            return image;

        }


    }
}
