﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using System.Reflection;
using System.Collections.ObjectModel;
using Caliburn.Micro;
using System.Diagnostics;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class ShapeIsolation : Effect
    {
        public ShapeIsolation()
        {
            ColorsList.Add(Colors.White);
            var colors = typeof(Colors).GetRuntimeProperties();
            foreach (var color in colors)
            {

                Color c = (Color)color.GetValue(color);

                if ((c.R == 255 && c.G == 255 && c.B == 255) ||( c.R == 0 && c.G == 0 && c.B == 0))
                    continue;

                if (c.R < 230 && c.G < 230 && c.B < 230)
                {
                    c.A = 0;
                    ColorsList.Add(c);
                }

                Name = "Shape isolation";


            }
        }

        private List<Color> _ColorsList = new List<Color>();
        public List<Color> ColorsList
        {
            get { return _ColorsList; }
            set
            {
                _ColorsList = value;
                NotifyOfPropertyChange(() => ColorsList);
            }
        }

        private byte ColorIndex = 1;

        private ImageArray Image { get; set; }

        private void DiscoverLinear(int x, int y)
        {
            Image[x, y].R = ColorIndex;
            Image[x, y].G = ColorIndex;
            Image[x, y].B = ColorIndex;

            List<Pixel> rgbs = new List<Pixel>();
            rgbs.Add(new Pixel(Image[x, y]));

            while (true)
            {
                bool found = false;
                for (int _x = -1; _x <= 1; ++_x)
                {
                    for (int _y = -1; _y <= 1; ++_y)
                    {
                        if (_x == 0 && _y == 0)
                            continue;
                        if (Image[x + _x, y + _y].R == 0 )
                        {
                            x += _x;
                            y += _y;
                            Image[x, y].R = ColorIndex;
                            Image[x, y].G = ColorIndex;
                            Image[x, y].B = ColorIndex;
                            rgbs.Add(new Pixel(Image[x, y]));
                            found = true;
                            break;
                        }
                    }
                    if (found)
                        break;
                }
                if (found)
                    continue;

                for (int i = rgbs.Count - 1; i >= 0; --i)
                {
                    for (int _x = -1; _x <= 1; ++_x)
                    {
                        for (int _y = -1; _y <= 1; ++_y)
                        {
                            if (_x == 0 && _y == 0)
                                continue;
                            if (Image[rgbs[i].X + _x, rgbs[i].Y + _y].R == 0)
                            {
                                x = rgbs[i].X + _x;
                                y = rgbs[i].Y + _y;
                                Image[x, y].R = ColorIndex;
                                Image[x, y].G = ColorIndex;
                                Image[x, y].B = ColorIndex;
                                rgbs.Add(new Pixel(Image[x, y]));
                                found = true;
                                break;
                            }
                        }
                        if (found)
                            break;
                    }
                    if (found)
                        break;
                }

                if (found)
                    continue;
                break;
            }
            rgbs.Clear();
            rgbs = null;
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            ColorIndex = 1;
            Effect threshold = new GlobalThreshold();
            Effect border = new Border();
            original = threshold.ApplyEffectQuietly(original, action);
            original = border.ApplyEffectQuietly(original, action);

            Image = original;

            for (int x = 0; x < original.Width; ++x)
                for (int y = 0; y < original.Height; ++y)
                {
                    if (original[x, y].RGB != 0)
                        continue;

                    DiscoverLinear(x, y);
                    ColorIndex++;
                    if (ColorIndex == 255)
                        ColorIndex = 1;

                }
            foreach (var pixel in original)
                if (pixel.R != 255)
                {
                    int index = pixel.R;
                    if (pixel.R >= ColorsList.Count)
                        index = pixel.R % (ColorsList.Count);
                    pixel.R = ColorsList[index].R;
                    pixel.G = ColorsList[index].G;
                    pixel.B = ColorsList[index].B;
                }

            return original;
        }
    }
}
