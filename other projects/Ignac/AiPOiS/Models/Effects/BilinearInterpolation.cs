﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class BilinearInterpolation : AdjustableEffect
    {
        public BilinearInterpolation()
        {
            MinAdjustValue = 0.5;
            AdjustValue = 1.0;
            MaxAdjustValue = 4.0;
            AdjustStep = 0.1;
            Name = "Bilinear interpolation";
        }

        public override string GetTooltip(double value)
        {
            return String.Format("Scale: {0:#.##}x", value);
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            if (AdjustValue <= 2.0)
            {
                Scale scale = new Scale();
                scale.AdjustValue = AdjustValue;
                return scale.ApplyEffect(original, action);
            }

            ImageArray result = new ImageArray((int)(original.Width * AdjustValue), (int)(original.Height * AdjustValue));

            bool[,] known = new bool[result.Width, result.Height];

            foreach (var pixel in original)
            {
                result[(int)(pixel.X * AdjustValue), (int)(pixel.Y * AdjustValue)].Set(pixel.R, pixel.G, pixel.B);
                known[(int)(pixel.X * AdjustValue), (int)(pixel.Y * AdjustValue)] = true;

            }

            double x1, x2, y1, y2;
            byte k1, k2, k3, k4;
            double R1, R2;


            double k = 0.0;

            for (int x = 0; x < result.Width; ++x)
                for (int y = 0; y < result.Height; ++y)
                {
                    if (known[x, y])
                        continue;

                    x1 = (int)Math.Floor(x / AdjustValue);
                    x2 = (int)Math.Ceiling(x / AdjustValue);

                    if (x2 == original.Width)
                        x2--;

                    x1 = (int)(x1 * AdjustValue);
                    x2 = (int)(x2 * AdjustValue);


                    y1 = (int)Math.Floor(y / AdjustValue);
                    y2 = (int)Math.Ceiling(y / AdjustValue);

                    if (y2 == original.Height)
                        y2--;

                    y1 = (int)(y1 * AdjustValue);
                    y2 = (int)(y2 * AdjustValue);

                    for (int i = 0; i < 3; ++i)
                    {
                        k1 = result[(int)x1, (int)y1][i];
                        k2 = result[(int)x2, (int)y1][i];
                        k3 = result[(int)x1, (int)y2][i];
                        k4 = result[(int)x2, (int)y2][i];

                        if (x1 == x2 && y1 == y2)
                        {
                            result[x, y][i] = k1;
                            continue;
                        }
                        else if (x1 == x || x1 == x2)
                        {
                            if (y1 == y2)
                                result[x, y][i] = k1;
                            else
                                result[x, y][i] = (byte)((y2 - y) / (y2 - y1) * k1 + (y - y1) / (y2 - y1) * k3);

                            continue;
                        }
                        else if (x2 == x)
                        {
                            if (y1 == y2)
                                result[x, y][i] = k2;
                            else
                                result[x, y][i] = (byte)((y2 - y) / (y2 - y1) * k2 + (y - y1) / (y2 - y1) * k4);

                            continue;
                        }
                        else if (y1 == y || y1 == y2)
                        {
                            if (x1 == x2)
                                result[x, y][i] = k1;
                            else
                                result[x, y][i] = (byte)((x2 - x) / (x2 - x1) * k1 + (x - x1) / (x2 - x1) * k2);
                            continue;
                        }
                        else if (y2 == y)
                        {
                            if (x1 == x2)
                                result[x, y][i] = k3;
                            else
                                result[x, y][i] = (byte)((x2 - x) / (x2 - x1) * k3 + (x - x1) / (x2 - x1) * k4);
                            continue;
                        }


                        R1 = ((x2 - x) / (x2 - x1)) * k1 + (x - x1) / (x2 - x1) * k2;
                        R2 = ((x2 - x) / (x2 - x1)) * k3 + (x - x1) / (x2 - x1) * k4;

                        k = (y2 - y) / (y2 - y1) * R1 + (y - y1) / (y2 - y1) * R2;
                        result[x, y][i] = (byte)Math.Min(Math.Max(k, 0), 255);
                    }
                }

            return result;
        }
    }
}
