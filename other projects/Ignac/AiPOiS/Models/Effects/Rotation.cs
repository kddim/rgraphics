﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Rotation : AdjustableEffect
    {
        public Rotation()
        {
            MinAdjustValue = 0;
            AdjustValue = 0;
            MaxAdjustValue = 359;
            AdjustStep = 1;
            Name = "Rotation";
        }

        public override string GetTooltip(double value)
        {
            return String.Format("Degrees: {0}", value);
        }


        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            double x0 = original.Width / 2.0;
            double y0 = original.Height / 2.0;
            double r = Math.PI / 180.0 * AdjustValue;

            double cos = Math.Cos(-r);
            double sin = Math.Sin(-r);

            double resultWidth = 0;
            double resultHeight = 0;

            if (AdjustValue <= 90 || (AdjustValue > 180 && AdjustValue <= 270))
            {
                resultWidth = Math.Abs(2 * (cos * (original.Width / 2.0) - sin * (original.Height / 2.0)));
                resultHeight = Math.Abs(2 * (sin * (original.Width / 2.0) - cos * (original.Height / 2.0)));
            }
            else if (AdjustValue <= 180 || (AdjustValue > 270 && AdjustValue < 360))
            {
                resultWidth = Math.Abs(2 * (cos * (original.Width / 2.0) + sin * (original.Height / 2.0)));
                resultHeight = Math.Abs(2 * (sin * (original.Width / 2.0) + cos * (original.Height / 2.0)));
            }
            //r = Math.PI / 180.0 * (-AdjustValue);

         //   double x1 = x0 * cos - y0 * sin;
         //   double y1 = x0 * sin + y0 * cos;

            double x1 = resultWidth / 2.0;
            double y1 = resultHeight / 2.0;

            ImageArray result = new ImageArray((int)resultWidth, (int)resultHeight);

            for (int x = 0; x < result.Width; ++x)
                for (int y = 0; y < result.Height; ++y )
                {
                    double cx = x - x1;
                    double cy = y - y1;

                    double _x = cx * cos - cy * sin;
                    double _y = cx * sin + cy * cos;

                    _x += x0;
                    _y += y0;

                    if (_x >= 0 && _x < original.Width && _y >= 0 && _y < original.Height)
                        result[x, y].Set(original[(int)_x, (int)_y].R, original[(int)_x, (int)_y].G, original[(int)_x, (int)_y].B);

                }


                    /*

                    List<Pixel> pixels = new List<Pixel>();

                    double x2 = 0.0;
                    double y2 = 0.0;

                    foreach (var pixel in original)
                    {

                        x2 = ((cos * (pixel.X - x0) - sin * (pixel.Y - y0))) + x0;
                        y2 = ((sin * (pixel.X - x0) + cos * (pixel.Y - y0))) + y0;


                        pixels.Add(new Pixel((int)x2, (int)y2, pixel.R, pixel.G, pixel.B, pixel.A));
                    }

                    ImageArray result = new ImageArray(pixels);*/
                    return result;
        }
    }
}
