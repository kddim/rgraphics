﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Motorola.AiPOiS.Models.Effects
{
    public class SaltAndPepper : AdjustableEffect
    {
        public SaltAndPepper()
        {
            MinAdjustValue = 1;
            MaxAdjustValue = 50;
            AdjustValue = 5;
            Name = "Salt&Pepper noise";
        }

        public override string GetTooltip(double value)
        {
            return String.Format("Chance: {0} %", value);
        }

        protected override ImageArray Apply(ImageArray original, IAsyncAction action)
        {
            double chance = AdjustValue / 100.0;
            Random generator = new Random();
            for (int x = 0; x < original.Width; ++x)
            {
                for (int y = 0; y < original.Height; ++y)
                {
                    if (generator.NextDouble() > chance)
                        continue;

                    if (generator.NextDouble() < 0.5)
                        original[x, y].RGB = 0;
                    else
                        original[x, y].RGB = 255;
                }
            }

            return original;
        }
    }
}
