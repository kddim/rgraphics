﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motorola.AiPOiS.Models.Effects
{
    public class ImageArray : IEnumerable
    {
        public readonly int Width;
        public readonly int Height;

        // public readonly int CenterX;
        //  public readonly int CenterY;

        public int PixelsCount
        {
            get { return Width * Height; }
        }
        private Pixel[,] Image { get; set; }


        public ImageArray(int width, int height)
        {
            Width = width;
            Height = height;

            // CenterX = (int)(Width / 2);
            // CenterY = (int)(Height / 2);


            Image = new Pixel[width, height];
            for (int x = 0; x < width; ++x)
                for (int y = 0; y < height; ++y)
                {
                    Image[x, y] = new Pixel(x, y);
                }
        }
        public ImageArray(byte[] pixels, int width, int height)
        {
            Width = width;
            Height = height;

            // CenterX = (int)(Width / 2);
            //  CenterY = (int)(Height / 2);

            Image = new Pixel[width, height];


            for (int x = 0; x < width; ++x)
                for (int y = 0; y < height; ++y)
                {
                    int index = x * 4 + y * Width * 4;
                    Image[x, y] = new Pixel(x, y, pixels[index + 2], pixels[index + 1], pixels[index], pixels[index + 3]);
                }
        }

        public ImageArray(IEnumerable<Pixel> pixels)
        {
            int minX = int.MaxValue;
            int minY = int.MaxValue;
            int maxX = int.MinValue;
            int maxY = int.MinValue;

            foreach(var pixel in pixels)
            {

                if (pixel.X < minX)
                    minX = pixel.X;
                if (pixel.X > maxX)
                    maxX = pixel.X;
                if (pixel.Y > maxY)
                    maxY = pixel.Y;
                if (pixel.Y < minY)
                    minY = pixel.Y;
            }

            Width = Math.Abs(maxX - minX) + 1;
            Height = Math.Abs(minY - maxY) + 1;

            int addX = -minX;
            int addY = -minY;


            Image = new Pixel[Width, Height];

            for (int x = 0; x < Width; ++x)
                for (int y = 0; y < Height; ++y)
                    Image[x, y] = new Pixel(x, y);

           
            foreach(var pixel in pixels)
                Image[pixel.X + addX, pixel.Y + addY] = new Pixel(pixel.X + addX, pixel.Y + addY, pixel.R, pixel.G, pixel.B);
        }


        /// <summary>
        /// Gets the <see cref="Pixel"/> on the specified coordinates
        /// </summary>
        /// <value>
        /// The <see cref="Pixel"/>.
        /// </value>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <returns></returns>
        public Pixel this[int x, int y]
        {
            get
            {
                return Image[x, y];
            }
        }

        public Pixel Extended(int x, int y)
        {
            if (x < 0)
                x = -x - 1;
            else if (x >= Width)
                x = Width - (x - Width + 1);

            if (y < 0)
                y = -y - 1;
            else if (y >= Height)
                y = Height - (y - Height + 1);

            return this[x, y];
        }

        //public RGBA Centered(int x, int y)
        //{
        //    return Extended(CenterX + x, CenterY + y);
        //}

        public byte[] Get1DArray()
        {
            byte[] pixels = new byte[PixelsCount * 4];
            foreach (var pixel in this)
            {
                pixels[pixel.X * 4 + pixel.Y * Width * 4 + 0] = pixel.B;
                pixels[pixel.X * 4 + pixel.Y * Width * 4 + 1] = pixel.G;
                pixels[pixel.X * 4 + pixel.Y * Width * 4 + 2] = pixel.R;
                pixels[pixel.X * 4 + pixel.Y * Width * 4 + 3] = pixel.A;
            }
            return pixels;
        }


        public IEnumerator<Pixel> GetEnumerator()
        {
            foreach (var pixel in Image)
                yield return pixel;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
