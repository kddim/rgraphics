﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motorola.AiPOiS.Models.Effects
{
    public class Pixel
    {
        public int X
        {
            get;
            private set;
        }
        public int Y
        {
            get;
            private set;
        }
        public byte B
        {
            get;
            set;
        }

        public byte G
        {
            get;
            set;
        }

        public byte R
        {
            get;
            set;
        }

        public byte A
        {
            get;
            set;
        }

        public byte Max
        {
            get
            {
                return (byte)Math.Max(R, Math.Max(G, B));
            }
        }

        public byte RGB
        {
            get
            {
                return (byte)((R + G + B) / 3);
            }
            set
            {
                R = value;
                G = value;
                B = value;
            }
        }

        public void Set(byte r = 0, byte g = 0, byte b = 0, byte a = 0)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }



        /// <summary>
        /// Gets the <see cref="System.Byte"/> at the specified index.
        /// 0 - Red
        /// 1 - Green
        /// 2 - Blue
        /// 3 - Alpha
        /// </summary>
        /// <value>
        /// The <see cref="System.Byte"/>.
        /// </value>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Index must has value 0, 1, 2 or 3</exception>
        public byte this[int index]
        {
            get
            {
                if (index == 0)
                    return R;
                else if (index == 1)
                    return G;
                else if (index == 2)
                    return B;
                else if (index == 3)
                    return A;
                throw new ArgumentException("Index must has value 0, 1, 2 or 3");
            }
            set
            {
                if (index == 0)
                    R = value;
                else if (index == 1)
                    G = value;
                else if (index == 2)
                    B = value;
                else if (index == 3)
                    A = value;
                else
                    throw new ArgumentException("Index must has value 0, 1, 2 or 3");
            }
        }
        public Pixel(int x, int y, byte r = 0, byte g = 0, byte b = 0, byte a = 0)
        {
            X = x;
            Y = y;
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public Pixel(Pixel rgba)
        {
            X = rgba.X;
            Y = rgba.Y;
            R = rgba.R;
            G = rgba.G;
            B = rgba.B;
            A = rgba.A;
        }
    }
}
