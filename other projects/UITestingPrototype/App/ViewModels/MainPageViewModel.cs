﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.System.Threading;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using UITestApp.Effects;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;


namespace UITestApp.ViewModels
{
    public class MainPageViewModel : Screen, IHandle<IEffect>
    {
        private readonly INavigationService navigationService;
        public MainPageViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            IoC.Get<IEventAggregator>().Subscribe(this);
          
        }

        public void Histogram()
        {
            NavigationExtensions.NavigateToViewModel<HistogramViewModel>(navigationService, "test");
            byte[] image = new byte[SourceImage.Length];
            Array.Copy(SourceImage, image, SourceImage.Length);
            
            IoC.Get<IEventAggregator>().PublishOnCurrentThread(new HistogramMessage() { Image = image, Width = Decoder.PixelWidth, Height = Decoder.PixelHeight });
        }

        protected override void OnInitialize()
        {
            Effects = new ObservableCollection<IEffect>();
            Effects.Add(new Effects.Original());
            Effects.Add(new Effects.Negative());
            Effects.Add(new Effects.Grayscale());
            Effects.Add(new Effects.Grayscale_v2());
            Effects.Add(new Effects.Sepia());
            Effects.Add(new Effects.RobertsCross());
            Effects.Add(new Effects.Sobel());
            Effects.Add(new Effects.GlobalThreshold());
            Effects.Add(new Effects.LocalThreshold());

        }

        public async void LoadImage()
        {
            FileOpenPicker fop = new FileOpenPicker();
            fop.ViewMode = PickerViewMode.Thumbnail;
            fop.SuggestedStartLocation = PickerLocationId.Desktop;
            fop.FileTypeFilter.Add(".jpg");
            fop.FileTypeFilter.Add(".png");
            fop.FileTypeFilter.Add(".bmp");
            fop.FileTypeFilter.Add(".jpeg");
            fop.FileTypeFilter.Add(".gif");

            StorageFile file = await fop.PickSingleFileAsync();

            if (file == null)
                return;

            FileStream = await file.OpenAsync(FileAccessMode.Read);
            BitmapImage bimg = new BitmapImage();
            bimg.SetSource(FileStream);


            switch (file.FileType.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    DecoderId = BitmapDecoder.JpegDecoderId;
                    break;
                case ".png":
                    DecoderId = BitmapDecoder.PngDecoderId;
                    break;
                case ".bmp":
                    DecoderId = BitmapDecoder.BmpDecoderId;
                    break;
                case ".gif":
                    DecoderId = BitmapDecoder.GifDecoderId;
                    break;
            }

            Decoder = await BitmapDecoder.CreateAsync(DecoderId, FileStream);
            PixelDataProvider pixelData = await Decoder.GetPixelDataAsync(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Straight, new BitmapTransform(), ExifOrientationMode.IgnoreExifOrientation, ColorManagementMode.DoNotColorManage);

            SourceImage = pixelData.DetachPixelData();


            AreEffectsEnabled = true;


            Storyboard sb = (GetView() as FrameworkElement).Resources["FadeIn"] as Storyboard;
            if (sb != null)
                sb.Begin();

            Image = null;

            if (Effect == null && Effects.Count > 0)
                Effect = Effects[0];
            else if (Effect != null)
                ApplyEffect();


        }


        private byte[] _SourceImage;
        public byte[] SourceImage
        {
            get { return _SourceImage; }
            set
            {
                _SourceImage = value;
                NotifyOfPropertyChange(() => SourceImage);

            }
        }

        private byte[] ResultPixels { get; set; }

        private WriteableBitmap _Image;
        public WriteableBitmap Image
        {
            get { return _Image; }
            set
            {
                _Image = value;
                NotifyOfPropertyChange(() => Image);
            }
        }

        private Boolean _IsEffectsListEnabled;
        public Boolean AreEffectsEnabled
        {
            get { return _IsEffectsListEnabled; }
            set
            {
                _IsEffectsListEnabled = value;
                NotifyOfPropertyChange(() => AreEffectsEnabled);
            }
        }

        private Boolean _IsNormalizingHistogram;
        public Boolean IsNormalizingHistogram
        {
            get { return _IsNormalizingHistogram; }
            set
            {
                if (value != _IsNormalizingHistogram)
                {
                    _IsNormalizingHistogram = value;
                    ApplyEffect();
                    NotifyOfPropertyChange(() => IsNormalizingHistogram);
                }
            }
        }

        private IEffect NormalizeEffect = new Effects.Normalize();

        private IEffect _Effect;
        public IEffect Effect
        {
            get { return _Effect; }
            set
            {
                if (value != _Effect)
                {
                    _Effect = value;
                    if (value != null)
                        ApplyEffect();
                    NotifyOfPropertyChange(() => Effect);
                }
            }
        }

        private async void ApplyEffect()
        {
            ResultPixels = new byte[SourceImage.Length];
            Array.Copy(SourceImage, ResultPixels, SourceImage.Length);


            await ThreadPool.RunAsync(new WorkItemHandler((IAsyncAction action) => ResultPixels = Effect.ApplyEffect(ResultPixels, Decoder.PixelWidth, Decoder.PixelHeight)));
            if(IsNormalizingHistogram)
                await ThreadPool.RunAsync(new WorkItemHandler((IAsyncAction action) => ResultPixels = NormalizeEffect.ApplyEffect(ResultPixels, Decoder.PixelWidth, Decoder.PixelHeight)));
            WriteableBitmap writeableBitmap = new WriteableBitmap((int)Decoder.PixelWidth, (int)Decoder.PixelHeight);

            using (Stream stream = writeableBitmap.PixelBuffer.AsStream())
            {
                await stream.WriteAsync(ResultPixels, 0, SourceImage.Length);
            }
            Image = writeableBitmap;

            NotifyOfPropertyChange(() => Effect);
        }


        private ObservableCollection<IEffect> _Effects;
        public ObservableCollection<IEffect> Effects
        {
            get { return _Effects; }
            set
            {
                _Effects = value;
                NotifyOfPropertyChange(() => Effects);
            }
        }


        public void Handle(IEffect effect)
        {
            _Effect = effect;
            ApplyEffect();
        }

        private IRandomAccessStream FileStream { get; set; }
        private Guid DecoderId { get; set; }
        private BitmapDecoder Decoder { get; set; }
    }
}
