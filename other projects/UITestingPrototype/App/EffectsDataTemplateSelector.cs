﻿using UITestApp.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace UITestApp
{
    public class EffectsDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate StandardTemplate { get; set; }
        public DataTemplate SliderTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            if (item is IAdjustable)
                return SliderTemplate;
            return StandardTemplate;
        }
    }

}
