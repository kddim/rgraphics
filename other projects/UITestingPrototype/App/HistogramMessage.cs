﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp
{
    public class HistogramMessage
    {
        public byte[] Image { get; set; }
        public uint Width { get; set; }
        public uint Height { get; set; }
    }
}
