﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public enum RGB
    {
        B = 0,
        G = 1,
        R = 2
    }

    public static class Helper
    {

        /// <summary>
        /// Returns index of pixel in 1D array
        /// </summary>
        /// <param name="rgb">RGB offset specified by enum</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">Image width</param>
        public static uint I(RGB rgb, uint x, uint y, uint width)
        {
            return x * 4 + y *width * 4 + (uint)rgb;
        }

        public static byte[] ApplyFilter<T>(byte[] sourcePixels, uint width, uint height) where T :  IEffect, new()
        {
            T effect = new T();
            return effect.ApplyEffect(sourcePixels, width, height);
        }
    }
}
