﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public class RobertsCross : IEffect
    {

        public string Name
        {
            get { return "Robert's Cross"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            int a = 0;
            int b = 0;
            byte[] pixels = new byte[sourcePixels.Length];
            RGB[] rgbs = new RGB[] { RGB.R, RGB.G, RGB.B };

            for (uint x = 0; x < width - 1; ++x)
                for (uint y = 0; y < height - 1; ++y)
                {
                    foreach (RGB rgb in rgbs)
                    {
                        a = sourcePixels[Helper.I(rgb, x, y, width)] - sourcePixels[Helper.I(rgb, x + 1, y + 1, width)];
                        b = sourcePixels[Helper.I(rgb, x + 1, y, width)] - sourcePixels[Helper.I(rgb, x, y + 1, width)];
                        pixels[Helper.I(rgb, x, y, width)] = (byte)Math.Min(Math.Abs(a + b), 255);
                    }
                }
            return pixels;
        }
    }
}
