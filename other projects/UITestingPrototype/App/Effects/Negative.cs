﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public class Negative : IEffect
    {

        public string Name
        {
            get { return "Negative"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            byte dpp = 255;

            for (int j = 0; j < sourcePixels.Length; j+=4)
            {
                sourcePixels[j] = (byte)(dpp - sourcePixels[j]);
                sourcePixels[j+1] = (byte)(dpp - sourcePixels[j+1]);
                sourcePixels[j+2] = (byte)(dpp - sourcePixels[j+2]);
            }

            return sourcePixels;
        }
    }
}
