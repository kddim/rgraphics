﻿using System;

namespace UITestApp.Effects
{
    public class Pixel
    {
        private byte _R;
        public byte R
        {
            get { return _R; }
            set
            {
                _R = Math.Max(value, (byte)0);
                _R = Math.Min(value, (byte)255);
            }
        }

        private byte _G;
        public byte G
        {
            get { return _G; }
            set
            {
                _G = Math.Max(value, (byte)0);
                _G = Math.Min(value, (byte)255);
            }
        }

        private byte _B;
        public byte B
        {
            get { return _B; }
            set
            {
                _B = Math.Max(value, (byte)0);
                _B = Math.Min(value, (byte)255);
            }
        }

        private byte _A;
        public byte A
        {
            get { return _A; }
            set
            {
                _A = Math.Max(value, (byte)0);
                _A = Math.Min(value, (byte)255);
            }
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        
        internal Pixel(byte r, byte g, byte b, byte a, int x, int y)
        {
            R = r;
            G = g;
            B = b;
            A = a;
            X = x;
            Y = y;
        }
        

    }
}
