﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public class Sobel : IEffect
    {
        public string Name
        {
            get { return "Sobel's filter"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            byte[] pixels = new byte[sourcePixels.Length];
            for (uint x = 1; x < width - 1; ++x)
                for (uint y = 1; y < height - 1; ++y)
                {
                    RGB[] rgbs = new RGB[] { RGB.R, RGB.G, RGB.B };


                    foreach (RGB rgb in rgbs)
                    {

                        int p0 = sourcePixels[Helper.I(rgb, x - 1, y - 1, width)];
                        int p1 = sourcePixels[Helper.I(rgb, x, y - 1, width)];
                        int p2 = sourcePixels[Helper.I(rgb, x + 1, y - 1, width)];
                        int p3 = sourcePixels[Helper.I(rgb, x + 1, y, width)];
                        int p4 = sourcePixels[Helper.I(rgb, x + 1, y + 1, width)];
                        int p5 = sourcePixels[Helper.I(rgb, x, y + 1, width)];
                        int p6 = sourcePixels[Helper.I(rgb, x - 1, y + 1, width)];
                        int p7 = sourcePixels[Helper.I(rgb, x - 1, y, width)];

                        double X = ((double)(p2 + (2 * p3) + p4)) - ((double)(p0 + (2 * p7) + p6));
                        double Y = ((double)(p6 + (2 * p5) + p4)) - ((double)(p0 + (2 * p1) + p2));
                        double PX = X * X + Y * Y;
                        PX = Math.Sqrt(PX);
                        PX = Math.Min(255.0, PX);

                        pixels[Helper.I(rgb, x, y, width)] = (byte)PX;
                    }
                }

            return pixels;
        }
    }
}
