﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public interface IAdjustable
    {
        double MinAdjustValue { get; }
        double MaxAdjustValue { get; }
        double AdjustValue { get; set; }
        double AdjustStep { get; }
    }
}
