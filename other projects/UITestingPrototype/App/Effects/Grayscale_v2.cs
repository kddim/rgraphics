﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public class Grayscale_v2 : IEffect
    {
        public string Name
        {
            get { return "Grayscale v2"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            int i = 0;
            for (int w = 0; w < width; ++w)
                for (int h = 0; h < height; ++h)
                {
                    // - 0.3*R+0.59*G+0.11*B 
                    int avg = (int)(0.11 * sourcePixels[i] + 0.59 * sourcePixels[i + 1] + 0.3 * sourcePixels[i + 2]);
                    sourcePixels[i] = (byte)avg;
                    sourcePixels[i + 1] = (byte)avg;
                    sourcePixels[i + 2] = (byte)avg;
                    i += 4;
                }

            return sourcePixels;
        }
    }
}
