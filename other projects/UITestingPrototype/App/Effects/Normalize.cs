﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public class Normalize : IEffect
    {
        public string Name
        {
            get { return "Normalize Histogram"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            //pixel[x,y]=255*(pixel[x,y]-minPix)/(maxPix-minPix)

            byte[] min = new byte[3] { 255, 255, 255};
            byte[] max = new byte[3] { 0, 0, 0};
            RGB[] rgbs = new RGB[] { RGB.B, RGB.G, RGB.R };

            for (int i = 0; i < sourcePixels.Length; i += 4)
                foreach (RGB rgb in rgbs)
                {
                    if (sourcePixels[i + (int)rgb] < min[(int)rgb])
                        min[(int)rgb] = sourcePixels[i + (int)rgb];

                    if(sourcePixels[i + (int)rgb] > max[(int)rgb])
                        max[(int)rgb] = sourcePixels[i + (int)rgb];
                }

            //pixel[x,y]=255*(pixel[x,y]-minPix)/(maxPix-minPix)

            for (int i = 0; i < sourcePixels.Length; i+=4 )
            {
                foreach (RGB rgb in rgbs)
                    sourcePixels[i + (int)rgb] = (byte)Math.Min(255, Math.Max(0, (255 * ((byte)(sourcePixels[i + (int)rgb] - min[(int)rgb]))) / (max[(int)rgb] - min[(int)rgb])));
            }



            return sourcePixels;
        }
    }
}
