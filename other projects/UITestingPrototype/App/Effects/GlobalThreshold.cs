﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public class GlobalThreshold : IEffect
    {

        public string Name
        {
            get { return "Global Threshold"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            sourcePixels = Helper.ApplyFilter<Grayscale>(sourcePixels, width, height);
            double avg = 0;
            for (int i = 0; i < sourcePixels.Length; i += 4)
                avg += sourcePixels[i];

            avg /= (sourcePixels.Length / 4);

            for(int i = 0; i < sourcePixels.Length; i += 4)
            {
                sourcePixels[i] = (sourcePixels[i] > avg) ? (byte)255 : (byte)0;
                sourcePixels[i + 1] = sourcePixels[i + 2] = sourcePixels[i];

            }

            return sourcePixels;

        }
    }
}
