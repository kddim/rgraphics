﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public class Grayscale : IEffect
    {
        public string Name
        {
            get { return "Grayscale"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            int i = 0;
            for (int w = 0; w < width; ++w)
                for (int h = 0; h < height; ++h )
                {
                    //(R+G+B)/3
                    int avg = sourcePixels[i] + sourcePixels[i + 1] + sourcePixels[i + 2];
                    avg /= 3;
                    sourcePixels[i] = (byte)avg;
                    sourcePixels[i+1] = (byte)avg;
                    sourcePixels[i+2] = (byte)avg;
                    i += 4;

                }

            return sourcePixels;
        }
    }
}
