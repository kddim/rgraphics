﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public class Original : IEffect
    {

        public string Name
        {
            get { return "Original"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            return sourcePixels;
        }
    }
}
