﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.System.Threading;

namespace UITestApp.Effects
{
    public class Sepia : PropertyChangedBase, IEffect, IAdjustable
    {
        public Sepia()
        {
            MinAdjustValue = 20.0;
            MaxAdjustValue = 40.0;
        }

        private DateTime dtime { get; set; }

        public string Name
        {
            get { return "Sepia"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            int i = 0;
            for (int w = 0; w < width; ++w)
                for (int h = 0; h < height; ++h )
                {
                    //    R=R+2*W, G=G+W, B=B
                    int r = Math.Min(sourcePixels[i + 2] + 2 * (int)AdjustValue, 255);
                    int g = Math.Min(sourcePixels[i] + (int)AdjustValue, 255);

                    sourcePixels[i + 1] = (byte)g;
                    sourcePixels[i + 2] = (byte)r;
                    i += 4;

                }

            return sourcePixels;
        }

        private double _MinAdjustValue;
        public double MinAdjustValue
        {
            get { return _MinAdjustValue; }
            set
            {
                _MinAdjustValue = value;
                NotifyOfPropertyChange(() => MinAdjustValue);
            }
        }

        private double _MaxAdjustValue;
        public double MaxAdjustValue
        {
            get { return _MaxAdjustValue; }
            set
            {
                _MaxAdjustValue = value;
                NotifyOfPropertyChange(() => MaxAdjustValue);
            }
        }

        private double _AdjustValue = 30.0;
        public double AdjustValue
        {
            get
            {
                return _AdjustValue;
            }
            set
            {
                _AdjustValue = value;
                dtime = DateTime.Now;

                Task DelayTask = new Task(() => //haha wkoncu na to wpadlem...
                {
                    DateTime dt = dtime;
                    DateTime dt2 = DateTime.Now;
                    bool result = true;
                    while ((DateTime.Now - dt2).Milliseconds < 333)
                    {
                        if (dt != dtime)
                        {
                            result = false;
                            break;
                        }
                    }

                    if (result)
                        IoC.Get<IEventAggregator>().PublishOnUIThread(this as IEffect);

                });
                DelayTask.Start();

                NotifyOfPropertyChange(() => AdjustValue);
            }
        }

        private double _AdjustStep = 1.0;
        public double AdjustStep
        {
            get { return _AdjustStep; }
            set
            {
                _AdjustStep = value;
                NotifyOfPropertyChange(() => AdjustStep);
            }
        }
        
    }
}
