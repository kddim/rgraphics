﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public interface IEffect
    {
        string Name { get; }
        byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height);
    }
}
