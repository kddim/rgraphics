﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestApp.Effects
{
    public class LocalThreshold : PropertyChangedBase, IEffect, IAdjustable
    {
        private int[] Values = new int[] { 5, 11, 15, 21, 25 };
        private int index = 0; 

        public string Name
        {
            get { return "Local Threshold"; }
        }

        public byte[] ApplyEffect(byte[] sourcePixels, uint width, uint height)
        {
            return sourcePixels;
        }

        public double MinAdjustValue
        {
            get { return 5d; }
        }

        public double MaxAdjustValue
        {
            get { return 25d; }
        }

        private double _AdjustValue = 5;
        public double AdjustValue
        {
            get
            {
                return _AdjustValue;
                
            }
            set
            {
                if (value > _AdjustValue)
                    _AdjustValue = Values[index++];
                else if (value < _AdjustValue)
                    _AdjustValue = Values[index--];

                if (index == -1)
                    index = 0;
                else if (index == Values.Length)
                    index = Values.Length - 1;

                if (index == Values.Length - 1)
                    AdjustStep = Values[Values.Length - 1] - Values[Values.Length - 2];
                else
                    AdjustStep = Values[index + 1] - Values[index];

                NotifyOfPropertyChange(() => AdjustValue);
            }
        }
        private double _AdjustStep = 6;
        public double AdjustStep
        {
            get { return _AdjustStep; }
            set
            {
                _AdjustStep = value;
                NotifyOfPropertyChange(() => AdjustStep);
            }
        }
    }
}
