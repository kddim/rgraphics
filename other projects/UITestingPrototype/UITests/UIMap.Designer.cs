﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by coded UI test builder.
//      Version: 12.0.0.0
//
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------

namespace UITests
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITest.Input;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
    using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = Microsoft.VisualStudio.TestTools.UITest.Input.MouseButtons;
    
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public partial class UIMap
    {
        
        #region Properties
        public UIMenuStartPane UIMenuStartPane
        {
            get
            {
                if ((this.mUIMenuStartPane == null))
                {
                    this.mUIMenuStartPane = new UIMenuStartPane();
                }
                return this.mUIMenuStartPane;
            }
        }
        
        public UIBiometria01Window UIBiometria01Window
        {
            get
            {
                if ((this.mUIBiometria01Window == null))
                {
                    this.mUIBiometria01Window = new UIBiometria01Window();
                }
                return this.mUIBiometria01Window;
            }
        }
        #endregion
        
        #region Fields
        private UIMenuStartPane mUIMenuStartPane;
        
        private UIBiometria01Window mUIBiometria01Window;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIMenuStartPane : UITestControl
    {
        
        public UIMenuStartPane()
        {
            #region Search Criteria
            this.TechnologyName = "UIA";
            this.SearchProperties[UITestControl.PropertyNames.ControlType] = "Pane";
            this.SearchProperties[UITestControl.PropertyNames.Name] = "Menu Start";
            this.SearchProperties["FrameworkId"] = "Win32";
            this.SearchProperties[UITestControl.PropertyNames.ClassName] = "ImmersiveLauncher";
            this.WindowTitles.Add("Menu Start");
            #endregion
        }
        
        #region Properties
        public UIAplikacjestartoweList UIAplikacjestartoweList
        {
            get
            {
                if ((this.mUIAplikacjestartoweList == null))
                {
                    this.mUIAplikacjestartoweList = new UIAplikacjestartoweList(this);
                }
                return this.mUIAplikacjestartoweList;
            }
        }
        #endregion
        
        #region Fields
        private UIAplikacjestartoweList mUIAplikacjestartoweList;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIAplikacjestartoweList : DirectUIControl
    {
        
        public UIAplikacjestartoweList(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "GridRoot";
            this.WindowTitles.Add("Menu Start");
            #endregion
        }
        
        #region Properties
        public DirectUIControl UIAplikacjeButton
        {
            get
            {
                if ((this.mUIAplikacjeButton == null))
                {
                    this.mUIAplikacjeButton = new DirectUIControl(this);
                    #region Search Criteria
                    this.mUIAplikacjeButton.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "ViewSwitchButton";
                    this.mUIAplikacjeButton.WindowTitles.Add("Menu Start");
                    #endregion
                }
                return this.mUIAplikacjeButton;
            }
        }
        
        public UIµTorrentGroup UIµTorrentGroup
        {
            get
            {
                if ((this.mUIµTorrentGroup == null))
                {
                    this.mUIµTorrentGroup = new UIµTorrentGroup(this);
                }
                return this.mUIµTorrentGroup;
            }
        }
        
        public UIBiometria01Group UIBiometria01Group
        {
            get
            {
                if ((this.mUIBiometria01Group == null))
                {
                    this.mUIBiometria01Group = new UIBiometria01Group(this);
                }
                return this.mUIBiometria01Group;
            }
        }
        #endregion
        
        #region Fields
        private DirectUIControl mUIAplikacjeButton;
        
        private UIµTorrentGroup mUIµTorrentGroup;
        
        private UIBiometria01Group mUIBiometria01Group;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIµTorrentGroup : DirectUIControl
    {
        
        public UIµTorrentGroup(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "Group 0";
            this.WindowTitles.Add("Menu Start");
            #endregion
        }
        
        #region Properties
        public UIBiometria01ListItem UIBiometria01ListItem
        {
            get
            {
                if ((this.mUIBiometria01ListItem == null))
                {
                    this.mUIBiometria01ListItem = new UIBiometria01ListItem(this);
                }
                return this.mUIBiometria01ListItem;
            }
        }
        #endregion
        
        #region Fields
        private UIBiometria01ListItem mUIBiometria01ListItem;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIBiometria01ListItem : DirectUIControl
    {
        
        public UIBiometria01ListItem(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "7ce78daf-db72-4e64-8b1b-fcf0633745fa_kz15j230kjmx0!App";
            this.WindowTitles.Add("Menu Start");
            #endregion
        }
        
        #region Properties
        public DirectUIControl UILogoImage
        {
            get
            {
                if ((this.mUILogoImage == null))
                {
                    this.mUILogoImage = new DirectUIControl(this);
                    #region Search Criteria
                    this.mUILogoImage.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "Logo";
                    this.mUILogoImage.WindowTitles.Add("Menu Start");
                    #endregion
                }
                return this.mUILogoImage;
            }
        }
        #endregion
        
        #region Fields
        private DirectUIControl mUILogoImage;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIBiometria01Group : DirectUIControl
    {
        
        public UIBiometria01Group(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "Group 2317102008282120192";
            this.WindowTitles.Add("Menu Start");
            #endregion
        }
        
        #region Properties
        public DirectUIControl UIBiometria01ListItem
        {
            get
            {
                if ((this.mUIBiometria01ListItem == null))
                {
                    this.mUIBiometria01ListItem = new DirectUIControl(this);
                    #region Search Criteria
                    this.mUIBiometria01ListItem.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "7ce78daf-db72-4e64-8b1b-fcf0633745fa_kz15j230kjmx0!App";
                    this.mUIBiometria01ListItem.WindowTitles.Add("Menu Start");
                    #endregion
                }
                return this.mUIBiometria01ListItem;
            }
        }
        #endregion
        
        #region Fields
        private DirectUIControl mUIBiometria01ListItem;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIBiometria01Window : XamlWindow
    {
        
        public UIBiometria01Window()
        {
            #region Search Criteria
            this.SearchProperties[XamlControl.PropertyNames.Name] = "Biometria 01";
            this.SearchProperties[XamlControl.PropertyNames.ClassName] = "Windows.UI.Core.CoreWindow";
            this.WindowTitles.Add("Biometria 01");
            #endregion
        }
        
        #region Properties
        public XamlButton UILoadImageButton
        {
            get
            {
                if ((this.mUILoadImageButton == null))
                {
                    this.mUILoadImageButton = new XamlButton(this);
                    #region Search Criteria
                    this.mUILoadImageButton.SearchProperties[XamlButton.PropertyNames.AutomationId] = "LoadImage";
                    this.mUILoadImageButton.WindowTitles.Add("Biometria 01");
                    #endregion
                }
                return this.mUILoadImageButton;
            }
        }
        
        public UIWidoksiatkiList UIWidoksiatkiList
        {
            get
            {
                if ((this.mUIWidoksiatkiList == null))
                {
                    this.mUIWidoksiatkiList = new UIWidoksiatkiList(this);
                }
                return this.mUIWidoksiatkiList;
            }
        }
        
        public UIKoszykwyboruToolBar UIKoszykwyboruToolBar
        {
            get
            {
                if ((this.mUIKoszykwyboruToolBar == null))
                {
                    this.mUIKoszykwyboruToolBar = new UIKoszykwyboruToolBar(this);
                }
                return this.mUIKoszykwyboruToolBar;
            }
        }
        
        public XamlText UINegativeText
        {
            get
            {
                if ((this.mUINegativeText == null))
                {
                    this.mUINegativeText = new XamlText(this);
                    #region Search Criteria
                    this.mUINegativeText.SearchProperties[XamlText.PropertyNames.Name] = "Negative";
                    this.mUINegativeText.WindowTitles.Add("Biometria 01");
                    #endregion
                }
                return this.mUINegativeText;
            }
        }
        
        public UIEffectList UIEffectList
        {
            get
            {
                if ((this.mUIEffectList == null))
                {
                    this.mUIEffectList = new UIEffectList(this);
                }
                return this.mUIEffectList;
            }
        }
        #endregion
        
        #region Fields
        private XamlButton mUILoadImageButton;
        
        private UIWidoksiatkiList mUIWidoksiatkiList;
        
        private UIKoszykwyboruToolBar mUIKoszykwyboruToolBar;
        
        private XamlText mUINegativeText;
        
        private UIEffectList mUIEffectList;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIWidoksiatkiList : DirectUIControl
    {
        
        public UIWidoksiatkiList(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "GridRoot";
            this.WindowTitles.Add("Biometria 01");
            #endregion
        }
        
        #region Properties
        public UIWindowsSimulatorGroup UIWindowsSimulatorGroup
        {
            get
            {
                if ((this.mUIWindowsSimulatorGroup == null))
                {
                    this.mUIWindowsSimulatorGroup = new UIWindowsSimulatorGroup(this);
                }
                return this.mUIWindowsSimulatorGroup;
            }
        }
        #endregion
        
        #region Fields
        private UIWindowsSimulatorGroup mUIWindowsSimulatorGroup;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIWindowsSimulatorGroup : DirectUIControl
    {
        
        public UIWindowsSimulatorGroup(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "Group 0";
            this.WindowTitles.Add("Biometria 01");
            #endregion
        }
        
        #region Properties
        public DirectUIControl UIWallpaper2006231jpgListItem
        {
            get
            {
                if ((this.mUIWallpaper2006231jpgListItem == null))
                {
                    this.mUIWallpaper2006231jpgListItem = new DirectUIControl(this);
                    #region Search Criteria
                    this.mUIWallpaper2006231jpgListItem.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "D:\\Ignac\\Obrazy\\wallpaper-2006231.jpg";
                    this.mUIWallpaper2006231jpgListItem.WindowTitles.Add("Biometria 01");
                    #endregion
                }
                return this.mUIWallpaper2006231jpgListItem;
            }
        }
        #endregion
        
        #region Fields
        private DirectUIControl mUIWallpaper2006231jpgListItem;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIKoszykwyboruToolBar : DirectUIControl
    {
        
        public UIKoszykwyboruToolBar(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "BasketBar";
            this.WindowTitles.Add("Biometria 01");
            #endregion
        }
        
        #region Properties
        public DirectUIControl UIOtwórzButton
        {
            get
            {
                if ((this.mUIOtwórzButton == null))
                {
                    this.mUIOtwórzButton = new DirectUIControl(this);
                    #region Search Criteria
                    this.mUIOtwórzButton.SearchProperties[DirectUIControl.PropertyNames.AutomationId] = "CommitButton";
                    this.mUIOtwórzButton.WindowTitles.Add("Biometria 01");
                    #endregion
                }
                return this.mUIOtwórzButton;
            }
        }
        #endregion
        
        #region Fields
        private DirectUIControl mUIOtwórzButton;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "12.0.21005.1")]
    public class UIEffectList : XamlList
    {
        
        public UIEffectList(XamlControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[XamlList.PropertyNames.AutomationId] = "Effect";
            this.WindowTitles.Add("Biometria 01");
            #endregion
        }
        
        #region Properties
        public XamlListItem UIBiometria_01EffectsGListItem
        {
            get
            {
                if ((this.mUIBiometria_01EffectsGListItem == null))
                {
                    this.mUIBiometria_01EffectsGListItem = new XamlListItem(this);
                    #region Search Criteria
                    this.mUIBiometria_01EffectsGListItem.SearchProperties[XamlListItem.PropertyNames.Name] = "Biometria_01.Effects.Grayscale_v2";
                    this.mUIBiometria_01EffectsGListItem.WindowTitles.Add("Biometria 01");
                    #endregion
                }
                return this.mUIBiometria_01EffectsGListItem;
            }
        }
        
        public XamlListItem UIUITestAppEffectsGrayListItem
        {
            get
            {
                if ((this.mUIUITestAppEffectsGrayListItem == null))
                {
                    this.mUIUITestAppEffectsGrayListItem = new XamlListItem(this);
                    #region Search Criteria
                    this.mUIUITestAppEffectsGrayListItem.SearchProperties[XamlListItem.PropertyNames.Name] = "UITestApp.Effects.Grayscale";
                    this.mUIUITestAppEffectsGrayListItem.WindowTitles.Add("Biometria 01");
                    #endregion
                }
                return this.mUIUITestAppEffectsGrayListItem;
            }
        }
        #endregion
        
        #region Fields
        private XamlListItem mUIBiometria_01EffectsGListItem;
        
        private XamlListItem mUIUITestAppEffectsGrayListItem;
        #endregion
    }
}
