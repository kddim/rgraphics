﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class UITestDemo
    {
        public UITestDemo()
        {
        }

        [TestMethod]
        public void BasicUITest()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
          //  Gesture.Tap(UIMap.UIMenuStartPane.UIAplikacjestartoweList.UIAplikacjeButton);
            Mouse.Click(UIMap.UIMenuStartPane.UIAplikacjestartoweList.UIBiometria01Group.UIBiometria01ListItem);
        //    Mouse.Click(UIMap.UIMenuStartPane.UIAplikacjestartoweList.UIAplikacjeButton);
        //    Mouse.Click(UIMap.UIMenuStartPane.UIAplikacjestartoweList.UIµTorrentGroup.UIBiometria01ListItem);
            Mouse.Click(UIMap.UIBiometria01Window.UILoadImageButton);

            var results = UIMap.UIBiometria01Window.UIWidoksiatkiList.UIWindowsSimulatorGroup.GetChildren();
            Mouse.Click(results[5]);


            Mouse.Click(UIMap.UIBiometria01Window.UIKoszykwyboruToolBar.UIOtwórzButton);
            Mouse.Click(UIMap.UIBiometria01Window.UIEffectList.UIUITestAppEffectsGrayListItem);
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
