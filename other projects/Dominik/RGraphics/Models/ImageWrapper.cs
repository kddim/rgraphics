﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGraphics.Models
{
    public class ImageWrapper
    {
        public ImageWrapper(byte[] byteArray, uint width, uint height)
        {
            ByteArray = byteArray;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Creates copy of specified ImageWrapper
        /// </summary>
        /// <param name="other">ImageWrapper to be copied from</param>
        public ImageWrapper(ImageWrapper other)
        {
            ByteArray = new byte[other.ByteArray.Length];
            for (int i = 0; i < ByteArray.Length; ++i)
                ByteArray[i] = other.ByteArray[i];

            Width = other.Width;
            Height = other.Height;
        }

        /// <summary>
        /// Creates BLACK image (all channels are 0=black) of specified size
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public ImageWrapper(long width, long height)
        {
            Width = (uint)width;
            Height = (uint)height;
            ByteArray = new byte[Height * Width * channels];
        }

        public const int channels = 4;
        public readonly uint Height;
        public readonly uint Width;
        public byte[] ByteArray { get; set; }

        /// <summary>
        /// Returns byte value of specified color channel. Use it ONLY FOR READING (it returns value)
        /// </summary>
        /// <param name="x">horizontal pixel position</param>
        /// <param name="y">vertical pixel position</param>
        /// <param name="channel">0 - Blue, 1 - Green, 2 - Red, 3 - Alpha</param>
        /// <returns></returns>
        public byte this[long x, long y, int channel]
        {
            get
            {
                return ByteArray[y * Width * channels + x * channels + channel];
            }
        }

        public long GetIndex(long x, long y, int channel=0)
        {
            return y * Width * channels + x * channels + channel;
        }

        public void SetRGB(int index, byte R, byte G, byte B)
        {
            ByteArray[index] = B;
            ByteArray[index + 1] = G;
            ByteArray[index + 2] = R;
        }

        public void SetRGB(long x, long y, byte color)
        {
            long index = y * Width * channels + x * channels;
            ByteArray[index] = ByteArray[index + 1] = ByteArray[index + 2] = color;
        }

        public void SetRGB(long x, long y, byte R, byte G, byte B)
        {
            long index = y * Width * channels + x * channels;
            ByteArray[index] = B;
            ByteArray[index + 1] = G;
            ByteArray[index + 2] = R;
        }

        public void SetRGB(long x, long y, long R, long G, long B)
        {
            long index = y * Width * channels + x * channels;
            ByteArray[index] = CutColorRange(B);
            ByteArray[index + 1] = CutColorRange(G);
            ByteArray[index + 2] = CutColorRange(R);
        }

        public void SetRGB(long x, long y, byte R, byte G, byte B, byte A)
        {
            long index = y * Width * channels + x * channels;
            ByteArray[index] = B;
            ByteArray[index + 1] = G;
            ByteArray[index + 2] = R;
            ByteArray[index + 3] = A;
        }

        public static byte CutColorRange(int val)
        {
            return val > 255 ? (byte)255 : val < 0 ? (byte)0 : (byte)val;
        }

        public static byte CutColorRange(long val)
        {
            return val > 255 ? (byte)255 : val < 0 ? (byte)0 : (byte)val;
        }

        public static byte PreventZeroDiv(int val)
        {
            return val != 0 ? (byte)val : (byte)1;
        }

        public static void CopyAlphaChannel(byte[] imagePixel, byte[] copyFrom)
        {
            for (int i = 3; i < imagePixel.Length; i += 4)
                imagePixel[i] = copyFrom[i];
        }

        /// <summary>
        /// Deeply copies image increasing it by duplicating its' border pixels.
        /// Original image stays in the middle of returned image.
        /// </summary>
        /// <param name="region">Number of pixels to increase image by</param>
        /// <returns>Copied image.</returns>
        public unsafe ImageWrapper CopyImageByIncreasingUsingBorders(int region)
        {
            return CopyImageByIncreasingUsingBorders(region, region);
        }

        /// <summary>
        /// Deeply copies image increasing it by duplicating its' border pixels.
        /// Original image stays in the middle of returned image.
        /// </summary>
        /// <param name="regionX">Number of pixels to increase image's width by</param>
        /// <param name="regionY">Number of pixels to increase image's height by</param>
        /// <returns>Copied image.</returns>
        public unsafe ImageWrapper CopyImageByIncreasingUsingBorders(int regionX, int regionY)
        {
            long newHeight = Height + regionY * 2;
            long newWidth = Width + regionX * 2;
            ImageWrapper returnImage = new ImageWrapper(newWidth, newHeight);

            long retIndex, index;
            fixed (byte* returnImg = returnImage.ByteArray)
            {
                fixed (byte* img = this.ByteArray)
                {
                    for (int y = 0; y < returnImage.Height; ++y)
                        for (int x = 0; x < returnImage.Width; ++x)
                        {
                            retIndex = returnImage.GetIndex(x, y, 0);

                            // if coordinates are in the region where the "old" image should be.
                            if (x >= regionX && x < regionX + this.Width && y >= regionY && y < regionY + this.Height)
                            {
                                index = GetIndex(x - regionX, y - regionY, 0);

                                returnImg[retIndex] = img[index];
                                returnImg[retIndex + 1] = img[index + 1];
                                returnImg[retIndex + 2] = img[index + 2];
                                returnImg[retIndex + 3] = img[index + 3];
                                //*((uint*)(returnImg + retIndex)) = *((uint*)(img + index));       // TODO [additional]: see if this approach is faster then the lines above
                            }
                            else  // else use the border pixel
                            {
                                long xUse = 0;
                                long yUse = 0;

                                if (x > regionX)
                                    xUse = this.Width - 1;

                                if (y > regionY)
                                    yUse = this.Height - 1;
                                index = GetIndex(xUse, yUse, 0);

                                returnImg[retIndex] = img[index];
                                returnImg[retIndex + 1] = img[index + 1];
                                returnImg[retIndex + 2] = img[index + 2];
                                returnImg[retIndex + 3] = img[index + 3];
                                //*((uint*)(returnImg + retIndex)) = *((uint*)(img + index));
                            }

                        }
                }
            }
            return returnImage;
        }

        /// <summary>
        /// Deeply copies image increasing it by duplicating specified pixel
        /// Original image stays in the middle of returned image.
        /// </summary>
        /// <param name="region">Number of pixels to increase image by</param>
        /// <param name="R">Red channel of the duplicating pixel</param>
        /// <param name="G">Green channel of the duplicating pixel</param>
        /// <param name="B">Blue channel of the duplicating pixel</param>
        /// <param name="A">Alpha channel of the duplicating pixel</param>
        /// <returns></returns>
        public ImageWrapper CopyImageBySpecifiedPixel(int region, byte R, byte G, byte B, byte A)
        {
            long newHeight = Height + region * 2;
            long newWidth = Width + region * 2;
            ImageWrapper returnImage = new ImageWrapper(newWidth, newHeight);

            for (int y = 0; y < returnImage.Height; ++y)
                for (int x = 0; x < returnImage.Width; ++x)
                {
                    long index = returnImage.GetIndex(x, y, 0);
                    // if coordinates are in the region where the "old" image should be.
                    if (x >= region && x < region + this.Width && y >= region && y < region + this.Height)
                    {
                        returnImage.ByteArray[index] = this[x - region, y - region, 0];
                        returnImage.ByteArray[index + 1] = this[x - region, y - region, 1];
                        returnImage.ByteArray[index + 2] = this[x - region, y - region, 2];
                        returnImage.ByteArray[index + 3] = this[x - region, y - region, 3];
                    }
                    else
                    {
                        returnImage.ByteArray[index] = R;
                        returnImage.ByteArray[index + 1] = G;
                        returnImage.ByteArray[index + 2] = B;
                        returnImage.ByteArray[index + 3] = A; // ALPHA
                    }
                }

            return returnImage;
        }

        public int[] BlueHistogramData()
        {
            int[] result = new int[256];
            byte[] channelArray = ByteArray.Where((item, index) => index % 4 == 0).ToArray();
            //for (int i = 0; i < ByteArray.Length; i += 4)
            //    ++result[ByteArray[i]];
            for (int i = 0; i <= 255; ++i)
                result[i] = channelArray.Count(channel => channel == i);
            return result;
        }

        /// <summary>
        /// Checks if image is in gray scale
        /// </summary>
        /// <returns>True if image is in gray scale, otherwise false</returns>
        public unsafe bool IsInGrayScale()
        {
            // even if you decode as BGRA8 you still get it like this: //that's weird, isn't it?
            const uint redMask = 0xFF0000;
            const uint greenMask = 0xFF00;
            const uint blueMask = 0xFF;

            fixed (byte* ptr = ByteArray)
            {
                byte* pixel = ptr;
                byte* maximumPtr = ptr + (Height * Width + 4);
                byte R, G, B;

                while (pixel < maximumPtr)
                {
                    R = (byte)((*((uint*)pixel) & redMask) >> 16);
                    G = (byte)((*((uint*)pixel) & greenMask) >> 8);
                    B = (byte)(*((uint*)pixel) & blueMask);

                    if (R != B || R != G)
                        return false;
                    pixel += 4;
                }
            }
            return true;
        }
    }
}