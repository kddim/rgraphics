﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RGraphics.Models.ImageEffects
{
    class SkeletonizationK3M : IImageEffect
    {
        public string Name { get { return (String)Application.Current.Resources["SkeletonizationK3MName"]; } }

        public string Description { get { return (String)Application.Current.Resources["SkeletonizationK3MDescription"]; } }

        private static readonly int[][] A = new int[][] {
            new int [] {3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56, 60, 62, 63, 96, 112, 120, 124, 126, 127, 129, 131, 135,143, 159, 191, 192, 193, 195, 199, 207, 223, 224, 225, 227, 231, 239, 240, 241, 243, 247, 248, 249, 251, 252, 253, 254},
            new int [] {7, 14, 28, 56, 112, 131, 193, 224},
            new int [] {7, 14, 15, 28, 30, 56, 60, 112, 120, 131, 135, 193, 195, 224, 225, 240},
            new int [] {7, 14, 15, 28, 30, 31, 56, 60, 62, 112, 120, 124, 131, 135, 143, 193, 195, 199, 224, 225, 227, 240, 241, 248},
            new int [] {7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120, 124, 126, 131, 135, 143, 159, 193, 195, 199, 207, 224, 225, 227, 231, 240, 241, 243, 248, 249, 252},
            new int [] {7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120, 124, 126, 131, 135, 143, 159, 191, 193, 195, 199, 207, 224, 225, 227, 231, 239, 240, 241, 243, 248, 249, 251, 252, 254}
        };

        private static readonly int[] A1pix = { 3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56, 60, 62, 63, 96, 112, 120, 124, 126, 127, 129, 131, 135, 143, 159, 191, 192, 193, 195, 199, 207, 223, 224, 225, 227, 231, 239, 240, 241, 243, 247, 248, 249, 251, 252, 253, 254 };

        private static readonly int[,] Mask = {
            {128, 64, 32},
            {  1,  0, 16},
            {  2,  4,  8}
        };

        const byte BLACK = 0;
        const byte WHITE = 255;

        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image = new GreyScale().ApplyEffect(image);

            // TODO: Check if it will be faster if the Tuple would also contain index? (probably a stupid idea...)
            List<Tuple<int, int>> blackPixels = new List<Tuple<int, int>>(1000000);
            List<Tuple<int, int>> adjacentPixels = new List<Tuple<int, int>>(1000000);
            Tuple<int, int> currPixel;
            int sum, index, bIndex, x, y;

            fixed (byte* img = image.ByteArray)
            {
                byte* ptr = img;
                #region GETTING BLACK PIXELS LIST
                for (y = 0; y < (int)image.Height; ++y)
                    for (x = 0; x < (int)image.Width; ++x)
                    {
                        index = (int)image.GetIndex(x, y, 0);
                        if (img[index] == BLACK)
                            blackPixels.Add(new Tuple<int,int>(x,y));
                    }
                #endregion

                #region WHILE LOOP - STEPS 0-5
                bool sthChanged = true;
                while (sthChanged)
                {
                    sthChanged = false;

                    #region MARKING PIXELS AS ADJACENT
                    for (bIndex = 0; bIndex < blackPixels.Count; ++bIndex)
                    {
                        currPixel = blackPixels[bIndex];
                        x = currPixel.Item1;
                        y = currPixel.Item2;
                        sum = 0;
                        // counting the sum, by formula: sum = pixel[surroundingX, surroundingY] * mask[j+1, i+1], where i, j are the offset values
                        for (int i = -1; i < 2; ++i)
                            for (int j = -1; j < 2; ++j)
                            {
                                if (x + j < 0 || x + j >= image.Width || y + i < 0 || y + i >= image.Height || (i == 0 && j == 0))  // if it goes out of image, or is the pixel we are inspecting
                                    continue;
                                if (img[(int)image.GetIndex(x + j, y + i, 0)] != WHITE)
                                    sum += Mask[j + 1, i + 1];
                            }
                        if (A[0].Contains(sum))
                        {
                            blackPixels.RemoveAt(bIndex);
                            --bIndex;
                            adjacentPixels.Add(currPixel);
                        }
                    }
                    #endregion

                    #region DELETING ADJACENT PIXELS WHICH SUM (SURROUNDING * MASK) A1
                    for (bIndex = 0; bIndex < adjacentPixels.Count; ++bIndex)
                    {
                        currPixel = adjacentPixels[bIndex];
                        x = currPixel.Item1;
                        y = currPixel.Item2;
                        sum = 0;
                        // counting the sum, by formula: sum = pixel[surroundingX, surroundingY] * mask[j+1, i+1], where i, j are the offset values
                        for (int i = -1; i < 2; ++i)
                            for (int j = -1; j < 2; ++j)
                            {
                                if (x + j < 0 || x + j >= image.Width || y + i < 0 || y + i >= image.Height || (i == 0 && j == 0))  // if it goes out of image, or is the pixel we are inspecting
                                    continue;
                                if (img[(int)image.GetIndex(x + j, y + i, 0)] != WHITE)
                                    sum += Mask[i + 1, j + 1];
                            }
                        if (A[1].Contains(sum))
                        {
                            sthChanged = true;
                            adjacentPixels.RemoveAt(bIndex);
                            --bIndex;
                            index = (int)image.GetIndex(x,y,0);
                            *((uint*)(ptr + index)) = 0xFFFFFF;   // making the pixel WHITE
                        }
                    }
                    #endregion

                    #region DELETING ADJACENT PIXELS WHICH SUM (SURROUNDING * MASK) A2
                    for (bIndex = 0; bIndex < adjacentPixels.Count; ++bIndex)
                    {
                        currPixel = adjacentPixels[bIndex];
                        x = currPixel.Item1;
                        y = currPixel.Item2;
                        sum = 0;
                        // counting the sum, by formula: sum = pixel[surroundingX, surroundingY] * mask[j+1, i+1], where i, j are the offset values
                        for (int i = -1; i < 2; ++i)
                            for (int j = -1; j < 2; ++j)
                            {
                                if (x + j < 0 || x + j >= image.Width || y + i < 0 || y + i >= image.Height || (i == 0 && j == 0))  // if it goes out of image, or is the pixel we are inspecting
                                    continue;
                                if (img[(int)image.GetIndex(x + j, y + i, 0)] != WHITE)
                                    sum += Mask[i + 1, j + 1];
                            }
                        if (A[2].Contains(sum))
                        {
                            sthChanged = true;
                            adjacentPixels.RemoveAt(bIndex);
                            --bIndex;
                            index = (int)image.GetIndex(x, y, 0);
                            *((uint*)(ptr + index)) = 0xFFFFFF;   // making the pixel WHITE
                        }
                    }
                    #endregion

                    #region DELETING ADJACENT PIXELS WHICH SUM (SURROUNDING * MASK) A3
                    for (bIndex = 0; bIndex < adjacentPixels.Count; ++bIndex)
                    {
                        currPixel = adjacentPixels[bIndex];
                        x = currPixel.Item1;
                        y = currPixel.Item2;
                        sum = 0;
                        // counting the sum, by formula: sum = pixel[surroundingX, surroundingY] * mask[j+1, i+1], where i, j are the offset values
                        for (int i = -1; i < 2; ++i)
                            for (int j = -1; j < 2; ++j)
                            {
                                if (x + j < 0 || x + j >= image.Width || y + i < 0 || y + i >= image.Height || (i == 0 && j == 0))  // if it goes out of image, or is the pixel we are inspecting
                                    continue;
                                if (img[(int)image.GetIndex(x + j, y + i, 0)] != WHITE)
                                    sum += Mask[i + 1, j + 1];
                            }
                        if (A[3].Contains(sum))
                        {
                            sthChanged = true;
                            adjacentPixels.RemoveAt(bIndex);
                            --bIndex;
                            index = (int)image.GetIndex(x, y, 0);
                            *((uint*)(ptr + index)) = 0xFFFFFF;   // making the pixel WHITE
                        }
                    }
                    #endregion

                    #region DELETING ADJACENT PIXELS WHICH SUM (SURROUNDING * MASK) A4
                    for (bIndex = 0; bIndex < adjacentPixels.Count; ++bIndex)
                    {
                        currPixel = adjacentPixels[bIndex];
                        x = currPixel.Item1;
                        y = currPixel.Item2;
                        sum = 0;
                        // counting the sum, by formula: sum = pixel[surroundingX, surroundingY] * mask[j+1, i+1], where i, j are the offset values
                        for (int i = -1; i < 2; ++i)
                            for (int j = -1; j < 2; ++j)
                            {
                                if (x + j < 0 || x + j >= image.Width || y + i < 0 || y + i >= image.Height || (i == 0 && j == 0))  // if it goes out of image, or is the pixel we are inspecting
                                    continue;
                                if (img[(int)image.GetIndex(x + j, y + i, 0)] != WHITE)
                                    sum += Mask[i + 1, j + 1];
                            }
                        if (A[4].Contains(sum))
                        {
                            sthChanged = true;
                            adjacentPixels.RemoveAt(bIndex);
                            --bIndex;
                            index = (int)image.GetIndex(x, y, 0);
                            *((uint*)(ptr + index)) = 0xFFFFFF;   // making the pixel WHITE
                        }
                    }
                    #endregion

                    #region DELETING ADJACENT PIXELS WHICH SUM (SURROUNDING * MASK) A5
                    for (bIndex = 0; bIndex < adjacentPixels.Count; ++bIndex)
                    {
                        currPixel = adjacentPixels[bIndex];
                        x = currPixel.Item1;
                        y = currPixel.Item2;
                        sum = 0;
                        // counting the sum, by formula: sum = pixel[surroundingX, surroundingY] * mask[j+1, i+1], where i, j are the offset values
                        for (int i = -1; i < 2; ++i)
                            for (int j = -1; j < 2; ++j)
                            {
                                if (x + j < 0 || x + j >= image.Width || y + i < 0 || y + i >= image.Height || (i == 0 && j == 0))  // if it goes out of image, or is the pixel we are inspecting
                                    continue;
                                if (img[(int)image.GetIndex(x + j, y + i, 0)] != WHITE)
                                    sum += Mask[i + 1, j + 1];
                            }
                        if (A[5].Contains(sum))
                        {
                            sthChanged = true;
                            adjacentPixels.RemoveAt(bIndex);
                            --bIndex;
                            index = (int)image.GetIndex(x, y, 0);
                            *((uint*)(ptr + index)) = 0xFFFFFF;   // making the pixel WHITE
                        }
                    }
                    #endregion

                    blackPixels.AddRange(adjacentPixels);
                    adjacentPixels.Clear();
                }
                #endregion

                #region LAST STEP - A1pix MASK
                for (bIndex = 0; bIndex < blackPixels.Count; ++bIndex)
                {
                    currPixel = blackPixels[bIndex];
                    x = currPixel.Item1;
                    y = currPixel.Item2;
                    sum = 0;
                    // counting the sum, by formula: sum = pixel[surroundingX, surroundingY] * mask[j+1, i+1], where i, j are the offset values
                    for (int i = -1; i < 2; ++i)
                        for (int j = -1; j < 2; ++j)
                        {
                            if (x + j < 0 || x + j >= image.Width || y + i < 0 || y + i >= image.Height || (i == 0 && j == 0))  // if it goes out of image, or is the pixel we are inspecting
                                continue;
                            if (img[(int)image.GetIndex(x + j, y + i, 0)] != WHITE)
                                sum += Mask[j + 1, i + 1];
                        }
                    if (A1pix.Contains(sum))
                    {
                        blackPixels.RemoveAt(bIndex);
                        --bIndex;
                        index = (int)image.GetIndex(x, y, 0);
                        *((uint*)(ptr + index)) = 0xFFFFFF;   // making the pixel WHITE
                    }
                }
                #endregion
            }

            return image;
        }
    }
}
