﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RGraphics.Models.ImageEffects
{
    class SobelFilter : IImageEffect
    {
        public string Name { get { return (String)Application.Current.Resources["SobelFilterName"]; } }
        public string Description { get { return (String)Application.Current.Resources["SobelFilterDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            const uint channels = 4;
            byte[] result = new byte[image.ByteArray.Length];

            ImageWrapper.CopyAlphaChannel(result, image.ByteArray);

            //  p0 | p1 | p2
            // --------------
            //  p7 | px | p3
            // --------------
            //  p6 | p5 | p4

            // creating most of the image
            for (uint y = 1; y < image.Height - 1; ++y)
            {
                for (uint x = 1; x < image.Width - 1; ++x)
                {
                    for (uint i = 0; i < 3; ++i)
                    {
                        // px = (x, y)
                        uint px = y * image.Width * channels + x * channels + i;

                        uint moveBottom = image.Width * channels;
                        // p0 = (x-1, y-1)
                        uint p0 = px - channels - moveBottom;
                        // p1 = (x, y-1)
                        uint p1 = px - moveBottom;
                        // p2 = (x+1, y-1)
                        uint p2 = px + channels - moveBottom;
                        // p3 = (x+1, y)
                        uint p3 = px + channels;
                        // p4 = (x+1, y+1)
                        uint p4 = px + channels + moveBottom;
                        // p5 = (x, y+1)
                        uint p5 = px + moveBottom;
                        // p6 = (x-1, y+1)
                        uint p6 = px - channels + moveBottom;
                        // p7 = (x-1, y)
                        uint p7 = px - channels;

                        // tmp1 = (p2 + 2*p3 + p4) - (p0 + 2*p7 + p6)
                        int tmp1 = (
                            (
                                image.ByteArray[p2] + (2 * image.ByteArray[p3]) + image.ByteArray[p4]
                            )
                            -
                            (
                                image.ByteArray[p0] + (2 * image.ByteArray[p7]) + image.ByteArray[p6]
                            ));
                        // tmp2 = (p6 + 2*p5 + p4) - (p0 + 2*p1 + p2)
                        int tmp2 = (
                            (
                                image.ByteArray[p6] + (2 * image.ByteArray[p5]) + image.ByteArray[p4]
                            )
                            -
                            (
                                image.ByteArray[p0] + (2 * image.ByteArray[p1]) + image.ByteArray[p2]
                            ));

                        // (x,y) = sqrt(tmp1^2 + tmp2^2)
                        result[px] = ImageWrapper.CutColorRange((uint)(Math.Pow((Math.Pow(tmp1, 2) + Math.Pow(tmp2, 2)), 0.5)));
                    }
                }
            }
            image.ByteArray = result;
            return image;
        }
    }
}
