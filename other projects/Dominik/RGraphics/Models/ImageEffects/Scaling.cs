﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml;

namespace RGraphics.Models.ImageEffects
{
    public enum ScalingVersions { ScaleClosestNeighbour, ScaleByAveraging, BilinearInterpolation };
    public class Scaling : PropertyChangedBase, IImageEffect
    {
        public Scaling()
        {
            SelectedScale = 0;
        }

        private int _SelectedScale;
        public int SelectedScale
        {
            get { return _SelectedScale; }
            set
            {
                _SelectedScale = value;
                NotifyOfPropertyChange(() => SelectedScale);
            }
        }

        public string Name { get { return (String)Application.Current.Resources["ScalingName"]; } }

        public string Description { get { return (String)Application.Current.Resources["ScalingDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            if (SelectedScale == (int)ScalingVersions.BilinearInterpolation)
            {
                if (Scale <= 1)
                {
                    ErrorMsg msg = new ErrorMsg("UpperScalingScaleErrorMsg", "ErrorTitle");
                    IoC.Get<IEventAggregator>().PublishOnUIThread(msg);
                    return image;
                }
                return BilinearInterpolation(image);
            }
            else if (SelectedScale == (int)ScalingVersions.ScaleByAveraging)
            {
                if (Scale >= 1)
                {
                    ErrorMsg msg = new ErrorMsg("DownScalingScaleErrorMsg", "ErrorTitle");
                    IoC.Get<IEventAggregator>().PublishOnUIThread(msg);
                    return image;
                }
                return ScaleByAveraging(image);
            }
            else
                return ScaleClosestNeighbour(image);
        }

        const int region = 1;

        private ImageWrapper BilinearInterpolation(ImageWrapper image)
        {
            ImageWrapper returnImage = new ImageWrapper((int)(image.Width * Scale), (int)(image.Height * Scale));
            bool[,] pixels;

            #region Scale <= 2
            if (Scale <= 2)  // Niewypełnione piksele należy wypełnić za pomocą uśredniania sąsiadów wypełnionych w pierwszym kroku. 
            {
                pixels = new bool[returnImage.Height, returnImage.Width];

                for (int y = 0; y < image.Height; ++y)
                    for (int x = 0; x < image.Width; ++x)
                    {
                        int xCoord = (int)(x * Scale);
                        int yCoord = (int)(y * Scale);
                        int index = (int)image.GetIndex(x, y, 0);

                        byte B = image.ByteArray[index];
                        byte G = image.ByteArray[index + 1];
                        byte R = image.ByteArray[index + 2];
                        byte A = image.ByteArray[index + 3];
                        pixels[yCoord, xCoord] = true;
                        returnImage.SetRGB(xCoord, yCoord, R, G, B, A);
                    }

                for (int y = 0; y < returnImage.Height; ++y)
                    for (int x = 0; x < returnImage.Width; ++x)
                    {
                        if (pixels[y, x] == false)
                        {
                            int divisionNumber = 0;
                            int R = 0;
                            int G = 0;
                            int B = 0;
                            //int Asum = 0;
                            for (int i = -region / 2; i <= region; ++i)
                                for (int j = -region; j <= region; ++j)
                                {
                                    if (x + j < 0 || x + j >= returnImage.Width || y + i < 0 || y + i >= returnImage.Height || (i == 0 && j == 0) || pixels[y + i, x + j] == false)
                                        continue;
                                    ++divisionNumber;
                                    int index = (int)returnImage.GetIndex(x + j, y + i, 0);
                                    B += returnImage.ByteArray[index];
                                    G += returnImage.ByteArray[index + 1];
                                    R += returnImage.ByteArray[index + 2];
                                    //Asum += returnImage.ByteArray[index + 3];
                                }
                            R /= divisionNumber;
                            G /= divisionNumber;
                            B /= divisionNumber;

                            returnImage.SetRGB(x, y, (byte)R, (byte)G, (byte)B, 0);
                        }
                    }
            }
            #endregion
            else
            {
                pixels = new bool[returnImage.Height, returnImage.Width];

                for (int y = 0; y < image.Height; ++y)
                    for (int x = 0; x < image.Width; ++x)
                    {
                        int xCoord = (int)(x * Scale);
                        int yCoord = (int)(y * Scale);
                        int index = (int)image.GetIndex(x, y, 0);

                        byte B = image.ByteArray[index];
                        byte G = image.ByteArray[index + 1];
                        byte R = image.ByteArray[index + 2];
                        byte A = image.ByteArray[index + 3];
                        pixels[yCoord, xCoord] = true;
                        returnImage.SetRGB(xCoord, yCoord, R, G, B, A);
                    }

                #region PART 2
                //*
                for (int y = 0; y < returnImage.Height; ++y)
                    for (int x = 0; x < returnImage.Width; ++x)
                    {
                        int x1 = -1, x2 = -1, y1 = -1, y2 = -1;
                        if (pixels[y, x] == false)
                        {

                            x1 = (int)(Scale * Math.Floor(x / Scale));

                            x2 = (int)(Scale * Math.Ceiling(x / Scale));
                            x2 = (int)Math.Min(x2, returnImage.Width - 1);

                            y1 = (int)(Scale * Math.Floor(y / Scale));

                            y2 = (int)(Scale * Math.Ceiling(y / Scale));
                            y2 = (int)Math.Min(y2, returnImage.Height - 1);

                            // tmpDiv = x2-x1
                            // tmp1 = (x2-xN) / tmpDiv
                            // tmp2 = (xN-x1) / tmpDiv
                            // tmp3 = y2 - y1
                            // R1 = tmp1 * k1 + tmp2 * k2
                            // R2 = tmp1 * k3 + tmp2 * k4

                            // kTmp1 = y2-yN;
                            // kTmp2 = yN-y1;
                            // k? = kTmp1 / tmp3 * R1 + kTmp2 / tmp3 * R2

                            double tmpDiv = x2 - x1;
                            double tmp3 = y2 - y1;

                            double kTmp1 = y2 - y;
                            double kTmp2 = y - y1;

                            double R1_Blue, R1_Green, R1_Red, R2_Blue, R2_Green, R2_Red;
                            if (tmpDiv == 0 || tmp3 == 0)
                            {
                                R1_Blue = returnImage[x1, y1, 0];
                                R1_Green = returnImage[x1, y1, 1];
                                R1_Red = returnImage[x1, y1, 2];

                                R2_Blue = returnImage[x1, y2, 0];
                                R2_Green = returnImage[x1, y2, 1];
                                R2_Red = returnImage[x1, y2, 2];
                            }
                            else
                            {
                                double tmp1 = (x2 - x) / tmpDiv;
                                double tmp2 = (x - x1) / tmpDiv;
                                R1_Blue = tmp1 * returnImage[x1, y1, 0] + tmp2 * returnImage[x2, y1, 0];
                                R1_Green = tmp1 * returnImage[x1, y1, 1] + tmp2 * returnImage[x2, y1, 1];
                                R1_Red = tmp1 * returnImage[x1, y1, 2] + tmp2 * returnImage[x2, y1, 2];

                                R2_Blue = tmp1 * returnImage[x1, y2, 0] + tmp2 * returnImage[x2, y2, 0];
                                R2_Green = tmp1 * returnImage[x1, y2, 1] + tmp2 * returnImage[x2, y2, 1];
                                R2_Red = tmp1 * returnImage[x1, y2, 2] + tmp2 * returnImage[x2, y2, 2];
                            }
                            if (tmp3 == 0)
                                tmp3 = 1;

                            if (kTmp1 == 0 && kTmp1 == kTmp2)
                            {
                                kTmp1 = 1;
                                kTmp2 = 1;
                                tmp3 = 2;
                            }

                            byte R = ImageWrapper.CutColorRange((long)((kTmp1 / tmp3) * R1_Red + (kTmp2 / tmp3) * R2_Red));
                            byte G = ImageWrapper.CutColorRange((long)((kTmp1 / tmp3) * R1_Green + (kTmp2 / tmp3) * R2_Green));
                            byte B = ImageWrapper.CutColorRange((long)((kTmp1 / tmp3) * R1_Blue + (kTmp2 / tmp3) * R2_Blue));
                            byte A = 0;

                            returnImage.SetRGB(x, y, (byte)R, (byte)G, (byte)B, A);
                        }
                    }
                // */
                #endregion
            }

            return returnImage;
        }
        private ImageWrapper ScaleClosestNeighbour(ImageWrapper image)
        {
            ImageWrapper returnImage = new ImageWrapper((int)(image.Width * Scale), (int)(image.Height * Scale));
            for (int y = 0; y < returnImage.Height; ++y)
                for (int x = 0; x < returnImage.Width; ++x)
                {
                    int xCoord = (int)(x / Scale);
                    int yCoord = (int)(y / Scale);
                    byte B = image[xCoord, yCoord, 0];
                    byte G = image[xCoord, yCoord, 1];
                    byte R = image[xCoord, yCoord, 2];
                    byte A = image[xCoord, yCoord, 3];

                    returnImage.SetRGB(x, y, R, G, B, A);
                }
            return returnImage;
        }

        private ImageWrapper ScaleByAveraging(ImageWrapper image)
        {
            ImageWrapper returnImage = new ImageWrapper((int)(image.Width * Scale), (int)(image.Height * Scale));
            for (int y = 0; y < returnImage.Height; ++y)
                for (int x = 0; x < returnImage.Width; ++x)
                {
                    int xi = (int)((double)x / Scale);
                    int yi = (int)((double)y / Scale);

                    int scaleRegion = (int)(Math.Ceiling((1 / Scale) / 2));
                    int divisionNumber = 0;
                    int R = 0;
                    int G = 0;
                    int B = 0;
                    //int Asum = 0;
                    for (int i = -scaleRegion / 2; i <= scaleRegion; ++i)
                        for (int j = -scaleRegion; j <= scaleRegion; ++j)
                        {
                            if (xi + j < 0 || xi + j >= image.Width || yi + i < 0 || yi + i >= image.Height || (i == 0 && j == 0))
                                continue;
                            ++divisionNumber;
                            int index = (int)image.GetIndex(xi + j, yi + i, 0);
                            B += image.ByteArray[index];
                            G += image.ByteArray[index + 1];
                            R += image.ByteArray[index + 2];
                        }
                    R /= divisionNumber;
                    G /= divisionNumber;
                    B /= divisionNumber;

                    returnImage.SetRGB(x, y, (byte)R, (byte)G, (byte)B, 0);
                }
            return returnImage;
        }

        private double _Scale;
        public double Scale
        {
            get { return _Scale; }
            set
            {
                _Scale = value;
                NotifyOfPropertyChange(() => Scale);
            }
        }

    }
}
