﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RGraphics.Models.ImageEffects
{
    class Sepia : PropertyChangedBase, IImageEffect 
    {
        public string Name { get { return (String)Application.Current.Resources["SepiaName"]; } }
        public string Description { get { return (String)Application.Current.Resources["SepiaDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            int index = 0;
            for (int y = 0; y < image.Height; ++y)
            {
                for (int x = 0; x < image.Width; ++x)
                {
                    int G = image.ByteArray[index + 1] + (int)SepiaParameter;
                    int R = image.ByteArray[index + 2] + 2 * (int)SepiaParameter;
                    image.ByteArray[index + 2] = ImageWrapper.CutColorRange(R);
                    image.ByteArray[index + 1] = ImageWrapper.CutColorRange(G);
                    index += 4;
                }
            }
            return image;
        }

        private double _SepiaParameter;
        public double SepiaParameter
        {
            get { return _SepiaParameter; }
            set
            {
                _SepiaParameter = value;
                
                NotifyOfPropertyChange(() => SepiaParameter);
            }
        }
        
    }
}
