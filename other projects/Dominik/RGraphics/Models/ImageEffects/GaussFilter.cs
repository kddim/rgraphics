﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RGraphics.Models.ImageEffects
{
    public class GaussFilter : PropertyChangedBase, IImageEffect
    {
        public GaussFilter()
        {
            MaskWidth = 5;
            MaskHeight = 5;
            MaskGrain = 5.5;
        }

        public string Name { get { return (String)Application.Current.Resources["GaussFilterName"]; } }

        public string Description { get { return (String)Application.Current.Resources["GaussFilterDescription"]; } }

        private int _MaskWidth;
        public int MaskWidth
        {
            get { return _MaskWidth; }
            set
            {
                _MaskWidth = value;
                NotifyOfPropertyChange(() => MaskWidth);
            }
        }
        
        
        private int _MaskHeight;
        public int MaskHeight
        {
            get { return _MaskHeight; }
            set
            {
                _MaskHeight = value;
                NotifyOfPropertyChange(() => MaskHeight);
            }
        }


        private double _MaskGrain;
        public double MaskGrain
        {
            get { return _MaskGrain; }
            set
            {
                _MaskGrain  = value;
                NotifyOfPropertyChange(() => MaskGrain);
            }
        }
        
        public unsafe ImageWrapper ApplyEffect(ImageWrapper image)
        {
            // formula : f(offsetX, offsetY) = A * B
            // A = 1 / ( 2 * Math.PI * Grain^2)
            // B = 1 / Math.E ^ ( ( - x^2 + y^2 ) / (2 * Grain^2) )

            double GrainSquare = MaskGrain * MaskGrain;

            double A = 1.0 / (2.0 * Math.PI * GrainSquare);

            // MaskWidth and MaskHeight must be odd; fixing it if they're not
            if (MaskWidth % 2 == 0)
                ++MaskWidth;
            if (MaskHeight % 2 == 0)
                ++MaskHeight;

            int halfMaskW = MaskWidth / 2;
            int halfMaskH = MaskHeight / 2;

            double[,] mask = new double[MaskHeight, MaskWidth];
            double maskValue = 0.0;
            for (int i = -halfMaskH; i <= halfMaskH; ++i)
                for (int j = -halfMaskW; j <= halfMaskW; ++j)
                    maskValue += mask[i + halfMaskH, j + halfMaskW] = A * 1.0 / Math.Pow(Math.E, (-i * i + j * j) / (2.0 * GrainSquare));

            ImageWrapper imageHelper = image.CopyImageByIncreasingUsingBorders(halfMaskW, halfMaskH);

            double R, G, B, val;
            int index;

            fixed (byte* ptr = imageHelper.ByteArray)
            {
                for (int y = halfMaskH; y < imageHelper.Height - halfMaskH; ++y)
                    for (int x = halfMaskW; x < imageHelper.Width - halfMaskW; ++x)
                    {
                        B = G = R = 0.0;

                        for (int i = -halfMaskH; i <= halfMaskH; ++i)
                            for (int j = -halfMaskW; j <= halfMaskW; ++j)
                            {
                                index = (int)imageHelper.GetIndex(x + j, y + i);
                                val = mask[i + halfMaskH, j + halfMaskW];
                                B += ptr[index] * val;
                                G += ptr[index + 1] * val;
                                R += ptr[index + 2] * val;
                            }
                        B /= maskValue;
                        G /= maskValue;
                        R /= maskValue;

                        B = Math.Min(Math.Max(B, 0), 255);
                        G = Math.Min(Math.Max(G, 0), 255);
                        R = Math.Min(Math.Max(R, 0), 255);

                        image.SetRGB(x - halfMaskW, y - halfMaskH, (byte)R, (byte)G, (byte)B);
                    }

                return image;
            }
        }
    }
}

