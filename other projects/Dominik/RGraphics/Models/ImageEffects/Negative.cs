﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RGraphics.Models.ImageEffects
{
    class Negative : IImageEffect
    {
        public String Name { get { return (String)Application.Current.Resources["NegativeName"]; } }
        public string Description { get { return (String)Application.Current.Resources["NegativeDescription"]; } }

        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            image.ByteArray = image.ByteArray.Select((channel, index) => index % 4 == 3 ? channel : (byte)(255 - channel)).ToArray();
            return image;
        }
    }
}
