﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RGraphics.Models.ImageEffects
{
    public class KuwaharaFilter : PropertyChangedBase, IImageEffect
    {
        public KuwaharaFilter()
        {
            FilterWidth = 7;
        }
        public string Name { get { return (String)Application.Current.Resources["KuwaharaFilterName"]; } }

        public string Description { get { return (String)Application.Current.Resources["KuwaharaFilterDescription"]; } }

        private int _FilterWidth;
        public int FilterWidth
        {
            get { return _FilterWidth; }
            set
            {
                _FilterWidth = value;
                NotifyOfPropertyChange(() => FilterWidth);
            }
        }
        
        public ImageWrapper ApplyEffect(ImageWrapper image)
        {
            if (image.IsInGrayScale())
                return KuwaharaFilter4GrayImage(image);
            return KuwaharaFilter4ColorImage(image);
        }

        public unsafe ImageWrapper KuwaharaFilter4GrayImage(ImageWrapper image)
        {
            int region = FilterWidth / 2 + 1;
            ImageWrapper imageHelper = image.CopyImageByIncreasingUsingBorders(region - 1);
            imageHelper = new GreyScale().ApplyEffect(imageHelper);

            double topLeftAvgGray, topRightAvgGray, bottomLeftAvgGray, bottomRightAvgGray;
            double topLeftVariance, topRightVariance, bottomLeftVariance, bottomRightVariance;
            double max;
            int regionFrom = region - 1;
            int regionXTo = (int)imageHelper.Width - region;
            int regionYTo = (int)imageHelper.Height - region;

            int squareRegion = region * region;

            fixed (byte* ptr = imageHelper.ByteArray)
            {

                for (int y = regionFrom; y < regionYTo; ++y)
                    for (int x = regionFrom; x < regionXTo; ++x)
                    {
                        topLeftAvgGray = topRightAvgGray = bottomLeftAvgGray = bottomRightAvgGray = 0.0;
                        topLeftVariance = topRightVariance = bottomLeftVariance = bottomRightVariance = 0.0;
                        //double Asum = 0.0;
                        #region TOP LEFT REGION
                        // Average counting
                        for (int yRegion = -region + 1; yRegion <= 0; ++yRegion)
                            for (int xRegion = -region + 1; xRegion <= 0; ++xRegion)
                                topLeftAvgGray += ptr[imageHelper.GetIndex(x + xRegion, y + yRegion, 0)];
                        topLeftAvgGray /= squareRegion;
                        
                        // Variance counting
                        for (int yRegion = -region + 1; yRegion <= 0; ++yRegion)
                            for (int xRegion = -region + 1; xRegion <= 0; ++xRegion)
                                topLeftVariance += Math.Pow(topLeftAvgGray - ptr[imageHelper.GetIndex(x + xRegion, y + yRegion, 0)], 2);
                        topLeftVariance /= squareRegion;
                        #endregion

                        #region TOP RIGHT REGION
                        // Average counting
                        for (int yRegion = -region + 1; yRegion <= 0; ++yRegion)
                            for (int xRegion = 0; xRegion < region; ++xRegion)
                                topRightAvgGray += ptr[imageHelper.GetIndex(x + xRegion, y + yRegion, 0)];
                        topRightAvgGray /= squareRegion;

                        // Variance counting
                        for (int yRegion = -region + 1; yRegion <= 0; ++yRegion)
                            for (int xRegion = 0; xRegion < region; ++xRegion)
                                topRightVariance += Math.Pow(topRightAvgGray - ptr[imageHelper.GetIndex(x + xRegion, y + yRegion, 0)], 2);
                        topRightVariance /= squareRegion;
                        #endregion

                        #region BOTTOM LEFT REGION
                        // Average counting
                        for (int yRegion = 0; yRegion < region; ++yRegion)
                            for (int xRegion = -region + 1; xRegion <= 0; ++xRegion)
                                bottomLeftAvgGray += ptr[imageHelper.GetIndex(x + xRegion, y + yRegion, 0)];
                        bottomLeftAvgGray /= squareRegion;
                        
                        // Variance counting
                        for (int yRegion = 0; yRegion < region; ++yRegion)
                            for (int xRegion = -region + 1; xRegion <= 0; ++xRegion)
                                bottomLeftVariance += Math.Pow(bottomLeftAvgGray - ptr[imageHelper.GetIndex(x + xRegion, y + yRegion, 0)], 2);
                        bottomLeftVariance /= squareRegion;
                        #endregion

                        #region BOTTOM RIGHT REGION
                        // Average counting
                        for (int yRegion = 0; yRegion < region; ++yRegion)
                            for (int xRegion = 0; xRegion < region; ++xRegion)
                                bottomRightAvgGray += ptr[imageHelper.GetIndex(x + xRegion, y + yRegion, 0)];
                        bottomRightAvgGray /= squareRegion;

                        // Variance counting
                        for (int yRegion = 0; yRegion < region; ++yRegion)
                            for (int xRegion = 0; xRegion < region; ++xRegion)
                                bottomRightVariance += Math.Pow(bottomRightAvgGray - ptr[imageHelper.GetIndex(x + xRegion, y + yRegion, 0)], 2);
                        bottomRightVariance /= squareRegion;
                        #endregion

                        max = Math.Min(Math.Min(bottomRightVariance, bottomLeftVariance), Math.Min(topRightVariance, topLeftVariance));

                        byte R, G, B;
                        if (max == topLeftVariance)
                        {
                            R = (byte)topLeftAvgGray;
                            G = (byte)topLeftAvgGray;
                            B = (byte)topLeftAvgGray;
                        }
                        else if (max == topRightVariance)
                        {
                            R = (byte)topRightAvgGray;
                            G = (byte)topRightAvgGray;
                            B = (byte)topRightAvgGray;
                        }
                        else if (max == bottomLeftVariance)
                        {
                            R = (byte)bottomLeftAvgGray;
                            G = (byte)bottomLeftAvgGray;
                            B = (byte)bottomLeftAvgGray;
                        }
                        else
                        {
                            R = (byte)bottomRightAvgGray;
                            G = (byte)bottomRightAvgGray;
                            B = (byte)bottomRightAvgGray;
                        }

                        image.SetRGB(x - regionFrom, y - regionFrom, R, G, B);
                    }
            }
           
            return image;
        }

        public unsafe ImageWrapper KuwaharaFilter4ColorImage(ImageWrapper image)
        {
            int region = FilterWidth / 2 + 1;
            ImageWrapper imageHelper = image.CopyImageByIncreasingUsingBorders(region - 1);

            double topLeftAvgB, topLeftAvgG, topLeftAvgR, topLeftLightness;
            double topRightAvgB, topRightAvgG, topRightAvgR, topRightLightness;
            double bottomLeftAvgB, bottomLeftAvgG, bottomLeftAvgR, bottomLeftLightness;
            double bottomRightAvgB, bottomRightAvgG, bottomRightAvgR, bottomRightLightness;
            int index;
            int regionFrom = region - 1;
            int regionXTo = (int)imageHelper.Width - region;
            int regionYTo = (int)imageHelper.Height - region;

            int squareRegion = region * region;

            fixed(byte* ptr = imageHelper.ByteArray)
            { 

            for (int y = regionFrom; y < regionYTo; ++y)
                for (int x = regionFrom; x < regionXTo; ++x)
                {
                    topLeftAvgB = topLeftAvgG = topLeftAvgR = topLeftLightness =
                    topRightAvgB = topRightAvgG = topRightAvgR = topRightLightness = 
                    bottomLeftAvgB = bottomLeftAvgG = bottomLeftAvgR = bottomLeftLightness = 
                    bottomRightAvgB = bottomRightAvgG = bottomRightAvgR = bottomRightLightness = 0.0;

                    //double Asum = 0.0;
                    #region TOP LEFT REGION
                    // Average counting
                    for (int yRegion = -region + 1; yRegion <= 0; ++yRegion)
                        for (int xRegion = -region + 1; xRegion <= 0; ++xRegion)
                        {
                            index = (int)imageHelper.GetIndex(x + xRegion, y + yRegion, 0);
                            topLeftLightness += Math.Max(Math.Max(ptr[index], ptr[index + 1]), ptr[index + 2]);
                            topLeftAvgB += ptr[index];
                            topLeftAvgG += ptr[index + 1];
                            topLeftAvgR += ptr[index + 2];
                            //Asum += imageHelper.ByteArray[index + 3];
                        }
                    topLeftAvgB /= squareRegion;
                    topLeftAvgG /= squareRegion;
                    topLeftAvgR /= squareRegion;
                    topLeftLightness /= squareRegion;
                    #endregion

                    #region TOP RIGHT REGION

                    for (int yRegion = -region + 1; yRegion <= 0; ++yRegion)
                        for (int xRegion = 0; xRegion < region; ++xRegion)
                        {
                            index = (int)imageHelper.GetIndex(x + xRegion, y + yRegion, 0);
                            topRightLightness += Math.Max(Math.Max(ptr[index], ptr[index + 1]), ptr[index + 2]);
                            topRightAvgB += ptr[index];
                            topRightAvgG += ptr[index + 1];
                            topRightAvgR += ptr[index + 2];
                            //Asum += imageHelper.ByteArray[index + 3];
                        }
                    topRightAvgB /= squareRegion;
                    topRightAvgG /= squareRegion;
                    topRightAvgR /= squareRegion;
                    topRightLightness /= squareRegion;
                    #endregion

                    #region BOTTOM LEFT REGION
                    for (int yRegion = 0; yRegion < region; ++yRegion)
                        for (int xRegion = -region + 1; xRegion <= 0; ++xRegion)
                        {
                            index = (int)imageHelper.GetIndex(x + xRegion, y + yRegion, 0);
                            bottomLeftLightness += Math.Max(Math.Max(ptr[index], ptr[index + 1]), ptr[index + 2]);
                            bottomLeftAvgB += ptr[index];
                            bottomLeftAvgG += ptr[index + 1];
                            bottomLeftAvgR += ptr[index + 2];
                            //Asum += imageHelper.ByteArray[index + 3];
                        }
                    bottomLeftAvgR /= squareRegion;
                    bottomLeftAvgG /= squareRegion;
                    bottomLeftAvgB /= squareRegion;
                    bottomLeftLightness /= squareRegion;
                    #endregion

                    #region BOTTOM RIGHT REGION
                    for (int yRegion = 0; yRegion < region; ++yRegion)
                        for (int xRegion = 0; xRegion < region; ++xRegion)
                        {
                            index = (int)imageHelper.GetIndex(x + xRegion, y + yRegion, 0);
                            bottomRightLightness += Math.Max(Math.Max(ptr[index], ptr[index + 1]), ptr[index + 2]);
                            bottomRightAvgB += ptr[index];
                            bottomRightAvgG += ptr[index + 1];
                            bottomRightAvgR += ptr[index + 2];
                            //Asum += imageHelper.ByteArray[index + 3];
                        }
                    bottomRightAvgR /= squareRegion;
                    bottomRightAvgG /= squareRegion;
                    bottomRightAvgB /= squareRegion;
                    bottomRightLightness /= squareRegion;
                    #endregion

                    byte max = (byte)Math.Min(
                        Math.Min(topLeftLightness, topRightLightness),
                        Math.Min(bottomLeftLightness, bottomRightLightness)
                        );

                    byte R, G, B;
                    if (max == topLeftLightness)
                    {
                        R = (byte)topLeftAvgR;
                        G = (byte)topLeftAvgG;
                        B = (byte)topLeftAvgB;
                    }
                    else if (max == topRightLightness)
                    {
                        R = (byte)topRightAvgR;
                        G = (byte)topRightAvgG;
                        B = (byte)topRightAvgB;
                    }
                    else if (max == bottomLeftLightness)
                    {
                        R = (byte)bottomLeftAvgR;
                        G = (byte)bottomLeftAvgG;
                        B = (byte)bottomLeftAvgB;
                    }
                    else
                    {
                        R = (byte)bottomRightAvgR;
                        G = (byte)bottomRightAvgG;
                        B = (byte)bottomRightAvgB;
                    }

                    image.SetRGB(x - regionFrom, y - regionFrom, R, G, B);
                }
            }

            return image;
        }
    }
}
