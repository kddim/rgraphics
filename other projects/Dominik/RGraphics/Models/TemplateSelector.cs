﻿using RGraphics.Models.ImageEffects;
using RGraphics.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace RGraphics.Models
{
    public class TemplateSelector : DataTemplateSelector
    {
        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            return StandardTemplate;
        }

        public DataTemplate StandardTemplate { get; set; }
    }
}
