﻿using Caliburn.Micro;
using RGraphics.Models;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace RGraphics.ViewModels
{
    // TODO: FIX THIS VIEW!
    public class HistogramViewModel : ViewModelBase
    {
        public HistogramViewModel(INavigationService _navigationService)
        :base(_navigationService)
        {
            const int minX = 0;
            const int maxX = 255;
            const int minY = 0;
            const int maxY = 500;

            RedPlotModel = new PlotModel() { Title = (String)Application.Current.Resources["RedChannelHistogram"], LegendTextColor = col, LegendTitleColor = col, SelectionColor = col, TitleColor = col, TextColor = col, PlotAreaBorderColor = col, SubtitleColor = col };
            GreenPlotModel = new PlotModel() { Title = (String)Application.Current.Resources["GreenChannelHistogram"], LegendTextColor = col, LegendTitleColor = col, SelectionColor = col, TitleColor = col, TextColor = col, PlotAreaBorderColor = col, SubtitleColor = col };
            BluePlotModel = new PlotModel() { Title = (String)Application.Current.Resources["BlueChannelHistogram"], LegendTextColor = col, LegendTitleColor = col, SelectionColor = col, TitleColor = col, TextColor = col, PlotAreaBorderColor = col, SubtitleColor = col };
            GreyPlotModel = new PlotModel() { Title = (String)Application.Current.Resources["GreyScaleChannelHistogram"], LegendTextColor = col, LegendTitleColor = col, SelectionColor = col, TitleColor = col, TextColor = col, PlotAreaBorderColor = col, SubtitleColor = col };

            RedPlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom, minX, maxX, (String)Application.Current.Resources["XAxisDescription"]) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            RedPlotModel.Axes.Add(new LinearAxis(AxisPosition.Left, minY, maxY, (String)Application.Current.Resources["YAxisDescription"]) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });

            GreenPlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom, minX, maxX, (String)Application.Current.Resources["XAxisDescription"]) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            GreenPlotModel.Axes.Add(new LinearAxis(AxisPosition.Left, minY, maxY, (String)Application.Current.Resources["YAxisDescription"]) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });

            BluePlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom, minX, maxX, (String)Application.Current.Resources["XAxisDescription"]) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            BluePlotModel.Axes.Add(new LinearAxis(AxisPosition.Left, minY, maxY, (String)Application.Current.Resources["YAxisDescription"]) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });

            GreyPlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom, minX, maxX, (String)Application.Current.Resources["XAxisDescription"]) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
            GreyPlotModel.Axes.Add(new LinearAxis(AxisPosition.Left, minY, maxY, (String)Application.Current.Resources["YAxisDescription"]) { TicklineColor = col, TextColor = col, AxislineColor = col, MajorGridlineColor = col, MinorGridlineColor = col, ExtraGridlineColor = col });
        }

        OxyColor col = OxyColors.White;
        protected override void OnActivate()
        {
            this.HistogramConfig = IoC.Get<MenuViewModel>().HistogramConfig;
            RedPlotModel.Series.Clear();
            GreenPlotModel.Series.Clear();
            BluePlotModel.Series.Clear();
            GreyPlotModel.Series.Clear();

            Init();
            base.OnActivate();
        }

        public HistogramConfig HistogramConfig { get; set; }

        private int[] CountPixels(byte[] channelArray)
        {
            int[] result = new int[256];

            for (int i = 0; i <= 255; ++i)
                result[i] = channelArray.Count(channel => channel == i);

            return result;
        }

        void Rescale(int[] arr, int max)
        {
            for (int i = 0; i < arr.Length; ++i)
                arr[i] = 500 * arr[i] / max;
        }

        private int[] RedChannel;
        private int[] GreenChannel;
        private int[] BlueChannel;

        private PlotModel _RedPlotModel;
        public PlotModel RedPlotModel
        {
            get { return _RedPlotModel; }
            set
            {
                _RedPlotModel = value;
                NotifyOfPropertyChange(() => RedPlotModel);
            }
        }

        private PlotModel _GreenPlotModel;
        public PlotModel GreenPlotModel
        {
            get { return _GreenPlotModel; }
            set
            {
                _GreenPlotModel = value;
                NotifyOfPropertyChange(() => GreenPlotModel);
            }
        }

        private PlotModel _BluePlotModel;
        public PlotModel BluePlotModel
        {
            get { return _BluePlotModel; }
            set
            {
                _BluePlotModel = value;
                NotifyOfPropertyChange(() => BluePlotModel);
            }
        }

        private PlotModel _GreyPlotModel;
        public PlotModel GreyPlotModel
        {
            get { return _GreyPlotModel; }
            set
            {
                _GreyPlotModel = value;
                NotifyOfPropertyChange(() => GreyPlotModel);
            }
        }
        public void Init()
        {

            byte[] GreyByteArr = new byte[HistogramConfig.RedChannel.Length];
            for (int i = 0; i < HistogramConfig.RedChannel.Length; ++i)
            {
                GreyByteArr[i] = (byte)(((int)HistogramConfig.RedChannel[i] + (int)HistogramConfig.GreenChannel[i] + (int)HistogramConfig.BlueChannel[i]) / 3);
            }
            int[] GreyChannel = CountPixels(GreyByteArr);
            Rescale(GreyChannel, GreyChannel.Max());

            //await ThreadPool.RunAsync(new WorkItemHandler((IAsyncAction action) =>
            //{
            RedChannel = CountPixels(HistogramConfig.RedChannel);
            GreenChannel = CountPixels(HistogramConfig.GreenChannel);
            BlueChannel = CountPixels(HistogramConfig.BlueChannel);
            Rescale(RedChannel, RedChannel.Max());
            Rescale(GreenChannel, GreenChannel.Max());
            Rescale(BlueChannel, BlueChannel.Max());
            //}));
            for (int i = 0; i < 256; ++i)
            {
                var RedPlotDataSeries = new AreaSeries();
                RedPlotDataSeries.Points.Add(new DataPoint(i, RedChannel[i]));
                RedPlotDataSeries.Points.Add(new DataPoint(i + 1, RedChannel[i]));
                RedPlotDataSeries.Points2.Add(new DataPoint(i, 0));
                RedPlotDataSeries.Points2.Add(new DataPoint(i + 1, 0));
                RedPlotDataSeries.Color = OxyColor.FromRgb((byte)i, 0, 0);
                RedPlotDataSeries.Fill = OxyColor.FromRgb((byte)i, 0, 0);
                RedPlotDataSeries.Background = OxyColors.Gold;
                RedPlotModel.Series.Add(RedPlotDataSeries);
            }
            
            for (int i = 0; i < 256; ++i)
            {
                var GreenPlotDataSeries = new AreaSeries();
                GreenPlotDataSeries.Points.Add(new DataPoint(i, GreenChannel[i]));
                GreenPlotDataSeries.Points.Add(new DataPoint(i + 1, GreenChannel[i]));
                GreenPlotDataSeries.Points2.Add(new DataPoint(i, 0));
                GreenPlotDataSeries.Points2.Add(new DataPoint(i + 1, 0));
                GreenPlotDataSeries.Color = OxyColor.FromRgb(0, (byte)i, 0);
                GreenPlotDataSeries.Fill = OxyColor.FromRgb(0, (byte)i, 0);
                GreenPlotDataSeries.Background = OxyColors.Gold;
                GreenPlotModel.Series.Add(GreenPlotDataSeries);
            }
            
            for (int i = 0; i < 256; ++i)
            {
                var BluePlotDataSeries = new AreaSeries();
                BluePlotDataSeries.Points.Add(new DataPoint(i, BlueChannel[i]));
                BluePlotDataSeries.Points.Add(new DataPoint(i + 1, BlueChannel[i]));
                BluePlotDataSeries.Points2.Add(new DataPoint(i, 0));
                BluePlotDataSeries.Points2.Add(new DataPoint(i + 1, 0));
                BluePlotDataSeries.Color = OxyColor.FromRgb(0, 0, (byte)i);
                BluePlotDataSeries.Fill = OxyColor.FromRgb(0, 0, (byte)i);
                BluePlotDataSeries.Background = OxyColors.Gold;
                BluePlotModel.Series.Add(BluePlotDataSeries);
            }
            
            for (int i = 0; i < 256; ++i)
            {
                var GreyPlotDataSeries = new AreaSeries();
                GreyPlotDataSeries.Points.Add(new DataPoint(i, GreyChannel[i]));
                GreyPlotDataSeries.Points.Add(new DataPoint(i + 1, GreyChannel[i]));
                GreyPlotDataSeries.Points2.Add(new DataPoint(i, 0));
                GreyPlotDataSeries.Points2.Add(new DataPoint(i + 1, 0));
                GreyPlotDataSeries.Color = OxyColor.FromRgb((byte)i, (byte)i, (byte)i);
                GreyPlotDataSeries.Fill = OxyColor.FromRgb((byte)i, (byte)i, (byte)i);
                GreyPlotDataSeries.Background = OxyColors.Gold;
                GreyPlotModel.Series.Add(GreyPlotDataSeries);
            }
        }
    }
}
