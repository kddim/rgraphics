﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace RGraphics.UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class ImageEffects : UITestBase
    {
        public ImageEffects()
        {
        }

        /// <summary>
        /// Test ImageEffects, checking all controls
        /// </summary>
        [TestMethod]
        public void ImageEffectsButton()
        {
            ShowAppMenu();
            Mouse.Click(window.UIEffectsButton);

            Mouse.Move(window.UIBackButton, new Point(10,10));
            Assert.IsTrue(window.UIBackButton.Exists);

            Mouse.Move(window.UIItemList.UIGrayscale, new Point(10,10));
            Assert.IsTrue(window.UIItemList.UIGrayscale.Exists);

            Mouse.Move(window.UIItemList.UINegative, new Point(10, 10));
            Assert.IsTrue(window.UIItemList.UINegative.Exists);

            Mouse.Move(window.UIItemList.UISepia, new Point(10, 10));
            Assert.IsTrue(window.UIItemList.UISepia.Exists);

            Mouse.Move(window.UIItemList.UINoise, new Point(10, 10));
            Assert.IsTrue(window.UIItemList.UINoise.Exists);

            Mouse.Move(window.UIItemList.UIEdgeDetection, new Point(10, 10));
            Assert.IsTrue(window.UIItemList.UIEdgeDetection.Exists);

            Mouse.Move(window.UIItemList.UIUnnoise, new Point(10, 10));
            Assert.IsTrue(window.UIItemList.UIUnnoise.Exists);

            Mouse.Move(window.UIItemList.UIBlur, new Point(10, 10));
            Assert.IsTrue(window.UIItemList.UIBlur.Exists);

            Mouse.Move(window.UIApplyCrop, new Point(10, 10));
            Assert.IsTrue(window.UIApplyCrop.Exists);

            Mouse.Move(window.UIBackButton, new Point(10, 10));
            Mouse.Click();

        }

        /// <summary>
        /// Test ImageEffects -> Grayscale, filtering image from file
        /// </summary>
        [TestMethod]
        public void Grayscale()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UIGrayscale);
            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("grayscale");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Test ImageEffects -> Negative, filtering image from file
        /// </summary>
        [TestMethod]
        public void Negative()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UINegative);
            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("negative");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Test ImageEffects -> Sepia, filtering image from file with 0,20,40 sepia level
        /// </summary>
        [TestMethod]
        public void Sepia()
        {

            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UISepia);

            Mouse.Move(window.UISepiaSlider, new Point(6, 10));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(1000), GetYCord(384) - 200));
            Mouse.Move(new Point(GetXCord(683) - 200, GetYCord(384) - 200));
            Mouse.StopDragging(new Point(GetXCord(683) - 200, GetYCord(384) - 200));

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("sepia_0");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UISepia);

            Mouse.Move(window.UISepiaSlider, new Point(6, 10));
            Point temp = Mouse.Location;
            temp.X += 120;
            Mouse.StartDragging();
            Mouse.StopDragging(temp);

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("sepia_20");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UISepia);

            Mouse.Move(window.UISepiaSlider, new Point(6, 10));
            temp = Mouse.Location;
            temp.X += 300;
            Mouse.StartDragging();
            Mouse.StopDragging(temp);
            

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("sepia_40");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }


        }

        /// <summary>
        /// Test ImageEffects -> Noise, noise creation using Salt&Pepper and Uniform method
        /// </summary>
        [TestMethod]
        public void Noise()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UINoise);

            Mouse.Click(window.UIFontsComboBox);
            Mouse.Click(window.UIFontsComboBox.UISaltPepperListItem);

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("salt_and_pepper");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UINoise);

            Mouse.Click(window.UIFontsComboBox);
            Mouse.Click(window.UIFontsComboBox.UIUniformListItem);

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("uniform_noise");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }
        }


        /// <summary>
        /// Test ImageEffects -> EdgeDetection, edge detection using Robert's Cross and Sobel's Filter method
        /// </summary>
        [TestMethod]
        public void EdgeDetection()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UIEdgeDetection);

            Mouse.Click(window.UIFontsComboBox);
            Mouse.Click(window.UIFontsComboBox.UIRobertsCrossListItem);

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("robertscross");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UIEdgeDetection);

            Mouse.Click(window.UIFontsComboBox);
            Mouse.Click(window.UIFontsComboBox.UISobelsFilterListItem);

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("sobel");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Test ImageEffects -> Unnoise, unnoise image using AveragingFilter, MedianFilter and ExtendedMedianFilter method
        /// </summary>
        [TestMethod]
        public void Unnoise()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UIUnnoise);

            Mouse.Click(window.UIFontsComboBox);
            Mouse.Click(window.UIFontsComboBox.UIAveragingFilterListItem);

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("averagingfilter");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UIUnnoise);

            Mouse.Click(window.UIFontsComboBox);
            Mouse.Click(window.UIFontsComboBox.UIMedianFilterListItem);

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("medianfilter");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UIUnnoise);

            Mouse.Click(window.UIFontsComboBox);
            Mouse.Click(window.UIFontsComboBox.UIExtendedMedianFilterListItem);

            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("extendedmedianfilter");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Test ImageEffects -> Blur, bluring image
        /// </summary>
        [TestMethod]
        public void Blur()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIEffectsButton);
            Mouse.Click(window.UIItemList.UIBlur);
            Mouse.Click(window.UIApplyCrop);
            Playback.Wait(5000);
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("blur");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }
        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
