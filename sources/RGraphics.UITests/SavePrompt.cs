﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace RGraphics.UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class SavePrompt : UITestBase
    {
        public SavePrompt()
        {
        }

        /// <summary>
        /// Test SavePrompt, saving image with SavePrompt function and checking correctness
        /// </summary>
        [TestMethod]
        public void SPrompt()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);

            window.UIInputWidthEdit.SetFocus();
            window.UIInputWidthEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            window.UIInputHeightEdit.SetFocus();
            window.UIInputHeightEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            Mouse.Click(window.UIApplyButton);

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIShapeListItem);

            Mouse.Move(new Point(GetXCord(683) - 150, GetYCord(384) - 150));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 150, GetYCord(384) + 150));

            Mouse.Click(window.UINewButton);

            Mouse.Click(window.UIButtonBarToolBar.UIYesButton1);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("saveprompt");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }

            Mouse.Click(window.UIBackButton);

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UISaveButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("saveprompt");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            Mouse.Click(window.UIButtonBarToolBar.UINoButton);

            Mouse.Click(window.UISavingToolBar.UICancelButton);

            Playback.Wait(1000);
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
