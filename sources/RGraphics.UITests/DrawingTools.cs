﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Windows.UI.Xaml.Controls;


namespace RGraphics.UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class DrawingTools : UITestBase
    {
        public DrawingTools()
        {
        }

        /// <summary>
        /// Test DrawingTools, checking all controls
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void DrawingToolsButton()
        {
            ShowAppMenu();
            Mouse.Click(window.UIDrawingtoolsToggleButton);

            Assert.IsTrue(window.UIItemList.UIPointerListItem.Exists);
            Mouse.Click(window.UIItemList.UIPointerListItem);

            Assert.IsTrue(window.UIItemList.UIPencilListItem.Exists);
            Assert.IsTrue(window.UIItemList.UIPencilListItem.UIItemButton.Exists);
            Mouse.Click(window.UIItemList.UIPencilListItem);

            Assert.IsTrue(window.UIItemList.UITextListItem.Exists);
            Assert.IsTrue(window.UIItemList.UITextListItem.UIItemButton.Exists);
            Mouse.Click(window.UIItemList.UITextListItem);

            Assert.IsTrue(window.UIItemList.UIFloodFillListItem.Exists);
            Assert.IsTrue(window.UIItemList.UIFloodFillListItem.UIItemButton.Exists);
            Mouse.Click(window.UIItemList.UIFloodFillListItem);

            Assert.IsTrue(window.UIItemList.UIShapeListItem.Exists);
            Assert.IsTrue(window.UIItemList.UIShapeListItem.UIItemButton.Exists);
            Mouse.Click(window.UIItemList.UIShapeListItem);

            Assert.IsTrue(window.UIItemList.UIScaleListItem.Exists);
            Assert.IsTrue(window.UIItemList.UIScaleListItem.UIItemButton.Exists);
            Mouse.Click(window.UIItemList.UIScaleListItem);
            
            Assert.IsTrue(window.UIItemList.UIRotationListItem.Exists);
            Assert.IsTrue(window.UIItemList.UIRotationListItem.UIItemButton.Exists);
            Mouse.Click(window.UIItemList.UIRotationListItem);

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(361, 194)); // Sample Color 1
            Mouse.Click();

            Mouse.Move(new Point(396, 194)); // Sample Color 2
            Mouse.Click();

            Mouse.Move(new Point(429, 194)); // Sample Color 3
            Mouse.Click();

            Mouse.Move(new Point(460, 194)); // Sample Color 4
            Mouse.Click();

            Mouse.Move(new Point(380, 260)); // Some color from colors ring
            Mouse.Click();

            Mouse.Move(new Point(440, 560)); // Some color from colors ring
            Mouse.Click();
            

        }

        /// <summary>
        /// Test DrawingTools -> Pencil, drawing with various thickness and colors
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void Pencil()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);

            window.UIInputWidthEdit.SetFocus();
            window.UIInputWidthEdit.WaitForControlEnabled();
            Keyboard.SendKeys("400");

            window.UIInputHeightEdit.SetFocus();
            window.UIInputHeightEdit.WaitForControlEnabled();
            Keyboard.SendKeys("400");

            Mouse.Click(window.UIApplyButton);

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIPencilListItem);

            Point relative_point = new Point(400,400);

            Mouse.StartDragging(window.UIScrollViewerPanel.UIImageImage, new Point(10, 10));
            Mouse.StopDragging(window.UIScrollViewerPanel.UIImageImage, new Point(30, 200));

            Mouse.Move(new Point(216, 88)); // Pencil -> more options
            Mouse.Click();

            Mouse.Move(new Point(252,85)); // Thickness level 2
            Mouse.Click();

            Mouse.Click(relative_point);
            Mouse.StartDragging(window.UIScrollViewerPanel.UIImageImage, new Point(40, 160));
            Mouse.StopDragging(window.UIScrollViewerPanel.UIImageImage, new Point(380, 30));

            Mouse.Move(new Point(188, 364)); // Color picker
            Mouse.Click();

            Mouse.Move(new Point(429, 194)); // Sample color 3 (green)
            Mouse.Click();

            Mouse.Move(new Point(216, 88)); // Pencil -> more options
            Mouse.Click();
            Mouse.Click();

            Mouse.Move(new Point(252, 130)); // Thickness level 4
            Mouse.Click();

            Mouse.Click(relative_point);
            Mouse.StartDragging(window.UIScrollViewerPanel.UIImageImage, new Point(40, 370));
            Mouse.StopDragging(window.UIScrollViewerPanel.UIImageImage, new Point(210, 380));

            Mouse.Move(new Point(188, 364)); // Color picker
            Mouse.Click();

            Mouse.Move(new Point(460, 194)); // Sample color 4 (blue)
            Mouse.Click();

            Mouse.Move(new Point(216, 88)); // Pencil -> more options
            Mouse.Click();
            Mouse.Click();

            Mouse.Move(new Point(252, 66)); // Thickness level 1
            Mouse.Click();

            Mouse.Click(relative_point);
            Mouse.StartDragging(window.UIScrollViewerPanel.UIImageImage, new Point(370, 395));
            Mouse.StopDragging(window.UIScrollViewerPanel.UIImageImage, new Point(240, 240));
      
            
        }

        /// <summary>
        /// Test DrawingTools -> Text, add text with various styles
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void Text()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);

            window.UIInputWidthEdit.SetFocus();
            window.UIInputWidthEdit.WaitForControlEnabled();
            Keyboard.SendKeys("400");

            window.UIInputHeightEdit.SetFocus();
            window.UIInputHeightEdit.WaitForControlEnabled();
            Keyboard.SendKeys("400");

            Mouse.Click(window.UIApplyButton);

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UITextListItem);

            Point relative_point = new Point(GetXCord(800), GetYCord(20));

            Mouse.Move(window.UIScrollViewerPanel.UIImageImage, new Point(20, 20));
            Mouse.Click();

            Assert.IsTrue(window.UIScrollViewerPanel.UIImageImage.UITextAddBox.Exists);
            Assert.IsTrue(window.UIScrollViewerPanel.UITextNoButton.Exists);
            Assert.IsTrue(window.UIScrollViewerPanel.UITextYesButton.Exists);

            double textbox_width = window.UIScrollViewerPanel.UIImageImage.UITextAddBox.Width;
            double textbox_height = window.UIScrollViewerPanel.UIImageImage.UITextAddBox.Height;

            double xdistance_from_accept = window.UIScrollViewerPanel.UITextYesButton.Left - window.UIScrollViewerPanel.UIImageImage.UITextAddBox.Left;
            double ydistance_from_accept = window.UIScrollViewerPanel.UITextYesButton.Top - window.UIScrollViewerPanel.UIImageImage.UITextAddBox.Top;

            double xdistance_from_cancel = window.UIScrollViewerPanel.UITextNoButton.Left - window.UIScrollViewerPanel.UIImageImage.UITextAddBox.Left;
            double ydistance_from_cancel = window.UIScrollViewerPanel.UITextNoButton.Top - window.UIScrollViewerPanel.UIImageImage.UITextAddBox.Top;

            int button_width = window.UIScrollViewerPanel.UITextNoButton.Width;
            int button_height = window.UIScrollViewerPanel.UITextNoButton.Height;

            Keyboard.SendKeys("RGraphics");

            Mouse.Click(window.UIScrollViewerPanel.UITextYesButton);

            Mouse.Click(window.UIItemList.UITextListItem.UIItemButton);

            Mouse.Move(new Point(342, 93)); // + font size
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();

            Mouse.Move(new Point(260, 160)); // font select
            Mouse.Click();

            Mouse.Move(new Point(250, 290)); // Comic Sans font
            Mouse.Click();

            Mouse.Move(new Point(317, 204)); // italic font style
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Move(window.UIScrollViewerPanel.UIImageImage, new Point(200, 40));
            Mouse.Click();

            Point cancel_point = Mouse.Location; // Cords for Cancel text box button
            cancel_point.X += (int)xdistance_from_cancel + (button_width);
            cancel_point.Y += (int)ydistance_from_cancel + (button_height);

            Point accept_point = Mouse.Location; // Cords for Accept text box button
            accept_point.X += (int)xdistance_from_accept + (button_width);
            accept_point.Y += (int)ydistance_from_accept + (button_height);

            Point location_point = Mouse.Location; // Cords for text box's button to dragging
            location_point.X += (int)textbox_width + 1;
            location_point.Y += (int)textbox_height + 1;

            Keyboard.SendKeys("IS");

            Mouse.Move(location_point);
            Mouse.StartDragging();
            Mouse.Move(new Point(100, 100));
            Mouse.StopDragging(location_point);

            Mouse.Move(cancel_point);
            Mouse.Click();

            Mouse.Move(window.UIScrollViewerPanel.UIImageImage, new Point(200, 40));
            Mouse.Click();

            Keyboard.SendKeys("is");

            Mouse.Move(accept_point);
            Mouse.Click();

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(429, 194)); // Sample Color 3 (green)
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UITextListItem);
            Mouse.Click(window.UIItemList.UITextListItem.UIItemButton);

            Mouse.Move(new Point(257, 93)); // - font size
            Mouse.Click();
            Mouse.Click();

            Mouse.Move(new Point(260, 160)); // font select
            Mouse.Click();

            Mouse.Move(new Point(250, 290)); // Verdana font style
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Move(window.UIScrollViewerPanel.UIImageImage, new Point(60, 120));
            Mouse.Click();

            accept_point = Mouse.Location; // Cords for Accept text box button
            accept_point.X += (int)xdistance_from_accept + (button_width);
            accept_point.Y += (int)ydistance_from_accept + (button_height);

            Keyboard.SendKeys("THE      BEST");

            Mouse.Move(accept_point);
            Mouse.Click();

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(570, 570)); // Violet Color
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UITextListItem);
            Mouse.Click(window.UIItemList.UITextListItem.UIItemButton);

            Mouse.Move(new Point(342, 93)); // + font size
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();

            Mouse.Move(new Point(260, 160)); // font select
            Mouse.Click();

            Mouse.Move(new Point(250, 245)); // Impact font style
            Mouse.Click();

            Mouse.Move(new Point(317, 204)); // disable italic font style
            Mouse.Click();

            Mouse.Move(new Point(247, 204)); // bold font style
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Move(window.UIScrollViewerPanel.UIImageImage, new Point(140, 300));
            Mouse.Click();

            accept_point = Mouse.Location; // Cords for Accept text box button
            accept_point.X += (int)xdistance_from_accept + (button_width);
            accept_point.Y += (int)ydistance_from_accept + (button_height);

            Keyboard.SendKeys("Application !!!!!!");

            Mouse.Move(accept_point);
            Mouse.Click();

        }

        /// <summary>
        /// Test DrawingTools -> FloodFill, filling areas with various color and tolerancy
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void FloodFill()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);

            window.UIInputWidthEdit.SetFocus();
            window.UIInputWidthEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            window.UIInputHeightEdit.SetFocus();
            window.UIInputHeightEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            Mouse.Click(window.UIApplyButton);

            Mouse.Click(window.UIDrawingtoolsToggleButton);

            Mouse.Click(window.UIItemList.UIFloodFillListItem);

            Mouse.Click(window.UIScrollViewerPanel.UIImageImage);

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(361, 194)); // White Color
            Mouse.Click();

            Point relative_point = new Point(GetXCord(800), GetYCord(20));
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIFloodFillListItem);

            Mouse.Click(window.UIScrollViewerPanel.UIImageImage);

            

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(396, 194)); // Red Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Point middle = new Point(GetXCord(683), GetYCord(384));

            Mouse.Click(window.UIItemList.UIPencilListItem);

            Mouse.Move(middle);
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683), GetYCord(384)+305));
            Mouse.Move(new Point(GetXCord(683), GetYCord(384)-305));
            Mouse.Move(middle);
            Mouse.Move(new Point(GetXCord(683)-305, GetYCord(384)));
            Mouse.Move(new Point(GetXCord(683)+305, GetYCord(384)));
            Mouse.StopDragging(middle);

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(429, 194)); // Green Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIFloodFillListItem);
            Mouse.Click(window.UIItemList.UIFloodFillListItem.UIItemButton);

            Mouse.Move(new Point(252, 200)); // Cords for tolerancy slider
            Mouse.StartDragging();
            Mouse.Move(new Point(300, 200));
            Mouse.Move(new Point(230, 200));
            Mouse.Move(new Point(252, 200));
            Mouse.StopDragging(new Point(252, 200));
            Mouse.Click(relative_point);

            Mouse.Click(new Point(GetXCord(683)-100, GetYCord(384)-100));

            //////////////////////////////////////////////////////////////
            
            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(460, 194)); // Green Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIFloodFillListItem);
            Mouse.Click(window.UIItemList.UIFloodFillListItem.UIItemButton);

            Mouse.Move(new Point(252, 200)); // Cords for tolerancy slider
            Mouse.StartDragging();
            Mouse.Move(new Point(292, 200));
            Mouse.StopDragging(new Point(292, 200));
            Mouse.Click(relative_point);

            Mouse.Click(new Point(GetXCord(683)+100, GetYCord(384)-100));

            //////////////////////////////////////////////////////////////

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(570, 570)); // Violet Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIFloodFillListItem);
            Mouse.Click(window.UIItemList.UIFloodFillListItem.UIItemButton);

            Mouse.Move(new Point(292, 200)); // Cords for tolerancy slider
            Mouse.StartDragging();
            Mouse.Move(new Point(332, 200));
            Mouse.StopDragging(new Point(332, 200));
            Mouse.Click(relative_point);

            Mouse.Click(new Point(GetXCord(683)-100, GetYCord(384)+100));

            //////////////////////////////////////////////////////////////

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(389, 247)); // Yellow Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIFloodFillListItem);
            Mouse.Click(window.UIItemList.UIFloodFillListItem.UIItemButton);

            Mouse.Move(new Point(332, 200)); // Cords for tolerancy slider
            Mouse.StartDragging();
            Mouse.Move(new Point(382, 200));
            Mouse.StopDragging(new Point(382, 200));
            Mouse.Click(relative_point);

            Mouse.Click(new Point(GetXCord(683)+100, GetYCord(384)+100));

        }

        /// <summary>
        /// Test DrawingTools -> Shapes, drawing filled and unfilled shapes with various thickness and colors
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void Shapes()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);

            window.UIInputWidthEdit.SetFocus();
            window.UIInputWidthEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            window.UIInputHeightEdit.SetFocus();
            window.UIInputHeightEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            Mouse.Click(window.UIApplyButton);

            Point relative_point = new Point(GetXCord(800), GetYCord(20));

            Mouse.Click(window.UIDrawingtoolsToggleButton);

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);
            Mouse.Click(new Point(260, 165));
            Mouse.Click(relative_point);

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(GetXCord(683)-180, GetYCord(384)-280));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683)-120, GetYCord(384)-180));
            Mouse.StopDragging(new Point(GetXCord(683)-120, GetYCord(384)-180));

            Mouse.Move(new Point(GetXCord(683)-160, GetYCord(384)-250));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683)-90, GetYCord(384)-160));
            Mouse.StopDragging(new Point(GetXCord(683)-90, GetYCord(384)-160));

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(460, 194)); // Blue Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);

            Mouse.Move(new Point(265, 240)); // Thickness level 1
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(GetXCord(683)+100, GetYCord(384)+100));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683)+320, GetYCord(384)+320));
            Mouse.StopDragging(new Point(GetXCord(683)+320, GetYCord(384)+320));

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);
            Mouse.Click(new Point(250, 335)); // filled button
            Mouse.Click(relative_point);

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(GetXCord(683)+140, GetYCord(384)+240));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683)+220, GetYCord(384)+302));
            Mouse.StopDragging(new Point(GetXCord(683)+220, GetYCord(384)+302));

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(429, 194)); // Green Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);

            Mouse.Move(new Point(310, 170)); // Elipse shape
            Mouse.Click();
            Mouse.Move(new Point(265, 302)); // Thickness level 4
            Mouse.Click();
            Mouse.Click(new Point(250, 335)); // filled button
            Mouse.Click(relative_point);

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(GetXCord(683)-100, GetYCord(384)-100));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683)+200, GetYCord(384)+200));
            Mouse.StopDragging(new Point(GetXCord(683)+200, GetYCord(384)+200));

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);

            Mouse.Move(new Point(265, 260)); // Thickness level 2
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(GetXCord(683)-60, GetYCord(384)-60));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683)+160, GetYCord(384)+160));
            Mouse.StopDragging(new Point(GetXCord(683)+160, GetYCord(384)+160));

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(389, 247)); // Yellow Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);

            Mouse.Move(new Point(255, 340));
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(GetXCord(683)-296, GetYCord(384)-296));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683)+296, GetYCord(384)+296));
            Mouse.StopDragging(new Point(GetXCord(683)+296, GetYCord(384)+296));

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(560, 560)); // Violet Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);

            Mouse.Move(new Point(352, 170)); // Line shape
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(GetXCord(683), GetYCord(384)));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683), GetYCord(384)-290));
            Mouse.Move(new Point(GetXCord(683)+290, GetYCord(384)));
            Mouse.Move(new Point(GetXCord(683), GetYCord(384)+290));
            Mouse.Move(new Point(GetXCord(683)-290, GetYCord(384)));
            Mouse.Move(new Point(GetXCord(683), GetYCord(384)-290));
            Mouse.Move(new Point(GetXCord(683)+290, GetYCord(384)));
            Mouse.Move(new Point(GetXCord(683), GetYCord(384)+290));
            Mouse.Move(new Point(GetXCord(683)-290, GetYCord(384)));
            Mouse.Move(new Point(GetXCord(683), GetYCord(384)-290));
            Mouse.StopDragging(new Point(GetXCord(683), GetYCord(384)-290));

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

        }

        /// <summary>
        /// Test DrawingTools -> Scale, scaling proportionally and unproportionally with correct and incorrect new sizes
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void Scale()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);

            window.UIInputWidthEdit.SetFocus();
            window.UIInputWidthEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            window.UIInputHeightEdit.SetFocus();
            window.UIInputHeightEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            Mouse.Click(window.UIApplyButton);

            Point relative_point = new Point(GetXCord(800), GetYCord(20));

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIScaleListItem);
            Mouse.Click(window.UIItemList.UIScaleListItem.UIItemButton);

            Mouse.Move(new Point(310, 255));
            Mouse.DoubleClick(); // new width
            Keyboard.SendKeys("0");

            Mouse.Move(new Point(310, 282));
            Mouse.DoubleClick(); // new height
            Keyboard.SendKeys("afdsdf");

            Mouse.Move(new Point(252, 352)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(310, 255));
            Mouse.DoubleClick(); // new width
            Keyboard.SendKeys("60");

            Mouse.Move(new Point(310, 282));
            Mouse.DoubleClick(); // new height
            Keyboard.SendKeys("340");

            Mouse.Move(new Point(252, 352)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(310, 255));
            Mouse.DoubleClick(); // new width
            Keyboard.SendKeys("0");

            Mouse.Move(new Point(310, 282));
            Mouse.DoubleClick(); // new height
            Keyboard.SendKeys("afdsdf");

            Mouse.Move(new Point(252, 352)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(310, 255));
            Mouse.DoubleClick(); // new width
            Keyboard.SendKeys("9999");

            Mouse.Move(new Point(310, 282));
            Mouse.DoubleClick(); // new height
            Keyboard.SendKeys("9999");

            Mouse.Move(new Point(252, 352)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(250, 313)); // ascept ratio box
            Mouse.Click();

            Mouse.Move(new Point(310, 255));
            Mouse.DoubleClick(); // new width
            Keyboard.SendKeys("ab-.6;;*0ada0ff");

            Mouse.Move(new Point(252, 352)); // apply button
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);

            Mouse.Move(new Point(262, 170)); // square shape
            Mouse.Click();
            Mouse.Click(new Point(250, 340));
            Mouse.Click(relative_point);

            Mouse.StartDragging(window.UIScrollViewerPanel.UIImageImage, new Point(100, 100));
            Mouse.StopDragging(window.UIScrollViewerPanel.UIImageImage, new Point(300, 270));

            Mouse.Click(window.UIItemList.UIScaleListItem);
            Mouse.Click(window.UIItemList.UIScaleListItem.UIItemButton);

            Mouse.Move(new Point(310, 255));
            Mouse.DoubleClick(); // new width
            Keyboard.SendKeys("40");

            Mouse.Move(new Point(252, 352)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(310, 255));
            Mouse.DoubleClick(); // new width
            Keyboard.SendKeys("600");

            Mouse.Move(new Point(252, 352)); // apply button
            Mouse.Click();

            Mouse.Click(relative_point);
            
        }

        /// <summary>
        /// Test DrawingTools -> Rotation, image rotation with and without crop, with variuos angles
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void Rotation()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);

            window.UIInputWidthEdit.SetFocus();
            window.UIInputWidthEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            window.UIInputHeightEdit.SetFocus();
            window.UIInputHeightEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            Mouse.Click(window.UIApplyButton);

            Point relative_point = new Point(GetXCord(800), GetYCord(20));

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIRotationListItem);
            Mouse.Click(window.UIItemList.UIRotationListItem.UIItemButton);

            Mouse.Move(new Point(255, 370)); // apply button
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            Mouse.Click(new Point(GetXCord(540), GetYCord(320))); //empty image
            Mouse.Click(new Point(GetXCord(650), GetYCord(338))); // width
            Keyboard.SendKeys("600");
            Mouse.Click(new Point(GetXCord(650), GetYCord(370))); // height
            Keyboard.SendKeys("600");
            Mouse.Click(new Point(GetXCord(550), GetYCord(435))); // apply

            Mouse.Click(window.UIItemList.UIRotationListItem.UIItemButton);

            Mouse.Move(new Point(255, 340)); // crop box
            Mouse.Click();

            Mouse.Move(new Point(255, 370)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(306, 306)); // angle's value
            Mouse.DoubleClick();
            Keyboard.SendKeys("90");

            Mouse.Move(new Point(255, 370)); // apply button
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            Mouse.Click(new Point(GetXCord(540), GetYCord(320))); //empty image
            Mouse.Click(new Point(GetXCord(650), GetYCord(338))); // width
            Keyboard.SendKeys("500");
            Mouse.Click(new Point(GetXCord(650), GetYCord(370))); // height
            Keyboard.SendKeys("200");
            Mouse.Click(new Point(GetXCord(550), GetYCord(435))); // apply

            Mouse.Click(window.UIItemList.UIRotationListItem.UIItemButton);

            Mouse.Move(new Point(255, 340)); // crop box
            Mouse.Click();

            Mouse.Move(new Point(306, 306)); // angle's value
            Mouse.DoubleClick();
            Keyboard.SendKeys("90");

            Mouse.Move(new Point(255, 370)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(306, 306)); // angle's value
            Mouse.DoubleClick();
            Keyboard.SendKeys("160");

            Mouse.Move(new Point(255, 370)); // apply button
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            Mouse.Click(new Point(GetXCord(540), GetYCord(320))); //empty image
            Mouse.Click(new Point(GetXCord(650), GetYCord(338))); // width
            Keyboard.SendKeys("600");
            Mouse.Click(new Point(GetXCord(650), GetYCord(370))); // height
            Keyboard.SendKeys("600");
            Mouse.Click(new Point(GetXCord(550), GetYCord(435))); // apply

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);

            Mouse.Move(new Point(262, 170)); // square shape
            Mouse.Click();
            Mouse.Click(new Point(250, 340));
            Mouse.Click(relative_point);

            Mouse.StartDragging(window.UIScrollViewerPanel.UIImageImage, new Point(100, 100));
            Mouse.StopDragging(window.UIScrollViewerPanel.UIImageImage, new Point(300, 270));

            Mouse.Click(window.UIItemList.UIRotationListItem);
            Mouse.Click(window.UIItemList.UIRotationListItem.UIItemButton);

            Mouse.Move(new Point(306, 306)); // angle's value
            Mouse.DoubleClick();
            Keyboard.SendKeys("60");

            Mouse.Move(new Point(255, 370)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(306, 306)); // angle's value
            Mouse.DoubleClick();
            Keyboard.SendKeys("ad-vx0506");

            Mouse.Move(new Point(255, 370)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(306, 306)); // angle's value
            Mouse.DoubleClick();
            Keyboard.SendKeys("a   a  a");

            Mouse.Move(new Point(255, 370)); // apply button
            Mouse.Click();

            Mouse.Move(new Point(306, 306)); // angle's value
            Mouse.DoubleClick();
            Keyboard.SendKeys("9999999");

            Mouse.Move(new Point(255, 370)); // apply button
            Mouse.Click();

            Mouse.Click(relative_point);

            Mouse.Move(new Point(188, 364)); // Cords for color picker
            Mouse.Click();

            Mouse.Move(new Point(560, 560)); // Violet Color
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Click(window.UIItemList.UIShapeListItem.UIItemButton);

            Mouse.Move(new Point(305, 165)); // elipse shape
            Mouse.Click();
            Mouse.Click(new Point(250, 340));
            Mouse.Click(relative_point);

            Mouse.StartDragging(window.UIScrollViewerPanel.UIImageImage, new Point(100, 100));
            Mouse.StopDragging(window.UIScrollViewerPanel.UIImageImage, new Point(300, 270));

        }
        

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
