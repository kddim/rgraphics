﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace RGraphics.UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class Histogram : UITestBase
    {
        public Histogram()
        {
        }

        /// <summary>
        /// Test Histogram, checking histogram for different images
        /// </summary>
        [TestMethod]
        public void ShowHistogram()
        {
            ShowAppMenu();

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

            ShowAppMenu();

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIPencilListItem);

            Point relative_point = new Point(GetXCord(700), GetYCord(20));

            Mouse.Move(new Point(GetXCord(683)-100, GetYCord(384)-70));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) - 100, GetYCord(384) + 70));

            Mouse.Move(new Point(GetXCord(683) - 60, GetYCord(384) - 70));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) - 60, GetYCord(384) + 70));

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);

            Mouse.Move(new Point(188, 364)); // Color picker
            Mouse.Click();

            Mouse.Move(new Point(429, 194)); // Sample color 3 (green)
            Mouse.Click();
            Mouse.Click(relative_point);

            Mouse.Click(window.UIItemList.UIPencilListItem);

            Mouse.Move(new Point(GetXCord(683), GetYCord(384) - 70));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683), GetYCord(384) + 70));

            Mouse.Move(new Point(GetXCord(683) +40 , GetYCord(384) - 70));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 40, GetYCord(384) + 70));

            Mouse.Move(new Point(GetXCord(683) + 90, GetYCord(384) - 70));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 90, GetYCord(384) + 70));

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

            ShowAppMenu();

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            Mouse.Click(new Point(GetXCord(540), GetYCord(320))); //empty image
            Mouse.Click(new Point(GetXCord(650), GetYCord(338))); // width
            Keyboard.SendKeys("600");
            Mouse.Click(new Point(GetXCord(650), GetYCord(370))); // height
            Keyboard.SendKeys("600");
            Mouse.Click(new Point(GetXCord(550), GetYCord(435))); // apply

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIFloodFillListItem);

            Mouse.Click(new Point(GetXCord(683), GetYCord(384)));

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
