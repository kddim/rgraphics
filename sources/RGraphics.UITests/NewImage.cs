﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace RGraphics.UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class NewImage : UITestBase
    {
        public NewImage()
        {
        }

        /// <summary>
        /// Test NewImage button, checking all controlls
        /// </summary>
        [TestMethod]
        public void NewImageButton()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Assert.IsTrue(window.UINewImageText.Exists);
            Assert.IsTrue(window.UICapturefromcameraButton.Exists);
            Assert.IsTrue(window.UIEmptyimageButton.Exists);
            Assert.IsTrue(window.UIFromfileButton.Exists);
            Assert.IsTrue(window.UIBackButton.Exists);
        }

        /// <summary>
        /// Tests all New -> EmptyFile dialog (with wrong and good inputs)
        /// </summary>
        [TestMethod]
        public void EmptyFile()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);
            Assert.IsTrue(window.UIWidthText.Exists);
            Assert.IsTrue(window.UIHeightText.Exists);
            Assert.IsTrue(window.UIApplyButton.Exists);
            Assert.IsTrue(window.UICancelButton.Exists);
            Assert.IsTrue(window.UINewImageText.Exists);
            Assert.IsTrue(window.UIInputWidthEdit.Exists);
            Assert.IsTrue(window.UIInputHeightEdit.Exists);

            ////////////////////////////////////////////////////////////////////////////////
            /////////////////////// Testing width TextBox filtering ////////////////////////
            ////////////////////////////////////////////////////////////////////////////////

            Mouse.Click(new Point(GetXCord(650), GetYCord(338))); // width
            Playback.Wait(2000);

            // Fail scenario (this should get filtered out)
            Keyboard.SendKeys("unwqey uy000e wud");
            Assert.AreEqual(String.Empty, window.UIInputWidthEdit.Text);

            // This should get filtered into "1231" (max value)
            Keyboard.SendKeys("12313");
            Assert.AreEqual("1231", window.UIInputWidthEdit.Text);

            // This should clean our textbox
            Keyboard.SendKeys("\b\b\b\b");
            Assert.AreEqual(String.Empty, window.UIInputWidthEdit.Text);

            // This should get filtered out
            Keyboard.SendKeys("00000");
            Assert.AreEqual(String.Empty, window.UIInputWidthEdit.Text);

            // Correct input
            Keyboard.SendKeys("300");
            Assert.AreEqual("300", window.UIInputWidthEdit.Text);

            ////////////////////////////////////////////////////////////////////////////////
            ////////////////////// Testing height TextBox filtering ////////////////////////
            ////////////////////////////////////////////////////////////////////////////////

            Mouse.Click(new Point(GetXCord(650), GetYCord(370))); // height
            Playback.Wait(2000);
            // Fail scenario (this should get filtered out)
            Keyboard.SendKeys("unwqey dsdsd sdsfdgwrfgh wud");
            Assert.AreEqual(String.Empty, window.UIInputHeightEdit.Text);

            // This should get filtered into "405"
            Keyboard.SendKeys("unwqey dsd4sd sdsf0dgwrfgh w5ud");
            Assert.AreEqual("405", window.UIInputHeightEdit.Text);

            // This should clean our textbox
            Keyboard.SendKeys("\b\b\b\b");
            Assert.AreEqual(String.Empty, window.UIInputHeightEdit.Text);

            // This should get filtered into "2032" (max value)
            Keyboard.SendKeys("20329256");
            Assert.AreEqual("2032", window.UIInputHeightEdit.Text);

            // This should clean our textbox
            Keyboard.SendKeys("\b\b\b\b");
            Assert.AreEqual(String.Empty, window.UIInputHeightEdit.Text);

            // Correct input
            Keyboard.SendKeys("200");
            Assert.AreEqual("200", window.UIInputHeightEdit.Text);




            Mouse.Click(window.UICancelButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIEmptyimageButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIApplyButton);
            Playback.Wait(2000);
            Assert.IsTrue(window.UIScrollViewerPanel.Exists);
            Assert.IsTrue(window.UIScrollViewerPanel.UIImageImage.Exists);

            Assert.AreEqual(300, window.UIScrollViewerPanel.UIImageImage.Width);
            Assert.AreEqual(200, window.UIScrollViewerPanel.UIImageImage.Height);
            
        }


        /// <summary>
        /// Test all New -> FromFile (with good and wrong images)
        /// You need to have specific image in specific location to test passed
        /// </summary>
        [TestMethod]
        public void FromFile()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIFromfileButton);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIObrazyListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIATestsListItem);

            Assert.IsTrue(window.UIGridViewList.Exists);
            Assert.IsTrue(window.UIGridViewList.UIATestsGroup.Exists);
            Assert.IsTrue(window.UIChoiceBasketToolBar.Exists);
            Assert.IsTrue(window.UIChoiceBasketToolBar.UIOpenButton.Exists);
            Assert.IsTrue(window.UIChoiceBasketToolBar.UICancelButton.Exists);

            Assert.IsFalse(window.UIChoiceBasketToolBar.UIOpenButton.Enabled);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            Assert.IsTrue(window.UIChoiceBasketToolBar.UIOpenButton.Enabled);

            Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

            Assert.IsTrue(window.UIScrollViewerPanel.UIImageImage.Exists);
            window.UIScrollViewerPanel.UIImageImage.WaitForControlEnabled();
            Assert.AreEqual(259, window.UIScrollViewerPanel.UIImageImage.Width);
            Assert.AreEqual(195, window.UIScrollViewerPanel.UIImageImage.Height);

            
            // TODO , opening larger image than allowed
            //Mouse.Click(window.UINewButton);
            //Assert.IsTrue(window.UIFromfileButton.Enabled);
            //Mouse.Click(window.UIFromfileButton); //                                      <-- Here test crash
            //Mouse.Click(window.UIGridViewList.UIATestsGroup.UICorrectImageListItem);
            //Mouse.Click(window.UIChoiceBasketToolBar.UICancelButton);

            //Assert.IsTrue(window.UINewImageText.Exists);
            //Assert.IsTrue(window.UICapturefromcameraButton.Exists);
            //Assert.IsTrue(window.UIEmptyimageButton.Exists);
            //Assert.IsTrue(window.UIFromfileButton.Exists);
            //Assert.IsTrue(window.UIBackButton.Exists);

            //Mouse.Click(window.UIFromfileButton);
            //Mouse.Click(window.UIGridViewList.UIATestsGroup.UIToLargeImageListItem);
            //Mouse.Click(window.UIChoiceBasketToolBar.UIOpenButton);

        }

        /// <summary>
        /// Test New -> Capture From Camera, simple capturing test
        /// This test requires access to the camera settings otherwise it fails
        /// </summary>
        [TestMethod]
        public void CaptureFromCameraWithoutExtraOptions()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UICapturefromcameraButton);

            Assert.IsTrue(window.UICameraWindow.Exists);
            Assert.IsTrue(window.UICameraWindow.UIPreviewAreaListItem.Exists);
            Assert.IsTrue(window.UICameraWindow.UIPreviewAreaListItem.Enabled);
            Assert.IsTrue(window.UICameraWindow.UICloseButton.Exists);
            Assert.IsTrue(window.UIIdCaptureCommandBarToolBar.Exists);
            Assert.IsTrue(window.UIIdCaptureCommandBarToolBar.UITimepieceButton.Exists);
            Assert.IsTrue(window.UIIdCaptureCommandBarToolBar.UIOptionsButton.Exists);
            Assert.IsTrue(window.UIIdCaptureCommandBarToolBar.UISwitchonvcButton.Exists);

            Mouse.Click(window.UICameraWindow.UIPreviewAreaListItem);

            Assert.IsTrue(window.UICameraWindow.UITrimImageTool.Exists);
            Assert.IsTrue(window.UIIdPhotoCommandBarToolBar.UIAcceptCurrentPhotoButton.Exists);
            Assert.IsTrue(window.UIIdPhotoCommandBarToolBar.UITakePictureAgainButton.Exists);

            Mouse.Click(window.UIIdPhotoCommandBarToolBar.UIAcceptCurrentPhotoButton);

            Assert.IsTrue(window.UIScrollViewerPanel.UIImageImage.Exists);

        }

        /// <summary>
        /// Test New -> Capture From Camera, checking all possible options
        /// This test requires access to the camera settings otherwise it fails
        /// </summary>
        [TestMethod]
        public void CaptureFromCameraWithExtraOptions()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UICapturefromcameraButton);

            Point middle = window.UICameraWindow.UIPreviewAreaListItem.GetClickablePoint();

            Mouse.Click(window.UIIdCaptureCommandBarToolBar.UIOptionsButton);

            Assert.IsTrue(window.UICameraOptionsGroup.Exists);
            Assert.IsTrue(window.UICameraOptionsWindow.Exists);
            Assert.IsTrue(window.UICameraOptionsWindow.UIResolutionSelectorComboBox.Exists);
            Assert.IsTrue(window.UICameraOptionsWindow.UIVideoStabilizationSlider.Exists);
            Assert.IsTrue(window.UICameraOptionsWindow.UIMoreHyperlink.Exists);

            Mouse.Click(window.UICameraOptionsWindow.UIVideoStabilizationSlider);
            Mouse.Click(window.UICameraOptionsWindow.UIResolutionSelectorComboBox);
            Mouse.Click(window.UITouchSelect_ItemListList.UIItem01MP43ListItem);
            Mouse.Click(window.UICameraOptionsWindow.UIResolutionSelectorComboBox);
            Mouse.Click(window.UITouchSelect_ItemListList.UIItem03MP43ListItem);

            Mouse.Click(window.UICameraOptionsWindow.UIMoreHyperlink);

            Assert.IsTrue(window.UICameraOptionsWindow.UIContrastManualSelectSlider.Exists);
            Assert.IsTrue(window.UICameraOptionsWindow.UIFrequencyFlickerComboBox.Exists);
            Assert.IsTrue(window.UICameraOptionsWindow.UIBrightnessManualSeleSlider.Exists);

            Mouse.StartDragging(window.UICameraOptionsWindow.UIBrightnessManualSeleSlider);
            Mouse.Move(window.UICameraOptionsWindow.UIBrightnessManualSeleSlider, new Point(70,0));
            Mouse.Move(window.UICameraOptionsWindow.UIBrightnessManualSeleSlider, new Point(200, 0));
            Mouse.StopDragging(window.UICameraOptionsWindow.UIBrightnessManualSeleSlider, new Point(200, 0));

            Mouse.StartDragging(window.UICameraOptionsWindow.UIContrastManualSelectSlider);
            Mouse.Move(window.UICameraOptionsWindow.UIBrightnessManualSeleSlider, new Point(220, 0));
            Mouse.Move(new Point(GetXCord(100), 0));
            Mouse.Move(window.UICameraOptionsWindow.UIBrightnessManualSeleSlider, new Point(30, 0));
            Mouse.StopDragging(window.UICameraOptionsWindow.UIBrightnessManualSeleSlider, new Point(150, 0));

            Mouse.Click(window.UICameraOptionsWindow.UIFrequencyFlickerComboBox);
            Assert.IsTrue(window.UITouchSelect_ItemListList.UIItem60HzListItem.Exists);
            Mouse.Click(window.UITouchSelect_ItemListList.UIItem60HzListItem);
            Mouse.Click(window.UICameraOptionsWindow.UIFrequencyFlickerComboBox);
            Assert.IsTrue(window.UITouchSelect_ItemListList.UIItem50HzListItem.Exists);
            Mouse.Click(window.UITouchSelect_ItemListList.UIItem50HzListItem);

            Mouse.Click(middle);
            Mouse.Click(window.UICameraWindow.UIPreviewAreaListItem);
            Mouse.Click(window.UIIdPhotoCommandBarToolBar.UITakePictureAgainButton);
            Mouse.Click(window.UICameraWindow.UIPreviewAreaListItem);

            Mouse.StartDragging(window.UICameraWindow.UITrimImageTool);
            Mouse.Move(new Point(GetXCord(100), 0));
            Mouse.Move(new Point(GetXCord(600), GetYCord(200)));
            Mouse.Move(new Point(GetXCord(200), GetYCord(600))); 
            Mouse.StopDragging(window.UICameraWindow.UITrimImageTool);

            Mouse.Click(window.UIIdPhotoCommandBarToolBar.UIAcceptCurrentPhotoButton);

            Assert.IsTrue(window.UIScrollViewerPanel.UIImageImage.Exists);

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
