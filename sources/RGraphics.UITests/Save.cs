﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace RGraphics.UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class Save : UITestBase
    {
        public Save()
        {
        }


        /// <summary>
        /// Test Save button, checking all controls
        /// </summary>
        [TestMethod]
        public void SaveButton()
        {
            ShowAppMenu();
            Mouse.Click(window.UISaveButton);

            Assert.IsTrue(window.UILocationToolBar.UIOneDriveDocumentsButton.Exists);
            Assert.IsTrue(window.UITaskToolBar.UIGoUpButton.Exists);
            Assert.IsTrue(window.UIGridViewList.UIATestsGroup.Exists);
            Assert.IsTrue(window.UIEditFilenameGroup.Exists);
            Assert.IsTrue(window.UIEditFilenameGroup.UIEditFilename.Exists);
            Assert.IsTrue(window.UISavingToolBar.Exists);
            Assert.IsTrue(window.UISavingToolBar.UIFileTypesComboBox.Exists);
            Assert.IsTrue(window.UISavingToolBar.UICancelButton.Exists);
            Assert.IsTrue(window.UISavingToolBar.UISaveFileButton.Exists);

            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Assert.IsTrue(window.UIQuickAccessLinksMenu.UIOneDriveListItem.Exists);
            Assert.IsTrue(window.UIQuickAccessLinksMenu.UINetworkListItem.Exists);
            Assert.IsTrue(window.UIQuickAccessLinksMenu.UIThisPCListItem.Exists);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem.UIProp1Edit);

            Mouse.Click(window.UISavingToolBar.UICancelButton);

            Assert.IsTrue(window.UIScrollViewerPanel.UIImageImage.Exists);
        }

        /// <summary>
        /// Test Save with , validating file saving
        /// </summary>
        [TestMethod]
        public void SaveImage()
        {
            ShowAppMenu();
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("rgTest");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton); 
            }
            catch (Exception)
            {

            }

            Assert.IsTrue(window.UIScrollViewerPanel.UIImageImage.Exists);

            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            Assert.IsTrue(window.UIGridViewList.UIATestsGroup.UIRgTestListItem.Exists);
            Assert.AreEqual("rgTest", window.UIGridViewList.UIATestsGroup.UIRgTestListItem.Name);

        }

        /// <summary>
        /// Test Save, validating file names with illegal characters
        /// </summary>
        [TestMethod]
        public void SaveFileNames()
        {
            ShowAppMenu();
            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("a1nv//:3n:uu  |||<>a<>b");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            try
            {
                Mouse.Click(window.UIButtonBarToolBar.UIYesButton);
            }
            catch (Exception)
            {

            }


            Mouse.Click(window.UISaveButton);
            Mouse.Click(window.UILocationToolBar.UIOneDriveDocumentsButton);
            Mouse.Click(window.UIQuickAccessLinksMenu.UIThisPCListItem);
            Mouse.Click(window.UIGridViewList.UIATestsGroup.UIPulpitListItem);

            Assert.IsTrue(window.UIGridViewList.UIATestsGroup.UIA1nv3nuuabListItem.Exists);
            Assert.AreEqual("a1nv3nuu  ab", window.UIGridViewList.UIATestsGroup.UIA1nv3nuuabListItem.Name);

            window.UIEditFilenameGroup.UIEditFilename.SetFocus();
            window.UIEditFilenameGroup.UIEditFilename.WaitForControlEnabled();

            Keyboard.SendKeys("a1nv3nuu  ab");

            Mouse.Click(window.UISavingToolBar.UISaveFileButton);

            Mouse.Click(window.UIButtonBarToolBar.UINoButton);

            Mouse.Click(window.UISavingToolBar.UICancelButton);

            Assert.IsTrue(window.UIScrollViewerPanel.TryFind());
        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
