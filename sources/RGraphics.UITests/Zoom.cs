﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace RGraphics.UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class Zoom : UITestBase
    {
        public Zoom()
        {
        }

        /// <summary>
        /// Test Zoom, zooming image and scrolling when zoomed
        /// </summary>
        [TestMethod]
        public void ZoomImage()
        {
            ShowAppMenu();
            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);

            window.UIInputWidthEdit.SetFocus();
            window.UIInputWidthEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            window.UIInputHeightEdit.SetFocus();
            window.UIInputHeightEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            Mouse.Click(window.UIApplyButton);
            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIShapeListItem);

            Mouse.Move(new Point(GetXCord(800), GetYCord(20)));
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(GetXCord(683)-200, GetYCord(384)-200));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 200, GetYCord(384) + 200));

            Mouse.Move(new Point(GetXCord(683) - 100, GetYCord(384) - 100));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 100, GetYCord(384) + 100));

            Mouse.Move(new Point(GetXCord(683) - 20, GetYCord(384) - 20));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 20, GetYCord(384) + 20));

            Mouse.Click(window.UIItemList.UIPencilListItem);
            Mouse.Click(new Point(GetXCord(683), GetYCord(384)));

            Mouse.Move(new Point(GetXCord(683), GetYCord(384)));
            Keyboard.PressModifierKeys(ModifierKeys.Control);
            for (int i = 0; i < 30; ++i)
            {
                Mouse.MoveScrollWheel(2);
                Playback.Wait(300);
            }
            Keyboard.ReleaseModifierKeys(ModifierKeys.Control);

            for (int i = 0; i < 30; ++i)
            {
                Mouse.MoveScrollWheel(6);
                Playback.Wait(100);
            }

            for (int i = 0; i < 40; ++i)
            {
                Mouse.MoveScrollWheel(-6);
                Playback.Wait(100);
            }

            Keyboard.PressModifierKeys(ModifierKeys.Control);

            for (int i = 0; i < 40; ++i)
            {
                Mouse.MoveScrollWheel(-2);
                Playback.Wait(300);
            }

            Keyboard.ReleaseModifierKeys(ModifierKeys.Control);

            

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
