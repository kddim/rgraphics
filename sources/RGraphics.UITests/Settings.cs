﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace RGraphics.UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class Settings : UITestBase
    {
        public Settings()
        {
        }

        /// <summary>
        /// Test Settings, checking all controls
        /// </summary>
        [TestMethod]
        public void SettingsButton()
        {
            UISettingsWindow settings = new UISettingsWindow();
            ShowAppMenu();
            Mouse.Click(window.UISettingsButton);
            Assert.IsTrue(settings.UISettingsList.Exists);

            Mouse.Move(settings.UISettingsList.UIDrawingtoolsListItem, new Point(10,10));
            Assert.IsTrue(settings.UISettingsList.UIDrawingtoolsListItem.Exists);

            Mouse.Move(settings.UISettingsList.UIGeneralListItem, new Point(10, 10));
            Assert.IsTrue(settings.UISettingsList.UIGeneralListItem.Exists);

            Mouse.Move(settings.UISettingsList.UIHistogramListItem, new Point(10, 10));
            Assert.IsTrue(settings.UISettingsList.UIHistogramListItem.Exists);

            Mouse.Move(settings.UISettingsList.UIPowersListItem, new Point(10, 10));
            Assert.IsTrue(settings.UISettingsList.UIPowersListItem.Exists);

            Mouse.Move(settings.UISettingsList.UIPowersListItem, new Point(10, 10));
            Assert.IsTrue(settings.UISettingsList.UIPowersListItem.Exists);

            Assert.IsTrue(settings.UINetworkButton.Exists);
            Mouse.Move(settings.UINetworkButton, new Point(10, 10));

            Assert.IsTrue(settings.UIVoiceButton.Exists);
            Mouse.Move(settings.UIVoiceButton, new Point(10, 10));

            Assert.IsTrue(settings.UIBrightnessButton.Exists);
            Mouse.Move(settings.UIBrightnessButton, new Point(10, 10));

            Assert.IsTrue(settings.UIMessagesSlider.Exists);
            Mouse.Move(settings.UIMessagesSlider, new Point(10, 10));

            Assert.IsTrue(settings.UIPowerOptionsButton.Exists);
            Mouse.Move(settings.UIPowerOptionsButton, new Point(10, 10));

            Assert.IsTrue(settings.UIKeyboardButton.Exists);
            Mouse.Move(settings.UIKeyboardButton, new Point(10, 10));

        }

        /// <summary>
        /// Test Settings -> General, checking all possibilities
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void GeneralSettings()
        {
            UISettingsWindow settings = new UISettingsWindow();
            ShowAppMenu();

            Mouse.Click(window.UISettingsButton);

            Mouse.Click(settings.UISettingsList.UIGeneralListItem);

            Mouse.Move(new Point(GetXCord(1200), GetYCord(150))); //width
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Keyboard.SendKeys("400");

            Mouse.Move(new Point(GetXCord(1200), GetYCord(203))); // height
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Keyboard.SendKeys("100");

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UISettingsButton);

            Mouse.Click(settings.UISettingsList.UIGeneralListItem);

            Mouse.Move(new Point(GetXCord(1200), GetYCord(150)));  //width
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Keyboard.SendKeys("asdf fds20 gfd 4");

            Mouse.Move(new Point(GetXCord(1200), GetYCord(203)));  // height
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Keyboard.SendKeys("999999");

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UISettingsButton);

            Mouse.Click(settings.UISettingsList.UIGeneralListItem);

            //Mouse.Move(new Point(GetXCord(1200), GetYCord(275)));  // max width
            //Mouse.Click();
            //Mouse.Click();
            //Mouse.Click();
            //Keyboard.SendKeys("600");

            //Mouse.Move(new Point(GetXCord(1200), GetYCord(327)));  // max height
            //Mouse.Click();
            //Mouse.Click();
            //Mouse.Click();
            //Keyboard.SendKeys("600");

            Mouse.Move(new Point(GetXCord(1200), GetYCord(150)));  //width
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Keyboard.SendKeys("800");

            Mouse.Move(new Point(GetXCord(1200), GetYCord(203)));  // height
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();
            Keyboard.SendKeys("800");

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UISettingsButton);

            Mouse.Click(settings.UISettingsList.UIGeneralListItem);

            Mouse.Click(window.UIPopupWindow.UIRestoredefaultButton);
      

        }

        /// <summary>
        /// Test Settings -> DrawingTools, chcecking few possibilities
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void DrawToolsSettings()
        {
            UISettingsWindow settings = new UISettingsWindow();
            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIPencilListItem);

            Mouse.Move(new Point(GetXCord(683)-200, GetYCord(384)-200));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 100, GetYCord(384) + 100));

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UIDrawingtoolsListItem);

            Mouse.Move(new Point(GetXCord(1137), GetYCord(150)));
            Mouse.Click();

            Mouse.Move(new Point(GetXCord(1216), GetYCord(366)));
            Mouse.Click();

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIPencilListItem);

            Mouse.Move(new Point(GetXCord(683) - 200, GetYCord(384) - 200));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 100, GetYCord(384) + 100));

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UIDrawingtoolsListItem);

            Mouse.Move(new Point(GetXCord(1137), GetYCord(274)));
            Mouse.Click();

            Mouse.Move(new Point(GetXCord(1262), GetYCord(366)));
            Mouse.Click();

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIPencilListItem);

            Mouse.Move(new Point(GetXCord(683) - 200, GetYCord(384) - 200));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 100, GetYCord(384) + 100));

            Mouse.Click(window.UIItemList.UIShapeListItem);
            Mouse.Move(new Point(GetXCord(683) - 200, GetYCord(384) - 200));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 100, GetYCord(384) + 100));

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UIGeneralListItem);
            Mouse.Click(window.UIPopupWindow.UIRestoredefaultButton);

        }

        /// <summary>
        /// Test Settings -> TextTools, checking few possibilities
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void TextToolSettings()
        {
            UISettingsWindow settings = new UISettingsWindow();
            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UITextListItem);

            Mouse.Click(new Point(GetXCord(683)-100, GetYCord(384)-100));
            Keyboard.SendKeys("TESTTTTT");
            Mouse.Click(window.UIScrollViewerPanel.UITextYesButton);

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UITexttoolListItem);

            Mouse.Move(new Point(GetXCord(1200),GetYCord(150)));
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();

            Keyboard.SendKeys("27");

            Mouse.Click(new Point(GetXCord(1080), GetYCord(220)));
            Mouse.Click(new Point(GetXCord(1080), GetYCord(306)));

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UITextListItem);

            Mouse.Click(new Point(GetXCord(683) - 100, GetYCord(384) - 100));
            Keyboard.SendKeys("TESTTTTT");
            Mouse.Click(window.UIScrollViewerPanel.UITextYesButton);

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UITexttoolListItem);

            Mouse.Move(new Point(GetXCord(1200), GetYCord(150)));
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();

            Keyboard.SendKeys("12");

            Mouse.Click(new Point(GetXCord(1080), GetYCord(220)));
            Mouse.Click(new Point(GetXCord(1080), GetYCord(394)));

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UITextListItem);

            Mouse.Click(new Point(GetXCord(683) - 100, GetYCord(384) - 100));
            Keyboard.SendKeys("TESTTTTT");
            Mouse.Click(window.UIScrollViewerPanel.UITextYesButton);

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UITexttoolListItem);

            Mouse.Move(new Point(GetXCord(1200), GetYCord(150)));
            Mouse.Click();
            Mouse.Click();
            Mouse.Click();

            Keyboard.SendKeys("40");

            Mouse.Click(new Point(GetXCord(1080), GetYCord(220)));
            Mouse.Click(new Point(GetXCord(1080), GetYCord(524)));

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UITextListItem);

            Mouse.Click(new Point(GetXCord(683) - 100, GetYCord(384) - 100));
            Keyboard.SendKeys("TESTTTTT");
            Mouse.Click(window.UIScrollViewerPanel.UITextYesButton);

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UIGeneralListItem);
            Mouse.Click(window.UIPopupWindow.UIRestoredefaultButton);
        }

        /// <summary>
        /// Test Settings -> Histogram, checking few color's sets
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void HistogramSettings()
        {
            UISettingsWindow settings = new UISettingsWindow();
            ShowAppMenu();

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

            ShowAppMenu();

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UIHistogramListItem);

            Mouse.Click(new Point(GetXCord(1080), GetYCord(150)));
            Mouse.Click(new Point(GetXCord(1080), GetYCord(245)));
            Mouse.Click(new Point(GetXCord(1080), GetYCord(340)));

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

            ShowAppMenu();

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UIHistogramListItem);

            Mouse.Click(new Point(GetXCord(1170), GetYCord(150)));
            Mouse.Click(new Point(GetXCord(1260), GetYCord(245)));
            Mouse.Click(new Point(GetXCord(1124), GetYCord(340)));

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

            ShowAppMenu();

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UIHistogramListItem);

            Mouse.Click(new Point(GetXCord(1124), GetYCord(150)));
            Mouse.Click(new Point(GetXCord(1124), GetYCord(245)));
            Mouse.Click(new Point(GetXCord(1124), GetYCord(340)));

            Cleanup();

            Initialize();

            ShowAppMenu();

            Mouse.Click(window.UIHistogramButton);
            Playback.Wait(2000);
            Mouse.Click(window.UIBackButton);

            ShowAppMenu();

            Mouse.Click(window.UISettingsButton);
            Mouse.Click(settings.UISettingsList.UIGeneralListItem);
            Mouse.Click(window.UIPopupWindow.UIRestoredefaultButton);

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
