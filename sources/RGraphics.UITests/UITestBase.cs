﻿using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RGraphics.UITests
{
    /// <summary>
    /// Base class for all UI Test classes
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class UITestBase
    {
        [TestInitialize]
        public void Initialize()
        {
            // Launching app
            // To get AppId: http://blogs.msdn.com/b/visualstudioalm/archive/2013/08/11/launching-a-windows-store-application-using-coded-ui-test-for-windows-store.aspx
            XamlWindow.Launch("bf7bcacd-f5ea-4ad5-9e61-99a639d2cd02_kz15j230kjmx0!App");

            // Initializing RGraphics window (tests have to call controls through this)
            window = new UIRGraphicsWindow();
        }

        [TestCleanup]
        public void Cleanup()
        {
            window.Close();
        }

        public int GetXCord(int x)
        {
            //var bounds = window.Width;// Window.Current.Bounds;
            double width = window.Width; //  bounds.Width;
            double ratio = width / 1366;
            int result = (int)(x*ratio);
            return result;
        }

        public int GetYCord(int y)
        {
            //var bounds = Window.Current.Bounds;
            double height = window.Height; //bounds.Height;
            double ratio = height / 768; // jestes pewien co do obliczania tego z jakims ratio? nie ale sprawdze :D ok
            int result = (int)(y * ratio);
            return result;
        }

        protected UIRGraphicsWindow window;

        /// <summary>
        /// Shows application menu by pressing RightButtonOfMouse
        /// </summary>
        public void ShowAppMenu()
        {
            Mouse.Click(MouseButtons.Right);
        }
    }
}
