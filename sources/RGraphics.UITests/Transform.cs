﻿using System;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.DirectUIControls;
using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace RGraphics.UITests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest(CodedUITestType.WindowsStore)]
    public class Transform : UITestBase
    {
        public Transform()
        {
        }

        /// <summary>
        /// Test Transform, crop images, moving and changing transforming area
        /// This test may not work on device with different screen resolution
        /// </summary>
        [TestMethod]
        public void Crop()
        {
            ShowAppMenu();

            Mouse.Click(window.UINewButton);
            Mouse.Click(window.UIEmptyimageButton);

            window.UIInputWidthEdit.SetFocus();
            window.UIInputWidthEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            window.UIInputHeightEdit.SetFocus();
            window.UIInputHeightEdit.WaitForControlEnabled();
            Keyboard.SendKeys("600");

            Mouse.Click(window.UIApplyButton);

            Mouse.Click(window.UIDrawingtoolsToggleButton);
            Mouse.Click(window.UIItemList.UIShapeListItem);

            Point relative_point = new Point(GetXCord(800), GetYCord(20));

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Move(new Point(GetXCord(683) - 280, GetYCord(384) - 280));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 280, GetYCord(384) + 280));

            Mouse.Move(new Point(GetXCord(683) - 200, GetYCord(384) - 200));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 200, GetYCord(384) + 200));

            Mouse.Move(new Point(GetXCord(683) - 100, GetYCord(384) - 100));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 100, GetYCord(384) + 100));

            Mouse.Move(new Point(GetXCord(683) - 20, GetYCord(384) - 20));
            Mouse.StartDragging();
            Mouse.StopDragging(new Point(GetXCord(683) + 20, GetYCord(384) + 20));

            Mouse.Move(relative_point);
            Mouse.Click(MouseButtons.Right);

            Mouse.Click(window.UITransformButton);


            Mouse.Move(new Point(GetXCord(683) - 149, GetYCord(384) - 94));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683), GetYCord(384)+55));
            Mouse.Move(new Point(GetXCord(683) - 149, GetYCord(384) - 94));
            Mouse.StopDragging(new Point(GetXCord(683) - 149, GetYCord(384) - 94));

            Mouse.Move(new Point(GetXCord(683) + 149, GetYCord(384) + 197));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683) + 200, GetYCord(384) + 248));
            Mouse.StopDragging(new Point(GetXCord(683) + 200, GetYCord(384) + 248));


            Mouse.Move(new Point(GetXCord(683), GetYCord(384)+50));
            Mouse.StartDragging();
            Mouse.Move(new Point(GetXCord(683) - 400, GetYCord(384)+50));
            Mouse.Move(new Point(GetXCord(683) - 400, GetYCord(384) - 350));
            Mouse.Move(new Point(GetXCord(683) + 400, GetYCord(384) - 350));
            Mouse.Move(new Point(GetXCord(683) - 400, GetYCord(384) + 400));
            Mouse.StopDragging(new Point(GetXCord(683) - 400, GetYCord(384) + 400));

            Mouse.Click(window.UIApplyCrop);

            Playback.Wait(1000);

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
