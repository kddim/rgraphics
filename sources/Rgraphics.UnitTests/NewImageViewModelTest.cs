﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Foundation;
using RGraphics.Common;
using Windows.UI;
using RGraphics.ViewModels;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Models;
using UI = Microsoft.VisualStudio.TestPlatform.UnitTestFramework.AppContainer;
using Caliburn.Micro;

namespace Rgraphics.UnitTests
{
    [TestClass]
    public class NewImageViewModelTest
    {
        private Bootstrapper bootstrapper;
        private NewImageViewModel VM;

        [TestInitialize]
        public void Initialize()
        {
            bootstrapper = new Bootstrapper();
            TestUtils.ExecuteOnUIThreadSync(() =>
                VM = IoC.Get<NewImageViewModel>()
            );
        }

        [DataTestMethod]
        [DataRow(false)]
        [DataRow(true)]
        public void IsOpen_GetSet(bool expected)
        {
            VM.IsOpen = expected;
            Assert.AreEqual(expected, VM.IsOpen);
        }


        [DataTestMethod]
        [DataRow("400")]
        [DataRow("-211")]
        [DataRow("asd")]
        public void NewImageWidth_SetGet(String Width)
        {
            VM.NewImageWidth = Width;
            Assert.AreEqual(Width, VM.NewImageWidth);
        }

        [DataTestMethod]
        [DataRow("400")]
        [DataRow("-213")]
        [DataRow("--")]
        public void NewImageHeight_SetGet(String Height)
        {
            VM.NewImageHeight = Height;
            Assert.AreEqual(Height, VM.NewImageHeight);
        }

        [DataTestMethod]
        [DataRow("400", "500")]
        [DataRow("500", "40")]
        [DataRow("cant parse this", "cant parse this")]
        [DataRow("", "")]
        [DataRow("null", "null")]
        public void ValidateSize_NoChangeExpected(String Width, String Height)
        {
            Width = Width.NullProxy();
            Height = Height.NullProxy();

            VM.NewImageWidth = Width;
            VM.NewImageHeight = Height;

            TestUtils.ExecuteOnUIThreadSync(() =>
                VM.ValidateSize()
            );

            Assert.AreEqual(Width, VM.NewImageWidth);
            Assert.AreEqual(Height, VM.NewImageHeight);
        }

        [DataTestMethod]
        [DataRow("50000", "40000")]
        public void ValidateSize_ChangeExpected(String Width, String Height)
        {
            VM.NewImageWidth = Width;
            VM.NewImageHeight = Height;

            TestUtils.ExecuteOnUIThreadSync(() =>
                VM.ValidateSize()
            );

            Assert.AreEqual("3000", VM.NewImageWidth);
            Assert.AreEqual("2500", VM.NewImageHeight);
        }

        [DataTestMethod]
        //[DataRow("", "")]
        //[DataRow("null", "")]
        //[DataRow("", "null")]
        //[DataRow("null", "null")]
        [DataRow("523", "null")]
        [DataRow("null", "523")]
        [DataRow("523", "")]
        [DataRow("", "523")]

        public void IsNewImageApplyEnabled_FalseExpected(String Width, String Height)
        {
            Width = Width.NullProxy();
            Height = Height.NullProxy();

            VM.NewImageWidth = Width;
            VM.NewImageHeight = Height;

            Assert.AreEqual(false, VM.IsNewImageApplyEnabled);
        }

        [DataTestMethod]
        [DataRow("523", "612")]
        public void IsNewImageApplyEnabled_TrueExpected(String Width, String Height)
        {
            VM.NewImageWidth = Width;
            VM.NewImageHeight = Height;

            Assert.AreEqual(true, VM.IsNewImageApplyEnabled);
        }

        /// <summary>
        /// Testing NewImageApply() fail scenario
        /// </summary>
        /// <param name="Width">new image width</param>
        /// <param name="Height">new image height</param>
        [DataTestMethod]
        [DataRow("cant parse this", "320")]
        [DataRow("512", "cant parse this")]
        [DataRow("", "531")]
        [DataRow("531", "")]
        [DataRow("cant parse this", "")]
        [DataRow("", "cant parse this")]
        [DataRow("", "null")]
        [DataRow("null", "")]
        [DataRow("null", "cant parse this")]
        [DataRow("cant parse this", "null")]
        [DataRow("null", "123")]
        [DataRow("123", "null")]
        [DataRow("null", "null")]
        [DataRow("", "")]
        [DataRow("-531", "531")]
        [DataRow("531", "-531")]
        [DataRow("531", "0")]
        [DataRow("0", "151")]
        [DataRow("0", "0")]
        public void NewImageApply_ExceptionExpected(String Width, String Height)
        {
            Width = Width.NullProxy();
            Height = Height.NullProxy();

            VM.NewImageWidth = Width;
            VM.NewImageHeight = Height;

            Exception e = null;
            try
            { VM.NewImageApply(); }
            catch (Exception ex)
            { e = ex; }

            Assert.IsNotNull(e);
            Assert.IsNull(VM.Result);
            Assert.AreEqual(false, VM.IsOpen);
        }


        [UI.UITestMethod]
        public void NewImageApply_PassExpected()
        {
            const int expW = 5;
            const int expH = 10;
            VM.NewImageWidth = expW.ToString();
            VM.NewImageHeight = expH.ToString();

            VM.NewImageApply();

            Assert.AreEqual(false, VM.IsOpen);
            Assert.IsNotNull(VM.Result);

            Assert.IsInstanceOfType(VM.Result, typeof(WriteableBitmap));
            WriteableBitmap wb = VM.Result as WriteableBitmap;

            Assert.AreEqual(expW, wb.PixelWidth);
            Assert.AreEqual(expH, wb.PixelHeight);

            foreach (byte b in wb.ToByteArray())
                Assert.AreEqual(255, b);
        }

        [UI.UITestMethod]
        public void ImageOnStartupTest()
        {
            VM.ImageOnStartup();
            WriteableBitmap wb = VM.Result as WriteableBitmap;
            Assert.IsNotNull(wb);
            Assert.AreEqual(IoC.Get<Settings>().ImageWidthOnStartup, wb.PixelWidth);
            Assert.AreEqual(IoC.Get<Settings>().ImageHeightOnStartup, wb.PixelHeight);
            Assert.IsFalse(VM.IsOpen);
        }

        // TODO
        //[TestMethod]
        //public async Task LoadFileTest()
        //{
        //    await TestUtils.ExecuteOnUIThread(
        //        () =>
        //        {
        //            var asyncOp = StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///images/loadFromFile.png"));
        //            asyncOp.AsTask().Wait();
        //            StorageFile file = asyncOp.GetResults();
        //            asyncOp.Close();
        //            Assert.IsNotNull(file);

        //            //VM.LoadFile(file).Wait();

        //            Assert.AreEqual(false, VM.IsOpen);

        //            // ImageTest.png
        //            // 4x2

        //            WriteableBitmap wb = VM.Result as WriteableBitmap;
        //            Assert.IsNotNull(wb);

        //            Assert.AreEqual(4, (wb.PixelWidth));
        //            Assert.AreEqual(2, (wb.PixelHeight));

        //            byte[] image = wb.ToByteArray();

        //            const int channels = 4;
        //            Func<int, int, Channel, int> GetIndex =
        //                (int x, int y, Channel ch) =>
        //                {
        //                    return y * wb.PixelWidth * channels + x * channels + (int)ch;
        //                };

        //            // RGBA pixels
        //            #region First row = (0, 0, 0, 0), (255, 0, 0, 0), (34, 177, 76, 0), (0, 162, 232, 0)
        //            /// 1st pixel: (0, 0, 0, 0)
        //            Assert.AreEqual(0, image[GetIndex(0, 0, Channel.Red)]);
        //            Assert.AreEqual(0, image[GetIndex(0, 0, Channel.Green)]);
        //            Assert.AreEqual(0, image[GetIndex(0, 0, Channel.Blue)]);
        //            Assert.AreEqual(0, image[GetIndex(0, 0, Channel.Alpha)]);

        //            /// 2nd pixel: (255, 0, 0, 0)
        //            Assert.AreEqual(255, image[GetIndex(1, 0, Channel.Red)]);
        //            Assert.AreEqual(0, image[GetIndex(1, 0, Channel.Green)]);
        //            Assert.AreEqual(0, image[GetIndex(1, 0, Channel.Blue)]);
        //            Assert.AreEqual(0, image[GetIndex(1, 0, Channel.Alpha)]);

        //            /// 3rd pixel: (34, 177, 76, 0)
        //            Assert.AreEqual(34, image[GetIndex(2, 0, Channel.Red)]);
        //            Assert.AreEqual(177, image[GetIndex(2, 0, Channel.Green)]);
        //            Assert.AreEqual(76, image[GetIndex(2, 0, Channel.Blue)]);
        //            Assert.AreEqual(0, image[GetIndex(2, 0, Channel.Alpha)]);

        //            /// 4th pixel: (0, 162, 232, 0)
        //            Assert.AreEqual(0, image[GetIndex(3, 0, Channel.Red)]);
        //            Assert.AreEqual(162, image[GetIndex(3, 0, Channel.Green)]);
        //            Assert.AreEqual(232, image[GetIndex(3, 0, Channel.Blue)]);
        //            Assert.AreEqual(0, image[GetIndex(3, 0, Channel.Alpha)]);
        //            #endregion

        //            #region Second row = (127, 127, 127, 0), (127, 127, 127, 0), (128, 0, 0, 0), (128, 0, 0, 0)
        //            /// 1st pixel: (127, 127, 127, 0)
        //            Assert.AreEqual(127, image[GetIndex(0, 1, Channel.Red)]);
        //            Assert.AreEqual(127, image[GetIndex(0, 1, Channel.Green)]);
        //            Assert.AreEqual(127, image[GetIndex(0, 1, Channel.Blue)]);
        //            Assert.AreEqual(0, image[GetIndex(0, 1, Channel.Alpha)]);

        //            /// 2nd pixel: (127, 127, 127, 0)
        //            Assert.AreEqual(127, image[GetIndex(1, 1, Channel.Red)]);
        //            Assert.AreEqual(127, image[GetIndex(1, 1, Channel.Green)]);
        //            Assert.AreEqual(127, image[GetIndex(1, 1, Channel.Blue)]);
        //            Assert.AreEqual(0, image[GetIndex(1, 1, Channel.Alpha)]);

        //            /// 3rd pixel: (128, 0, 0, 0)
        //            Assert.AreEqual(128, image[GetIndex(2, 1, Channel.Red)]);
        //            Assert.AreEqual(0, image[GetIndex(2, 1, Channel.Green)]);
        //            Assert.AreEqual(0, image[GetIndex(2, 1, Channel.Blue)]);
        //            Assert.AreEqual(0, image[GetIndex(2, 1, Channel.Alpha)]);

        //            /// 4th pixel: (128, 0, 0, 0)
        //            Assert.AreEqual(128, image[GetIndex(3, 1, Channel.Red)]);
        //            Assert.AreEqual(0, image[GetIndex(3, 1, Channel.Green)]);
        //            Assert.AreEqual(0, image[GetIndex(3, 1, Channel.Blue)]);
        //            Assert.AreEqual(0, image[GetIndex(3, 1, Channel.Alpha)]);
        //            #endregion
        //        });
        //}
    }
}
