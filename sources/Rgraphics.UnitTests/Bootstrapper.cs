﻿using Caliburn.Micro;
using RGraphics.Models;
using RGraphics.ViewModels;
using RGraphics.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;

namespace Rgraphics.UnitTests
{
    public class Bootstrapper
    {
        private WinRTContainer Container { get; set; }
        public Bootstrapper()
        {
            Container = new WinRTContainer();
            Container
                .Singleton<ShellViewModel>()
                .Singleton<ShellView>()
                .Singleton<ResourceLoader>()
                .Singleton<Settings>()
                .Singleton<IEventAggregator, EventAggregator>()
                .PerRequest<NewImageViewModel>()
                .PerRequest<IWorkspace, Workspace>()
                .PerRequest<ProgressReportDialogViewModel>();

            IoC.GetInstance = Container.GetInstance;
            IoC.GetAllInstances = Container.GetAllInstances;
            IoC.BuildUp = Container.BuildUp;
        }
    }
}
