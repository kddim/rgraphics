﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Models;
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Foundation;
using RGraphics.Common;
using Windows.UI;
using Windows.UI.Input.Inking;
using Caliburn.Micro;

namespace Rgraphics.UnitTests
{
    [TestClass]
    public class WorkspaceTest
    {
        IWorkspace Workspace;
        private Bootstrapper bootstrapper;

        [TestInitialize]
        public void Initialize()
        {
            bootstrapper = new Bootstrapper();

            TestUtils.ExecuteOnUIThreadSync(() =>
                Workspace = IoC.Get<IWorkspace>()
            );

            ////// This event is called from the zoom public methods
            ////// Normally it is attached by ScrollViewerBehavior to ScrollViewer control

            Workspace.ZoomChanged += (object sender, ZoomChangedEventArgs e) =>
            {
                Workspace.ZoomFactor = e.ZoomFactor;
            };
        }

        [TestMethod]
        public void ZoomChangedEventArgsTests()
        {
            const double zoomFactor = 5.0;
            double? horizontalOffset = null;
            double? verticalOffset = 213.312;

            ZoomChangedEventArgs eventArgs = new ZoomChangedEventArgs(
                zoomFactor,
                horizontalOffset,
                verticalOffset
            );

            Assert.AreEqual(zoomFactor, eventArgs.ZoomFactor);
            Assert.AreEqual(horizontalOffset, eventArgs.HorizontalOffset);
            Assert.AreEqual(verticalOffset, eventArgs.VerticalOffset);
        }

        [DataTestMethod]
        [DataRow(2, 3)]
        [DataRow(-2.4, 3.5)]
        public void SizeChangedEventArgsTests(double Width, double Height)
        {
            SizeChangedEventArgs sc = new SizeChangedEventArgs(Width, Height);
            Assert.AreEqual(Width, sc.NewWidth);
            Assert.AreEqual(Height, sc.NewHeight);
        }

        [DataTestMethod]
        [DataRow(6, 11)]
        [DataRow(0.0, 0.0)]
        [DataRow(-3.5, -6.5)]
        public void ActualSize_GetSet(double expectedWidth, double expectedHeight)
        {
            Workspace.UpdateActualSize(expectedWidth, expectedHeight);

            Assert.AreEqual(expectedWidth, Workspace.ActualWidth);
            Assert.AreEqual(expectedHeight, Workspace.ActualHeight);
        }

        [DataTestMethod]
        [DataRow(6, 11)]
        [DataRow(0.0, 0.0)]
        [DataRow(-3.5, -6.5)]
        public void WorkspaceSize_GetSet(double expectedWidth, double expectedHeight)
        {
            Workspace.UpdateWorkspaceSize(expectedWidth, expectedHeight);

            Assert.AreEqual(expectedWidth, Workspace.WorkspaceWidth);
            Assert.AreEqual(expectedHeight, Workspace.WorkspaceHeight);
        }

        [TestMethod]
        public void ZoomToFitWorkspace_Case1()
        {
            const double expectedActualWidth = 200.0;
            const double expectedActualHeight = 300.0;
            Workspace.UpdateActualSize(expectedActualWidth, expectedActualHeight);

            const double expectedWorkspaceWidth = 150;
            const double expectedWorkspaceHeight = 250;
            Workspace.UpdateWorkspaceSize(expectedWorkspaceWidth, expectedWorkspaceHeight);

            Workspace.ZoomToFitWorkspace();
            Assert.AreEqual(expectedWorkspaceWidth / expectedActualWidth, Workspace.ZoomFactor);
        }

        [TestMethod]
        public void ZoomToFitWorkspace_Case2()
        {
            const double expectedActualWidth2 = 200.0;
            const double expectedActualHeight2 = 300.0;
            Workspace.UpdateActualSize(expectedActualWidth2, expectedActualHeight2);

            const double expectedWorkspaceWidth2 = 500;
            const double expectedWorkspaceHeight2 = 250;
            Workspace.UpdateWorkspaceSize(expectedWorkspaceWidth2, expectedWorkspaceHeight2);

            Workspace.ZoomToFitWorkspace();
            Assert.AreEqual(expectedWorkspaceHeight2 / expectedActualHeight2, Workspace.ZoomFactor);
        }

        [TestMethod]
        public void ZoomToFitWorkspace_Case3()
        {
            const double expectedActualWidth3 = 200.0;
            const double expectedActualHeight3 = 300.0;
            Workspace.UpdateActualSize(expectedActualWidth3, expectedActualHeight3);

            const double expectedWorkspaceWidth3 = 500;
            const double expectedWorkspaceHeight3 = 500;
            Workspace.UpdateWorkspaceSize(expectedWorkspaceWidth3, expectedWorkspaceHeight3);

            Assert.AreEqual(expectedWorkspaceWidth3, Workspace.WorkspaceWidth);
            Assert.AreEqual(expectedWorkspaceHeight3, Workspace.WorkspaceHeight);

            Workspace.ZoomToFitWorkspace();
            Assert.AreEqual(1.0, Workspace.ZoomFactor);
        }

        [TestMethod]
        public void Zoom_UpperLeft()
        {
            const double zoomFactor = 5.0;

            const double actualWidth = 200;
            const double actualHeight = 500;
            Workspace.UpdateActualSize(actualWidth, actualHeight);

            const double workWidth = 350;
            const double workHeight = 450;
            Workspace.UpdateWorkspaceSize(workWidth, workHeight);

            Workspace.ZoomChanged += (object sender, ZoomChangedEventArgs e) =>
                {
                    Assert.AreEqual(0, e.HorizontalOffset);
                    Assert.AreEqual(0, e.VerticalOffset);
                };

            Workspace.Zoom(zoomFactor, WorkspacePart.UpperLeft);
        }

        [TestMethod]
        public void Zoom_00()
        {
            const double zoomFactor = 5.0;

            const double actualWidth = 200;
            const double actualHeight = 500;
            Workspace.UpdateActualSize(actualWidth, actualHeight);

            const double workWidth = 350;
            const double workHeight = 450;
            Workspace.UpdateWorkspaceSize(workWidth, workHeight);

            Workspace.ZoomChanged += (object sender, ZoomChangedEventArgs e) =>
                {
                    Assert.AreEqual(0, e.HorizontalOffset);
                    Assert.AreEqual(0, e.VerticalOffset);
                };

            Workspace.Zoom(zoomFactor, 0, 0);
        }

        [TestMethod]
        public void Zoom_Center()
        {
            const double zoomFactor = 5.0;

            const double actualWidth = 200;
            const double actualHeight = 500;
            Workspace.UpdateActualSize(actualWidth, actualHeight);

            const double workWidth = 350;
            const double workHeight = 450;
            Workspace.UpdateWorkspaceSize(workWidth, workHeight);

            const double expectedHorizOffset = actualWidth * zoomFactor / 2.0 - workWidth / 2.0;
            const double expectedVertOffset = actualHeight * zoomFactor / 2.0 - workHeight / 2.0;

            Workspace.ZoomChanged += (object sender, ZoomChangedEventArgs e) =>
                {
                    Assert.AreEqual(expectedHorizOffset, e.HorizontalOffset);
                    Assert.AreEqual(expectedVertOffset, e.VerticalOffset);
                };

            Workspace.Zoom(zoomFactor, WorkspacePart.Center);
        }

        [TestMethod]
        public void RefreshUI()
        {
            bool done = false;

            Workspace.UIRefreshed +=
            (object sender, EventArgs e) =>
            {
                done = true;
            };

            Workspace.RefreshUI();

            Assert.IsTrue(done);
        }

        [DataTestMethod]
        [DataRow(5.5)]
        [DataRow(-23.3)]
        [DataRow(0.0)]
        public void ZoomFactor_GetSet(double expected)
        {
            Workspace.ZoomFactor = expected;
            Assert.AreEqual(expected, Workspace.ZoomFactor);
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public void IsZoomEnabled_GetSet(bool expected)
        {
            Workspace.IsZoomEnabled = expected;
            Assert.AreEqual(expected, Workspace.IsZoomEnabled);
        }

        [DataTestMethod]
        [DataRow(5.5)]
        [DataRow(-23.3)]
        [DataRow(0.0)]
        public void MinZoomFactor_GetSet(double expected)
        {
            Workspace.MinZoomFactor = expected;
            Assert.AreEqual(expected, Workspace.MinZoomFactor);
        }

        [DataTestMethod]
        [DataRow(5.5)]
        [DataRow(-23.3)]
        [DataRow(0.0)]
        public void MaxZoomFactor_GetSet(double expected)
        {
            Workspace.MaxZoomFactor = expected;
            Assert.AreEqual(expected, Workspace.MaxZoomFactor);
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public void IsClipToBoundsEnabled_GetSet(bool expected)
        {
            Workspace.IsClipToBoundsEnabled = expected;
            Assert.AreEqual(expected, Workspace.IsClipToBoundsEnabled);
        }

        [TestMethod] // testmethod bcoz Colors cant be created in DataRow...
        public void SelectedColor_GetSet()
        {
            Color expected = Colors.Aqua;

            Workspace.SelectedColor = expected;
            Assert.AreEqual(expected, Workspace.SelectedColor);

            expected = Color.FromArgb(255, 111, 22, 33);
            Workspace.SelectedColor = expected;
            Assert.AreEqual(expected, Workspace.SelectedColor);
        }
    }
}
