﻿using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

using RGraphics.Common;
using Windows.Storage;
using Windows.UI.Xaml.Media;
using Windows.Storage.Streams;

namespace Rgraphics.UnitTests
{
    [TestClass]
    public class ExtensionsTest
    {
        /* Tests list (01.06.2014 date):
         *  GetFonts                        - done?
         *  ShowDialog                      - TODO: coded ui?
         *  ShowDialogAsync                 - TODO: coded ui?
         *  CreateWriteableBitmapFromFile   - to fix (makes deadlock)
         *  Save(output, encoder)           - TODO
         *  Save(output)                    - to fix (makes deadlock)
         *  int.CutToByte                   - done
         *  long.CutToByte                  - done
         *  WriteableBitmap.BAIndex         - done
         *  WriteableBitmap.Length          - done
        */



        [DataTestMethod]
        [DataRow(123)]
        [DataRow(65)]
        [DataRow(-5)]
        [DataRow(-256)]
        [DataRow(25125)]
        [DataRow(256)]
        [DataRow(-1)]
        public void Long_CutToByte(int val)
        {
            byte expected = (byte)Math.Min(Math.Max(0, val), 255);
            Assert.AreEqual(expected, val.CutToByte());
        }

        [DataTestMethod]
        [DataRow(123L)]
        [DataRow(65L)]
        [DataRow(-5L)]
        [DataRow(-256L)]
        [DataRow(25125L)]
        [DataRow(256L)]
        [DataRow(-1L)]
        public void Long_CutToByte(long val)
        {
            byte expected = (byte)Math.Min(Math.Max(0, val), 255);
            Assert.AreEqual(expected, val.CutToByte());
        }



        [DataTestMethod]
        [DataRow(3, 5, 5, 1, 0)]
        [DataRow(5, 4, 1, 3, 2)]
        public void WriteableBitmap_BAIndex(int width, int height, int x, int y, int ch)
        {
            TestUtils.ExecuteOnUIThreadSync(
                () =>
                {
                    WriteableBitmap wb = new WriteableBitmap(width, height);
                    Assert.AreEqual(4 * (width * y + x) + ch, wb.BAIndex(x, y, ch));
                }
            );
        }

        [DataTestMethod]
        [DataRow(5, 5)]
        [DataRow(1, 1)]
        public void WriteableBitmap_Length(int width, int height)
        {
            TestUtils.ExecuteOnUIThreadSync(
                () =>
                {
                    WriteableBitmap wb = new WriteableBitmap(width, height);
                    Assert.AreEqual(width * height * 4, wb.Length());
                }
            );
        }

        // TODO FIX DEADLOCK argh
        //[TestMethod]
        //public void CreateWriteableBitmapFromFileTest()
        //{
        //    Assert.IsNull(true, "This test is doing deadlock");
        //    WriteableBitmap first = null;
        //    WriteableBitmap second = null;
        //    TestUtils.ExecuteOnUIThreadSync(
        //        () =>
        //        {
        //            var asyncOp = StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///images/constructFromFile.png"));
        //            asyncOp.AsTask().Wait();
        //            StorageFile file = asyncOp.GetResults();

        //            Task<WriteableBitmap> first_image_task = Extensions.CreateWriteableBitmapFromFile(file);
        //            first_image_task.Wait();
        //            asyncOp = StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///images/constructFromFile.png"));
        //            asyncOp.AsTask().Wait();
        //            file = asyncOp.GetResults();
        //            Task<WriteableBitmap> second_image_task = Extensions.CreateWriteableBitmapFromFile(file);
        //            second_image_task.Wait();

        //            first = first_image_task.Result;
        //            second = second_image_task.Result;
        //        });

        //    Assert.IsNotNull(first);
        //    Assert.IsNotNull(second);
        //    Assert.IsTrue(TestUtils.AreEqual(first, second));
        //}

       /*[TestMethod]
        public void GetFontsTest()
        {
            List<FontFamily> result_list = null;
            TestUtils.ExecuteOnUIThreadSync(
                 () =>
                 {
                     result_list = Extensions.GetFonts(); 
                 });
            Assert.IsNotNull(result_list);
            Assert.IsTrue(result_list.Count > 0);
        }*/
        
        // TODO: DEADLOCK ...
       // [TestMethod]
       //public void SaveTest()
       //{
       //    WriteableBitmap wb = TestUtils.GetExampleImage();
       //     WriteableBitmap wb_readed = null;
       //     StorageFile file=null;
       //    TestUtils.ExecuteOnUIThreadSync(
       //         () =>
       //         {
       //             var asyncOp = ApplicationData.Current.LocalFolder.CreateFileAsync("test.jpg");
       //             asyncOp.AsTask().Wait();
       //             file = asyncOp.GetResults();
       //             var task = Extensions.Save(wb, file);
                    
       //             task.Wait();
       //             file = null;
       //             asyncOp = ApplicationData.Current.LocalFolder.GetFileAsync("test.jpg");
       //             asyncOp.AsTask().Wait();
       //             file = asyncOp.GetResults();


       //             var asyncOp2 = file.OpenAsync(FileAccessMode.Read);
       //             asyncOp2.AsTask().Wait();
       //             IRandomAccessStream fileStream = asyncOp2.GetResults();

       //             wb_readed.SetSource(fileStream);
       //         });

       //    Assert.IsNotNull(file);
       //    Assert.IsNotNull(wb_readed);
       //    Assert.IsTrue(TestUtils.AreEqual(wb, wb_readed));

       //}
    }
}
