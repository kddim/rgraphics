﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Models;
using Windows.Foundation;
using Windows.UI.Xaml.Media;
using Windows.UI;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using UI = Microsoft.VisualStudio.TestPlatform.UnitTestFramework.AppContainer;
using RGraphics.Models.DrawingTools;
using Caliburn.Micro;

namespace Rgraphics.UnitTests
{
    [TestClass]
    public class TextWriterTest
    {
        private IWorkspace Workspace;
        private Bootstrapper bootstrapper;

        [TestInitialize]
        public void Initialize()
        {
            bootstrapper = new Bootstrapper();
            TestUtils.ExecuteOnUIThreadSync(() =>
                Workspace = IoC.Get<IWorkspace>()
            );
            Workspace.UpdateActualSize(300, 300);
        }

        [TestMethod]
        public void TextWriter_Create()
        {
            TextWriter tw = new TextWriter(Workspace);
        }

        [UI.UITestMethod]
        public void TextWriter_Write()
        {
            Canvas helper = new Canvas();
            Workspace.Children = helper.Children;

            TextWriter tw = new TextWriter(Workspace);

            #region test properties
            Point expectedPoint = new Point( 2.0, 2.0 );
            String expectedText = "TEST123";
            int expectedFontSize = 9;
            FontFamily expectedFontFamily = new FontFamily( "Arial");
            Brush expectedForeground = new SolidColorBrush(Colors.Blue);
            FontStyle expectedFontStyle = FontStyle.Italic;
            FontWeight expectedFontWeight = FontWeights.Normal;
            Thickness expectedThickness = new Thickness(12);
            Thickness expectedBorderThickness = new Thickness(10);
            double expectedWidth = 100;
            double expectedHeight = 50;
            #endregion

            tw.WriteText(expectedPoint, expectedText, expectedFontSize, expectedFontFamily, expectedForeground, 
                expectedFontStyle, expectedFontWeight, expectedThickness, expectedBorderThickness, expectedWidth, expectedHeight);

            TextBox tb = Workspace.Children.ElementAt(0) as TextBox;

            //var ttv = tb.TransformToVisual(Window.Current.Content);
            #region asserts
            //Assert.AreEqual(expectedPoint.X, ttv.TransformPoint(new Point(0, 0)).X);
            Assert.AreEqual(expectedText, tb.Text);
            Assert.AreEqual(expectedFontSize, tb.FontSize);
            Assert.ReferenceEquals(expectedFontFamily, tb.FontFamily);
            Assert.AreEqual(expectedForeground, tb.Foreground);
            Assert.AreEqual(expectedFontStyle, tb.FontStyle);
            Assert.AreEqual(expectedFontWeight, tb.FontWeight);
            Assert.AreEqual(expectedThickness, tb.Margin);
            Assert.AreEqual(expectedBorderThickness, tb.BorderThickness);
            Assert.AreEqual(expectedWidth, tb.Width);
            Assert.AreEqual(expectedHeight, tb.Height);
            #endregion
        }
    }
}
