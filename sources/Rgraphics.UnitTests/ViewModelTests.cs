﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Models;
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Foundation;
using Caliburn.Micro;
using RGraphics.ViewModels;
using UI = Microsoft.VisualStudio.TestPlatform.UnitTestFramework.AppContainer;

namespace Rgraphics.UnitTests
{
    [TestClass]
    public class ViewModelTests
    {
        private Bootstrapper bootstrapper;
        private IWorkspace workspace;

        [TestInitialize]
        public void Initialize()
        {
            bootstrapper = new Bootstrapper();
            TestUtils.ExecuteOnUIThreadSync(() => 
                workspace = IoC.Get<IWorkspace>()
            );
            //navigation_service = IoC.Get<INavigationService>();
        }

        [UI.UITestMethod]
        public void FloodFillViewModelTest()
        {
            FloodFillViewModel vm = new FloodFillViewModel(workspace);
            Assert.IsNotNull(workspace);
            Assert.IsNotNull(vm);
            Assert.AreEqual(vm.Tolerance, 20);
            vm.Tolerance = 10;
            Assert.AreEqual(vm.Tolerance, 10);
        }

        [UI.UITestMethod]
        public void HistogramViewModelTest()
        {
            HistogramViewModel vm = new HistogramViewModel(null, workspace);
            Assert.IsNotNull(workspace);
            Assert.IsNotNull(vm);

        }
    }
}
