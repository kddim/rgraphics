﻿using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Models;


namespace Rgraphics.UnitTests
{
    [TestClass]
    public class SettingsTest
    {
        private RGraphics.Models.Settings settings;
        [TestInitialize]
        public void Initialize()
        {
            TestUtils.ExecuteOnUIThreadSync(
                () =>
                {
                    settings = new Settings();
                });
        }

        [DataTestMethod]
        [DataRow(100,200)]
        [DataRow(200,100)]
        [DataRow(600,700)]
        [DataRow(2000,1500)]
        [DataRow(3000,2500)]
        public void ImageSizeOnStartupTest(int width, int height)
        {
            settings.ImageWidthOnStartup = width;
            settings.ImageHeightOnStartup = height;
            Assert.AreEqual(settings.ImageWidthOnStartup, width);
            Assert.AreEqual(settings.ImageHeightOnStartup, height);

        }
        [DataTestMethod]
        [DataRow(100, 200)]
        [DataRow(200, 100)]
        [DataRow(600, 700)]
        [DataRow(2000, 1500)]
        [DataRow(3000, 2500)]
        public void ImageMaxSizeTest(int width, int height)
        {
            settings.MaxImageHeight = height;
            settings.MaxImageWidth = width;
            Assert.AreEqual(settings.MaxImageWidth, width);
            Assert.AreEqual(settings.MaxImageHeight, height);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        [DataRow(3)]
        public void DefaultColorTest(int index)
        {
            Assert.IsNotNull(settings.DefaultColor);
            settings.DefaultColorIndex = index;
            Assert.AreEqual(settings.DefaultColorIndex, index);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        [DataRow(3)]
        public void DefaultThicknessTest(int thickness)
        {
            settings.DefaultThickness = thickness;
            Assert.AreEqual(settings.DefaultThickness, thickness);
        }

        [DataTestMethod]
        [DataRow(1,10)]
        [DataRow(2,15)]
        [DataRow(3,20)]
        public void DefaultFontTest(int index, int size)
        {
            Assert.IsNotNull(settings.AvailableFonts);
            Assert.IsTrue(settings.AvailableFonts.Count > 0);

            settings.DefaultFont = index;
            settings.DefaultFontSize = size;
            Assert.AreEqual(settings.DefaultFont, index);
            Assert.AreEqual(settings.DefaultFontSize, size);
        }

        [DataTestMethod]
        [DataRow(130, 100)]
        [DataRow(200, 60)]
        [DataRow(300, 120)]
        public void MinTextBoxSizeTest(int width, int height)
        {
            settings.MinTextBoxHeight = height;
            settings.MinTextBoxWidth = width;
            Assert.AreEqual(settings.MinTextBoxHeight, height);
            Assert.AreEqual(settings.MinTextBoxWidth, width);
        }

        [DataTestMethod]
        [DataRow(28, 28)]
        [DataRow(20, 20)]
        [DataRow(33, 33)]
        public void ThumbTest(int size, int font_size)
        {
            settings.MinThumbSize = size;
            settings.MinThumbFontSize = font_size;
            Assert.AreEqual(settings.MinThumbSize, size);
            Assert.AreEqual(settings.MinThumbFontSize, font_size);
        }

        [DataTestMethod]
        [DataRow(2)]
        [DataRow(4)]
        [DataRow(3)]
        public void MinTextToolBorderThicknessTest(int thickness)
        {
            settings.MinTextToolBorderThickness = thickness;
            Assert.AreEqual(settings.MinTextToolBorderThickness, thickness);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        [DataRow(4)]
        public void HistogramTextColorTest(int index)
        {
            Assert.IsNotNull(settings.HistogramTextColor);
            settings.HistogramTextColorIndex = index;
            Assert.AreEqual(settings.HistogramTextColorIndex, index);
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(3)]
        [DataRow(2)]
        public void HistogramTicklineColorTest(int index)
        {
            Assert.IsNotNull(settings.HistogramTicklineColor);
            settings.HistogramTicklineColorIndex = index;
            Assert.AreEqual(settings.HistogramTicklineColorIndex, index);
        }


        [DataTestMethod]
        [DataRow(2)]
        [DataRow(4)]
        [DataRow(1)]
        public void HistogramAxislineColorTest(int index)
        {
            Assert.IsNotNull(settings.HistogramAxislineColor);
            settings.HistogramAxislineColorIndex = index;
            Assert.AreEqual(settings.HistogramAxislineColorIndex, index);
        }

        [TestMethod]
        public void RestoreDefaultTest()
        {

            int value = settings.DefaultColorIndex;
            settings.DefaultColorIndex = value + 1;
            Assert.AreNotEqual(value, settings.DefaultColorIndex);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.DefaultColorIndex);

            value = settings.DefaultThickness;
            settings.DefaultThickness = value + 10;
            Assert.AreNotEqual(value, settings.DefaultThickness);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.DefaultThickness);

            value = settings.DefaultFontSize;
            settings.DefaultFontSize = value + 10;
            Assert.AreNotEqual(value, settings.DefaultFontSize);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.DefaultFontSize);

            value = settings.DefaultFont;
            settings.DefaultFont = value + 1;
            Assert.AreNotEqual(value, settings.DefaultFont);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.DefaultFont);

            value = settings.MinTextBoxHeight;
            settings.MinTextBoxHeight = value + 10;
            Assert.AreNotEqual(value, settings.MinTextBoxHeight);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.MinTextBoxHeight);

            value = settings.MinTextBoxWidth;
            settings.MinTextBoxWidth = value + 10;
            Assert.AreNotEqual(value, settings.MinTextBoxWidth);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.MinTextBoxWidth);

            value = settings.MinThumbSize;
            settings.MinThumbSize = value + 10;
            Assert.AreNotEqual(value, settings.MinThumbSize);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.MinThumbSize);

            value = settings.MinThumbFontSize;
            settings.MinThumbFontSize = value + 10;
            Assert.AreNotEqual(value, settings.MinThumbFontSize);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.MinThumbFontSize);

            value = settings.MinTextToolBorderThickness;
            settings.MinTextToolBorderThickness = value + 10;
            Assert.AreNotEqual(value, settings.MinTextToolBorderThickness);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.MinTextToolBorderThickness);

            value = settings.HistogramTextColorIndex;
            settings.HistogramTextColorIndex = value + 1;
            Assert.AreNotEqual(value, settings.HistogramTextColorIndex);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.HistogramTextColorIndex);

            value = settings.HistogramTicklineColorIndex;
            settings.HistogramTicklineColorIndex = value + 1;
            Assert.AreNotEqual(value, settings.HistogramTicklineColorIndex);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.HistogramTicklineColorIndex);

            value = settings.HistogramAxislineColorIndex;
            settings.HistogramAxislineColorIndex = value + 1;
            Assert.AreNotEqual(value, settings.HistogramAxislineColorIndex);
            settings.RestoreDefaultSettings();
            Assert.AreEqual(value, settings.HistogramAxislineColorIndex);
        }
        [TestMethod]
        public void UpdateTextBoxSizeTest()
        {
            //no other idea how to test it (scalefactors are private)
            int textbox_width = settings.MinTextBoxWidth;
            int textbox_height = settings.MinTextBoxHeight;
            int thumb_size = settings.MinThumbSize;
            int thumb_font_size = settings.MinThumbFontSize;
            int thickness = settings.MinTextToolBorderThickness;
            settings.UpdateTextBoxSize(2000, 3000);

            Assert.AreNotEqual(textbox_width, settings.MinTextBoxWidth);
            Assert.AreNotEqual(textbox_height, settings.MinTextBoxHeight);
            Assert.AreNotEqual(thumb_size, settings.MinThumbSize);
            Assert.AreNotEqual(thumb_font_size, settings.MinThumbFontSize);
            Assert.AreNotEqual(thickness, settings.MinTextToolBorderThickness);
        }
    }
}
