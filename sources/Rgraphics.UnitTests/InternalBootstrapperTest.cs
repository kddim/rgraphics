﻿using Caliburn.Micro;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Models;
using RGraphics.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI = Microsoft.VisualStudio.TestPlatform.UnitTestFramework.AppContainer;

namespace Rgraphics.UnitTests
{
    [TestClass]
    public class InternalBootstrapperTest
    {

        private Bootstrapper bootstrapper;

        [TestInitialize]
        public void Initialize()
        {
            bootstrapper = new Bootstrapper();
        }

        [UI.UITestMethod]
        public void Test_IoC()
        {
            ShellViewModel svm = IoC.Get<ShellViewModel>();
            Assert.IsNotNull(svm);

            ShellViewModel svm2 = IoC.Get<ShellViewModel>();
            Assert.IsNotNull(svm2);

            Assert.AreSame(svm, svm2);
        }
    }
}
