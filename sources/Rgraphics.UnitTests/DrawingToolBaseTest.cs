﻿using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Models;
using Windows.UI.Input;
using Windows.UI.Xaml.Input;
using Windows.Foundation;
using RGraphics.Models.DrawingTools;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace Rgraphics.UnitTests
{
    class HelpfulClass : DrawingToolBase
    {
        public HelpfulClass(IWorkspace w) : base(w) {}
        public Point TLastPoint;

        protected override Task Started(PointerPoint p) { TLastPoint = p.Position; return Task.FromResult(0); }
        protected override Task Updated(PointerPoint p) { TLastPoint = p.Position; return Task.FromResult(0); }
        protected override Task Finished(PointerPoint p) { TLastPoint = p.Position; return Task.FromResult(0); }
    }

    [TestClass]
    public class DrawingToolBaseTest
    {
        IWorkspace workspace;
        HelpfulClass fhc;
        private Bootstrapper bootstrapper;

        [TestInitialize]
        public void Initialize()
        {
            bootstrapper = new Bootstrapper();

            TestUtils.ExecuteOnUIThreadSync(() =>
                workspace = IoC.Get<IWorkspace>()
            );

            fhc = new HelpfulClass(workspace);
        }

        [TestMethod]
        public void DrawingToolBase_CreateTest()
        {
            var hc = new HelpfulClass(workspace);
            Assert.ReferenceEquals(workspace, hc.AssignedWorkspace);
        }

        // TODO: mocks don't work properly - problem with fakeiteasy.core?

        //[DataTestMethod]
        //[DataRow(2.0, 3.5)]
        //[DataRow(-2.0, 0)]
        //public void DrawingToolBase_StartTest(double x, double y)
        //{
        //    var tp = new Point(x, y);
        //    var p = A.Fake<PointerPoint>();

        //    A.CallTo(() => p.Position).Returns(tp);
        //    fhc.Start(p);

        //    Assert.ReferenceEquals(p.Position, fhc.TLastPoint);
        //}

        //[DataTestMethod]
        //[DataRow(2.0, 3.5)]
        //[DataRow(-2.0, 0)]
        //public void DrawingToolBase_UpdatetTest(double x, double y)
        //{
        //    var tp = new Point(x, y);
        //    var p = A.Fake<PointerPoint>();

        //    A.CallTo(() => p.Position).Returns(tp);
        //    fhc.Update(p);

        //    Assert.ReferenceEquals(p.Position, fhc.TLastPoint);
        //}

        //[DataTestMethod]
        //[DataRow(2.0, 3.5)]
        //[DataRow(-2.0, 0)]
        //public void DrawingToolBase_FinishTest(double x, double y)
        //{
        //    var tp = new Point(x, y);
        //    var p = A.Fake<PointerPoint>();

        //    A.CallTo(() => p.Position).Returns(tp);
        //    fhc.Finish(p);

        //    Assert.ReferenceEquals(p.Position, fhc.TLastPoint);
        //}
    }
}
