﻿using UI = Microsoft.VisualStudio.TestPlatform.UnitTestFramework.AppContainer;
using RGraphics.Models;
using RGraphics.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.UI.Core;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Models.DrawingTools;
using Caliburn.Micro;

namespace Rgraphics.UnitTests
{
    [TestClass]
    public class DrawingToolsTest
    {
        private IWorkspace Workspace;
        private ShapesViewModel SVM;
        private Bootstrapper bootstrapper;

        [TestInitialize]
        public void Initialize()
        {
            bootstrapper = new Bootstrapper();
            TestUtils.ExecuteOnUIThreadAsync(
                () =>
                {
                    Workspace = IoC.Get<IWorkspace>();
                    SVM = new ShapesViewModel(Workspace);
                }).AsTask().Wait();
        }


        [UI.UITestMethod]
        public void LineTool_Create()
        {
            LineTool lt = new LineTool(Workspace, SVM);

            Assert.ReferenceEquals(Workspace, lt.AssignedWorkspace);
        }

        [UI.UITestMethod]
        public void PointerTool_Create()
        {
            PointerTool pt = new PointerTool(Workspace);

            Assert.ReferenceEquals(Workspace, pt.AssignedWorkspace);
        }

        [UI.UITestMethod]
        public void RectangleTool_Create()
        {
            RectangleTool rt = new RectangleTool(Workspace, SVM);

            Assert.ReferenceEquals(Workspace, rt.AssignedWorkspace);
        }

        [UI.UITestMethod]
        public void EllipseTool_Create()
        {
            EllipseTool et = new EllipseTool(Workspace, SVM);
            Assert.ReferenceEquals(Workspace, et.AssignedWorkspace);
        }

        [UI.UITestMethod]
        public void FloodFillTool_Create()
        {
            FloodFillViewModel fft = new FloodFillViewModel(Workspace);

            Assert.ReferenceEquals(Workspace, fft.AssignedWorkspace);
        }

        [UI.UITestMethod]
        public void RotationTool_Create()
        {
            RotationViewModel rt = new RotationViewModel(Workspace);

            Assert.ReferenceEquals(Workspace, rt.AssignedWorkspace);
        }

        [UI.UITestMethod]
        public void ScalingTool_Create()
        {
            ScalingViewModel st = new ScalingViewModel(Workspace, new Caliburn.Micro.EventAggregator());

            Assert.ReferenceEquals(Workspace, st.AssignedWorkspace);
        }

        [UI.UITestMethod]
        public void TextTool_Create()
        {
            TextToolViewModel st = new TextToolViewModel(Workspace);

            Assert.ReferenceEquals(Workspace, st.AssignedWorkspace);
        }
    }
}
