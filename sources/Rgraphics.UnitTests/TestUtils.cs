﻿using Caliburn.Micro;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Common;
using RGraphics.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Models;

namespace Rgraphics.UnitTests
{
    public static class TestUtils
    {
        /// <summary>
        /// <para> Invokes Action on UI Thread asynchronously. </para>
        /// <para> DO NOT CALL THIS INSIDE ExecuteOnUIThreadAsync/Sync block or feel a deadlock. </para>
        /// </summary>
        /// <param name="Action">Handler to be invoked</param>
        /// <returns>IAsyncAction that should be awaited</returns>
        public static IAsyncAction ExecuteOnUIThreadAsync(DispatchedHandler Action)
        {
            return CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.High, Action);
        }

        /// <summary>
        /// <para> Invokes Action on UI Thread synchronously. </para>
        /// <para> DO NOT CALL THIS INSIDE ExecuteOnUIThreadAsync/Sync block or feel a deadlock. </para>
        /// </summary>
        /// <param name="Action">Handler to be invoked</param>
        public static void ExecuteOnUIThreadSync(DispatchedHandler Action)
        {
            CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.High, Action).AsTask().Wait();
        }


        /// <summary>
        /// <para> Proxy for strings used in DataTestMethod that might be null. </para>
        /// <para> DataRow() seem not to support null value for String. </para>
        /// </summary>
        /// <param name="str">String to be proxied</param>
        /// <returns>Proxied string</returns>
        public static String NullProxy(this String str)
        {
            if (str == "null")
                str = null;
            return str;
        }

        /// <summary>
        /// <para> Gets example image from assembly file at runtime. </para>
        /// <para> This metod CAN'T BE called inside ExecuteOnUIThread/ExecuteOnUIThreadSync, since it already uses it. </para>
        /// </summary>
        /// <returns>Created WriteableBitmap</returns>
        public static WriteableBitmap GetExampleImage()
        {
            WriteableBitmap wb = null;

            ExecuteOnUIThreadSync(
                () =>
                {
                    wb = new WriteableBitmap(4, 2);

                    var asyncOp = StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///images/constructFromFile.png"));
                    asyncOp.AsTask().Wait();
                    StorageFile file = asyncOp.GetResults();

                    var asyncOp2 = file.OpenAsync(FileAccessMode.Read);
                    asyncOp2.AsTask().Wait();
                    IRandomAccessStream fileStream = asyncOp2.GetResults();

                    wb.SetSource(fileStream);
                });

            return wb;
        }

        /// <summary>
        /// Checks whether WriteableBitmaps contains the same images.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns>True if images are equal, otherwise false.</returns>
        public static bool AreEqual(WriteableBitmap first, WriteableBitmap second)
        {
            bool return_value = true;
            ExecuteOnUIThreadSync(
                () =>
                {
                    if (first.PixelWidth != second.PixelWidth || first.PixelHeight != second.PixelHeight)
                        throw new ArgumentException("Images are different size!");

                    Assert.AreEqual(first.PixelHeight, second.PixelHeight);
                    Assert.AreEqual(first.PixelWidth, second.PixelWidth);
                    using (BitmapContext first_context = first.GetBitmapContext(ReadWriteMode.ReadOnly),
                        second_context = second.GetBitmapContext(ReadWriteMode.ReadOnly))
                    {
                        int[] first_array = first_context.Pixels;
                        int[] second_array = second_context.Pixels;
                        for (int i = 0; i < first_array.Length; ++i)
                        {
                            if (first_array[i] != second_array[i])
                            {
                                return_value = false;
                                break;
                            }

                        }
                    }

                });
            return return_value;
        }

        /// <summary>
        /// Gets the image by its name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns></returns>
        public static WriteableBitmap GetImageByName(String name, int width, int height)
        {
            WriteableBitmap wb = null;

            ExecuteOnUIThreadSync(
                () =>
                {
                    wb = new WriteableBitmap(width, height);

                    var asyncOp = StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///images/" + name));
                    asyncOp.AsTask().Wait();
                    StorageFile file = asyncOp.GetResults();

                    var asyncOp2 = file.OpenAsync(FileAccessMode.Read);
                    asyncOp2.AsTask().Wait();
                    IRandomAccessStream fileStream = asyncOp2.GetResults();

                    wb.SetSource(fileStream);
                });

            return wb;
        }

        /// <summary>
        /// Determines whether the specified image is in grayscale.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns></returns>
        public static bool IsGrayscale(WriteableBitmap image)
        {
            const int MASK_R = 0x00FF0000;
            const int MASK_G = 0x0000FF00;
            const int MASK_B = 0x000000FF;
            const int SHIFT_R = 16;
            const int SHIFT_G = 8;
            const int shift_B = 0;
            bool return_value = true;
            ExecuteOnUIThreadSync(
                () =>
                {
                    using (BitmapContext context = image.GetBitmapContext(ReadWriteMode.ReadOnly))
                    {
                        int index = 0;
                        int[] first_array = context.Pixels;
                        for (int i = 0; i < first_array.Length; ++i)
                        {
                            int r = (first_array[index] & MASK_R) >> SHIFT_R;
                            int g = (first_array[index] & MASK_G) >> SHIFT_G;
                            int b = (first_array[index] & MASK_B) >> shift_B;

                            if (r != g || r != b || g != b)
                            {
                                return_value = false;
                                break;
                            }
                            ++index;

                        }
                    }

                });
            return return_value;
        }

        /// <summary>
        /// Determines whether one image is negation of the other one.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns>True if one image is negation of the other one.</returns>
        public static bool isNegation(WriteableBitmap first, WriteableBitmap second)
        {
            const int MASK_R = 0x00FF0000;
            const int MASK_G = 0x0000FF00;
            const int MASK_B = 0x000000FF;
            const int SHIFT_R = 16;
            const int SHIFT_G = 8;
            const int shift_B = 0;
            bool return_value = true;
            ExecuteOnUIThreadSync(
                () =>
                {
                    using (BitmapContext first_context = first.GetBitmapContext(ReadWriteMode.ReadOnly),
                        second_context = second.GetBitmapContext(ReadWriteMode.ReadOnly))
                    {
                        int index = 0;
                        int[] first_array = first_context.Pixels;
                        int[] second_array = second_context.Pixels;

                        for (int i = 0; i < first_array.Length; ++i)
                        {
                            if ((first_array[index] & MASK_R) >> SHIFT_R != (255 - ((second_array[index] & MASK_R) >> SHIFT_R)))
                            {
                                return_value = false;
                                break;
                            }
                            if ((first_array[index] & MASK_G) >> SHIFT_G != (255 - ((second_array[index] & MASK_G) >> SHIFT_G)))
                            {
                                return_value = false;
                                break;
                            }
                            if ((first_array[index] & MASK_B) >> shift_B != (255 - ((second_array[index] & MASK_B) >> shift_B)))
                            {
                                return_value = false;
                                break;
                            }
                            ++index;

                        }
                    }

                });
            return return_value;
        }

        /// <summary>
        /// Determines whether the specified image is in colors.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="color">The color.</param>
        /// <returns></returns>
        public static bool isColor(WriteableBitmap image, Color color)
        {
            const int MASK_R = 0x00FF0000;
            const int MASK_G = 0x0000FF00;
            const int MASK_B = 0x000000FF;
            const int SHIFT_R = 16;
            const int SHIFT_G = 8;
            const int shift_B = 0;
            bool return_value = true;
            ExecuteOnUIThreadSync(
                () =>
                {
                    using (BitmapContext context = image.GetBitmapContext(ReadWriteMode.ReadOnly))
                    {
                        int index = 0;
                        int[] first_array = context.Pixels;
                        for (int i = 0; i < first_array.Length; ++i)
                        {
                            int r = (first_array[index] & MASK_R) >> SHIFT_R;
                            int g = (first_array[index] & MASK_G) >> SHIFT_G;
                            int b = (first_array[index] & MASK_B) >> shift_B;

                            if (r != color.R || g != color.G || b != color.B)
                            {
                                return_value = false;
                                break;
                            }
                            ++index;

                        }
                    }

                });
            return return_value;
        }
    }
}
