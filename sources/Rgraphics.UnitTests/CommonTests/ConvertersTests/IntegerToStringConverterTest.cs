﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Common;


namespace Rgraphics.UnitTests.CommonTests.ConvertersTests
{
    [TestClass]
    public class IntegerToStringConverterTest
    {
        [DataTestMethod]
        [DataRow(123)]
        [DataRow(-12)]
        public void IntegerToString_ConvertTest(int testVal)
        {
            String expectedValue = testVal.ToString();

            var converter = new IntegerToStringConverter();
            var value = (String)converter.Convert(testVal, typeof(int), null, null);

            Assert.AreEqual(expectedValue, value);
        }

        [DataTestMethod]
        [DataRow("123")]
        [DataRow("-12")]
        [DataRow("")]
        public void IntegerToString_ConvertBackTest(String testVal)
        {
            var expectedValue = testVal.CompareTo(String.Empty) == 0 ? 0 : int.Parse(testVal);

            var converter = new IntegerToStringConverter();
            var value = (int)converter.ConvertBack(testVal, typeof(String), null, null);

            Assert.AreEqual(expectedValue, value);
        }
    }
}
