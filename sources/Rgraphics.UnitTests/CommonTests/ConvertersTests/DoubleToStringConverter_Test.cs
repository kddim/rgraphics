﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Common;


namespace Rgraphics.UnitTests.CommonTests.ConvertersTests
{
    [TestClass]
    public class DoubleToStringConverter_Test
    {
        [DataTestMethod]
        [DataRow(123.45)]
        [DataRow(-12.99)]
        public void DoubleToString_ConvertTest(double testVal)
        {
            String expectedValue = String.Format("{0:0.0}", testVal);

            var converter = new DoubleToStringConverter();
            var value = (String)converter.Convert(testVal, typeof(double), null, null);

            Assert.AreEqual(expectedValue, value);
        }
    }
}
