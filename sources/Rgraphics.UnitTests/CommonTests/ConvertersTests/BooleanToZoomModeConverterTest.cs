﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using RGraphics.Common;
using Windows.UI.Xaml.Controls;

namespace Rgraphics.UnitTests.CommonTests.ConvertersTests
{
    [TestClass]
    public class BooleanToZoomModeConverterTest
    {

        [TestMethod]
        public void BooleanToZoomMode_ConvertTest()
        {
            var converter = new BooleanToZoomModeConverter();
            var value = (ZoomMode)converter.Convert(true, typeof(Boolean), null, null);

            Assert.AreEqual(ZoomMode.Enabled, value);

            value = (ZoomMode)converter.Convert(false, typeof(Boolean), null, null);

            Assert.AreEqual(ZoomMode.Disabled, value);
        }
    }
}
