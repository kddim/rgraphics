﻿using Caliburn.Micro;
using RGraphics.Models;
using RGraphics.ViewModels;
using RGraphics.ViewModels.Effects;
using RGraphics.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.DataTransfer.ShareTarget;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

/// <summary>
/// RGraphics Project - simple raster graphics editor for Windows Tablets and PCs.
/// Authors: Damian Janas, Kamil Dziedzic, Marcin Jezierski, Dominik Czarnota, Ignacy Biernacki
/// </summary>
namespace RGraphics
{
	/// <summary>
	/// RGraphic's app class. Based on CaliburnApplication from Caliburn.Micro mvvm framework.
	/// </summary>
	public sealed partial class App
	{
		private WinRTContainer container;
		private INavigationService navigationService;

		/// <summary>
		/// Initializes a new instance of the <see cref="App"/> class.
		/// </summary>
		public App()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Configures the framework and setup your IoC container.
		/// </summary>
		protected override void Configure()
		{
			Application.Current.DebugSettings.EnableFrameRateCounter = true;

			LogManager.GetLog = t => new DebugLog(t);

			ConventionManager.AddElementConvention<AppBarButton>(AppBarButton.IsEnabledProperty, "DataContext", "Click");

			container = new WinRTContainer();
			container.RegisterWinRTServices();

			container.RegisterSharingService();

			var settingsService = container.RegisterSettingsService();

			// settingsService.RegisterFlyoutCommand<SampleSettingsViewModel>("Custom");
			// settingsService.RegisterUriCommand("View Website", new Uri("http://caliburnmicro.codeplex.com"));

			container
				.Singleton<ShellViewModel>()
				.Singleton<ShellView>()
				.Singleton<ResourceLoader>()
				.PerRequest<NewImageViewModel>()
				.Singleton<IWorkspace, Workspace>()
				.PerRequest<CropViewModel>()
				.PerRequest<ProgressReportDialogViewModel>()
				.PerRequest<HistogramViewModel>()
				.PerRequest<GeneralSettingsViewModel>()
				.PerRequest<DrawingToolsSettingsViewModel>()
				.PerRequest<TextToolSettingsViewModel>()
				.PerRequest<HistogramSettingsViewModel>()
				.Singleton<Settings>()
				.PerRequest<IProgressReporter, ProgressReportDialogViewModel>()
				.PerRequest<EffectsDialogViewModel>()
				.PerRequest<IImageEffectsSource, DefaultImageEffectsSource>();

			#region Setting Service Commands

			var res = new Settings().Res_UI;

			settingsService.RegisterFlyoutCommand<GeneralSettingsViewModel>(res.GetString("Settings_GeneralTitle"));
			settingsService.RegisterFlyoutCommand<DrawingToolsSettingsViewModel>(res.GetString("Settings_DrawingToolsTitle"));
			settingsService.RegisterFlyoutCommand<TextToolSettingsViewModel>(res.GetString("Settings_TextToolTitle"));
			settingsService.RegisterFlyoutCommand<HistogramSettingsViewModel>(res.GetString("Settings_HistogramTitle"));
			#endregion

			this.DebugSettings.EnableFrameRateCounter = false;

			// We want to use the Frame in OnLaunched so set it up here

			PrepareViewFirst();
		}

		/// <summary>
		/// Provides an IoC specific implementation.
		/// </summary>
		/// <param name="service">The service to locate.</param>
		/// <param name="key">The key to locate.</param>
		/// <returns>
		/// The located service.
		/// </returns>
		/// <exception cref="System.Exception">Could not locate any instances.</exception>
		protected override object GetInstance(Type service, string key)
		{
			var instance = container.GetInstance(service, key);
			if (instance != null)
				return instance;

			throw new Exception("Could not locate any instances.");
		}

		/// <summary>
		/// Provides an IoC specific implementation
		/// </summary>
		/// <param name="service">The service to locate.</param>
		/// <returns>
		/// The located services.
		/// </returns>
		protected override IEnumerable<object> GetAllInstances(Type service)
		{
			return container.GetAllInstances(service);
		}

		/// <summary>
		/// Provides an IoC specific implementation.
		/// </summary>
		/// <param name="instance">The instance to perform injection on.</param>
		protected override void BuildUp(object instance)
		{
			container.BuildUp(instance);
		}

		/// <summary>
		/// Registers a navigation service.
		/// </summary>
		/// <param name="rootFrame">The root frame of the application.</param>
		protected override void PrepareViewFirst(Frame rootFrame)
		{
			navigationService = container.RegisterNavigationService(rootFrame);
		}

		/// <summary>
		/// Invoked when the application is launched. Override this method to perform application initialization and to display initial content in the associated Window.
		/// </summary>
		/// <param name="args">Event data for the event.</param>
		protected override void OnLaunched(LaunchActivatedEventArgs args)
		{
			Initialize();

			var resumed = false;

			if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
			{
				resumed = navigationService.ResumeState();
			}

			if (!resumed)
				DisplayRootView<ShellView>();

		}

		/// <summary>
		/// Adds custom behavior when the application transitions to Suspended state from some other state.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The event args.</param>
		protected override void OnSuspending(object sender, SuspendingEventArgs e)
		{
			navigationService.SuspendState();
		}
	}
}
