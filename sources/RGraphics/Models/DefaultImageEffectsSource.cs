﻿using RGraphics.ViewModels.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGraphics.Models
{
    /// <summary>
    /// <para> Provides default image effects source that may be used in UI binding. </para>
    /// <para> It was implemented because Caliburn.Micro's IoC lacks functionality of returning all known implementations of specified interface in an collection. </para>
    /// </summary>
    public class DefaultImageEffectsSource : IImageEffectsSource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultImageEffectsSource"/> class.
        /// </summary>
        public DefaultImageEffectsSource(IWorkspace workspace)
        {
            List<IImageEffect> effects = new List<IImageEffect>();
            effects.Add(new GrayscaleViewModel());
            effects.Add(new NegativeViewModel());
            effects.Add(new SepiaViewModel());
            effects.Add(new NoiseViewModel());
            effects.Add(new EdgeDetectionViewModel());
            effects.Add(new UnnoiseViewModel());
            effects.Add(new BlurViewModel(workspace));
            effects.Add(new CannyViewModel());
            effects.Add(new ThresholdingViewModel());
            effects.Add(new MirrorViewModel(workspace));
            Source = effects;
        }

        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public IEnumerable<IImageEffect> Source
        {
            get;
            private set;
        }
    }
}
