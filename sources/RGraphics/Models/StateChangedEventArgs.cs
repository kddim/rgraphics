﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGraphics.Models
{
    /// <summary>
    /// Manages handling of the state changed event args.
    /// </summary>
    public class StateChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the old state.
        /// </summary>
        /// <value>
        /// The old state.
        /// </value>
        public Object OldState { get; private set; }
        /// <summary>
        /// Gets the new state.
        /// </summary>
        /// <value>
        /// The new state.
        /// </value>
        public Object NewState { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="StateChangedEventArgs"/> class.
        /// </summary>
        /// <param name="oldState">The old state.</param>
        /// <param name="newState">The new state.</param>
        public StateChangedEventArgs(object oldState, object newState)
        {
            OldState = oldState;
            NewState = newState;
        }
    }
}
