﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGraphics.Models
{
	public class LimitedStack<T> : PropertyChangedBase
	{
		public readonly int Limit;
		private readonly List<T> _Stack;

		private int _Index;
		private int Index
		{
			get { return _Index; }
			set
			{
				if (value >= 0 && value < Count)
					_Index = value;
				CanGoBack = _Index > 0 ? true : false;
				CanGoNext = _Index < _Stack.Count - 1 ? true : false;
			}
		}

		private bool _CanGoBack;
		public bool CanGoBack
		{
			get { return _CanGoBack; }
			set
			{
				_CanGoBack = value;
				NotifyOfPropertyChange(() => CanGoBack);
			}
		}

		private bool _CanGoNext;
		public bool CanGoNext
		{
			get { return _CanGoNext; }
			set
			{
				_CanGoNext = value;
				NotifyOfPropertyChange(() => CanGoNext);
			}
		}

		public LimitedStack(int limit = 8)
		{
			Limit = limit;
			_Stack = new List<T>(limit);
			_Index = -1;
		}

		public void Push(T item)
		{
			if (_Stack.Count - 1 >= 0)
				while( Index + 1 != _Stack.Count )
					_Stack.RemoveAt(_Stack.Count - 1);

			if (_Stack.Count == Limit)
				_Stack.RemoveAt(0);

			_Stack.Add(item);

			++Index;
		}

		/// <summary>
		/// Returns current
		/// </summary>
		public T Peek()
		{
			return _Stack[Index];
		}

		public void Pop()
		{
			_Stack.RemoveAt(_Stack.Count - 1);
		}

		public T Back()
		{
			--Index;
			return Peek();
		}

		public T Next()
		{
			++Index;
			return Peek();
		}

		public void Clear()
		{
			_Stack.Clear();
			_Index = -1;
		}

		public T First()
		{
			return _Stack[0];
		}

		public int Count
		{
			get { return _Stack.Count; }
		}
	}
}
