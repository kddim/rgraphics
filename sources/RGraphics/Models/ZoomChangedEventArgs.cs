﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RGraphics.Models
{
    /// <summary>
    /// Manages handling of the zoom changed event args.
    /// </summary>
    public class ZoomChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the zoom factor.
        /// </summary>
        /// <value>
        /// The zoom factor.
        /// </value>
        public double ZoomFactor { get; private set; }

        /// <summary>
        /// Gets the horizontal offset.
        /// </summary>
        /// <value>
        /// The horizontal offset.
        /// </value>
        public double? HorizontalOffset { get; private set; }

        /// <summary>
        /// Gets the vertical offset.
        /// </summary>
        /// <value>
        /// The vertical offset.
        /// </value>
        public double? VerticalOffset { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZoomChangedEventArgs"/> class.
        /// </summary>
        /// <param name="zoomFactor">The zoom factor.</param>
        /// <param name="horizontalOffset">The horizontal offset.</param>
        /// <param name="verticalOffset">The vertical offset.</param>
        public ZoomChangedEventArgs(double zoomFactor, double? horizontalOffset, double? verticalOffset)
        {
            this.ZoomFactor = zoomFactor;
            this.HorizontalOffset = horizontalOffset;
            this.VerticalOffset = verticalOffset;
        }
    }
}
