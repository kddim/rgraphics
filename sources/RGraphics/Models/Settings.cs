﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Windows.ApplicationModel.Resources;

namespace RGraphics.Models
{
    /// <summary>
    /// Manages all settings components and includes specified configuration settings.
    /// </summary>
    public class Settings
    {
        #region Predefault settings
        /// <summary>
        /// The pre default font size
        /// </summary>
        private const int PreDefaultFontSize = 20;
        /// <summary>
        /// The pre default font
        /// </summary>
        private const int PreDefaultFont = 0;
        /// <summary>
        /// The pre default thickness
        /// </summary>
        private const int PreDefaultThickness = 2;
        /// <summary>
        /// The pre default color
        /// </summary>
        private const int PreDefaultColor = 1;
        /// <summary>
        /// The pre histogram text color
        /// </summary>
        private const int PreHistogramTextColor = 0;
        /// <summary>
        /// The pre histogram tickline color
        /// </summary>
        private const int PreHistogramTicklineColor = 0;
        /// <summary>
        /// The pre histogram axisline color
        /// </summary>
        private const int PreHistogramAxislineColor = 0;
        /// <summary>
        /// The pre image width on startup
        /// </summary>
        private const int PreImageWidthOnStartup = 800;
        /// <summary>
        /// The pre image height on startup
        /// </summary>
        private const int PreImageHeightOnStartup = 800;
        #endregion

        #region Setting Keys
        /// <summary>
        /// The font size key
        /// </summary>
        private readonly String FontSizeKey = "FontSizeKey";
        /// <summary>
        /// The font key
        /// </summary>
        private readonly String FontKey = "FontKey";
        /// <summary>
        /// The thickness key
        /// </summary>
        private readonly String ThicknessKey = "ThicknessKey";
        /// <summary>
        /// The color key
        /// </summary>
        private readonly String ColorKey = "ColorKey";
        /// <summary>
        /// The histogram text color key
        /// </summary>
        private readonly String HistogramTextColorKey = "HistogramTextColorKey";
        /// <summary>
        /// The histogram tickline color key
        /// </summary>
        private readonly String HistogramTicklineColorKey = "HistogramTicklineColorKey";
        /// <summary>
        /// The histogram axisline color key
        /// </summary>
        private readonly String HistogramAxislineColorKey = "HistogramAxislineColorKey";
        /// <summary>
        /// The image width on startup key
        /// </summary>
        private readonly String ImageWidthOnStartupKey = "ImageWidthOnStartupKey";
        /// <summary>
        /// The image height on startup key
        /// </summary>
        private readonly String ImageHeightOnStartupKey = "ImageHeightOnStartupKey";
        /// <summary>
        /// The maximum image width key
        /// </summary>
        private readonly String MaxImageWidthKey = "MaxImageWidthKey";
        /// <summary>
        /// The maximum image height key
        /// </summary>
        private readonly String MaxImageHeightKey = "MaxImageHeightKey";
        #endregion

        #region Absolute values
        /// <summary>
        /// The absolute maximum image height
        /// </summary>
        public readonly int AbsoluteMaxImageHeight = 2500;
        /// <summary>
        /// The absolute maximum image width
        /// </summary>
        public readonly int AbsoluteMaxImageWidth = 3000;
        /// <summary>
        /// The absolute maximum font size
        /// </summary>
        public readonly int AbsoluteMaxFontSize = 1000;
        /// <summary>
        /// The absolute minimum text tool width
        /// </summary>
        private readonly int AbsoluteMinTextToolWidth = 120;
        /// <summary>
        /// The absolute minimum text tool height
        /// </summary>
        private readonly int AbsoluteMinTextToolHeight = 50;
        /// <summary>
        /// The absolute minimum text tool thumb size
        /// </summary>
        private readonly int AbsoluteMinTextToolThumbSize = 20;
        /// <summary>
        /// The absolute minimum text tool thumb font size
        /// </summary>
        private readonly int AbsoluteMinTextToolThumbFontSize = 8;
        /// <summary>
        /// The absolute minimum text tool border thickness
        /// </summary>
        private readonly int AbsoluteMinTextToolBorderThickness = 2;
        #endregion

        #region Scale factors
        /// <summary>
        /// The minimum text tool width scale factor
        /// </summary>
        private const double MinTextToolWidthScaleFactor = 0.15;
        /// <summary>
        /// The minimum text tool height scale factor
        /// </summary>
        private const double MinTextToolHeightScaleFactor = 0.0625;
        /// <summary>
        /// The minimum text tool thumb size scale factor
        /// </summary>
        private const double MinTextToolThumbSizeScaleFactor = 0.025;
        /// <summary>
        /// The minimum text tool thumb font size scale factor
        /// </summary>
        private const double MinTextToolThumbFontSizeScaleFactor = 0.01;
        /// <summary>
        /// The minimum text tool border thickness scale factor
        /// </summary>
        private const double MinTextToolBorderThicknessScaleFactor = 0.0025;
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public Settings()
        {
            AvailableFonts = new ObservableCollection<FontFamily>();

            #region fonts addition to the collection
            AvailableFonts.Add(new FontFamily("Arial"));
            AvailableFonts.Add(new FontFamily("Comic Sans"));
            AvailableFonts.Add(new FontFamily("Calibri"));
            AvailableFonts.Add(new FontFamily("Cambria"));
            AvailableFonts.Add(new FontFamily("Courier"));
            AvailableFonts.Add(new FontFamily("Consolas"));
            AvailableFonts.Add(new FontFamily("Georgia"));
            AvailableFonts.Add(new FontFamily("Impact"));
            AvailableFonts.Add(new FontFamily("Times New Roman"));
            AvailableFonts.Add(new FontFamily("Verdana"));
            //Fonts = new ObservableCollection<FontFamily>(FontFamilyEnumeration.GetFonts()); // change to all fonts - find solution.. unhadled exception
            #endregion

            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            #region load settings
            loadFontSizeSetting(localSettings);
            loadFontSetting(localSettings);
            loadThicknessSetting(localSettings);
            loadColorSetting(localSettings);
            loadHistogramTextColorSetting(localSettings);
            loadHistogramTicklineColorSetting(localSettings);
            loadHistogramAxislineColorSetting(localSettings);
            loadWidthOnStartupSetting(localSettings);
            loadHeightOnStartupSetting(localSettings);
            #endregion

            MaxImageWidth = AbsoluteMaxImageWidth;
            MaxImageHeight = AbsoluteMaxImageHeight;

            MinTextBoxWidth = AbsoluteMinTextToolWidth;
            MinTextBoxHeight = AbsoluteMinTextToolHeight;
            MinThumbSize = AbsoluteMinTextToolThumbSize;
            MinThumbFontSize = AbsoluteMinTextToolThumbFontSize;
            MinTextToolBorderThickness = AbsoluteMinTextToolBorderThickness;
        }

        #region GENERAL SETTINGS

        /// <summary>
        /// The image width on the startup
        /// </summary>
        private int _ImageWidthOnStartup;
        /// <summary>
        /// Gets or sets the image width on the startup.
        /// </summary>
        /// <value>
        /// The image width on the startup.
        /// </value>
        public int ImageWidthOnStartup
        {
            get { return _ImageWidthOnStartup; }
            set
            {
                _ImageWidthOnStartup = value;

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values[ImageWidthOnStartupKey] = _ImageWidthOnStartup.ToString();
            }
        }

        /// <summary>
        /// Loads the width on startup setting.
        /// </summary>
        /// <param name="localSettings">The local settings.</param>
        private void loadWidthOnStartupSetting(Windows.Storage.ApplicationDataContainer localSettings)
        {
            Object value = localSettings.Values[ImageWidthOnStartupKey];

            if (value == null)
                ImageWidthOnStartup = PreImageWidthOnStartup;
            else
                ImageWidthOnStartup = int.Parse(value.ToString());
        }

        /// <summary>
        /// The image height on the startup
        /// </summary>
        private int _ImageHeightOnStartup;
        /// <summary>
        /// Gets or sets the image height on the startup.
        /// </summary>
        /// <value>
        /// The image height on the startup.
        /// </value>
        public int ImageHeightOnStartup
        {
            get { return _ImageHeightOnStartup; }
            set
            {
                _ImageHeightOnStartup = value;

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values[ImageHeightOnStartupKey] = _ImageHeightOnStartup.ToString();
            }
        }

        /// <summary>
        /// Loads the height on startup setting.
        /// </summary>
        /// <param name="localSettings">The local settings.</param>
        private void loadHeightOnStartupSetting(Windows.Storage.ApplicationDataContainer localSettings)
        {
            Object value = localSettings.Values[ImageHeightOnStartupKey];

            if (value == null)
                ImageHeightOnStartup = PreImageHeightOnStartup;
            else
                ImageHeightOnStartup = int.Parse(value.ToString());
        }

        /// <summary>
        /// The maximum image width
        /// </summary>
        private int _MaxImageWidth;
        /// <summary>
        /// Gets or sets the maximum width of the image.
        /// </summary>
        /// <value>
        /// The maximum width of the image.
        /// </value>
        public int MaxImageWidth
        {
            get { return _MaxImageWidth; }
            set
            {
                _MaxImageWidth = value;
            }
        }

        /// <summary>
        /// The maximum image height
        /// </summary>
        private int _MaxImageHeight;
        /// <summary>
        /// Gets or sets the maximum height of the image.
        /// </summary>
        /// <value>
        /// The maximum height of the image.
        /// </value>
        public int MaxImageHeight
        {
            get { return _MaxImageHeight; }
            set
            {
                _MaxImageHeight = value;
            }
        }
#endregion

        #region DRAWING TOOLS SETTINGS

        /// <summary>
        /// The available colors used in the color settings.
        /// </summary>
        private readonly Color[] ColorsTab = { Colors.White, Colors.Red, Colors.Blue, Colors.Green, Colors.Black };

        /// <summary>
        /// Gets the default color.
        /// </summary>
        /// <value>
        /// The default color.
        /// </value>
        public Color DefaultColor { get; private set; }

        /// <summary>
        /// The default color index
        /// </summary>
        private int _DefaultColorIndex;
        /// <summary>
        /// Gets or sets the default index of the color.
        /// </summary>
        /// <value>
        /// The default index of the color.
        /// </value>
        public int DefaultColorIndex
        {
            get { return _DefaultColorIndex; }
            set
            {
                _DefaultColorIndex = value;
                DefaultColor = ColorsTab[_DefaultColorIndex];

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values[ColorKey] = _DefaultColorIndex.ToString();
            }
        }


        /// <summary>
        /// Loads the color setting.
        /// </summary>
        /// <param name="localSettings">The local settings.</param>
        private void loadColorSetting(Windows.Storage.ApplicationDataContainer localSettings)
        {
            Object value = localSettings.Values[ColorKey];

            if (value == null)
                DefaultColorIndex = PreDefaultColor;
            else
                DefaultColorIndex = int.Parse(value.ToString());
            DefaultColor = ColorsTab[DefaultColorIndex];
        }

        /// <summary>
        /// The default thickness
        /// </summary>
        private int _DefaultThickness;
        /// <summary>
        /// Gets or sets the default thickness.
        /// </summary>
        /// <value>
        /// The default thickness.
        /// </value>
        public int DefaultThickness
        {
            get { return _DefaultThickness; }
            set
            {
                _DefaultThickness = value;

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values[ThicknessKey] = _DefaultThickness.ToString();
            }
        }

        /// <summary>
        /// Loads the thickness setting.
        /// </summary>
        /// <param name="localSettings">The local settings.</param>
        private void loadThicknessSetting(Windows.Storage.ApplicationDataContainer localSettings)
        {
            Object value = localSettings.Values[ThicknessKey];

            if (value == null)
                DefaultThickness = PreDefaultThickness;
            else
                DefaultThickness = int.Parse(value.ToString());
        }
        #endregion

        #region TEXT TOOL SETTINGS
        /// <summary>
        /// Gets the available fonts from observable collection.
        /// </summary>
        /// <value>
        /// The available fonts.
        /// </value>
        public ObservableCollection<FontFamily> AvailableFonts { get; private set; }

        /// <summary>
        /// The default font size
        /// </summary>
        private int _DefaultFontSize;
        /// <summary>
        /// Gets or sets the default size of the font.
        /// </summary>
        /// <value>
        /// The default size of the font.
        /// </value>
        public int DefaultFontSize
        {
            get { return _DefaultFontSize; }
            set
            {
                _DefaultFontSize = value;

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values[FontSizeKey] = _DefaultFontSize.ToString();
            }
        }

        /// <summary>
        /// The default font
        /// </summary>
        private int _DefaultFont;
        /// <summary>
        /// Gets or sets the default font.
        /// </summary>
        /// <value>
        /// The default font.
        /// </value>
        public int DefaultFont
        {
            get { return _DefaultFont; }
            set
            {
                _DefaultFont = value;

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values[FontKey] = _DefaultFont.ToString();
            }
        }

        /// <summary>
        /// Loads the font size setting.
        /// </summary>
        /// <param name="localSettings">The local settings.</param>
        private void loadFontSizeSetting(Windows.Storage.ApplicationDataContainer localSettings)
        {
            Object value = localSettings.Values[FontSizeKey];

            if (value == null)
                DefaultFontSize = PreDefaultFontSize;
            else
                DefaultFontSize = int.Parse(value.ToString());
        }

        /// <summary>
        /// Loads the font setting.
        /// </summary>
        /// <param name="localSettings">The local settings.</param>
        private void loadFontSetting(Windows.Storage.ApplicationDataContainer localSettings)
        {
            Object value = localSettings.Values[FontKey];

            if (value == null)
                DefaultFont = PreDefaultFont;
            else
                DefaultFont = int.Parse(value.ToString());
        }

        /// <summary>
        /// The minimum text box width from text tool.
        /// </summary>
        private int _MinTextBoxWidth;
        /// <summary>
        /// Gets or sets the minimum width of the text box from text tool.
        /// </summary>
        /// <value>
        /// The minimum width of the text box.
        /// </value>
        public int MinTextBoxWidth
        {
            get { return _MinTextBoxWidth; }
            set { _MinTextBoxWidth = Math.Max(AbsoluteMinTextToolWidth, value);}
        }

        /// <summary>
        /// The minimum text box height from text tool.
        /// </summary>
        private int _MinTextBoxHeight;
        /// <summary>
        /// Gets or sets the minimum height of the text box from text tool.
        /// </summary>
        /// <value>
        /// The minimum height of the text box.
        /// </value>
        public int MinTextBoxHeight
        {
            get { return _MinTextBoxHeight; }
            set { _MinTextBoxHeight = Math.Max(AbsoluteMinTextToolHeight, value); }
        }

        /// <summary>
        /// The minimum thumb size from text tool.
        /// </summary>
        private int _MinThumbSize;
        /// <summary>
        /// Gets or sets the minimum size of the thumb from text tool.
        /// </summary>
        /// <value>
        /// The minimum size of the thumb.
        /// </value>
        public int MinThumbSize
        {
            get { return _MinThumbSize; }
            set { _MinThumbSize = Math.Max(AbsoluteMinTextToolThumbSize, value); }
        }

        /// <summary>
        /// The minimum thumb font size
        /// </summary>
        private int _MinThumbFontSize;
        /// <summary>
        /// Gets or sets the minimum size of the thumb font from text tool.
        /// </summary>
        /// <value>
        /// The minimum size of the thumb font.
        /// </value>
        public int MinThumbFontSize
        {
            get { return _MinThumbFontSize; }
            set { _MinThumbFontSize = Math.Max(AbsoluteMinTextToolThumbFontSize, value); }
        }

        /// <summary>
        /// The minimum text tool border thickness from text tool
        /// </summary>
        private int _MinTextToolBorderThickness;
        /// <summary>
        /// Gets or sets the minimum text tool border thickness.
        /// </summary>
        /// <value>
        /// The minimum text tool border thickness.
        /// </value>
        public int MinTextToolBorderThickness
        {
            get { return _MinTextToolBorderThickness; }
            set { _MinTextToolBorderThickness = Math.Max(AbsoluteMinTextToolBorderThickness, value); }
        }

        #endregion 

        #region HISTOGRAM SETTINGS

        /// <summary>
        /// The available histogram colors used in the histogram settings.
        /// </summary>
        private readonly OxyColor[] HistogramColors = { OxyColor.FromArgb(100,255,255,255),
                                                        OxyColor.FromArgb(100,255,0,0),
                                                        OxyColor.FromArgb(100, 102, 51, 153),
                                                        OxyColor.FromArgb(100,0,0,255),
                                                        OxyColor.FromArgb(100,0,255,0)};

        /// <summary>
        /// Gets the color of the histogram text.
        /// </summary>
        /// <value>
        /// The color of the histogram text.
        /// </value>
        public OxyColor HistogramTextColor { get; private set; }
        /// <summary>
        /// The histogram text color index
        /// </summary>
        private int _HistogramTextColorIndex;
        /// <summary>
        /// Gets or sets the index of the histogram text color.
        /// </summary>
        /// <value>
        /// The index of the histogram text color.
        /// </value>
        public int HistogramTextColorIndex
        {
            get { return _HistogramTextColorIndex; }
            set
            {
                _HistogramTextColorIndex = value;
                HistogramTextColor = HistogramColors[_HistogramTextColorIndex];

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values[HistogramTextColorKey] = _HistogramTextColorIndex.ToString();
            }
        }

        /// <summary>
        /// Loads the histogram text color setting.
        /// </summary>
        /// <param name="localSettings">The local settings.</param>
        private void loadHistogramTextColorSetting(Windows.Storage.ApplicationDataContainer localSettings)
        {
            Object value = localSettings.Values[HistogramTextColorKey];

            if (value == null)
                HistogramTextColorIndex = PreHistogramTextColor;
            else
                HistogramTextColorIndex = int.Parse(value.ToString());
            HistogramTextColor = HistogramColors[_HistogramTextColorIndex];
        }

        /// <summary>
        /// Gets the color of the histogram tickline.
        /// </summary>
        /// <value>
        /// The color of the histogram tickline.
        /// </value>
        public OxyColor HistogramTicklineColor { get; private set; }
        /// <summary>
        /// The histogram tickline color index
        /// </summary>
        private int _HistogramTicklineColorIndex;
        /// <summary>
        /// Gets or sets the index of the histogram tickline color.
        /// </summary>
        /// <value>
        /// The index of the histogram tickline color.
        /// </value>
        public int HistogramTicklineColorIndex
        {
            get { return _HistogramTicklineColorIndex; }
            set
            {
                _HistogramTicklineColorIndex = value;
                HistogramTicklineColor = HistogramColors[_HistogramTicklineColorIndex];

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values[HistogramTicklineColorKey] = _HistogramTicklineColorIndex.ToString();
            }
        }

        /// <summary>
        /// Loads the histogram tickline color setting.
        /// </summary>
        /// <param name="localSettings">The local settings.</param>
        private void loadHistogramTicklineColorSetting(Windows.Storage.ApplicationDataContainer localSettings)
        {
            Object value = localSettings.Values[HistogramTicklineColorKey];

            if (value == null)
                HistogramTicklineColorIndex = PreHistogramTicklineColor;
            else
                HistogramTicklineColorIndex = int.Parse(value.ToString());
            HistogramTicklineColor = HistogramColors[_HistogramTicklineColorIndex];
        }

        /// <summary>
        /// Gets the color of the histogram axisline.
        /// </summary>
        /// <value>
        /// The color of the histogram axisline.
        /// </value>
        public OxyColor HistogramAxislineColor { get; private set; }
        /// <summary>
        /// The histogram axisline color index
        /// </summary>
        private int _HistogramAxislineColorIndex;
        /// <summary>
        /// Gets or sets the index of the histogram axisline color.
        /// </summary>
        /// <value>
        /// The index of the histogram axisline color.
        /// </value>
        public int HistogramAxislineColorIndex
        {
            get { return _HistogramAxislineColorIndex; }
            set
            {
                _HistogramAxislineColorIndex = value;
                HistogramAxislineColor = HistogramColors[_HistogramAxislineColorIndex];

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values[HistogramAxislineColorKey] = _HistogramAxislineColorIndex.ToString();
            }
        }

        /// <summary>
        /// Loads the histogram axisline color setting.
        /// </summary>
        /// <param name="localSettings">The local settings.</param>
        private void loadHistogramAxislineColorSetting(Windows.Storage.ApplicationDataContainer localSettings)
        {
            Object value = localSettings.Values[HistogramAxislineColorKey];

            if (value == null)
                HistogramAxislineColorIndex = PreHistogramAxislineColor;
            else
                HistogramAxislineColorIndex = int.Parse(value.ToString());
            HistogramAxislineColor = HistogramColors[_HistogramAxislineColorIndex];
        }
#endregion

        #region METHODS

        /// <summary>
        /// Restores the default settings with pre-default values.
        /// </summary>
        public void RestoreDefaultSettings()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            //localSettings.DeleteContainer(localSettings.Name);

            #region restore default settings with pre-values
            DefaultFontSize = PreDefaultFontSize;
            DefaultFont = PreDefaultFont;
            DefaultThickness = PreDefaultThickness;
            DefaultColorIndex = PreDefaultColor;
            HistogramTextColorIndex = PreHistogramTextColor;
            HistogramTicklineColorIndex = PreHistogramTicklineColor;
            HistogramAxislineColorIndex = PreHistogramAxislineColor;
            ImageWidthOnStartup = PreImageWidthOnStartup;
            ImageHeightOnStartup = PreImageHeightOnStartup;
            MinTextBoxHeight = AbsoluteMinTextToolHeight;
            MinTextBoxWidth = AbsoluteMinTextToolWidth;
            MinThumbSize = AbsoluteMinTextToolThumbSize;
            MinThumbFontSize = AbsoluteMinTextToolThumbFontSize;
            MinTextToolBorderThickness = AbsoluteMinTextToolBorderThickness;
            #endregion
        }

        /// <summary>
        /// Updates the size of the text box from text tool with absolute values and appropriate factors. 
        /// </summary>
        /// <param name="WorkspaceWidth">Width of the workspace.</param>
        /// <param name="WorkspaceHeight">Height of the workspace.</param>
        public void UpdateTextBoxSize( double WorkspaceWidth, double WorkspaceHeight)
        {
            double SignificantSize = Math.Min(WorkspaceWidth, WorkspaceHeight);

            MinTextBoxWidth = Math.Max(AbsoluteMinTextToolWidth, (int)(SignificantSize * MinTextToolWidthScaleFactor));
            MinTextBoxHeight = Math.Max( AbsoluteMinTextToolHeight, (int)(SignificantSize * MinTextToolHeightScaleFactor) );
            MinThumbSize = Math.Max( AbsoluteMinTextToolThumbSize, (int)(SignificantSize * MinTextToolThumbSizeScaleFactor) );
            MinThumbFontSize = Math.Max( AbsoluteMinTextToolThumbFontSize, (int)(SignificantSize * MinTextToolThumbFontSizeScaleFactor) );
            MinTextToolBorderThickness = Math.Max( AbsoluteMinTextToolBorderThickness, (int)(SignificantSize * MinTextToolBorderThicknessScaleFactor) );
        }
#endregion

        #region LOCALIZATION RESOURCES
        /// <summary>
        /// ResourceLoader for ImageEffects
        /// </summary>
        public ResourceLoader Res_ImageEffects = ResourceLoader.GetForCurrentView("ImageEffects");
        /// <summary>
        /// ResourceLoader for UI
        /// </summary>
        public ResourceLoader Res_UI = ResourceLoader.GetForCurrentView("UI");
        #endregion
    }
}
