﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace RGraphics.Models.DrawingTools
{

    /// <summary>
    /// Class responsible for the creation of appropriate textbox and setup it on the specified position on the workspace.
    /// </summary>
    public class TextWriter
    {

        /// <summary>
        /// Gets or sets the workspace.
        /// </summary>
        private IWorkspace WorkSpace { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextWriter"/> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        public TextWriter(IWorkspace workspace)
        {
            this.WorkSpace = workspace;
        }
        
        /// <summary>
        /// Method responsible for addition text into the canvas. It creates temporary textbox which is appropriate formatted (with obturation of useless functionalities) and ready for pasting into the workspace.
        /// </summary>
        /// <param name="cursorPosition">The cursor position.</param>
        /// <param name="text">The text.</param>
        /// <param name="fontSize">Size of the font.</param>
        /// <param name="font">The font.</param>
        /// <param name="color">The color.</param>
        /// <param name="fstyle">The font style.</param>
        /// <param name="fweight">The font weight.</param>
        /// <param name="margin">The margin thickness.</param>
        /// <param name="borderThickness">The border thickness.</param>
        public void WriteText( Point cursorPosition, String text, int fontSize, FontFamily font, Brush color, FontStyle fstyle, FontWeight fweight, Thickness margin, Thickness borderThickness, double width, double height) 
        {
            TextBox textBox = new TextBox()
            {
                #region create transparent text box
                AcceptsReturn = true,
                Text = text,
                FontSize = fontSize,
                FontFamily = font,
                Foreground = color,
                BorderThickness = borderThickness,
                Margin = margin,
                Padding = new Thickness(0),
                BorderBrush = new SolidColorBrush(Colors.Transparent),
                Background = new SolidColorBrush(Colors.Transparent),
                FontStyle = fstyle,
                FontWeight = fweight,
                Width = width,
                Height = height,
                #endregion

                #region disable all functionality
                IsReadOnly = true,
                IsHitTestVisible = false,
                IsDoubleTapEnabled = false,
                IsHoldingEnabled = false,
                IsRightTapEnabled = false,
                IsTapEnabled = false,
                #endregion
            };
            textBox.SelectionChanged += new RoutedEventHandler(disableSelection);

            Canvas.SetTop(textBox, cursorPosition.Y );
            Canvas.SetLeft(textBox, cursorPosition.X );

            ClipToBounds(textBox, cursorPosition);

            WorkSpace.Children.Add(textBox);
        }
        
        /// <summary>
        /// Disables the selection in textbox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The instance containing the event data.</param>
        private void disableSelection(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).SelectionLength = 0;
        }

        /// <summary>
        /// Checks if the textbox is beetween ranges, if not it will be clip to bounds with the specified tolerance.
        /// </summary>
        /// <param name="tb">The textbox.</param>
        /// <param name="position">The position of textbox.</param>
        private void ClipToBounds(TextBox tb, Point position)
        {
            const double tolerance = 0.5;

            Rect rect = new Rect( new Point(0,0), new Point(tb.Width, tb.Height));
            double X1 = 0.0, Y1=0.0, X2 =tb.Width, Y2 =tb.Height;
            bool IfX1Changed = false, IfY1Changed = false;

            if (position.X < 0)
            {
                X1 = Math.Abs(position.X) - tb.Margin.Left + tolerance;
                IfX1Changed = true;
            }

            if (position.X + tb.Width >= WorkSpace.ActualWidth - tb.Margin.Right)
            {
                X2 = WorkSpace.ActualWidth - position.X - tb.Margin.Right;
                if (IfX1Changed) X2 -= X1;
            }

            if (position.Y < 0)
            {
                Y1 = Math.Abs(position.Y) - tb.Margin.Top + tolerance;
                IfY1Changed = true;
            }

            if (position.Y + tb.Height >= WorkSpace.ActualHeight - tb.Margin.Bottom)
            {
                Y2 = WorkSpace.ActualHeight - position.Y - tb.Margin.Bottom;
                if (IfY1Changed) Y2 -= Y1;
            }

            rect = new Rect(X1,Y1,X2,Y2);
            tb.Clip = new RectangleGeometry() { Rect = rect }; 
        }
    }
}
