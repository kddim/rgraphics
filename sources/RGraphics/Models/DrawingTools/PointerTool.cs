﻿using System.Threading.Tasks;
using Windows.UI.Input;
using Windows.UI.Xaml.Controls;

namespace RGraphics.Models.DrawingTools
{
    /// <summary>
    /// Pointer tool. (it does not draw)
    /// </summary>
    public class PointerTool : DrawingToolBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PointerTool"/> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        public PointerTool(IWorkspace workspace)
            : base(workspace)
        {
            Icon = new SymbolIcon(Symbol.Go);
        }


        /// <summary>
        /// <para> The `start drawing` event called when the user clicks/taps on the workspace. </para>
        /// <para> Empty implementation, because the PointerTool does not implement any drawing. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with tap/click.</param>
        protected override Task Started(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }

        /// <summary>
        /// <para> The `continue drawing` event called when user moves the pointer (and when the drawing was started). </para>
        /// <para> Empty implementation, because the PointerTool does not implement any drawing. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with moving the pointer.</param>
        protected override Task Updated(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }

        /// <summary>
        /// <para> The `end drawing` event called when user stops holding the tap/mouse click. </para>
        /// <para> Empty implementation, because the PointerTool does not implement any drawing. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
        protected override Task Finished(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }
    }
}
