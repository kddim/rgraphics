﻿using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.UI.Input;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Streams;
using Windows.Foundation;
using Windows.UI;
using WinRTXamlToolkit.Imaging;
using Windows.UI.Xaml.Media;
using RGraphics.ViewModels;
using Windows.UI.Xaml.Shapes;
using System.Threading.Tasks;

namespace RGraphics.Models.DrawingTools
{
	/// <summary>
	/// Line drawing tool.
	/// </summary>
	public class LineTool : DrawingToolBase
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LineTool"/> class.
		/// </summary>
		/// <param name="workspace">The workspace.</param>
		/// <param name="source">The binding source (like ShapesViewModel).</param>
		public LineTool(IWorkspace workspace, object source)
			: base(workspace)
		{
			Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/line_icon.png"), Foreground = new SolidColorBrush(Colors.White) };
			ScaleX = 1.0;
			ScaleY = 1.0;
			bindingSource = source as ShapesViewModel;
		}

		/// <summary>
		/// Gets or sets the line.
		/// </summary>
		/// <value>
		/// The line.
		/// </value>
		private Line line { get; set; }

		/// <summary>
		/// Gets or sets the binding source.
		/// </summary>
		/// <value>
		/// The binding source.
		/// </value>
		private ShapesViewModel bindingSource { get; set; }

		/// <summary>
		/// Gets or sets the start point.
		/// </summary>
		/// <value>
		/// The start point.
		/// </value>
		PointerPoint startPoint { get; set; }

		/// <summary>
		/// Gets or sets the end point of the selection.
		/// </summary>
		/// <value>
		/// The end point.
		/// </value>
		PointerPoint endPoint { get; set; }

		/// <summary>
		/// The base point of the selection.
		/// </summary>
		Point basePoint = new Point(0, 0);

		/// <summary>
		/// The `start drawing` event called when the user clicks/taps on the workspace.
		/// </summary>
		/// <param name="p">The PointerPoint associated with tap/click.</param>
		protected override Task Started(PointerPoint p)
		{
			line = new Line()
			{
				Stroke = new SolidColorBrush(AssignedWorkspace.SelectedColor),
				StrokeThickness = bindingSource.SelectedThickness,
				X1 = p.Position.X,
				X2 = p.Position.X,
				Y1 = p.Position.Y,
				Y2 = p.Position.Y
			};

			// TODO : MAKE THIS WORK PLOX
			IsRecordingOutOfBounds = true;

			AssignedWorkspace.Children.Add(line);
			startPoint = p;
			endPoint = p;

			// Since this implementation deos not need to await,
			// then returning this, since task with predefined result creates no scheduling overhead
			return Task.FromResult(0);
		}

		/// <summary>
		/// The `continue drawing` event called when user moves the pointer (and when the drawing was started).
		/// </summary>
		/// <param name="p">The PointerPoint associated with moving the pointer.</param>
		protected override Task Updated(PointerPoint p)
		{
			line.X2 = p.Position.X;
			line.Y2 = p.Position.Y;

			endPoint = p;

			// Since this implementation deos not need to await,
			// then returning this, since task with predefined result creates no scheduling overhead
			return Task.FromResult(0);
		}

		/// <summary>
		/// The `end drawing` event called when user stops holding the tap/mouse click.
		/// </summary>
		/// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
		protected override Task Finished(PointerPoint p)
		{
			double x = endPoint.Position.X < startPoint.Position.X ? endPoint.Position.X : startPoint.Position.X;
			double y = endPoint.Position.Y < startPoint.Position.Y ? endPoint.Position.Y : startPoint.Position.Y;

			basePoint = new Point(x, y);

			ClipToBounds(basePoint);

			// Since this implementation deos not need to await,
			// then returning this, since task with predefined result creates no scheduling overhead
			return Task.FromResult(0);
		}

		/// <summary>
		/// Checks the line bounds.
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		/// <returns>True if coordinates stay within workspace, otherwise false.</returns>
		private bool checkLineBounds(double x, double y)
		{
			if (x >= 0 && x < AssignedWorkspace.ActualWidth && y >= 0 && y < AssignedWorkspace.ActualHeight)
				return true;
			return false;
		}


		private void ClipToBounds(Point position)
		{
			double width = line.X2 < line.X1 ? line.X1 - line.X2 : line.X2 - line.X1;
			double height = line.Y2 < line.Y1 ? line.Y1 - line.Y2 : line.Y2 - line.Y1;

			double x = position.X >= 0 ? position.X : 0;
			double y = position.Y >= 0 ? position.Y : 0;

			double factor = 20;

			double X1 = x, Y1 = y, X2 = width, Y2 = height;
			bool IfX1Changed = false, IfY1Changed = false;
			bool IfX2Changed = false, IfY2Changed = false;

			if (position.X < 0)
			{
				X1 = 0;
				IfX1Changed = true;
			}
			else
			{
				X1 -= factor;
			}

			if (position.X + width >= AssignedWorkspace.ActualWidth)
			{
				X2 = AssignedWorkspace.ActualWidth - position.X + factor;
				if (IfX1Changed) X2 -= X1;
				IfX2Changed = true;
			}
			else
			{
				X2 += factor;
				if (!IfX1Changed) X2 += factor;
			}

			if (position.Y < 0)
			{
				Y1 = 0;
				IfY1Changed = true;
			}
			else
			{
				Y1 -= factor;
			}

			if (position.Y + height >= AssignedWorkspace.ActualHeight)
			{
				Y2 = AssignedWorkspace.ActualHeight - position.Y + factor;
				if (IfY1Changed) Y2 -= Y1;
				IfY2Changed = true;
			}
			else
			{
				Y2 += factor;
				if (!IfY1Changed) Y2 += factor;
			}

			if (IfX1Changed || IfX2Changed || IfY1Changed || IfY2Changed)
			{
				Rect rect = new Rect(X1, Y1, X2, Y2);
				line.Clip = new RectangleGeometry() { Rect = rect };
			}
		}

	}
}
