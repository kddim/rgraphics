﻿using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.UI.Input;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Streams;
using Windows.Foundation;
using Windows.UI;
using WinRTXamlToolkit.Imaging;
using Windows.UI.Xaml.Media;
using RGraphics.ViewModels;
using Windows.UI.Xaml.Shapes;
using System.Threading.Tasks;

namespace RGraphics.Models.DrawingTools
{
	/// <summary>
	/// Ellipse drawing tool.
	/// </summary>
	public class EllipseTool : DrawingToolBase
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EllipseTool"/> class.
		/// </summary>
		/// <param name="workspace">The workspace.</param>
		/// <param name="source">The binding source (like ShapesViewModel).</param>
		public EllipseTool(IWorkspace workspace, object source)
			: base(workspace)
		{
			Icon = new BitmapIcon()
			{
				UriSource = new Uri("ms-appx:///Assets/ellipse_icon.png"),
				Foreground = new SolidColorBrush(Colors.White)
			};

			ScaleX = 1.0;
			ScaleY = 1.0;

			bindingSource = source as ShapesViewModel;
		}

		/// <summary>
		/// Gets or sets the ellipse.
		/// </summary>
		/// <value>
		/// The ellipse.
		/// </value>
		private Ellipse ellipse { get; set; }

		/// <summary>
		/// Gets or sets the start point.
		/// </summary>
		/// <value>
		/// The start point.
		/// </value>
		PointerPoint startPoint { get; set; }

		/// <summary>
		/// Gets or sets the end point of the selection.
		/// </summary>
		/// <value>
		/// The end point.
		/// </value>
		PointerPoint endPoint { get; set; }

		/// <summary>
		/// The base point of the selection.
		/// </summary>
		Point basePoint = new Point(0, 0);

		/// <summary>
		/// Gets or sets the binding source.
		/// </summary>
		/// <value>
		/// The binding source.
		/// </value>
		private ShapesViewModel bindingSource { get; set; }

		/// <summary>
		/// The `start drawing` event called when the user clicks/taps on the workspace.
		/// </summary>
		/// <param name="p">The PointerPoint associated with tap/click.</param>
		protected override Task Started(PointerPoint p)
		{
			ellipse = new Ellipse()
			{
				Stroke = new SolidColorBrush(AssignedWorkspace.SelectedColor),
				StrokeThickness = bindingSource.SelectedThickness
			};

			if (bindingSource.IfFill)
				ellipse.Fill = new SolidColorBrush(AssignedWorkspace.SelectedColor);

			Canvas.SetTop(ellipse, p.Position.Y);
			Canvas.SetLeft(ellipse, p.Position.X);

			AssignedWorkspace.Children.Add(ellipse);
			startPoint = p;
			endPoint = p;

			// Since this implementation deos not need to await,
			// then returning this, since task with predefined result creates no scheduling overhead
			return Task.FromResult(0);
		}

		/// <summary>
		/// The `continue drawing` event called when user moves the pointer (and when the drawing was started).
		/// </summary>
		/// <param name="p">The PointerPoint associated with moving the pointer.</param>
		protected override Task Updated(PointerPoint p)
		{
			double x = (p.Position.X < startPoint.Position.X) ? p.Position.X : startPoint.Position.X;
			double y = (p.Position.Y < startPoint.Position.Y) ? p.Position.Y : startPoint.Position.Y;

			Canvas.SetLeft(ellipse, x);
			Canvas.SetTop(ellipse, y);

			ellipse.Width = Math.Abs(p.Position.X - startPoint.Position.X);
			ellipse.Height = Math.Abs(p.Position.Y - startPoint.Position.Y);

			endPoint = p;

			return Task.FromResult(0);
		}

		/// <summary>
		/// The `end drawing` event called when user stops holding the tap/mouse click.
		/// </summary>
		/// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
		protected override Task Finished(PointerPoint p)
		{
			double x = endPoint.Position.X < startPoint.Position.X ? endPoint.Position.X : startPoint.Position.X;
			double y = endPoint.Position.Y < startPoint.Position.Y ? endPoint.Position.Y : startPoint.Position.Y;

			basePoint = new Point(x, y);

			ClipToBounds(basePoint);

			return Task.FromResult(0);
		}

		/// <summary>
		/// Checks the rectangle bounds.
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		/// <param name="xRange">The x coordinate move (endX - startX).</param>
		/// <param name="yRange">The y coordinate move (endY - startY).</param>
		/// <returns>True if coordinates stay within workspace, otherwise false.</returns>
		private bool checkEllipseBounds(double x, double y, double xRange, double yRange)
		{
			if (x >= 0 && x + xRange < AssignedWorkspace.ActualWidth && y >= 0 && y + yRange < AssignedWorkspace.ActualHeight)
				return true;
			return false;
		}

		private void ClipToBounds(Point position)
		{
			double X1 = 0, Y1 = 0, X2 = ellipse.Width, Y2 = ellipse.Height;
			bool IfX1Changed = false, IfY1Changed = false;

			if (position.X < 0)
			{
				X1 = Math.Abs(position.X);
				IfX1Changed = true;
			}

			if (position.X + ellipse.Width >= AssignedWorkspace.ActualWidth)
			{
				X2 = AssignedWorkspace.ActualWidth - position.X;
				if (IfX1Changed) X2 -= X1;
			}

			if (position.Y < 0)
			{
				Y1 = Math.Abs(position.Y);
				IfY1Changed = true;
			}

			if (position.Y + ellipse.Height >= AssignedWorkspace.ActualHeight)
			{
				Y2 = AssignedWorkspace.ActualHeight - position.Y;
				if (IfY1Changed) Y2 -= Y1;
			}

			Rect rect = new Rect(X1, Y1, X2, Y2);
			ellipse.Clip = new RectangleGeometry() { Rect = rect };
		}
	}
}
