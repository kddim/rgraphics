﻿using Caliburn.Micro;
using RGraphics.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Input;
using Windows.UI.Xaml.Controls;

namespace RGraphics.Models.DrawingTools
{
    /// <summary>
    /// The drawing tool base class. Each drawing tool should inherit from this.
    /// </summary>
    public abstract class DrawingToolBase : Screen, IDrawingTool
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrawingToolBase"/> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        public DrawingToolBase(IWorkspace workspace)
        {
            AssignedWorkspace = workspace;
        }


        /// <summary>
        /// Called when Flyout is activated. By default activates the tool.
        /// </summary>
        public virtual void FlyoutActivated()
        {
            AssignedWorkspace.ActiveDrawingTool = this;
        }



        /// <summary>
        /// Gets the assigned workspace.
        /// </summary>
        /// <value>
        /// The assigned workspace.
        /// </value>
        public IWorkspace AssignedWorkspace { get; private set; }
        /// <summary>
        /// Gets the last point.
        /// </summary>
        /// <value>
        /// The last point.
        /// </value>
        protected PointerPoint LastPoint { get; private set; }


        private IconElement _Icon;
        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        public IconElement Icon
        {
            get { return _Icon; }
            set
            {
                _Icon = value;
                NotifyOfPropertyChange(() => Icon);
            }
        }

        private bool _IsRecordingOutOfBounds = false;
        /// <summary>
        /// <para>Gets or sets a value indicating whether Update() will be fired even if pointer is out of bounds of the Workspace.</para>
        /// <para>Default value is False</para>
        /// </summary>
        public bool IsRecordingOutOfBounds
        {
            get { return _IsRecordingOutOfBounds; }
            set
            {
                _IsRecordingOutOfBounds = value;
                NotifyOfPropertyChange(() => IsRecordingOutOfBounds);
            }
        }


        private double _ScaleX = 1.6;
        /// <summary>
        /// Gets or sets the scale x.
        /// </summary>
        /// <value>
        /// The scale x.
        /// </value>
        public double ScaleX
        {
            get { return _ScaleX; }
            set
            {
                _ScaleX = value;
                NotifyOfPropertyChange(() => ScaleX);
            }
        }

        private double _ScaleY = 1.6;
        /// <summary>
        /// Gets or sets the scale y.
        /// </summary>
        /// <value>
        /// The scale y.
        /// </value>
        public double ScaleY
        {
            get { return _ScaleY; }
            set
            {
                _ScaleY = value;
                NotifyOfPropertyChange(() => ScaleY);
            }
        }



        private Boolean _HasAttributes = false;
        /// <summary>
        /// Gets or sets a value indicating whether this instance has attributes.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has attributes; otherwise, <c>false</c>.
        /// </value>
        public Boolean HasAttributes
        {
            get { return _HasAttributes; }
            set
            {
                _HasAttributes = value;
                NotifyOfPropertyChange(() => HasAttributes);
            }
        }

        /// <summary>
        /// Starts drawing on the workspace.
        /// </summary>
        /// <param name="p">The PointerPoint.</param>
        public async Task Start(PointerPoint p)
        {
            RemoveTextBoxFromCanvas(); 
            
            await Started(p);
            LastPoint = p;
        }

        /// <summary>
        /// Updates the workspace by drawing next point(s).
        /// </summary>
        /// <param name="p">The PointerPoint.</param>
        public async Task Update(PointerPoint p)
        {
            await Updated(p);
            LastPoint = p;
        }

        /// <summary>
        /// Finishes the drawing.
        /// </summary>
        /// <param name="p">The PointerPoint.</param>
        public async Task Finish(PointerPoint p)
        {
            await Finished(p);
            LastPoint = null;
        }

        /// <summary>
        /// Removes the text box from canvas.
        /// </summary>
        private void RemoveTextBoxFromCanvas()
        {
            if (TextToolViewModel.moveableContainer != null)
                AssignedWorkspace.Children.Remove(TextToolViewModel.moveableContainer);
        }

        /// <summary>
        /// The `start drawing` event called when the user clicks/taps on the workspace.
        /// </summary>
        /// <param name="p">The PointerPoint associated with tap/click.</param>
        protected abstract Task Started(PointerPoint p);

        /// <summary>
        /// The `continue drawing` event called when user moves the pointer (and when the drawing was started).
        /// </summary>
        /// <param name="p">The PointerPoint associated with moving the pointer.</param>
        protected abstract Task Updated(PointerPoint p);

        /// <summary>
        /// The `end drawing` event called when user stops holding the tap/mouse click.
        /// </summary>
        /// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
        protected abstract Task Finished(PointerPoint p);
    }
}
