﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGraphics.Models.ImageEffects
{
    /// <summary>
    /// Image effects/processing utility class.
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// Gets index on the image with specified width. Does not check for errors.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="width">The image width.</param>
        /// <returns>Calculated index (x + y * width)</returns>
        public static int Index(int x, int y, int width)
        {
            return x + y * width;
        }
    }
}
