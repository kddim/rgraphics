﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGraphics.Models.ImageEffects
{
    /// <summary>
    /// Useful definitions for image effects/processing.
    /// </summary>
    public static class Def
    {

        /// <summary>
        /// <para> Alpha channel bitmask. </para>
        /// <para> Example usage: int r = (intColor &amp; Def.MASK_R) &gt;&gt; Def.SHIFT_R; </para>
        /// <para> Where intColor is the Integer with coded color (ARGB, 1 Byte = 8 bits per channel) </para>
        /// </summary>
        public const int MASK_A = unchecked((int)0xFF000000);
        
        /// <summary>
        /// <para> Red channel bitmask. </para>
        /// <para> Example usage: int r = (intColor &amp; Def.MASK_R) &gt;&gt; Def.SHIFT_R; </para>
        /// <para> Where intColor is the Integer with coded color (ARGB, 1 Byte = 8 bits per channel) </para>
        /// </summary>
        public const int MASK_R = 0x00FF0000;

        /// <summary>
        /// <para> Green channel bitmask. </para>
        /// <para> Example usage: int g = (intColor &amp; Def.MASK_G) &gt;&gt; Def.SHIFT_G; </para>
        /// <para> Where intColor is the Integer with coded color (ARGB, 1 Byte = 8 bits per channel) </para>
        /// </summary>
        public const int MASK_G = 0x0000FF00;


        /// <summary>
        /// <para> Blue channel bitmask. </para>
        /// <para> Example usage: int b = (intColor &amp; Def.MASK_B) &gt;&gt; Def.SHIFT_B; </para>
        /// <para> Where intColor is the Integer with coded color (ARGB, 1 Byte = 8 bits per channel) </para>
        /// </summary>
        public const int MASK_B = 0x000000FF;


        /// <summary>
        /// <para> Red channel bitshift value. </para>
        /// <para> Example usage: int color = (r &lt;&lt; Def.SHIFT_R) | (g &lt;&lt; Def.SHIFT_G) | (b &lt;&lt; Def.SHIFT_B); </para>
        /// <para> Where r, g, b are red, green and blue channel values [0-255]. </para>
        /// </summary>
        public const int SHIFT_R = 16;


        /// <summary>
        /// <para> Green channel bitshift value. </para>
        /// <para> Example usage: int color = (r &lt;&lt; Def.SHIFT_R) | (g &lt;&lt; Def.SHIFT_G) | (b &lt;&lt; Def.SHIFT_B); </para>
        /// <para> Where r, g, b are red, green and blue channel values [0-255]. </para>
        /// </summary>
        public const int SHIFT_G = 8;


        /// <summary>
        /// <para> Blue channel left bitshift value. </para>
        /// <para> Example usage: int color = (r &lt;&lt; Def.SHIFT_R) | (g &lt;&lt; Def.SHIFT_G) | (b &lt;&lt; Def.SHIFT_B); </para>
        /// <para> Where r, g, b are red, green and blue channel values [0-255]. </para>
        /// </summary>
        public const int SHIFT_B = 0;
    }
}
