﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Input.Inking;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace RGraphics.Models
{
    /// <summary>
    /// Represents part of the workspace, which (will be calculated via Actual&Workspace sizes)
    /// </summary>
    public enum WorkspacePart
    {
        /// <summary>
        /// Center of the image will be automatically calculated and applied i.e in ZoomToFactor functionality
        /// </summary>
        Center,
        /// <summary>
        /// Represents upper-left part of the image, which is equal to point (0, 0)
        /// </summary>
        UpperLeft
    }

    /// <summary>
    /// Workspace interface
    /// </summary>
    public interface IWorkspace
    {
        /// <summary>
        /// Gets or sets current zoom.
        /// </summary>
        double ZoomFactor { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether pinch-to-zoom (or ctrl+wheel) feature is enabled.
        /// </summary>
        /// </value>
        bool IsZoomEnabled { get; set; }
        /// <summary>
        /// Gets or sets the minimum zoom factor.
        /// </summary>>
        double MinZoomFactor { get; set; }
        /// <summary>
        /// Gets or sets the maximum zoom factor.
        /// </summary>
        double MaxZoomFactor { get; set; }
        /// <summary>
        /// Gets the width of the workspace (usually it's equal to screen's resolution)
        /// </summary>
        double WorkspaceWidth { get; }
        /// <summary>
        /// Gets the height of the workspace (usually it's equal to screen's resolution)
        /// </summary>
        double WorkspaceHeight { get; }
        /// <summary>
        /// Gets the actual width of the image.
        /// </summary>
        double ActualWidth { get; }
        /// <summary>
        /// Gets the actual height of the image.
        /// </summary>
        double ActualHeight { get; }

        /// <summary>
        /// Gets or sets the image processing tool.
        /// </summary>
        IImageProcessing ImageProcessing { get; set; }


        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        WriteableBitmap Image { get; set; }

        /// <summary>
        /// Occurs when [zoom changed].
        /// </summary>
        event EventHandler<ZoomChangedEventArgs> ZoomChanged;
        /// <summary>
        /// Occurs when [workspace size changed].
        /// </summary>
        event EventHandler<SizeChangedEventArgs> WorkspaceSizeChanged;
        /// <summary>
        /// Occurs when [actual size changed].
        /// </summary>
        event EventHandler<SizeChangedEventArgs> ActualSizeChanged;
        /// <summary>
        /// Occurs when [UI refreshed].
        /// </summary>
        event EventHandler<EventArgs> UIRefreshed;

        /// <summary>
        /// Gets or sets the Canvas's children UIElements (images, drawings, texts, etc.)
        /// </summary>
        UIElementCollection Children { get; set; }
        /// <summary>
        /// Gets or sets currently active drawing tool.
        /// </summary>
        /// <value>
        /// The active drawing tool.
        /// </value>
        IDrawingTool ActiveDrawingTool { get; set; }

        /// <summary>
        /// <para>Gets or sets a value indicating whether pixels which are out of workspace's bound will be rendered</para>
        /// <para>Default value is True.</para>
        /// </summary>
        bool IsClipToBoundsEnabled { get; set; }

        /// <summary>
        /// Gets or sets Selected Color.
        /// </summary>
        Color SelectedColor { get; set; }

        /// <summary>
        /// <para>ActualSize <= WorkspaceSize: Shows whole image with 1.0 zoom factor</para>
        /// <para>ActualSize > WorkspaceSize: zooms image to fit in the workspace</para>
        /// </summary>
        void ZoomToFitWorkspace();
        /// <summary>
        /// Explicitly zooms image to a given zoom factor.
        /// </summary>
        /// <param name="zoomFactor">The zoom factor.</param>
        /// <param name="workspacePart">Part of the image.</param>
        void Zoom(double zoomFactor, WorkspacePart workspacePart);
        /// <summary>
        /// Explicitly zooms image to a given zoom factor.
        /// </summary>
        /// <param name="zoomFactor">The zoom factor.</param>
        /// <param name="horizontalOffset">The horizontal offset ( (0,0) for left upper corner)</param>
        /// <param name="verticalOffset">The vertical offset ( (0,0) for left upper corner)</param>
        void Zoom(double zoomFactor, double? horizontalOffset, double? verticalOffset);

        /// <summary>
        /// Updates the Workspace's size.
        /// </summary>
        /// <param name="workspaceWidth">Width of the workspace.</param>
        /// <param name="workspaceHeight">Height of the workspace.</param>
        void UpdateWorkspaceSize(double workspaceWidth, double workspaceHeight);
        /// <summary>
        /// Invalidates UI (ScrollViewer and Canvas)
        /// </summary>
        void RefreshUI();
        /// <summary>
        /// Updates the actual size of the image.
        /// </summary>
        /// <param name="actualWidth">The actual image's width.</param>
        /// <param name="actualHeight">The actual image's height.</param>
        void UpdateActualSize(double actualWidth, double actualHeight);
    }
}
