﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace RGraphics.Models
{
    /// <summary>
    /// Manages save notifications.
    /// </summary>
    public class SaveNotififaction
    {
        /// <summary>
        /// Avaiable save modes.
        /// </summary>
        public enum Mode
        {
            /// <summary>
            /// This mode will prevent application from saving and/or overriding image.
            /// </summary>
            DoNotSave,
            /// <summary>
            /// This mode will indicate save of the current image's appearence.
            /// </summary>
            Save
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveNotififaction"/> class.
        /// </summary>
        /// <param name="saveMode">The save mode.</param>
        public SaveNotififaction(Mode saveMode)//, WriteableBitmap image)
        {
            this.SaveMode = saveMode;
            //this.Image = image;
        }

        /// <summary>
        /// Gets or sets the save mode.
        /// </summary>
        /// <value>
        /// The save mode.
        /// </value>
        public Mode SaveMode { get; set; }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        public WriteableBitmap Image { get; set; }
    }
}
