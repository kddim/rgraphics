﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml.Media.Imaging;

namespace RGraphics.Models
{
    /// <summary>
    /// Available ARGB channels.
    /// </summary>
    public enum Channel 
    {
        /// <summary>
        /// Alpha channel
        /// </summary>
        A,
        /// <summary>
        /// Red channel
        /// </summary>
        R,
        /// <summary>
        /// Green channel
        /// </summary>
        G,
        /// <summary>
        /// Blue channel
        /// </summary>
        B
    };

    /// <summary>
    /// Image effect interface
    /// </summary>
    public interface IImageEffect
    {
        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        String Name { get; }
        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        String Description { get; }

        /// <summary>
        /// Gets a value indicating whether apply button is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if apply button is enabled; otherwise, <c>false</c>.
        /// </value>
        bool ApplyEnabled { get; set; }

        /// <summary>
        /// Applies effect basing on specified source
        /// </summary
        /// <param name="source">The source image</param>
        /// <param name="operationState">Operation state: progress and cancel notification.</param>
        /// <returns><typeparamref name="IImageWrapper"/>instance with applied effect</returns>
        WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState);

    }
}
