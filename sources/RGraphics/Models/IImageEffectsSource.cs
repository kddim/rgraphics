﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGraphics.Models
{
    /// <summary>
    /// Image effects source interface
    /// </summary>
    public interface IImageEffectsSource
    {
        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        IEnumerable<IImageEffect> Source { get; }
    }
}
