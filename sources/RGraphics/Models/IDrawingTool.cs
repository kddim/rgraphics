﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Input;
using Windows.UI.Xaml.Controls;

namespace RGraphics.Models
{
    /// <summary>
    /// Drawing tool interface
    /// </summary>
    public interface IDrawingTool
    {
        /// <summary>
        /// <para>Gets or sets a value indicating whether Update() will be fireing even if pointer is out of bounds of the Workspace.</para>
        /// <para>Default value is False</para>
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is recording out of bounds; otherwise, <c>false</c>.
        /// </value>
        bool IsRecordingOutOfBounds { get; set; }
        /// <summary>
        /// Starts the specified action.
        /// </summary>
        /// <param name="p">The point.</param>
        /// <returns></returns>
        Task Start(PointerPoint p);
        /// <summary>
        /// Updates the specified action.
        /// </summary>
        /// <param name="p">The point.</param>
        /// <returns></returns>
        Task Update(PointerPoint p);
        /// <summary>
        /// Finishes the specified action.
        /// </summary>
        /// <param name="p">The point.</param>
        /// <returns></returns>
        Task Finish(PointerPoint p);
    }
}
