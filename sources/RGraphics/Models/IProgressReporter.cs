﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace RGraphics.Models
{
    /// <summary>
    /// Progress reporter interface
    /// </summary>
    public interface IProgressReporter
    {
        /// <summary>
        /// Gets or sets the content shown on the ProgressReporterDialogView
        /// </summary>
        object Content { get; set; }
        /// <summary>
        /// Gets or sets current progress of the operation.
        /// </summary>
        /// <value>
        /// The progress.
        /// </value>
        double Progress { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is indeterminate.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is indeterminate; otherwise, <c>false</c>.
        /// </value>
        bool IsIndeterminate { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether operation can be cancelled
        /// </summary>
        bool CanUserCancel { get; set; }
        /// <summary>
        /// Gets a value indicating whether operation was canceled.
        /// </summary>
        bool IsOperationCanceled { get; }
        /// <summary>
        /// Shows progress reporter dialog
        /// </summary>
        void Show();
        /// <summary>
        /// Closes the reporter dialog.
        /// </summary>
        void Completed();

        /// <summary>
        /// Gets a value indicating whether operation is completed.
        /// </summary>
        bool IsCompleted { get; }

        /// <summary>
        /// Resets whole progress reporter (called before reuse).
        /// </summary>
        void Reset();
    }
}
