﻿using Caliburn.Micro;
using RGraphics.ViewModels;
using System;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Input.Inking;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace RGraphics.Models
{
	/// <summary>
	/// Manages the workspace components and specified operations associated with the workspace processing.
	/// </summary>
	public class Workspace : PropertyChangedBase, IWorkspace
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Workspace"/> class.
		/// </summary>
		public Workspace(Settings settings)
		{
			ZoomFactor = 1.0;
			MinZoomFactor = 0.2;
			MaxZoomFactor = 100.0;
			IsZoomEnabled = true;

			SelectedColor = settings.DefaultColor;
			PreviousColor = SelectedColor;
		}

		/// <summary>
		/// The zoom factor
		/// </summary>
		private double _ZoomFactor;
		/// <summary>
		/// Gets or sets current zoom.
		/// </summary>
		public virtual double ZoomFactor
		{
			get { return _ZoomFactor; }
			set
			{
				_ZoomFactor = value;
				NotifyOfPropertyChange(() => ZoomFactor);
			}
		}

		/// <summary>
		/// The is zoom enabled
		/// </summary>
		private Boolean _IsZoomEnabled;
		/// <summary>
		/// Gets or sets a value indicating whether this instance is zoom enabled.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is zoom enabled; otherwise, <c>false</c>.
		/// </value>
		public Boolean IsZoomEnabled
		{
			get { return _IsZoomEnabled; }
			set
			{
				_IsZoomEnabled = value;
				NotifyOfPropertyChange(() => IsZoomEnabled);
			}
		}

		/// <summary>
		/// The is clip to bounds enabled
		/// </summary>
		private bool _IsClipToBoundsEnabled = true;
		/// <summary>
		/// <para>Gets or sets a value indicating whether pixels which are out of workspace's bound will be rendered</para>
		/// <para>Default value is True.</para>
		/// </summary>
		public bool IsClipToBoundsEnabled
		{
			get { return _IsClipToBoundsEnabled; }
			set
			{
				_IsClipToBoundsEnabled = value;
				NotifyOfPropertyChange(() => IsClipToBoundsEnabled);
			}
		}


		/// <summary>
		/// The minimum zoom factor
		/// </summary>
		private double _MinZoomFactor;
		/// <summary>
		/// Gets or sets the minimum zoom factor.
		/// </summary>
		/// &gt;
		public double MinZoomFactor
		{
			get { return _MinZoomFactor; }
			set
			{
				_MinZoomFactor = value;
				NotifyOfPropertyChange(() => MinZoomFactor);
			}
		}

		/// <summary>
		/// The maximum zoom factor
		/// </summary>
		private double _MaxZoomFactor;
		/// <summary>
		/// Gets or sets the maximum zoom factor.
		/// </summary>
		public double MaxZoomFactor
		{
			get { return _MaxZoomFactor; }
			set
			{
				_MaxZoomFactor = value;
				NotifyOfPropertyChange(() => MaxZoomFactor);
			}
		}

		/// <summary>
		/// The workspace width
		/// </summary>
		private double _WorkspaceWidth;
		/// <summary>
		/// Gets the width of the workspace (usually it's equal to screen's resolution)
		/// </summary>
		public double WorkspaceWidth
		{
			get { return _WorkspaceWidth; }
			protected set
			{
				_WorkspaceWidth = value;
				NotifyOfPropertyChange(() => WorkspaceWidth);

			}
		}

		/// <summary>
		/// The workspace height
		/// </summary>
		private double _WorkspaceHeight;
		/// <summary>
		/// Gets the height of the workspace (usually it's equal to screen's resolution)
		/// </summary>
		public double WorkspaceHeight
		{
			get { return _WorkspaceHeight; }
			protected set
			{
				_WorkspaceHeight = value;
				NotifyOfPropertyChange(() => WorkspaceHeight);
			}
		}

		/// <summary>
		/// The actual width
		/// </summary>
		private double _ActualWidth;
		/// <summary>
		/// Gets the actual width of the image.
		/// </summary>
		public double ActualWidth
		{
			get { return _ActualWidth; }
			protected set
			{
				_ActualWidth = value;
				NotifyOfPropertyChange(() => ActualWidth);
				// Send event by Caliburn.Micro's EventAggregator to inform objects interested in size change
				IoC.Get<IEventAggregator>().PublishOnCurrentThread(new SizeChangedEventArgs(ActualWidth, ActualHeight));
			}
		}

		/// <summary>
		/// The actual height
		/// </summary>
		private double _ActualHeight;
		/// <summary>
		/// Gets the actual height of the image.
		/// </summary>
		public double ActualHeight
		{
			get { return _ActualHeight; }
			protected set
			{
				_ActualHeight = value;
				NotifyOfPropertyChange(() => ActualHeight);
				// Send event by Caliburn.Micro's EventAggregator to inform objects interested in size change
				IoC.Get<IEventAggregator>().PublishOnCurrentThread(new SizeChangedEventArgs(ActualWidth, ActualHeight));
			}
		}

		/// <summary>
		/// The children
		/// </summary>
		private UIElementCollection _Children;
		/// <summary>
		/// Gets or sets the Canvas's children UIElements (images, drawings, texts, etc.)
		/// </summary>
		public UIElementCollection Children
		{
			get { return _Children; }
			set
			{
				_Children = value;
				NotifyOfPropertyChange(() => Children);
			}
		}

		/// <summary>
		/// The active drawing tool
		/// </summary>
		private IDrawingTool _ActiveDrawingTool;
		/// <summary>
		/// Gets or sets currently active drawing tool.
		/// </summary>
		/// <value>
		/// The active drawing tool.
		/// </value>
		public IDrawingTool ActiveDrawingTool
		{
			get { return _ActiveDrawingTool; }
			set
			{
				_ActiveDrawingTool = value;
				NotifyOfPropertyChange(() => ActiveDrawingTool);
			}
		}

		/// <summary>
		/// The selected color
		/// </summary>
		private Color _SelectedColor;
		/// <summary>
		/// Gets or sets Selected Color.
		/// </summary>
		public Color SelectedColor
		{
			get { return _SelectedColor; }
			set
			{
				if (_SelectedColor != null)
					PreviousColor = _SelectedColor;

				_SelectedColor = value;
				NotifyOfPropertyChange(() => SelectedColor);
			}
		}

		/// <summary>
		/// The previous selected color
		/// </summary>
		private Color _PreviousColor;
		/// <summary>
		/// Gets or sets previous selected Color.
		/// </summary>
		public Color PreviousColor
		{
			get { return _PreviousColor; }
			set
			{
				_PreviousColor = value;
				NotifyOfPropertyChange(() => PreviousColor);
			}
		}

		/// <summary>
		/// The image processing
		/// </summary>
		private IImageProcessing _ImageProcessing;
		/// <summary>
		/// Gets or sets the image processing tool.
		/// </summary>
		public IImageProcessing ImageProcessing
		{
			get { return _ImageProcessing; }
			set
			{
				_ImageProcessing = value;
				NotifyOfPropertyChange(() => ImageProcessing);
			}
		}


		/// <summary>
		/// The image
		/// </summary>
		private WriteableBitmap _Image;
		/// <summary>
		/// Gets or sets the image.
		/// </summary>
		/// <value>
		/// The image.
		/// </value>
		public WriteableBitmap Image
		{
			get { return _Image; }
			set
			{
				_Image = value;
				IoC.Get<IEventAggregator>().PublishOnCurrentThread(
					new SaveNotififaction(SaveNotififaction.Mode.Save));
				NotifyOfPropertyChange(() => Image);
			}
		}

		/// <summary>
		/// Occurs when [zoom changed].
		/// </summary>
		public event EventHandler<ZoomChangedEventArgs> ZoomChanged;
		/// <summary>
		/// Raises the <see cref="E:ZoomChanged" /> event.
		/// </summary>
		/// <param name="e">The <see cref="ZoomChangedEventArgs"/> instance containing the event data.</param>
		protected void OnZoomChanged(ZoomChangedEventArgs e)
		{
			if (ZoomChanged != null)
				ZoomChanged(this, e);
		}

		/// <summary>
		/// Occurs when [workspace size changed].
		/// </summary>
		public event EventHandler<SizeChangedEventArgs> WorkspaceSizeChanged;
		/// <summary>
		/// Raises the <see cr="E:WorkspaceSizeChanged" /> event.
		/// </summary>
		/// <param name="e">The <see cref="SizeChangedEventArgs"/> instance containing the event data.</param>
		protected void OnWorkspaceSizeChanged(SizeChangedEventArgs e)
		{
			if (WorkspaceSizeChanged != null)
				WorkspaceSizeChanged(this, e);
		}

		/// <summary>
		/// Occurs when [actual size changed].
		/// </summary>
		public event EventHandler<SizeChangedEventArgs> ActualSizeChanged;
		/// <summary>
		/// Raises the <see cref="E:ActualSizeChanged" /> event.
		/// </summary>
		/// <param name="e">The <see cref="SizeChangedEventArgs"/> instance containing the event data.</param>
		protected void OnActualSizeChanged(SizeChangedEventArgs e)
		{
			if (ActualSizeChanged != null)
			{
				ActualSizeChanged(this, e);
				UpdateRequiredProperties();
			}
		}

		/// <summary>
		/// Updates the required properties.
		/// </summary>
		private void UpdateRequiredProperties()
		{
			var set = IoC.Get<Settings>() as Settings;

			set.UpdateTextBoxSize(ActualWidth, ActualHeight);
		}

		/// <summary>
		/// Occurs when [UI refreshed].
		/// </summary>
		public event EventHandler<EventArgs> UIRefreshed;

		/// <summary>
		/// Zooms to fit workspace.
		/// </summary>
		public void ZoomToFitWorkspace()
		{
			double zoomFactor = 1.0;
			if (WorkspaceWidth < ActualWidth)
				zoomFactor = WorkspaceWidth / ActualWidth;
			else if (WorkspaceHeight < ActualHeight && WorkspaceHeight / ActualHeight < zoomFactor)
				zoomFactor = WorkspaceHeight / ActualHeight;

			OnZoomChanged(new ZoomChangedEventArgs(zoomFactor, 0, 0));
		}
		/// <summary>
		/// Explicitly zooms image to a given zoom factor.
		/// </summary>
		/// <param name="zoomFactor">The zoom factor.</param>
		/// <param name="workspacePart">Part of the image.</param>
		public void Zoom(double zoomFactor, WorkspacePart workspacePart)
		{
			double? horizontalOffset = null;
			double? verticalOffset = null;

			if (workspacePart == WorkspacePart.Center)
			{
				horizontalOffset = ActualWidth * zoomFactor / 2.0 - WorkspaceWidth / 2.0;
				verticalOffset = ActualHeight * zoomFactor / 2.0 - WorkspaceHeight / 2.0;
			}
			else if (workspacePart == WorkspacePart.UpperLeft)
			{
				horizontalOffset = 0;
				verticalOffset = 0;
			}

			OnZoomChanged(new ZoomChangedEventArgs(zoomFactor, horizontalOffset, verticalOffset));
		}
		/// <summary>
		/// Explicitly zooms image to a given zoom factor.
		/// </summary>
		/// <param name="zoomFactor">The zoom factor.</param>
		/// <param name="horizontalOffset">The horizontal offset ( (0,0) for left upper corner)</param>
		/// <param name="verticalOffset">The vertical offset ( (0,0) for left upper corner)</param>
		public void Zoom(double zoomFactor, double? horizontalOffset, double? verticalOffset)
		{
			OnZoomChanged(new ZoomChangedEventArgs(zoomFactor, horizontalOffset, verticalOffset));
		}
		/// <summary>
		/// Updates the Workspace's size.
		/// </summary>
		/// <param name="workspaceWidth">Width of the workspace.</param>
		/// <param name="workspaceHeight">Height of the workspace.</param>
		public void UpdateWorkspaceSize(double workspaceWidth, double workspaceHeight)
		{
			if (WorkspaceWidth != workspaceWidth || WorkspaceHeight != workspaceHeight)
			{
				WorkspaceWidth = workspaceWidth;
				WorkspaceHeight = workspaceHeight;
				OnWorkspaceSizeChanged(new SizeChangedEventArgs(WorkspaceWidth, WorkspaceHeight));
			}

		}

		/// <summary>
		/// Invalidates UI (ScrollViewer and Canvas)
		/// </summary>
		public void RefreshUI()
		{
			if (UIRefreshed != null)
				UIRefreshed(this, new EventArgs());
		}

		/// <summary>
		/// Updates the actual size of the image.
		/// </summary>
		/// <param name="actualWidth">The actual image's width.</param>
		/// <param name="actualHeight">The actual image's height.</param>
		public void UpdateActualSize(double actualWidth, double actualHeight)
		{
			if (ActualWidth != actualWidth || ActualHeight != actualHeight)
			{
				ActualWidth = actualWidth;
				ActualHeight = actualHeight;
				OnActualSizeChanged(new SizeChangedEventArgs(ActualWidth, ActualHeight));
			}

		}
	}
}
