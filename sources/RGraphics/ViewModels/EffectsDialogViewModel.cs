﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// The EffectsDialogViewModel associated with EffectsDialogView.
    /// </summary>
    public class EffectsDialogViewModel : Screen, IDialog
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EffectsDialogViewModel"/> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        /// <param name="navigationService">The navigation service.</param>
        /// <param name="effectsSource">The effects source.</param>
        /// <param name="progressReporter">The progress reporter.</param>
        public EffectsDialogViewModel(IWorkspace workspace, INavigationService navigationService, IImageEffectsSource effectsSource, IProgressReporter progressReporter)
        {
            this.Workspace = workspace;
            this.EffectsSource = effectsSource;
            this.ProgressReporter = progressReporter;
            this.NavigationService = navigationService;
        }

        /// <summary>
        /// Gets the workspace.
        /// </summary>
        /// <value>
        /// The workspace.
        /// </value>
        public IWorkspace Workspace { get; private set; }
        /// <summary>
        /// Gets the effects source.
        /// </summary>
        /// <value>
        /// The effects source.
        /// </value>
        public IImageEffectsSource EffectsSource { get; private set; }
        /// <summary>
        /// Gets or sets the progress reporter.
        /// </summary>
        /// <value>
        /// The progress reporter.
        /// </value>
        private IProgressReporter ProgressReporter { get; set; }
        /// <summary>
        /// Gets or sets the navigation service.
        /// </summary>
        /// <value>
        /// The navigation service.
        /// </value>
        private INavigationService NavigationService { get; set; }


        private bool _IsOpen;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is open.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is open; otherwise, <c>false</c>.
        /// </value>
        public bool IsOpen
        {
            get { return _IsOpen; }
            set
            {
                _IsOpen = value;
                NotifyOfPropertyChange(() => IsOpen);
            }
        }

        private IImageEffect _SelectedEffect;
        /// <summary>
        /// Gets or sets the selected effect.
        /// </summary>
        /// <value>
        /// The selected effect.
        /// </value>
        public IImageEffect SelectedEffect
        {
            get { return _SelectedEffect; }
            set
            {
                _SelectedEffect = value;
                NotifyOfPropertyChange(() => SelectedEffect);
            }
        }

        /// <summary>
        /// Applies the selected effect (or does nothing when SelectedEffect == null).
        /// </summary>
        public void Apply()
        {
            if (SelectedEffect == null)
                return;

            ProgressReporter.Reset();
            Workspace.Image = SelectedEffect.Apply(Workspace.Image, ProgressReporter);
            IsOpen = false;

            IoC.Get<ShellViewModel>().UndoRedoStack.Push(Workspace.Image);
        }
    }
}
