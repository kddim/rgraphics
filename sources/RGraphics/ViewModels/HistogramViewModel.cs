﻿using Caliburn.Micro;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.System.Threading;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Common;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// The HistogramViewModel associated with HistogramView.
    /// </summary>
    public class HistogramViewModel : Screen
    {
        /// <summary>
        /// The navigation service
        /// </summary>
        private readonly INavigationService navigationService;
        /// <summary>
        /// The workspace
        /// </summary>
        private readonly IWorkspace Workspace;

        /// <summary>
        /// The grey hue factors
        /// </summary>
        private readonly double[] greyHueFactors = { 0.3, 0.59, 0.11 };

        /// <summary>
        /// The pixel channels (Red, Green, Blue, Alpha)
        /// </summary>
        private enum channels : int { R = 2, G = 1, B = 0, A = 3 };

        /// <summary>
        /// Initializes a new instance of the <see cref="HistogramViewModel" /> class.
        /// </summary>
        /// <param name="navigationService">The navigation service.</param>
        /// <param name="workspace">The workspace.</param>
        public HistogramViewModel(INavigationService navigationService, IWorkspace workspace)
        {
            this.Workspace = workspace;
            this.navigationService = navigationService;
        }

        /// <summary>
        /// Called when initializing.
        /// </summary>
        protected override void OnInitialize()
        {
            var settings = IoC.Get<Settings>();

            PlotModel[] models = new PlotModel[] { RedModel, GreenModel, BlueModel, GrayscaleModel };

            foreach (var model in models)
            {
                model.TextColor = settings.HistogramTextColor;
                model.PlotAreaBorderColor = settings.HistogramAxislineColor;

                model.Axes.Add(new LinearAxis()
                    {
                        Position = AxisPosition.Bottom,
                        TextColor = settings.HistogramTextColor,
                        TicklineColor = settings.HistogramTicklineColor,
                        AxislineColor = settings.HistogramAxislineColor
                    }
                );

                model.Axes.Add(new LinearAxis()
                    {
                        Position = AxisPosition.Left,
                        TicklineColor = settings.HistogramTicklineColor,
                        AxislineColor = settings.HistogramAxislineColor
                    }
                );
            }
        }
        /// <summary>
        /// Called when an attached view's Loaded event fires.
        /// </summary>
        /// <param name="view"></param>
        protected async override void OnViewLoaded(object view)
        {
            AreaSeries redSeries = new AreaSeries();
            AreaSeries greenSeries = new AreaSeries();
            AreaSeries blueSeries = new AreaSeries();
            AreaSeries grayscaleSeries = new AreaSeries();


            int[] reds = new int[256];
            int[] blues = new int[256];
            int[] greens = new int[256];
            int[] grayscales = new int[256];

            int length = 0;
            byte[] image = null;

            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    new DispatchedHandler(() =>
                    {
                        length = Workspace.Image.Length();
                        image = Workspace.Image.ToByteArray();

                    }));

            await ThreadPool.RunAsync((IAsyncAction action) =>
            {
                for (int i = 0; i < length; i += 4)
                {
                    reds[image[i + (int)channels.R]]++;
                    greens[image[i + (int)channels.G]]++;
                    blues[image[i + (int)channels.B]]++;

                    int greyHue = (int)(image[i + (int)channels.R] * greyHueFactors[0]
                                        + image[i + (int)channels.G] * greyHueFactors[1]
                                        + image[i + (int)channels.B] * greyHueFactors[2]);
                    grayscales[greyHue]++;
                }

                ////////////////////////////////////////////////////////////////////////////////
                ///////////////////// Feeding red channel plot with data ///////////////////////
                ////////////////////////////////////////////////////////////////////////////////
                redSeries.Color = OxyColors.Red;

                redSeries.Points.Add(new DataPoint(0, 0));

                for (int i = 0; i < 256; ++i)
                    redSeries.Points.Add(new DataPoint(i, reds[i]));
                redSeries.Points.Add(new DataPoint(255, 0));
                redSeries.Points.Add(new DataPoint(0, 0));

                ////////////////////////////////////////////////////////////////////////////////
                //////////////////// Feeding green channel plot with data //////////////////////
                ////////////////////////////////////////////////////////////////////////////////
                greenSeries.Color = OxyColors.Green;

                greenSeries.Points.Add(new DataPoint(0, 0));

                for (int i = 0; i < 256; ++i)
                    greenSeries.Points.Add(new DataPoint(i, greens[i]));
                greenSeries.Points.Add(new DataPoint(255, 0));
                greenSeries.Points.Add(new DataPoint(0, 0));

                ////////////////////////////////////////////////////////////////////////////////
                ///////////////////// Feeding blue channel plot with data //////////////////////
                ////////////////////////////////////////////////////////////////////////////////
                blueSeries.Color = OxyColors.Blue;

                blueSeries.Points.Add(new DataPoint(0, 0));

                for (int i = 0; i < 256; ++i)
                    blueSeries.Points.Add(new DataPoint(i, blues[i]));
                blueSeries.Points.Add(new DataPoint(255, 0));
                blueSeries.Points.Add(new DataPoint(0, 0));

                ////////////////////////////////////////////////////////////////////////////////
                ////////////////////// Feeding grayscale plot with data ////////////////////////
                ////////////////////////////////////////////////////////////////////////////////
                grayscaleSeries.Color = OxyColors.LightGray;

                grayscaleSeries.Points.Add(new DataPoint(0, 0));

                for (int i = 0; i < 256; ++i)
                    grayscaleSeries.Points.Add(new DataPoint(i, grayscales[i]));
                grayscaleSeries.Points.Add(new DataPoint(255, 0));
                grayscaleSeries.Points.Add(new DataPoint(0, 0));
            });

            RedModel.Series.Add(redSeries);
            GreenModel.Series.Add(greenSeries);
            BlueModel.Series.Add(blueSeries);
            GrayscaleModel.Series.Add(grayscaleSeries);

            RedModel.InvalidatePlot(true);
            GreenModel.InvalidatePlot(true);
            BlueModel.InvalidatePlot(true);
            GrayscaleModel.InvalidatePlot(true);
        }

        /// <summary>
        /// Goes to the last opened page
        /// </summary>
        public void GoBack()
        {
            navigationService.GoBack();
        }

        /// <summary>
        /// Returns true if this page was opened from another one, otherwise false
        /// </summary>
        public bool CanGoBack
        {
            get
            {
                return navigationService.CanGoBack;
            }
        }

        private PlotModel _RedModel = new PlotModel();

        /// <summary>
        /// Red channel histogram plot model
        /// </summary>
        public PlotModel RedModel
        {
            get { return _RedModel; }
            set
            {
                _RedModel = value;
                NotifyOfPropertyChange(() => RedModel);
            }
        }


        private PlotModel _GreenModel = new PlotModel();

        /// <summary>
        /// Green channel histogram plot model
        /// </summary>
        public PlotModel GreenModel
        {
            get { return _GreenModel; }
            set
            {
                _GreenModel = value;
                NotifyOfPropertyChange(() => GreenModel);
            }
        }


        private PlotModel _BlueModel = new PlotModel();

        /// <summary>
        /// Blue channel histogram plot model
        /// </summary>
        public PlotModel BlueModel
        {
            get { return _BlueModel; }
            set
            {
                _BlueModel = value;
                NotifyOfPropertyChange(() => BlueModel);
            }
        }


        private PlotModel _GrayscaleModel = new PlotModel();

        /// <summary>
        /// Grayscale histogram plot model
        /// </summary>
        public PlotModel GrayscaleModel
        {
            get { return _GrayscaleModel; }
            set
            {
                _GrayscaleModel = value;
                NotifyOfPropertyChange(() => GrayscaleModel);
            }
        }

    }
}
