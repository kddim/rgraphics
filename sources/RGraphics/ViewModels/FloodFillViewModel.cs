﻿using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.UI.Input;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Streams;
using Windows.Foundation;
using Windows.UI;
using WinRTXamlToolkit.Imaging;
using Windows.UI.Xaml.Media;
using RGraphics.Models.DrawingTools;
using RGraphics.Common;
using System.Threading.Tasks;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// The FloodFillViewModel associated with FloodFillView.
    /// </summary>
    public class FloodFillViewModel : DrawingToolBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FloodFillViewModel"/> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        public FloodFillViewModel(IWorkspace workspace)
            : base(workspace)
        {
            Icon = new BitmapIcon()
            {
                UriSource = new Uri("ms-appx:///Assets/paint_bucket_icon.png"),
                Foreground = new SolidColorBrush(Colors.White)
            };

            ScaleX = 1.0;
            ScaleY = 1.0;

            HasAttributes = true;
        }

        /// <summary>
        /// The `start drawing` event called when the user clicks/taps on the workspace.
        /// </summary>
        /// <param name="p">The PointerPoint associated with tap/click.</param>
        protected async override Task Started(PointerPoint p)
        {
            await AssignedWorkspace.ImageProcessing.MergeWorkspaceAsync();
            int x = (int)p.Position.X;
            int y = (int)p.Position.Y;
            int newColor = AssignedWorkspace.SelectedColor.AsInt();
            int oldColor = AssignedWorkspace.Image.GetPixel(x, y).AsInt();

            AssignedWorkspace.Image.CustomFloodFillScanlineReplace(x, y, oldColor, newColor, (byte)Tolerance);

            // The extension method below comes from WinRTXamlToolkit and is ... bugged. Whenever you pass high tolerance the method uses loads of memory and freezes the app
            // TODO: send bugfix to WinRTXamlToolkit?
            //checked
            //{
            //    AssignedWorkspace.Image.FloodFillScanlineReplace(x, y, oldColor, newColor, (byte)Tolerance);
            //}
        }

        /// <summary>
        /// <para> The `continue drawing` event called when user moves the pointer (and when the drawing was started). </para>
        /// <para> Empty implementation, because `Started` is enough to handle FloodFill. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with moving the pointer.</param>
        protected override Task Updated(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }

        /// <summary>
        /// <para> The `end drawing` event called when user stops holding the tap/mouse click. </para>
        /// <para> Empty implementation, because `Started` is enough to handle FloodFill. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
        protected override Task Finished(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }

        private int _Tolerance = 20;

        /// <summary>
        /// Gets or sets the tolerance.
        /// </summary>
        /// <value>
        /// The tolerance.
        /// </value>
        public int Tolerance
        {
            get { return _Tolerance; }
            set
            {
                _Tolerance = Math.Max(1, value);
                _Tolerance = Math.Min(255, value);

                NotifyOfPropertyChange(() => Tolerance);
            }
        }
    }
}

