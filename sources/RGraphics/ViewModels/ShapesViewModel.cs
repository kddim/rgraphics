﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using RGraphics.Models.DrawingTools;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// <para> The ShapesViewModel associated with ShapesView. </para>
    /// <para> Manages the shapes addition process. </para>
    /// </summary>
    public class ShapesViewModel : DrawingToolBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShapesViewModel"/> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        public ShapesViewModel(IWorkspace workspace)
            : base(workspace)
        {
            Icon = new SymbolIcon(Symbol.OutlineStar); ;
            HasAttributes = true;

            Shapes = new BindableCollection<DrawingToolBase>();
            Shapes.Add(new RectangleTool(AssignedWorkspace, this));
            Shapes.Add(new EllipseTool(AssignedWorkspace, this));
            Shapes.Add(new LineTool(AssignedWorkspace, this));

            ActiveShape = Shapes[0];

            var settings = IoC.Get<Settings>() as Settings;
            ThicknessIndex = settings.DefaultThickness;
        }


        /// <summary>
        /// <para> The `start drawing` event called when the user clicks/taps on the workspace. </para>
        /// <para> Starts the shapes addition process. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with tap/click.</param>
        protected override async Task Started(PointerPoint p)
        {
            await ActiveShape.Start(p);
        }


        /// <summary>
        /// <para> The `continue drawing` event called when user moves the pointer (and when the drawing was started). </para>
        /// <para> Updates the shapes addition process. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with moving the pointer.</param>
        protected override async Task Updated(PointerPoint p)
        {
            await ActiveShape.Update(p);
        }

        /// <summary>
        /// <para> The `end drawing` event called when user stops holding the tap/mouse click. </para>
        /// <para> Finishes the shapes addition process. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
        protected override async Task Finished(PointerPoint p)
        {
            await ActiveShape.Finish(p);
            await AssignedWorkspace.ImageProcessing.MergeWorkspaceAsync();
        }

        /// <summary>
        /// Collection of available shapes.
        /// </summary>
        private BindableCollection<DrawingToolBase> _Shapes;
        /// <summary>
        /// Gets or sets the shapes collection.
        /// </summary>
        /// <value>
        /// The shapes.
        /// </value>
        public BindableCollection<DrawingToolBase> Shapes
        {
            get { return _Shapes; }
            set
            {
                _Shapes = value;
                NotifyOfPropertyChange(() => Shapes);
            }
        }

        /// <summary>
        /// The shape selected thickness
        /// </summary>
        private Double _SelectedThickness = 12;
        /// <summary>
        /// Gets or sets the shape selected thickness.
        /// </summary>
        /// <value>
        /// The selected thickness.
        /// </value>
        public Double SelectedThickness
        {
            get { return _SelectedThickness; }
            set
            {
                _SelectedThickness = Thicknesses[(int)value];
                NotifyOfPropertyChange(() => SelectedThickness);
            }
        }

        /// <summary>
        /// The shape thickness index
        /// </summary>
        private int _ThicknessIndex;
        /// <summary>
        /// Gets or sets the index of the shape thickness.
        /// </summary>
        /// <value>
        /// The index of the shape thickness.
        /// </value>
        public int ThicknessIndex
        {
            get { return _ThicknessIndex; }
            set
            {
                _ThicknessIndex = value;
                SelectedThickness = _ThicknessIndex;
                NotifyOfPropertyChange(() => ThicknessIndex);
            }
        }

        /// <summary>
        /// The array of available shape thicknesses.
        /// </summary>
        private readonly Double[] Thicknesses = { 5, 8, 12, 20 };

        /// <summary>
        /// The active shape.
        /// </summary>
        private DrawingToolBase _ActiveShape;
        /// <summary>
        /// Gets or sets the active shape.
        /// </summary>
        /// <value>
        /// The active shape.
        /// </value>
        public DrawingToolBase ActiveShape
        {
            get { return _ActiveShape; }
            set
            {
                _ActiveShape = value;
                NotifyOfPropertyChange(() => ActiveShape);
            }
        }

        /// <summary>
        /// Represents filled boolean value. It is true whether filled is checked.
        /// </summary>
        private Boolean _IfFill = false;
        /// <summary>
        /// Gets or sets a value indicating whether [if fill].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [if fill]; otherwise, <c>false</c>.
        /// </value>
        public Boolean IfFill
        {
            get { return _IfFill; }
            set
            {
                _IfFill = value;
                NotifyOfPropertyChange(() => IfFill);
            }
        }

        /// <summary>
        /// Handles checked event from filled check box and put throught information to start filling.
        /// </summary>
        private void Filled_Checked()
        {
            IfFill = true;
        }

        /// <summary>
        /// Handles unchecked event from filled check box and put throught information to stop filling.
        /// </summary>
        private void Filled_Unchecked()
        {
            IfFill = false;
        }

    }
}
