﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using WinRTXamlToolkit.Imaging;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Models.DrawingTools;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// <para> The ScalingViewModel associated with ScalingView. </para>
    /// <para> Contains methods and properties which are responsible for scaling operation. </para>
    /// </summary>
    public class ScalingViewModel : DrawingToolBase, IHandle<Models.SizeChangedEventArgs>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScalingViewModel" /> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        /// <param name="eventAggregator">The event aggregator.</param>
        public ScalingViewModel(IWorkspace workspace, IEventAggregator eventAggregator)
            : base(workspace)
        {
            // Subscribing to  Caliburn.Micro's EventAggregator, 
            // so we will handle Models.SizeChangedEventArgs published events
            eventAggregator.Subscribe(this);

            Icon = new SymbolIcon(Symbol.BackToWindow);
            HasAttributes = true;

            CurrentWidth = AssignedWorkspace.ActualWidth.ToString();
            CurrentHeight = AssignedWorkspace.ActualHeight.ToString();

            NewWidthString = CurrentWidth;
            NewHeightString = CurrentHeight;
        }


        /// <summary>
        /// <para> The `start drawing` event called when the user clicks/taps on the workspace. </para>
        /// <para> Empty implementation, because the Scaling does not implement any drawing. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with tap/click.</param>
        protected override Task Started(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0);
        }

        /// <summary>
        /// <para> The `continue drawing` event called when user moves the pointer (and when the drawing was started). </para>
        /// <para> Empty implementation, because the Scaling does not implement any drawing. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with moving the pointer.</param>
        protected override Task Updated(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }

        /// <summary>
        /// <para> The `end drawing` event called when user stops holding the tap/mouse click. </para>
        /// <para> Empty implementation, because the Scaling does not implement any drawing. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
        protected override Task Finished(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }

        /// <summary>
        /// Applies the button value changed.
        /// </summary>
        private async Task ApplyButton_ValueChanged()
        {
            if (CurrentWidth == NewWidthString && CurrentHeight == NewHeightString)
                return;

            if (!(NewWidthString.Equals(String.Empty) || NewHeightString.Equals(String.Empty)))
            {
                ValidateSize();

                CurrentWidth = NewWidthString;
                CurrentHeight = NewHeightString;

                AssignedWorkspace.Image = AssignedWorkspace.Image.Resize(_NewWidth, _NewHeight, WriteableBitmapExtensions.Interpolation.Bilinear);
                AssignedWorkspace.UpdateActualSize(AssignedWorkspace.Image.PixelWidth, AssignedWorkspace.Image.PixelHeight);
            }

			await AssignedWorkspace.ImageProcessing.MergeWorkspaceAsync();
        }

        /// <summary>
        /// Validates the size of new specified scaled image.
        /// </summary>
        private void ValidateSize()  // TODO: This is also in NewImageViewModel
        {
            var settings = IoC.Get<Settings>() as Settings;
            int maxWidth = settings.MaxImageWidth;
            int maxHeight = settings.MaxImageHeight;

            if (_NewWidth > maxWidth)
                NewWidthString = maxWidth.ToString();

            if (_NewHeight > maxHeight)
                NewHeightString = maxHeight.ToString();
        }

        /// <summary>
        /// Current width of the image.
        /// </summary>
        private String _CurrentWidth;
        /// <summary>
        /// Gets or sets the current width of the image.
        /// </summary>
        /// <value>
        /// The current width of the image.
        /// </value>
        public String CurrentWidth
        {
            get { return _CurrentWidth;  }
            set
            {
                _CurrentWidth = value;
                NotifyOfPropertyChange(() => CurrentWidth);
            }
        }

        /// <summary>
        /// Current height of the image.
        /// </summary>
        private String _CurrentHeight;
        /// <summary>
        /// Gets or sets the current height of the image.
        /// </summary>
        /// <value>
        /// The current height of the image.
        /// </value>
        public String CurrentHeight
        {
            get { return _CurrentHeight; }
            set
            {
                _CurrentHeight = value;
                NotifyOfPropertyChange(() => CurrentHeight);
            }
        }

        /// <summary>
        /// Represents the new width of the image. It is initialized from string new width.
        /// </summary>
        private int _NewWidth;

        /// <summary>
        /// Represents the new string width of the image.
        /// </summary>
        private String _NewWidthString;
        /// <summary>
        /// Gets or sets the new string width of the image.
        /// </summary>
        /// <value>
        /// The string of new image width.
        /// </value>
        public String NewWidthString
        {
            get { return _NewWidthString; }
            set
            {
                _NewWidthString = value;
                if (_NewWidthString != String.Empty)
                    _NewWidth = int.Parse(NewWidthString);

                updateSizeValues(IfUpdateWidth);

                NotifyOfPropertyChange(() => NewWidthString);
            }
        }

        /// <summary>
        /// Represents the new height of the image. It is initialized from string new height.
        /// </summary>
        private int _NewHeight;

        /// <summary>
        /// Represents the new string height of the image.
        /// </summary>
        private String _NewHeightString;
        /// <summary>
        /// Gets or sets the new string height of the image.
        /// </summary>
        /// <value>
        /// The string of new image height.
        /// </value>
        public String NewHeightString
        {
            get { return _NewHeightString; }
            set
            {
                _NewHeightString = value;
                if (_NewHeightString != String.Empty)
                    _NewHeight = int.Parse(NewHeightString);

                updateSizeValues(IfUpdateWidth);

                NotifyOfPropertyChange(() => NewHeightString);
            }
        }

        /// <summary>
        /// Informs whether size values updating is finished. 
        /// </summary>
        private Boolean IfContinueUpdating = false;

        /// <summary>
        /// Updates the size values associated with the aspect ratio. Depends on the specified proportion factor.
        /// </summary>
        /// <param name="widthSize">if set to <c>true</c> [width size].</param>
        private void updateSizeValues(Boolean widthSize)
        {
            if (IfPreserveAspectRatio && IfContinueUpdating)
            {
                double proportionFactor = calculateAspectRatio();
                IfContinueUpdating = false;

                if (widthSize)
                {
                    _NewWidth = (int)(_NewHeight * proportionFactor);
                    NewWidthString = Convert.ToString(_NewWidth);
                }
                else
                {
                    _NewHeight = (int)(_NewWidth / proportionFactor);
                    NewHeightString = Convert.ToString(_NewHeight);
                }
            }

        }

        /// <summary>
        /// Informs whether respect preserving aspect ratio.
        /// </summary>
        private Boolean _IfPreserveAspectRatio = false;

        /// <summary>
        /// Gets or sets a value indicating whether [if preserve aspect ratio].
        /// </summary>
        /// <value>
        /// <c>true</c> if [if preserve aspect ratio]; otherwise, <c>false</c>.
        /// </value>
        public Boolean IfPreserveAspectRatio
        {
            get { return _IfPreserveAspectRatio; }
            set
            {
                _IfPreserveAspectRatio = value;
                NotifyOfPropertyChange(() => IfPreserveAspectRatio);
            }
        }

        /// <summary>
        /// Calculates the proportion factor as the ratio of actual width and actual height.
        /// </summary>
        /// <returns>Aspect ratio proportion factor</returns>
        private double calculateAspectRatio()
        {
            return AssignedWorkspace.ActualWidth / AssignedWorkspace.ActualHeight;
        }

        /// <summary>
        /// Aspects the ratio checked and updates the size values.
        /// </summary>
        private void AspectRatio_Checked()
        {
            IfContinueUpdating = true;
            updateSizeValues(IfUpdateWidth);
        }

        /// <summary>
        /// Informs whether start updating image width.
        /// </summary>
        private Boolean IfUpdateWidth = false;

        /// <summary>
        /// Informs whether image height is already updated.
        /// </summary>
        private Boolean IsHeightUpdated = false;
        /// <summary>
        /// Informs whether image width is already updated.
        /// </summary>
        private Boolean IsWidthUpdated = false;

        /// <summary>
        /// Handles text changed event of new width and sets appropriate boolean values.
        /// </summary>
        /// <param name="tb">The analyzed text box.</param>
        private void NewWidth_TextChanged(TextBox tb)
        {
            if (IsHeightUpdated)
            {
                IfUpdateWidth = false;
                IfContinueUpdating = true;

                tb.Select(tb.Text.Length, 0);
                IsWidthUpdated = false;
            }
        }

        /// <summary>
        /// Handles text changed event of new height and sets appropriate boolean values.
        /// </summary>
        /// <param name="tb">The analyzed text box.</param>
        private void NewHeight_TextChanged(TextBox tb)
        {
            if (IsWidthUpdated)
            {
                IfUpdateWidth = true;
                IfContinueUpdating = true;

                tb.Select(tb.Text.Length, 0);
                IsHeightUpdated = false;
            }
        }

        /// <summary>
        /// Handles got focus event of new height and informs if boolean values are updated.
        /// </summary>
        private void NewHeight_GotFocus()
        {
            IsHeightUpdated = true;
            IsWidthUpdated = true;
        }

        /// <summary>
        /// Handles got focus event of new width and informs if boolean values are updated.
        /// </summary>
        private void NewWidth_GotFocus()
        {
            IsHeightUpdated = true;
            IsWidthUpdated = true;
        }

        /// <summary>
        /// <para> Handles the SizeChangedEventArgs message. </para>
        /// <para> Gets message sent by Caliburn.Micro's EventAggregator </para>
        /// </summary>
        /// <param name="message">The <see cref="Models.SizeChangedEventArgs"/> instance containing the event data.</param>
        public void Handle(Models.SizeChangedEventArgs message)
        {
            CurrentWidth = message.NewWidth.ToString();
            CurrentHeight = message.NewHeight.ToString();
        }
    }
}
