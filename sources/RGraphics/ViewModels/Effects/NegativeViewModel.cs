﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Turns image to Negative.
    /// </summary>
    public class NegativeViewModel : Screen, IImageEffect
    {
        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Negative_Name"); } }

        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Negative_Description"); } }

        public bool ApplyEnabled { get { return true; } set { throw new NotImplementedException(); } }

        /// <summary>
        /// Applies effect basing on specified source
        /// </summary
        /// <param name="source">The source image</param>
        /// <param name="operationState">Operation state: progress and cancel notification.</param>
        /// <returns><typeparamref name="IImageWrapper"/>instance with applied effect</returns>
        public unsafe WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {
            source.ForEach(
                new Func<int, int, Color, Color>
                (
                    (x, y, color) =>
                    {
                        return Color.FromArgb(
                            color.A,
                            (byte)(255 - color.R),
                            (byte)(255 - color.G),
                            (byte)(255 - color.B)
                        );
                    }
                )
            );
            return source;
        }
    }
}
