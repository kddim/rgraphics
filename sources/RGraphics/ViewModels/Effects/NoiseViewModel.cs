﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Common;
using RGraphics.Models.ImageEffects;

namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Noise effect
    /// </summary>
    public class NoiseViewModel : Screen, IImageEffect
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoiseViewModel"/> class.
        /// </summary>
        public NoiseViewModel()
        {
            Items.Add( IoC.Get<Settings>().Res_UI.GetString("SaltPepper"));
			Items.Add(IoC.Get<Settings>().Res_UI.GetString("Uniform"));

            SelectedItem = Items[0];

            Chance = 1;
        }

        /// <summary>
        /// The current noise sample
        /// </summary>
        private BitmapImage _CurrentSample;
        /// <summary>
        /// Gets or sets the current noise sample.
        /// </summary>
        /// <value>
        /// The current noise sample.
        /// </value>
        public BitmapImage CurrentSample
        {
            get { return _CurrentSample; }
            set
            {
                _CurrentSample = value;
                NotifyOfPropertyChange(() => CurrentSample);
            }
        }

        private ObservableCollection<string> _Items = new ObservableCollection<string>();
        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        public ObservableCollection<string> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }

        private string _SelectedItem;
        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;

                if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("SaltPepper"))
                    CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/saltpepper.png"));
				else if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("Uniform"))
                    CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/uniform.png"));

                NotifyOfPropertyChange(() => SelectedItem);
            }
        }

        private int _Chance;
        public int Chance
        {
            get { return _Chance; }
            set
            {
                _Chance = value;
                NotifyOfPropertyChange(() => Chance);
            }
        }
        

        /// <summary>
        /// The RNG
        /// </summary>
        private Random Rng = new Random();



        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
			get { return IoC.Get<Settings>().Res_UI.GetString("Noise"); }
        }

        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description
        {
            get
            {
	            return IoC.Get<Settings>().Res_UI.GetString("NoiseDesc"); }
        }

        public bool ApplyEnabled { get { return true; } set { throw new NotImplementedException(); } }

        /// <summary>
        /// Applies effect basing on specified source
        /// </summary
        /// <param name="source">The source image</param>
        /// <param name="operationState">Operation state: progress and cancel notification.</param>
        /// <returns><typeparamref name="IImageWrapper"/>instance with applied effect</returns>
        public WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {
            if (SelectedItem == Items[0])
                return ApplySaltAndPepper(source, operationState);
            else if (SelectedItem == Items[1])
                return ApplyUniform(source, operationState);
            else
                throw new InvalidOperationException();

        }
        /// <summary>
        /// Applies the UniformNoise to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        private WriteableBitmap ApplyUniform(WriteableBitmap source, IProgressReporter operationState)
        {
            using (BitmapContext context = source.GetBitmapContext())
            {
                int[] array = context.Pixels;
                for (int x = 0; x < source.PixelWidth; ++x)
                    for (int y = 0; y < source.PixelHeight; ++y)
                    {
                        if (Rng.NextDouble() <= (double)Chance/100.0)
                        {
                            int index = Util.Index(x, y, context.Width);

                            int noise = (int)(Rng.NextDouble() * 50);

                            int r = (array[index] & Def.MASK_R) >> Def.SHIFT_R;
                            int g = (array[index] & Def.MASK_G) >> Def.SHIFT_G;
                            int b = (array[index] & Def.MASK_B) >> Def.SHIFT_B;

                            r += noise;
                            g += noise;
                            b += noise;
                            r = r.CutToByte();
                            g = g.CutToByte();
                            b = b.CutToByte();
                            array[index] = (array[index] & Def.MASK_A) |
                                  (r << Def.SHIFT_R) |
                                  (g << Def.SHIFT_G) |
                                  (b << Def.SHIFT_B);
                        }
                    }
            }
            return source;
        }

        /// <summary>
        /// Applies the SaltAndPepper to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplySaltAndPepper(WriteableBitmap source, IProgressReporter operationState)
        {
            using (BitmapContext context = source.GetBitmapContext())
            {
                int[] array = context.Pixels;
                for (int x = 0; x < source.PixelWidth; ++x)
                    for (int y = 0; y < source.PixelHeight; ++y)
                    {
                        if (Rng.NextDouble() <= (double)Chance / 100.0)
                        {
                            int out_color;

                            if (Rng.NextDouble() <= 0.5)
                                out_color = 0;
                            else
                                out_color = 255;

                            int index = Util.Index(x, y, context.Width);
                            array[index] = (array[index] & Def.MASK_A) |
                                  (out_color << Def.SHIFT_R) |
                                  (out_color << Def.SHIFT_G) |
                                  (out_color << Def.SHIFT_B);
                        }
                    }
            }
            return source;
        }
    }
}
