﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Common;
using Windows.UI.Xaml.Controls;
using RGraphics.Models.ImageEffects;
using Windows.UI.Xaml;


namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Thresholding methods
    /// </summary>
    class ThresholdingViewModel : Screen, IImageEffect
    {
        public ThresholdingViewModel()
        {
            Region = 3;

            Items.Add(IoC.Get<Settings>().Res_UI.GetString("LocalThresholding"));
			Items.Add(IoC.Get<Settings>().Res_UI.GetString("GlobalThresholding"));
			Items.Add(IoC.Get<Settings>().Res_UI.GetString("OtsuMethod"));

            SelectedItem = Items[0];
        }

        /// <summary>
        /// The current thresholding sample
        /// </summary>
        private BitmapImage _CurrentSample;
        /// <summary>
        /// Gets or sets the current thresholding sample.
        /// </summary>
        /// <value>
        /// The current thresholding sample.
        /// </value>
        public BitmapImage CurrentSample
        {
            get { return _CurrentSample; }
            set
            {
                _CurrentSample = value;
                NotifyOfPropertyChange(() => CurrentSample);
            }
        }

        private ObservableCollection<string> _Items = new ObservableCollection<string>();
        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        public ObservableCollection<string> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }

        private string _SelectedItem;
        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;

				if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("LocalThresholding"))
                {
                    CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/localthresholding.png"));
                    LocalThresholdOptsVisibility = Visibility.Visible;
                }
                else
                {
					if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("GlobalThresholding"))
                        CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/globalthresholding.png"));
					else if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("OtsuMethod"))
                        CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/otsumethod.png"));
                    LocalThresholdOptsVisibility = Visibility.Collapsed;
                }
                NotifyOfPropertyChange(() => SelectedItem);
            }
        }

        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Thresholding_Name"); }
        }
        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description
        {
            get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Thresholding_Description"); }
        }

        public bool ApplyEnabled { get { return true; } set { throw new NotImplementedException(); } }

        /// <summary>
        /// Applies effect basing on specified source
        /// </summary
        /// <param name="source">The source image</param>
        /// <param name="operationState">Operation state: progress and cancel notification.</param>
        /// <returns><typeparamref name="IImageWrapper"/>instance with applied effect</returns>
        public WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {

            GrayscaleViewModel gray = new GrayscaleViewModel();
            source = gray.Apply(source, operationState);

            if (SelectedItem == Items[0])
                return ApplyLocalThresholding(source, operationState);
            else if (SelectedItem == Items[1])
                return ApplyGlobalThresholding(source, operationState);
            else if (SelectedItem == Items[2])
                return ApplyOtsuMethod(source, operationState);
            else
                throw new InvalidOperationException();
        }

        /// <summary>
        /// Applies the Otsu Method to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplyOtsuMethod(WriteableBitmap source, IProgressReporter operationState)
        {
            using (BitmapContext context = source.GetBitmapContext())
            {
                int[] array = context.Pixels;
                int[] histogram = new int[256];
                int total = context.Width * context.Height;
                for (int x = 0; x < context.Width; ++x)
                    for (int y = 0; y < context.Height; ++y)
                    {
                        int index = Util.Index(x, y, context.Width);
                        histogram[(array[index] & Def.MASK_R) >> Def.SHIFT_R]++;
                    }

                double sum = 0;
                for (int i = 0; i < 256; i++) sum += i * histogram[i];
                double sumB = 0;
                double wB = 0;
                double wF = 0;
                double mB = 0;
                double mF = 0;
                double max = 0.0;
                double between = 0.0;
                double threshold1 = 0.0;
                double threshold2 = 0.0;

                for (int i = 0; i < 256; ++i)
                {

                    wB += histogram[i];
                    if (wB == 0)
                        continue;
                    wF = total - wB;
                    if (wF == 0)
                        break;
                    sumB += i * histogram[i];
                    mB = sumB / wB;
                    mF = (sum - sumB) / wF;
                    between = (double)wB * wF * Math.Pow(mB - mF, 2);
                    if (between >= max)
                    {
                        threshold1 = i;
                        if (between > max)
                        {
                            threshold2 = i;
                        }
                        max = between;
                    }
                }

                double global = ((threshold1 + threshold2) / 2.0);

                for (int x = 0; x < context.Width; ++x)
                    for (int y = 0; y < context.Height; ++y)
                    {
                        int index = Util.Index(x, y, context.Width);
                        int val = (array[index] & Def.MASK_R) >> Def.SHIFT_R;
                        
                        val = val < global ? 0x00000000 : 0x00FFFFFF;

                        array[index] = (array[index] & Def.MASK_A) |
                              (val << Def.SHIFT_R) |
                              (val << Def.SHIFT_G) |
                              (val << Def.SHIFT_B);
                    }
            }

            return source;
        }
        /// <summary>
        /// Applies the Global Thresholding to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplyGlobalThresholding(WriteableBitmap source, IProgressReporter operationState)
        {
            using (BitmapContext context = source.GetBitmapContext())
            {
                int[] array = context.Pixels;
                int sum = 0;
                for (int x = 0; x < context.Width; ++x)
                    for (int y = 0; y < context.Height; ++y)
                    {
                        int index = Util.Index(x, y, context.Width);
                        sum += (array[index] & Def.MASK_R) >> Def.SHIFT_R;
                    }

                sum = sum / (context.Width * context.Height);

                for (int x = 0; x < context.Width; ++x)
                    for (int y = 0; y < context.Height; ++y)
                    {
                        int index = Util.Index(x, y, context.Width);
                        int val = (array[index] & Def.MASK_R) >> Def.SHIFT_R;

                        val = val < sum ? 0x00000000 : 0x00FFFFFF;

                        array[index] = (array[index] & Def.MASK_A) | val;
                    }
            }

            return source;
        }

        private Visibility _LocalThresholdOptsVisibility;
        public Visibility LocalThresholdOptsVisibility
        {
            get { return _LocalThresholdOptsVisibility; }
            set
            {
                _LocalThresholdOptsVisibility = value;
                NotifyOfPropertyChange(() => LocalThresholdOptsVisibility);
            }
        }
        

        private int _Region;
        public int Region
        {
            get { return _Region; }
            set
            {
                _Region = value;
                NotifyOfPropertyChange(() => Region);
            }
        }
        
        /// <summary>
        /// Applies the Local Thresholding to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplyLocalThresholding(WriteableBitmap source, IProgressReporter operationState)
        {
            int regionTo = Region / 2;
            int regionFrom = -regionTo;
            int size = Region * Region;

            using (BitmapContext context = source.GetBitmapContext(), copy = source.Clone().GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] array = context.Pixels;
                int[] copy_array = copy.Pixels;
                for (int x = 0; x < context.Width; ++x)
                    for (int y = 0; y < context.Height; ++y)
                    {
                        int sum = 0;
                        for (int i = regionFrom; i <= regionTo; ++i)
                        {
                            for (int j = regionFrom; j <= regionTo; ++j)
                            {
                                int px = x + i;
                                int py = y + j;
                                px = px < 0 ? 0 : px;
                                py = py < 0 ? 0 : py;
                                px = px >= context.Width ? context.Width - 1 : px;
                                py = py >= context.Height ? context.Height - 1 : py;
                                int index = Util.Index(px,py,context.Width);
                                sum += (copy_array[index] & Def.MASK_R) >> Def.SHIFT_R;
                            }
                        }
                        sum = sum / size;
                        int cord = Util.Index(x, y, context.Width);
                        int val = (copy_array[cord] & Def.MASK_R) >> Def.SHIFT_R;

                        val = val < sum ? 0x00000000 : 0x00FFFFFF;

                        array[cord] = (array[cord] & Def.MASK_A) |
                              (val << Def.SHIFT_R) |
                              (val << Def.SHIFT_G) |
                              (val << Def.SHIFT_B);
                    }
            }
            return source;
        }
    }
}
