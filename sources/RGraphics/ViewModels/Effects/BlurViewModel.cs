﻿using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;
using WinRTXamlToolkit.Imaging;
using RGraphics.Common;
using Caliburn.Micro;
using System.Collections.ObjectModel;

namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Blur effect.
    /// </summary>
    public class BlurViewModel : Screen, IImageEffect
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BlurViewModel"/> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        public BlurViewModel(IWorkspace workspace)
        {
            Workspace = workspace;

            Items = new ObservableCollection<string>();
            Items.Add(Gaussian3x3);
            Items.Add(Gaussian5x5);
            Items.Add(Sharpen3x3);

            SelectedKernel = Gaussian3x3;
        }
        private IWorkspace Workspace { get; set; }

        public ObservableCollection<String> Items { get; private set; }
        public String Gaussian3x3 { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Blur_Gaussian3x3"); } }
        public String Gaussian5x5 { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Blur_Gaussian5x5"); } }
        public String Sharpen3x3 { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Blur_Sharpen3x3"); } }

        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
		public string Name { get { return IoC.Get<Settings>().Res_UI.GetString("Blur"); } }

        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
		public string Description { get { return IoC.Get<Settings>().Res_UI.GetString("DescBlur");} }

        public bool ApplyEnabled { get { return true; } set { throw new NotImplementedException(); } }

		/// <summary>
		/// The current blur sample
		/// </summary>
		private BitmapImage _CurrentSample;
		/// <summary>
		/// Gets or sets the current blur sample.
		/// </summary>
		/// <value>
		/// The current blur sample.
		/// </value>
		public BitmapImage CurrentSample
		{
			get { return _CurrentSample; }
			set
			{
				_CurrentSample = value;
				NotifyOfPropertyChange(() => CurrentSample);
			}
		}

        /// <summary>
        /// Applies effect basing on specified source
        /// </summary
        /// <param name="source">The source image</param>
        /// <param name="operationState">Operation state: progress and cancel notification.</param>
        /// <returns><typeparamref name="IImageWrapper"/>instance with applied effect</returns>
        public WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {
            if (SelectedKernel == Gaussian3x3)
                return source.Convolute(Windows.UI.Xaml.Media.Imaging.WriteableBitmapExtensions.KernelGaussianBlur3x3);
            else if (SelectedKernel == Gaussian5x5)
                return source.Convolute(Windows.UI.Xaml.Media.Imaging.WriteableBitmapExtensions.KernelGaussianBlur5x5);
            else if (SelectedKernel == Sharpen3x3)
                return source.Convolute(Windows.UI.Xaml.Media.Imaging.WriteableBitmapExtensions.KernelSharpen3x3);

            throw new NotImplementedException();
        }

        private String _SelectedKernel;
        public String SelectedKernel
        {
            get { return _SelectedKernel; }
            set
            {
                _SelectedKernel = value;

				if( _SelectedKernel == Gaussian3x3 )
					CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/blur3x3.png"));
				else if( _SelectedKernel == Gaussian5x5 )
					CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/blur5x5.png"));
				else if (_SelectedKernel == Sharpen3x3)
					CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/sharpen3x3.png"));

                NotifyOfPropertyChange(() => SelectedKernel);
            }
        }
        
    }
}
