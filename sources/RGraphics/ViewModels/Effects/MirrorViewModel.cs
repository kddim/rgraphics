﻿using Caliburn.Micro;
using RGraphics.Models;
using RGraphics.Models.ImageEffects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Common;
using Windows.UI;


namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Mirror effect methods.
    /// </summary>
    class MirrorViewModel : Screen, IImageEffect
    {
        public MirrorViewModel(IWorkspace workspace)
        {
            Items.Add(IoC.Get<Settings>().Res_UI.GetString("Mirror_TopMirror"));
			Items.Add(IoC.Get<Settings>().Res_UI.GetString("Mirror_BottomMirror"));
			Items.Add(IoC.Get<Settings>().Res_UI.GetString("Mirror_RightMirror"));
			Items.Add(IoC.Get<Settings>().Res_UI.GetString("Mirror_LeftMirror"));
            SelectedItem = Items[0];
            this.Workspace = workspace;
            this.Size = "100";
        }

        private IWorkspace _Workspace;
        /// <summary>
        /// Gets or sets the workspace.
        /// </summary>
        /// <value>
        /// The workspace.
        /// </value>
        public IWorkspace Workspace
        {
            get { return _Workspace; }
            set
            {
                _Workspace = value;
                NotifyOfPropertyChange(() => Workspace);
            }
        }

        private ObservableCollection<string> _Items = new ObservableCollection<string>();
        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        public ObservableCollection<string> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }
        private string _SelectedItem;

        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;

				if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("Mirror_TopMirror"))
					CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/topmirror.png"));
				else if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("Mirror_BottomMirror"))
					CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/bottommirror.png"));
				else if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("Mirror_LeftMirror"))
					CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/leftmirror.png"));
				else if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("Mirror_RightMirror"))
					CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/rightmirror.png"));

                NotifyOfPropertyChange(() => SelectedItem);
            }
        }

        public string Size
        {
            get { return _Size; }
            set
            {
                _Size = value;
                NotifyOfPropertyChange(() => Size);
            }
        }
        private string _Size;
        public string Name
        {
			get { return IoC.Get<Settings>().Res_UI.GetString("Mirror_Title"); }
        }

        public string Description
        {
			get { return IoC.Get<Settings>().Res_UI.GetString("Mirror_Desc"); } 
        }

        public bool ApplyEnabled { get { return true; } set { throw new NotImplementedException(); } }

		/// <summary>
		/// The current mirror sample
		/// </summary>
		private BitmapImage _CurrentSample;
		/// <summary>
		/// Gets or sets the current mirror sample.
		/// </summary>
		/// <value>
		/// The current mirror sample.
		/// </value>
		public BitmapImage CurrentSample
		{
			get { return _CurrentSample; }
			set
			{
				_CurrentSample = value;
				NotifyOfPropertyChange(() => CurrentSample);
			}
		}

        public WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {
            if (Double.Parse(_Size) > 100)
            {
                _Size = "100";
            }
            else if (Double.Parse(_Size) <= 0)
            {
                _Size = "1";
            }

			WriteableBitmap wbOut;

            if (SelectedItem == Items[0])
                wbOut = ApplyTopMirror(source, operationState);
            else if (SelectedItem == Items[1])
                wbOut = ApplyBottomMirror(source, operationState);
            else if (SelectedItem == Items[2])
                wbOut = ApplyRightMirror(source, operationState);
            else if (SelectedItem == Items[3])
                wbOut = ApplyLeftMirror(source, operationState);
            else
                throw new InvalidOperationException();

			IoC.Get<ShellViewModel>().UndoRedoStack.Push(wbOut);

			return wbOut;
        }

        private WriteableBitmap ApplyLeftMirror(WriteableBitmap source, IProgressReporter operationState)
        {
            int newWidth = (int)((Double.Parse(_Size) / 100) * source.PixelWidth * 2);
            int newHeight = source.PixelHeight;
            WriteableBitmap resultBitmap = new WriteableBitmap(newWidth, newHeight);
            resultBitmap.FillRectangle(0, 0, resultBitmap.PixelWidth - 1, resultBitmap.PixelHeight - 1, Colors.Black);
            Workspace.UpdateActualSize(newWidth, newHeight);

            int middle = newWidth / 2;
            using (BitmapContext resultContext = resultBitmap.GetBitmapContext(), originalContext = source.GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] resultArray = resultContext.Pixels;
                int[] originalArray = originalContext.Pixels;
                for (int i = 0; i < middle; ++i)
                {
                    for (int j = 0; j < newHeight; ++j)
                    {
                        int originalIndex = Util.Index(i , j, source.PixelWidth);
                        //copy
                        int resultIndex = Util.Index(middle+i, j, resultBitmap.PixelWidth);
                        resultArray[resultIndex] = originalArray[originalIndex];
                        //reflection
                        resultIndex = Util.Index(middle - i, j, resultBitmap.PixelWidth);
                        resultArray[resultIndex] = originalArray[originalIndex];
                    }
                }



            }

            return resultBitmap;
        }

        private WriteableBitmap ApplyRightMirror(WriteableBitmap source, IProgressReporter operationState)
        {
            int newWidth = (int)((Double.Parse(_Size) / 100) * source.PixelWidth * 2);
            int newHeight = source.PixelHeight;
            WriteableBitmap resultBitmap = new WriteableBitmap(newWidth, newHeight);
            resultBitmap.FillRectangle(0, 0, resultBitmap.PixelWidth - 1, resultBitmap.PixelHeight - 1, Colors.Black);
            Workspace.UpdateActualSize(newWidth, newHeight);
            int middle = newWidth / 2;
            using (BitmapContext resultContext = resultBitmap.GetBitmapContext(), originalContext = source.GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] resultArray = resultContext.Pixels;
                int[] originalArray = originalContext.Pixels;
                int begin = source.PixelWidth - middle;
                for (int i = begin; i < source.PixelWidth; ++i)
                {
                    for (int j = 0; j < newHeight; ++j)
                    {
                        int originalIndex = Util.Index(i, j, source.PixelWidth);
                        //copy
                        int resultIndex = Util.Index(i - begin, j, resultBitmap.PixelWidth);
                        resultArray[resultIndex] = originalArray[originalIndex];
                        //reflection
                        resultIndex = Util.Index(resultBitmap.PixelWidth - (i - begin+1), j, resultBitmap.PixelWidth);
                        resultArray[resultIndex] = originalArray[originalIndex];
                    }
                }
            }

            return resultBitmap;
        }

        private WriteableBitmap ApplyBottomMirror(WriteableBitmap source, IProgressReporter operationState)
        {
            int newWidth = source.PixelWidth;
            int newHeight = (int)((Double.Parse(_Size) / 100) * source.PixelHeight * 2);
            WriteableBitmap resultBitmap = new WriteableBitmap(newWidth, newHeight);
            resultBitmap.FillRectangle(0, 0, resultBitmap.PixelWidth - 1, resultBitmap.PixelHeight - 1, Colors.Black);
            Workspace.UpdateActualSize(newWidth, newHeight);

            int middle = newHeight / 2;
            using (BitmapContext resultContext = resultBitmap.GetBitmapContext(), originalContext = source.GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] resultArray = resultContext.Pixels;
                int[] originalArray = originalContext.Pixels;
                for (int i = 0; i < newWidth; ++i)
                {
                    for (int j = 0; j < middle; ++j)
                    {
                        int originalIndex = Util.Index(i, j, source.PixelWidth);
                        //copy
                        int resultIndex = Util.Index(i, j, resultBitmap.PixelWidth);
                        resultArray[resultIndex] = originalArray[originalIndex];
                        //reflection
                        resultIndex = Util.Index(i, resultBitmap.PixelHeight-j-1, resultBitmap.PixelWidth);
                        resultArray[resultIndex] = originalArray[originalIndex];
                    }
                }
            }

            return resultBitmap;
        }

        private WriteableBitmap ApplyTopMirror(WriteableBitmap source, IProgressReporter operationState)
        {
            int newWidth = source.PixelWidth;
            int newHeight = (int)((Double.Parse(_Size) / 100) * source.PixelHeight * 2);
            WriteableBitmap resultBitmap = new WriteableBitmap(newWidth, newHeight);
            resultBitmap.FillRectangle(0, 0, resultBitmap.PixelWidth - 1, resultBitmap.PixelHeight - 1, Colors.Black);
            Workspace.UpdateActualSize(newWidth, newHeight);
            int middle = newHeight / 2;
            using (BitmapContext resultContext = resultBitmap.GetBitmapContext(), originalContext = source.GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] resultArray = resultContext.Pixels;
                int[] originalArray = originalContext.Pixels;
                for (int i = 0; i < newWidth; ++i)
                {
                    for (int j = 0; j < middle; ++j)
                    {
                        int originalIndex = Util.Index(i, j, source.PixelWidth);
                        //copy
                        int resultIndex = Util.Index(i, j+middle, resultBitmap.PixelWidth);
                        resultArray[resultIndex] = originalArray[originalIndex];
                        //reflection
                        resultIndex = Util.Index(i, middle - j-1, resultBitmap.PixelWidth);
                        resultArray[resultIndex] = originalArray[originalIndex];
                    }
                }
            }

            return resultBitmap;
        }
    }
}
