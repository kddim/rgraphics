﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using WinRTXamlToolkit.Imaging;
using RGraphics.Common;
using Windows.UI;

namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Grayscale effect.
    /// </summary>
    public class GrayscaleViewModel : Screen, IImageEffect
    {
        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Grayscale_Name"); } }

        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Grayscale_Description"); } }

        public bool ApplyEnabled { get { return true; } set { throw new NotImplementedException(); } }

        /// <summary>
        /// Applies effect basing on specified source
        /// </summary
        /// <param name="source">The source image</param>
        /// <param name="operationState">Operation state: progress and cancel notification.</param>
        /// <returns><typeparamref name="IImageWrapper"/>instance with applied effect</returns>
        public unsafe  WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {
            source.ForEach(
                new Func<int, int, Color, Color>
                (
                    (x, y, color) =>
                    {
                        byte v = (byte)((color.R + color.B + color.G) / 3);
                        return Color.FromArgb(color.A, v, v, v);
                    }
                )
            );
            return source;
        }
    }
}
