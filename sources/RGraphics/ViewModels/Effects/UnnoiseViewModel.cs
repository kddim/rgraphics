﻿using Caliburn.Micro;
using RGraphics.Models;
using RGraphics.Models.ImageEffects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Common;

namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Noise Reduction methods.
    /// </summary>
    public class UnnoiseViewModel : Screen, IImageEffect
    {
        public UnnoiseViewModel()
        {
            Items.Add(IoC.Get<Settings>().Res_UI.GetString("AveragingFilter"));
            Items.Add(IoC.Get<Settings>().Res_UI.GetString("MedianFilter"));
            Items.Add(IoC.Get<Settings>().Res_UI.GetString("ExtendedMedianFilter"));
            Items.Add(IoC.Get<Settings>().Res_UI.GetString("KuwaharaFilter"));
            SelectedItem = Items[0];
        }

        /// <summary>
        /// The current unnoise sample
        /// </summary>
        private BitmapImage _CurrentSample;
        /// <summary>
        /// Gets or sets the current unnoise sample.
        /// </summary>
        /// <value>
        /// The current unnoise sample.
        /// </value>
        public BitmapImage CurrentSample
        {
            get { return _CurrentSample; }
            set
            {
                _CurrentSample = value;
                NotifyOfPropertyChange(() => CurrentSample);
            }
        }

        private ObservableCollection<string> _Items = new ObservableCollection<string>();
        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        public ObservableCollection<string> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }

        private string _SelectedItem;
        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;

				if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("AveragingFilter"))
                    CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/averagingfilter.png"));
				else if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("MedianFilter"))
                    CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/medianfilter.png"));
                else if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("ExtendedMedianFilter"))
                    CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/extendedmedianfilter.png"));
				else if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("KuwaharaFilter"))
                    CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/kuwaharafilter.png"));
                NotifyOfPropertyChange(() => SelectedItem);
            }
        }

        /// <summary>
        /// The RNG
        /// </summary>
        private Random Rng = new Random();



        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return IoC.Get<Settings>().Res_UI.GetString("Unnoise"); }
        }

        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description
        {
			get { return IoC.Get<Settings>().Res_UI.GetString("UnnoiseDesc"); }
        }

        public bool ApplyEnabled { get { return true; } set { throw new NotImplementedException(); } }

        /// <summary>
        /// Applies effect basing on specified source
        /// </summary
        /// <param name="source">The source image</param>
        /// <param name="operationState">Operation state: progress and cancel notification.</param>
        /// <returns><typeparamref name="IImageWrapper"/>instance with applied effect</returns>
        public WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {
            if (SelectedItem == Items[0])
                return ApplyAveragingFilter(source, operationState);
            else if (SelectedItem == Items[1])
                return ApplyMedianFilter(source, operationState);
            else if (SelectedItem == Items[2])
                return ApplyMedianFilterEx(source, operationState);
            else if (SelectedItem == Items[3])
                return ApplyKuwaharaFilter(source, operationState);
            else
                throw new InvalidOperationException();

        }
        /// <summary>
        /// Applies the Kuwahara filter to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplyKuwaharaFilter(WriteableBitmap source, IProgressReporter operationState)
        {
            int Size = 5;
            Random TempRandom = new Random();
            int[] ApetureMinX = { -(Size / 2), 0, -(Size / 2), 0 };
            int[] ApetureMaxX = { 0, (Size / 2), 0, (Size / 2) };
            int[] ApetureMinY = { -(Size / 2), -(Size / 2), 0, 0 };
            int[] ApetureMaxY = { 0, 0, (Size / 2), (Size / 2) };

            using (BitmapContext context = source.GetBitmapContext(), copy = source.Clone().GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] array = context.Pixels;
                int[] copyArray = copy.Pixels;
                for (int x = 0; x < context.Width; ++x)
                    for (int y = 0; y < context.Height; ++y)
                    {
                        int[] RValues = { 0, 0, 0, 0 };
                        int[] GValues = { 0, 0, 0, 0 };
                        int[] BValues = { 0, 0, 0, 0 };
                        int[] NumPixels = { 0, 0, 0, 0 };
                        int[] MaxRValue = { 0, 0, 0, 0 };
                        int[] MaxGValue = { 0, 0, 0, 0 };
                        int[] MaxBValue = { 0, 0, 0, 0 };
                        int[] MinRValue = { 255, 255, 255, 255 };
                        int[] MinGValue = { 255, 255, 255, 255 };
                        int[] MinBValue = { 255, 255, 255, 255 };
                        for (int i = 0; i < 4; ++i)
                        {
                            for (int x2 = ApetureMinX[i]; x2 < ApetureMaxX[i]; ++x2)
                            {
                                int TempX = x + x2;
                                if (TempX >= 0 && TempX < context.Width)
                                {
                                    for (int y2 = ApetureMinY[i]; y2 < ApetureMaxY[i]; ++y2)
                                    {
                                        int TempY = y + y2;
                                        if (TempY >= 0 && TempY < context.Height)
                                        {
                                            int index = Util.Index(TempX, TempY, context.Width);
                                            int r = ((copyArray[index] & Def.MASK_R) >> Def.SHIFT_R);
                                            int g = ((copyArray[index] & Def.MASK_G) >> Def.SHIFT_G);
                                            int b = ((copyArray[index] & Def.MASK_B) >> Def.SHIFT_B);
                                            RValues[i] += r;
                                            GValues[i] += g;
                                            BValues[i] += b;
                                            if (r > MaxRValue[i])
                                            {
                                                MaxRValue[i] = r;
                                            }
                                            else if (r < MinRValue[i])
                                            {
                                                MinRValue[i] = r;
                                            }

                                            if (g > MaxGValue[i])
                                            {
                                                MaxGValue[i] = g;
                                            }
                                            else if (g < MinGValue[i])
                                            {
                                                MinGValue[i] = g;
                                            }

                                            if (b > MaxBValue[i])
                                            {
                                                MaxBValue[i] = b;
                                            }
                                            else if (b < MinBValue[i])
                                            {
                                                MinBValue[i] = b;
                                            }
                                            ++NumPixels[i];
                                        }
                                    }
                                }
                            }
                        }
                        int j = 0;
                        int MinDifference = 10000;
                        for (int i = 0; i < 4; ++i)
                        {
                            int CurrentDifference = (MaxRValue[i] - MinRValue[i]) + (MaxGValue[i] - MinGValue[i]) + (MaxBValue[i] - MinBValue[i]);
                            if (CurrentDifference < MinDifference && NumPixels[i] > 0)
                            {
                                j = i;
                                MinDifference = CurrentDifference;
                            }
                        }
                        int resultR = RValues[j] / NumPixels[j];
                        int resultG = GValues[j] / NumPixels[j];
                        int resultB = BValues[j] / NumPixels[j];

                        resultR = resultR.CutToByte();
                        resultG = resultG.CutToByte();
                        resultB = resultB.CutToByte();
                        int cord = Util.Index(x, y, context.Width);
                        array[cord] = (array[cord] & Def.MASK_A) |
                              (resultR << Def.SHIFT_R) |
                              (resultG << Def.SHIFT_G) |
                              (resultB << Def.SHIFT_B);
                    }
            }
            return source;
        }

        /// <summary>
        /// Applies the averaging filter to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplyAveragingFilter(WriteableBitmap source, IProgressReporter operationState)
        {
            const int region = 5;
            int regionTo = region / 2;
            int regionFrom = -regionTo;

            using (BitmapContext context = source.GetBitmapContext(), copy = source.Clone().GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] array = context.Pixels;
                int[] copyArray = copy.Pixels;

                for (int x = 0; x < source.PixelWidth; ++x)
                    for (int y = 0; y < source.PixelHeight; ++y)
                    {
                        int rSum = 0;
                        int gSum = 0;
                        int bSum = 0;
                        int counter = 0;
                        for (int k = regionFrom; k <= regionTo; ++k)
                            for (int l = regionFrom; l <= regionTo; ++l)
                            {
                                int xCord = x + k;
                                int yCord = y + l;
                                if (xCord >= 0 && xCord < source.PixelWidth && yCord >= 0 && yCord < source.PixelHeight)
                                {
                                    int regionIndex = Util.Index(xCord, yCord, context.Width);
                                    int r = (copyArray[regionIndex] & Def.MASK_R) >> Def.SHIFT_R;
                                    int g = (copyArray[regionIndex] & Def.MASK_G) >> Def.SHIFT_G;
                                    int b = (copyArray[regionIndex] & Def.MASK_B) >> Def.SHIFT_B;

                                    rSum += r;
                                    gSum += g;
                                    bSum += b;
                                    ++counter;
                                }
                            }
                        rSum /= counter;
                        gSum /= counter;
                        bSum /= counter;

                        int index = Util.Index(x, y, context.Width);
                        array[index] = (array[index] & Def.MASK_A) |
                              (rSum << Def.SHIFT_R) |
                              (gSum << Def.SHIFT_G) |
                              (bSum << Def.SHIFT_B);
                    }
            }
            return source;
        }

        /// <summary>
        /// Applies the median filter to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplyMedianFilter(WriteableBitmap source, IProgressReporter operationState)
        {
            const int region = 5;
            const int regionSquare = region * region;
            const int regionTo = region / 2;
            const int regionFrom = -regionTo;

            List<int> Rlist = new List<int>(regionSquare);
            List<int> Glist = new List<int>(regionSquare);
            List<int> Blist = new List<int>(regionSquare);
            using (BitmapContext context = source.GetBitmapContext(), copy = source.Clone().GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] array = context.Pixels;
                int[] copyArray = copy.Pixels;

                for (int x = 0; x < source.PixelWidth; ++x)
                    for (int y = 0; y < source.PixelHeight; ++y)
                    {
                        Rlist.Clear();
                        Glist.Clear();
                        Blist.Clear();
                        for (int k = regionFrom; k <= regionTo; ++k)
                            for (int l = regionFrom; l <= regionTo; ++l)
                            {
                                int xCord = x + k;
                                int yCord = y + l;
                                if (xCord > 0 && xCord < source.PixelWidth && yCord > 0 && yCord < source.PixelHeight)
                                {
                                    int regionIndex = Util.Index(xCord, yCord, context.Width);
                                    int r = (copyArray[regionIndex] & Def.MASK_R) >> Def.SHIFT_R;
                                    int g = (copyArray[regionIndex] & Def.MASK_G) >> Def.SHIFT_G;
                                    int b = (copyArray[regionIndex] & Def.MASK_B) >> Def.SHIFT_B;

                                    Rlist.Add(r);
                                    Blist.Add(b);
                                    Glist.Add(g);
                                }
                            }
                        Rlist.Sort();
                        Glist.Sort();
                        Blist.Sort();
                        int resultR = Rlist[Rlist.Count / 2];
                        int resultG = Glist[Glist.Count / 2];
                        int resultB = Blist[Blist.Count / 2];

                        int index = Util.Index(x, y, context.Width);
                        array[index] = (array[index] & Def.MASK_A) |
                              (resultR << Def.SHIFT_R) |
                              (resultG << Def.SHIFT_G) |
                              (resultB << Def.SHIFT_B);
                    }
            }

            return source;
        }

        /// <summary>
        /// Applies the extended median filter to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplyMedianFilterEx(WriteableBitmap source, IProgressReporter operationState)
        {
            const int region = 5;
            const int regionSquare = region * region;
            const int regionTo = region / 2;
            const int regionFrom = -regionTo;

            List<int> Rlist = new List<int>(regionSquare);
            List<int> Glist = new List<int>(regionSquare);
            List<int> Blist = new List<int>(regionSquare);
            using (BitmapContext context = source.GetBitmapContext(), copy = source.Clone().GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] array = context.Pixels;
                int[] copyArray = copy.Pixels;

                for (int x = 0; x < source.PixelWidth; ++x)
                    for (int y = 0; y < source.PixelHeight; ++y)
                    {
                        Rlist.Clear();
                        Glist.Clear();
                        Blist.Clear();
                        for (int k = regionFrom; k <= regionTo; ++k)
                            for (int l = regionFrom; l <= regionTo; ++l)
                            {
                                int xCord = x + k;
                                int yCord = y + l;
                                if (xCord > 0 && xCord < source.PixelWidth && yCord > 0 && yCord < source.PixelHeight)
                                {
                                    int regionIndex = Util.Index(xCord, yCord, context.Width);
                                    int r = (copyArray[regionIndex] & Def.MASK_R) >> Def.SHIFT_R;
                                    int g = (copyArray[regionIndex] & Def.MASK_G) >> Def.SHIFT_G;
                                    int b = (copyArray[regionIndex] & Def.MASK_B) >> Def.SHIFT_B;

                                    Rlist.Add(r);
                                    Blist.Add(b);
                                    Glist.Add(g);
                                }
                            }
                        Rlist.Sort();
                        Glist.Sort();
                        Blist.Sort();

                        int index = Util.Index(x, y, context.Width);
                        int currentR = (copyArray[index] & Def.MASK_R) >> Def.SHIFT_R;
                        int currentG = (copyArray[index] & Def.MASK_G) >> Def.SHIFT_G;
                        int currentB = (copyArray[index] & Def.MASK_B) >> Def.SHIFT_B;
                        int limit = (int)(Rlist.Count * 0.2);
                        int resultR;
                        int resultG;
                        int resultB;

                        if (currentR > Rlist[limit] && currentR < Rlist[Rlist.Count - 1 - limit])
                            resultR = currentR;
                        else
                            resultR = Rlist[Rlist.Count / 2];


                        if (currentG > Glist[limit] && currentG < Glist[Glist.Count - 1 - limit])
                            resultG = currentG;
                        else
                            resultG = Glist[Glist.Count / 2];

                        if (currentB > Blist[limit] && currentB < Blist[Blist.Count - 1 - limit])
                            resultB = currentB;
                        else
                            resultB = Blist[Blist.Count / 2];


                        array[index] = (array[index] & Def.MASK_A) |
                              (resultR << Def.SHIFT_R) |
                              (resultG << Def.SHIFT_G) |
                              (resultB << Def.SHIFT_B);


                    }
            }

            return source;
        }

    }
}
