﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Common;
using Windows.UI.Xaml.Controls;
using RGraphics.Models.ImageEffects;

namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Edge Detection methods
    /// </summary>
    public class EdgeDetectionViewModel : Screen, IImageEffect
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EdgeDetectionViewModel"/> class.
        /// </summary>
        public EdgeDetectionViewModel()
        {
            Items.Add(IoC.Get<Settings>().Res_UI.GetString("RobertsCross"));
            Items.Add(IoC.Get<Settings>().Res_UI.GetString("SobelFilter"));

            SelectedItem = Items[0];
        }

        /// <summary>
        /// The current edge detection sample
        /// </summary>
        private BitmapImage _CurrentSample;
        /// <summary>
        /// Gets or sets the current edge detection sample.
        /// </summary>
        /// <value>
        /// The current edge detection sample.
        /// </value>
        public BitmapImage CurrentSample
        {
            get { return _CurrentSample; }
            set
            {
                _CurrentSample = value;
                NotifyOfPropertyChange(() => CurrentSample);
            }
        }

        private ObservableCollection<string> _Items = new ObservableCollection<string>();
        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        public ObservableCollection<string> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;

                

                NotifyOfPropertyChange(() => Items);
            }
        }

        private string _SelectedItem;
        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;

				if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("RobertsCross"))
                    CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/robertcross.png"));
				else if (_SelectedItem == IoC.Get<Settings>().Res_UI.GetString("SobelFilter"))
                    CurrentSample = new BitmapImage(new Uri("ms-appx:///Assets/Effects Samples/sobelfilter.png"));

                    NotifyOfPropertyChange(() => SelectedItem);
            }
        }

        /// <summary>
        /// The RNG
        /// </summary>
        private Random Rng = new Random();



        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return IoC.Get<Settings>().Res_ImageEffects.GetString("EdgeDetection_Name");; }
        }

        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description
        {
            get
            {
                return IoC.Get<Settings>().Res_ImageEffects.GetString("EdgeDetection_Description");
            }
        }

        public bool ApplyEnabled { get { return true; } set { throw new NotImplementedException(); } }

        /// <summary>
        /// Applies the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        public WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {
            if (SelectedItem == Items[0])
                return ApplyRobertsCross(source, operationState);
            else if (SelectedItem == Items[1])
                return ApplySobelsFilter(source, operationState);
            else
                throw new InvalidOperationException();

        }

        /// <summary>
        /// Applies the RobertsCross to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplyRobertsCross(WriteableBitmap source, IProgressReporter operationState)
        {
            // px, p0, p1, p2 stands for the indexes of pixel and its neighbors in such a grid:
            //  px  p0
            //  p1  p2
            using (BitmapContext context = source.GetBitmapContext(), copy = source.Clone().GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] array = context.Pixels;
                int[] copy_array = copy.Pixels;
                for (int x = 0; x < context.Width; ++x)
                    for (int y = 0; y < context.Height; ++y)
                    {
                        int x1 = x + 1 == context.Width ? x : x + 1;
                        int y1 = y + 1 == context.Height ? y : y + 1;
                        int px = Util.Index(x, y, context.Width);
                        int p0 = Util.Index(x1, y, context.Width);
                        int p1 = Util.Index(x, y1, context.Width); 
                        int p2 = Util.Index(x1, y1, context.Width);

                        int r = Math.Abs(((copy_array[px] & Def.MASK_R) >> Def.SHIFT_R) - ((copy_array[p2] & Def.MASK_R) >> Def.SHIFT_R)) 
                            + Math.Abs(((copy_array[p0] & Def.MASK_R) >> Def.SHIFT_R) - ((copy_array[p1] & Def.MASK_R) >> Def.SHIFT_R));
                        int g = Math.Abs(((copy_array[px] & Def.MASK_G) >> Def.SHIFT_G) - ((copy_array[p2] & Def.MASK_G) >> Def.SHIFT_G))
                            + Math.Abs(((copy_array[p0] & Def.MASK_G) >> Def.SHIFT_G) - ((copy_array[p1] & Def.MASK_G) >> Def.SHIFT_G));
                        int b = Math.Abs(((copy_array[px] & Def.MASK_B) >> Def.SHIFT_B) - ((copy_array[p2] & Def.MASK_B) >> Def.SHIFT_B))
                            + Math.Abs(((copy_array[p0] & Def.MASK_B) >> Def.SHIFT_B) - ((copy_array[p1] & Def.MASK_B) >> Def.SHIFT_B));

                        r = r.CutToByte();
                        g = g.CutToByte();
                        b = b.CutToByte();
                        array[px] = (array[px] & Def.MASK_A) |
                              (r << Def.SHIFT_R) |
                              (g << Def.SHIFT_G) |
                              (b << Def.SHIFT_B);
                    }

            }


            return source;
        }

        /// <summary>
        /// Applies the SobelsFilter to the specified WriteableBitmap.
        /// </summary>
        /// <param name="source">The image.</param>
        /// <param name="operationState">State of the operation.</param>
        /// <returns>WriteableBitmap with applied effect.</returns>
        public WriteableBitmap ApplySobelsFilter(WriteableBitmap source, IProgressReporter operationState)
        {

            // p0, p1, p2, p3, p4, p5, p6, p7, px stands for the indexes of pixel and its neighbors in such a grid:
            //  p0 | p1 | p2
            // --------------
            //  p7 | px | p3
            // --------------
            //  p6 | p5 | p4
            using (BitmapContext context = source.GetBitmapContext(), copy = source.Clone().GetBitmapContext(ReadWriteMode.ReadOnly))
            {
                int[] array = context.Pixels;
                int[] copy_array = copy.Pixels;
                for (int x = 0; x < context.Width; ++x)
                    for (int y = 0; y < context.Height; ++y)
                    {
                        int xm1 = x == 0 ? 0 : x - 1;
                        int ym1 = y == 0 ? 0 : y - 1;
                        int xp1 = x+1 == context.Width ? x : x + 1;
                        int yp1 = y + 1 == context.Height ? y : y + 1;

                        int px = Util.Index(x, y, context.Width);
                        int p0 = Util.Index(xm1, ym1, context.Width);
                        int p1 = Util.Index(x, ym1, context.Width);
                        int p2 = Util.Index(xp1, ym1, context.Width);
                        int p3 = Util.Index(xp1, y, context.Width);
                        int p4 = Util.Index(xp1, yp1, context.Width);
                        int p5 = Util.Index(x, yp1, context.Width);
                        int p6 = Util.Index(xm1, yp1, context.Width);
                        int p7 = Util.Index(xm1, y, context.Width);

                        int tempX = ((copy_array[p2] & Def.MASK_R) >> Def.SHIFT_R) + 2* ((copy_array[p3] & Def.MASK_R) >> Def.SHIFT_R) + ((copy_array[p4] & Def.MASK_R) >> Def.SHIFT_R)
                             - (((copy_array[p0] & Def.MASK_R) >> Def.SHIFT_R) + 2 * ((copy_array[p7] & Def.MASK_R) >> Def.SHIFT_R)+((copy_array[p6] & Def.MASK_R) >> Def.SHIFT_R));
                        int tempY = ((copy_array[p6] & Def.MASK_R) >> Def.SHIFT_R) + 2 * ((copy_array[p5] & Def.MASK_R) >> Def.SHIFT_R) + ((copy_array[p4] & Def.MASK_R) >> Def.SHIFT_R)
                             - (((copy_array[p0] & Def.MASK_R) >> Def.SHIFT_R) + 2 * ((copy_array[p1] & Def.MASK_R) >> Def.SHIFT_R) + ((copy_array[p2] & Def.MASK_R) >> Def.SHIFT_R));
                        int r = (int)(Math.Sqrt(Math.Pow(tempX, 2) + Math.Pow(tempY, 2)));

                        tempX = ((copy_array[p2] & Def.MASK_G) >> Def.SHIFT_G) + 2 * ((copy_array[p3] & Def.MASK_G) >> Def.SHIFT_G) + ((copy_array[p4] & Def.MASK_G) >> Def.SHIFT_G)
                             - (((copy_array[p0] & Def.MASK_G) >> Def.SHIFT_G) + 2 * ((copy_array[p7] & Def.MASK_G) >> Def.SHIFT_G) + ((copy_array[p6] & Def.MASK_G) >> Def.SHIFT_G));
                        tempY = ((copy_array[p6] & Def.MASK_G) >> Def.SHIFT_G) + 2 * ((copy_array[p5] & Def.MASK_G) >> Def.SHIFT_G) + ((copy_array[p4] & Def.MASK_G) >> Def.SHIFT_G)
                             - (((copy_array[p0] & Def.MASK_G) >> Def.SHIFT_G) + 2 * ((copy_array[p1] & Def.MASK_G) >> Def.SHIFT_G) + ((copy_array[p2] & Def.MASK_G) >> Def.SHIFT_G));
                        int g = (int)(Math.Sqrt(Math.Pow(tempX, 2) + Math.Pow(tempY, 2)));

                        tempX = ((copy_array[p2] & Def.MASK_B) >> Def.SHIFT_B) + 2 * ((copy_array[p3] & Def.MASK_B) >> Def.SHIFT_B) + ((copy_array[p4] & Def.MASK_B) >> Def.SHIFT_B)
                             - (((copy_array[p0] & Def.MASK_B) >> Def.SHIFT_B) + 2 * ((copy_array[p7] & Def.MASK_B) >> Def.SHIFT_B) + ((copy_array[p6] & Def.MASK_B) >> Def.SHIFT_B));
                        tempY = ((copy_array[p6] & Def.MASK_B) >> Def.SHIFT_B) + 2 * ((copy_array[p5] & Def.MASK_B) >> Def.SHIFT_B) + ((copy_array[p4] & Def.MASK_B) >> Def.SHIFT_B)
                             - (((copy_array[p0] & Def.MASK_B) >> Def.SHIFT_B) + 2 * ((copy_array[p1] & Def.MASK_B) >> Def.SHIFT_B) + ((copy_array[p2] & Def.MASK_B) >> Def.SHIFT_B));
                        int b = (int)(Math.Sqrt(Math.Pow(tempX, 2) + Math.Pow(tempY, 2)));

                        r = r.CutToByte();
                        g = g.CutToByte();
                        b = b.CutToByte();
                        array[px] = (array[px] & Def.MASK_A) |
                              (r << Def.SHIFT_R) |
                              (g << Def.SHIFT_G) |
                              (b << Def.SHIFT_B);
                    }
            }

            return source;
        }
    }
}
