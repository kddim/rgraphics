﻿using Caliburn.Micro;
using RGraphics.Models;
using RGraphics.Models.ImageEffects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Common;
using System.Diagnostics;
using Windows.UI.Popups;

namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Makes the image appear old, by using formula:
    /// R = R + 2 * param
    /// G = G + param
    /// param is value between 0 and 40
    /// If the new value overflows byte maximum (255) it gets cut to the maximum 255.
    /// </summary>
    public class CannyViewModel : Screen, IImageEffect
    {
        public CannyViewModel()
        {
            ThresholdLow = 30;
            ThresholdHigh = 60;
        }

        private void RefreshApplyEnabled()
        {
            ApplyEnabled = ThresholdLow < ThresholdHigh ? true : false;
        }

        private double _ThresholdLow;
        public double ThresholdLow
        {
            get { return _ThresholdLow; }
            set
            {
                _ThresholdLow = value;
                NotifyOfPropertyChange(() => ThresholdLow);
                RefreshApplyEnabled();
            }
        }

        private double _ThresholdHigh;
        public double ThresholdHigh
        {
            get { return _ThresholdHigh; }
            set
            {
                _ThresholdHigh = value;
                NotifyOfPropertyChange(() => ThresholdHigh);
                RefreshApplyEnabled();
            }
        }

        private const int Channels = 4;
        private int ImageWidth;
        private int ImageHeight;
        private double[,] ImageGrads;

        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Canny_Name"); } }

        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Canny_Description"); } }


        private bool _ApplyEnabled;
        public bool ApplyEnabled
        {
            get { return _ApplyEnabled; } //alt+tab nie dziala ;(
            set
            {
                if (_ApplyEnabled != value)
                {
                    _ApplyEnabled = value; // dziaua musisz sobie ustawic przesylanie keyi gdzies tam xd, przeslij kombinacje przyciskow, mam to zaznaczone to dziwne
                    NotifyOfPropertyChange(() => ApplyEnabled);
                }
            }
        }
        /// <summary>
        /// Applies effect to the specified object
        /// </summary
        /// <param name="source">The source image</param>
        /// <param name="operationState">Operation state: progress and cancel notification.</param>
        /// <returns><typeparamref name="IImageWrapper"/>instance with applied effect</returns>
        public unsafe WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {
            using (BitmapContext context = source.GetBitmapContext())
            {
                ImageWidth = context.Width;
                ImageHeight = context.Height;
                ImageGrads = new double[ImageHeight, ImageWidth];

                fixed (int* pixels = context.Pixels)
                {
                    // Colored sobel
                    ColoredSobel(pixels);

                    // Non-maximum suppression
                    NonMaxSuppressStep(pixels);

                    // Double thresholding?
                    DoubleThresholdStep(pixels);

                    // Histeresis
                    HisteresisStep(pixels);
                }
            }

            ImageGrads = null;
            return source;
        }

        private enum SobelColor
        {
            None,
            R,
            G,
            B,
            Y,
        }

        private unsafe void ColoredSobel(int* pixels)
        {
            int length = ImageWidth * ImageHeight * 4; // *4 because of 4 channels (B G R A in byte[])
            byte[] copy = new byte[length];
            byte* result = (byte*)pixels;

            for (int i = 0; i < length; ++i)
                copy[i] = result[i];

            // p0, p1, p2, p3, p4, p5, p6, p7, px stands for the indexes of pixel and its neighbors in such a grid:
            //  p0 | p1 | p2
            // --------------
            //  p7 | px | p3
            // --------------
            //  p6 | p5 | p4

            // creating most of the image

            // Pinning the ByteArray memory, so GC won't move it during work
            // Using ptr[] instead of copy[] is faster because OutOfBoundsArray checks aren't made
            fixed (byte* ptr = copy)
            {
                for (int y = 1; y < ImageHeight - 1; ++y)
                {
                    for (int x = 1; x < ImageWidth - 1; ++x)
                    {
                        // px = (x, y)
                        int px = GetIndex(x, y, 0);

                        int moveBottom = ImageWidth * Channels;
                        // p0 = (x-1, y-1)
                        int p0 = px - Channels - moveBottom;
                        // p1 = (x, y-1)
                        int p1 = px - moveBottom;
                        // p2 = (x+1, y-1)
                        int p2 = px + Channels - moveBottom;
                        // p3 = (x+1, y)
                        int p3 = px + Channels;
                        // p4 = (x+1, y+1)
                        int p4 = px + Channels + moveBottom;
                        // p5 = (x, y+1)
                        int p5 = px + moveBottom;
                        // p6 = (x-1, y+1)
                        int p6 = px - Channels + moveBottom;
                        // p7 = (x-1, y)
                        int p7 = px - Channels;

                        // tmp1 = (p2 + 2*p3 + p4) - (p0 + 2*p7 + p6)
                        double tmp1 = (ptr[p2] + (2.0 * ptr[p3]) + ptr[p4]) - (ptr[p0] + (2.0 * ptr[p7]) + ptr[p6]);
                        // tmp2 =  (p0 + 2*p1 + p2) - (p6 + 2*p5 + p4)
                        double tmp2 = (ptr[p0] + (2.0 * ptr[p1]) + ptr[p2]) - (ptr[p6] + (2.0 * ptr[p5]) + ptr[p4]);

                        // Setting gradient here for later processing
                        ImageGrads[y, x] = Math.Sqrt(Math.Pow(tmp1, 2.0) + Math.Pow(tmp2, 2.0));

                        // But the result has to be in bytes :)
                        result[px] = ((int)ImageGrads[y, x]).CutToByte();


                        double theta = tmp1 == 0.0 ? Math.Atan(tmp2 / 1.0) : Math.Atan(tmp2 / tmp1);

                        if (result[px] < ThresholdLow)
                            SetBlack(&result[px]);
                        else if (theta >= -3.0 * Math.PI / 8.0 && theta < -1.0 * Math.PI / 8.0)
                        {   // RED
                            result[px] = 0;
                            result[px + 1] = 0;
                            result[px + 2] = 255;
                        }
                        else if (theta >= -1.0 * Math.PI / 8.0 && theta < Math.PI / 8.0)
                        {   // YELLOW
                            result[px] = 0;
                            result[px + 1] = 255;
                            result[px + 2] = 255;
                        }
                        else if (theta >= Math.PI / 8.0 && theta < 3.0 * Math.PI / 8.0)
                        {   // GREEN
                            result[px] = 0;
                            result[px + 1] = 255;
                            result[px + 2] = 0;
                        }
                        else
                        {   // BLUE
                            result[px] = 255;
                            result[px + 1] = 0;
                            result[px + 2] = 0;
                        }
                    }
                }
            }
        }

        private unsafe void NonMaxSuppressStep(int* pixels)
        {
            byte* ptr = (byte*)pixels;
            int index = Channels * ImageWidth;
            for (int y = 1; y < ImageHeight - 1; ++y)
            {
                index += 4;
                for (int x = 1; x < ImageWidth - 1; ++x)
                {
                    switch (GetColor(ptr + index))
                    {
                        case SobelColor.R:
                            {
                                if (!IsCurrentGradientMax(ImageGrads[y, x], ImageGrads[y + 1, x + 1], ImageGrads[y - 1, x - 1]))
                                    SetBlack(&ptr[index]);
                                break;
                            }
                        case SobelColor.G:
                            {
                                if (!IsCurrentGradientMax(ImageGrads[y, x], ImageGrads[y - 1, x + 1], ImageGrads[y + 1, x - 1]))
                                    SetBlack(&ptr[index]);
                                break;
                            }
                        case SobelColor.B:
                            {
                                if (!IsCurrentGradientMax(ImageGrads[y, x], ImageGrads[y - 1, x], ImageGrads[y + 1, x]))
                                    SetBlack(&ptr[index]);
                                break;
                            }
                        case SobelColor.Y:
                            {
                                if (!IsCurrentGradientMax(ImageGrads[y, x], ImageGrads[y, x + 1], ImageGrads[y, x - 1]))
                                    SetBlack(&ptr[index]);
                                break;
                            }
                    }
                    index += 4;
                }
                index += 4;
            }
        }

        private unsafe void DoubleThresholdStep(int* pixels)
        {
            byte* ptr = (byte*)pixels;

            int index = Channels * ImageWidth;
            for (int y = 1; y < ImageHeight - 1; ++y)
            {
                index += 4;
                for (int x = 1; x < ImageWidth - 1; ++x)
                {
                    if (GetColor(ptr + index) != SobelColor.None)
                        DoubleThreshold(ptr + index, ImageGrads[y, x]);
                    index += 4;
                }
                index += 4;
            }
        }

        private unsafe void HisteresisStep(int* pixels)
        {
            const uint WHITE = 0xFFFFFFFF;
            const uint GRAY = 0xFF808080;

            const int xStart = 5;
            const int yStart = 5;

            byte* resultPtr = (byte*)pixels;

            int length = ImageWidth * ImageHeight * Channels;
            byte[] copy = new Byte[length];

            fixed (byte* copyPtr = copy)
            {
                for (int i = 0; i < length; ++i)
                    copyPtr[i] = resultPtr[i];

                int index = Channels * ImageWidth * yStart;
                for (int y = yStart; y < ImageHeight - yStart; ++y)
                {
                    index += Channels * xStart;
                    for (int x = xStart; x < ImageWidth - xStart; ++x)
                    {
                        //if (sourcePtr[source.GetIndex(x, y)] != unchecked((int)0xFF000000) && sourcePtr[source.IntArrIndex(x, y)] != WHITE)
                        //    Debug.Assert(true);
                        if (*((uint*)(copyPtr + GetIndex(x, y, 0))) == GRAY)
                        {
                            bool found_gray = false;
                            for (int i = -1; i <= 1; ++i)
                            {
                                for (int j = -1; j <= 1; ++j)
                                {
                                    int iArrIndex = GetIndex(x + j, y + i, 0);
                                    if (*((uint*)(copyPtr + iArrIndex)) == WHITE)
                                    {
                                        SetWhite(resultPtr + index);
                                        goto CONTINUE_LOOP;
                                    }
                                    else if (*((uint*)(copyPtr + iArrIndex)) == GRAY && !(i == 0 && j == 0))
                                    {
                                        found_gray = true;
                                        goto FOUND_GRAY;
                                    }
                                }
                            }
                        FOUND_GRAY:
                            if (found_gray)
                            {
                                for (int i = -2; i <= 2; ++i)
                                {
                                    for (int j = -2; j <= 2; ++j)
                                    {
                                        int iArrIndex = GetIndex(x + j, y + i, 0);
                                        if (*((uint*)(copyPtr + iArrIndex)) == WHITE)
                                        {
                                            SetWhite(resultPtr + index);
                                            goto CONTINUE_LOOP;
                                        }
                                    }
                                }
                            }
                            SetBlack(resultPtr + index);
                        }
                    CONTINUE_LOOP:
                        index += 4;
                    }
                    index += 4 * xStart;
                }
            }
        }

        #region Helper methods
        private unsafe void DoubleThreshold(byte* ptr, double gradient)
        {
            if (gradient < ThresholdLow)
                SetBlack(ptr);
            else if (gradient < ThresholdHigh)
                SetGray(ptr);
            else
                SetWhite(ptr);
        }


        private unsafe void SetBlack(byte* ptr)
        {
            // Did you know...?
            // C# .NET 4.5 compiler will change below line to:
            // *ptr = (byte)(*(sbyte*)(ptr + 1) = *(sbyte*)(ptr + 2) = (sbyte)0);
            ptr[0] = ptr[1] = ptr[2] = 0;
        }

        private unsafe void SetWhite(byte* ptr)
        {
            // Did you know...?
            // C# .NET 4.5 compiler will change below line to:
            // *ptr = (byte)(*(sbyte*)(ptr + 1) = *(sbyte*)(ptr + 2) = (sbyte)-1);
            ptr[0] = ptr[1] = ptr[2] = 255;
        }

        private unsafe void SetGray(byte* ptr)
        {
            // Did you know...?
            // C# .NET 4.5 compiler will change below line to:
            // *ptr = (byte)(*(sbyte*)(ptr + 1) = *(sbyte*)(ptr + 2) = sbyte.MinValue);
            ptr[0] = ptr[1] = ptr[2] = 128;
        }

        private int GetIndex(int x, int y, int channel)
        {
            return y * ImageWidth * Channels + x * Channels + channel;
        }

        private unsafe SobelColor GetColor(byte* ptr)
        {
            //index:  B:0| G:1| R:2
            //-----------------------
            //Red       0|   0| 255
            //Green     0| 255|   0
            //Blue    255|   0|   0
            //Yellow    0| 255| 255

            if (ptr[0] == 0)
            {
                if (ptr[1] == 0 && ptr[2] == 255)
                    return SobelColor.R;
                if (ptr[1] == 255)
                {
                    if (ptr[2] == 0)
                        return SobelColor.G;
                    if (ptr[2] == 255)
                        return SobelColor.Y;
                }
            }
            else if (ptr[0] == 255 && ptr[1] == 0 && ptr[2] == 0)
                return SobelColor.B;

            return SobelColor.None;
        }
        private bool IsCurrentGradientMax(double current, double other1, double other2)
        {
            return current > other1 && current > other2;
        }

        #endregion
    }

}
