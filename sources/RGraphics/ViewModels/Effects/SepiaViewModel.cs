﻿using Caliburn.Micro;
using RGraphics.Models;
using RGraphics.Models.ImageEffects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Common;
using System.Diagnostics;

namespace RGraphics.ViewModels.Effects
{
    /// <summary>
    /// Makes the image appear old, by using formula:
    /// R = R + 2 * param
    /// G = G + param
    /// param is value between 0 and 40
    /// If the new value overflows byte maximum (255) it gets cut to the maximum 255.
    /// </summary>
    public class SepiaViewModel : Screen, IImageEffect
    {
        private double _SepiaParameter;
        /// <summary>
        /// Parameter used in Sepia effect
        /// Should be a value between 0 and 40
        /// </summary>
        public double SepiaParameter
        {
            get { return _SepiaParameter; }
            set
            {
                _SepiaParameter = value;
                NotifyOfPropertyChange(() => SepiaParameter);
            }
        }

        /// <summary>
        /// Gets the effect's name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Sepia_Name"); } }

        /// <summary>
        /// Gets the effect's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get { return IoC.Get<Settings>().Res_ImageEffects.GetString("Sepia_Description"); } }

        public bool ApplyEnabled { get { return true; } set { throw new NotImplementedException(); } }

        /// <summary>
        /// Applies effect basing on specified source
        /// </summary
        /// <param name="source">The source image</param>
        /// <param name="operationState">Operation state: progress and cancel notification.</param>
        /// <returns><typeparamref name="IImageWrapper"/>instance with applied effect</returns>
        public WriteableBitmap Apply(WriteableBitmap source, IProgressReporter operationState)
        {
            using (BitmapContext context = source.GetBitmapContext())
            {
                int[] array = context.Pixels;
                for (int x = 0; x < source.PixelWidth; ++x)
                    for (int y = 0; y < source.PixelHeight; ++y)
                    {
                        int index = Util.Index(x, y, context.Width);
                        int r = (array[index] & Def.MASK_R) >> Def.SHIFT_R;
                        int g = (array[index] & Def.MASK_G) >> Def.SHIFT_G;
                        int b = (array[index] & Def.MASK_B) >> Def.SHIFT_B;
                        r += 2 * ((byte)SepiaParameter);
                        g += ((byte)SepiaParameter);
                        r = r.CutToByte();
                        g = g.CutToByte();
                        array[index] = (array[index] & Def.MASK_A) |
                              (r << Def.SHIFT_R) |
                              (g << Def.SHIFT_G) |
                              (b << Def.SHIFT_B);
                    }
            }
            return source;
        }
    }

}
