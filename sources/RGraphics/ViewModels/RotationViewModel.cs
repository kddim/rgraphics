﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using WinRTXamlToolkit.Imaging;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Models.DrawingTools;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// <para> The RotationViewModel associated with RotationView. </para>
    /// <para> Manages the rotation process. </para>
    /// </summary>
    public class RotationViewModel : DrawingToolBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RotationViewModel"/> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        public RotationViewModel(IWorkspace workspace)
            : base(workspace)
        {
            Icon = new SymbolIcon(Symbol.RepeatAll);
            HasAttributes = true;
        }


        /// <summary>
        /// <para> The `start drawing` event called when the user clicks/taps on the workspace. </para>
        /// <para> Empty implementation, because the Rotation does not implement any drawing. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with tap/click.</param>
        protected override Task Started(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }


        /// <summary>
        /// <para> The `continue drawing` event called when user moves the pointer (and when the drawing was started). </para>
        /// <para> Empty implementation, because the Rotation does not implement any drawing. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with moving the pointer.</param>
        protected override Task Updated(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }


        /// <summary>
        /// <para> The `end drawing` event called when user stops holding the tap/mouse click. </para>
        /// <para> Empty implementation, because the Rotation does not implement any drawing. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
        protected override Task Finished(PointerPoint p)
        {
            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }

        /// <summary>
        /// Handles the value changed event from the button and apply rotation to the image.
        /// </summary>
        private async void ApplyButton_ValueChanged()
        {
            if (Angle % 360 == 0)
                return;

            WriteableBitmap wb= AssignedWorkspace.Image.RotateFree(Angle, AutomaticCrop);

            if (ValidateSize(wb.PixelWidth, wb.PixelHeight))
            {
                AssignedWorkspace.Image = wb;
                AssignedWorkspace.UpdateActualSize(AssignedWorkspace.Image.PixelWidth, AssignedWorkspace.Image.PixelHeight);
                AssignedWorkspace.Zoom(AssignedWorkspace.ZoomFactor, WorkspacePart.Center);
            }

			await AssignedWorkspace.ImageProcessing.MergeWorkspaceAsync();
        }

        /// <summary>
        /// Validates the size of the image and checks bounds.
        /// </summary>
        /// <param name="newWidth">The new width.</param>
        /// <param name="newHeight">The new height.</param>
        /// <returns></returns>
        public bool ValidateSize( double newWidth, double newHeight) 
        {
            var settings = IoC.Get<Settings>() as Settings;

            if (newWidth > settings.MaxImageWidth || newHeight > settings.MaxImageHeight)
                return false;
            return true;
        }

        /// <summary>
        /// Represents the automatic crop boolean value. It is true when automatic crop is checked.
        /// </summary>
        private bool _AutomaticCrop = true;

        /// <summary>
        /// Gets or sets a value indicating whether [automatic crop].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [automatic crop]; otherwise, <c>false</c>.
        /// </value>
        public bool AutomaticCrop
        {
            get { return _AutomaticCrop; }
            set
            {
                _AutomaticCrop = value;
                NotifyOfPropertyChange(() => AutomaticCrop);
            }
        }


        /// <summary>
        /// The primary angle
        /// </summary>
        private int _Angle = 30;

        /// <summary>
        /// Gets or sets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        public int Angle
        {
            get { return _Angle; }
            set
            {
                _Angle = value;
                NotifyOfPropertyChange(() => Angle);
            }
        }
    }
}
