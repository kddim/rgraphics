﻿using RGraphics.Models;
using System;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Input;
using Windows.UI.Input.Inking;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;
using System.Linq;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using System.Collections.ObjectModel;
using WinRTXamlToolkit.Converters;
using Windows.UI.Text;
using RGraphics.Models.DrawingTools;
using Caliburn.Micro;
using System.Threading.Tasks;

namespace RGraphics.ViewModels{

    /// <summary>
    /// <para> The TextToolViewModel associated with TextToolView. </para>
    /// <para> It is responsible for the management of text tool functionality. </para>
    /// </summary>
    public class TextToolViewModel : DrawingToolBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextToolViewModel"/> class.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        public TextToolViewModel(IWorkspace workspace)
            : base(workspace)
        {
            Icon = new SymbolIcon(Symbol.Font);
            HasAttributes = true;

            var settings = IoC.Get<Settings>() as Settings;
            Fonts = settings.AvailableFonts;
            FontSize = settings.DefaultFontSize;
            SelectedFontIndex = settings.DefaultFont;

            EditTextBoxWidth = settings.MinTextBoxWidth;
            EditTextBoxHeight = settings.MinTextBoxHeight;

            SelectedFont = Fonts[SelectedFontIndex];
        }

        /// <summary>
        /// The custom text box
        /// </summary>
        TextBox customTextBox;

        /// <summary>
        /// Gets the moveable container keeping contents associated with custom textbox.
        /// </summary>
        /// <value>
        /// The moveable container.
        /// </value>
        public static Grid moveableContainer { get; private set; }

        /// <summary>
        /// Gets or sets the rectangle displaying selected size of custom text box.
        /// </summary>
        /// <value>
        /// The rectangle.
        /// </value>
        private Rectangle rectangle { get; set; }

        /// <summary>
        /// Gets or sets the start point of the selection.
        /// </summary>
        /// <value>
        /// The start point.
        /// </value>
        PointerPoint startPoint { get; set; }

        /// <summary>
        /// Gets or sets the end point of the selection.
        /// </summary>
        /// <value>
        /// The end point.
        /// </value>
        PointerPoint endPoint { get; set; }

        /// <summary>
        /// The base point of the selection.
        /// </summary>
        Point basePoint = new Point(0, 0);


        /// <summary>
        /// <para> The `start drawing` event called when the user clicks/taps on the workspace. </para>
        /// <para> Starts displaying the selection of text box. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with tap/click.</param>
        protected override Task Started(PointerPoint p)
        {
            if (moveableContainer != null)
                RemoveFromWorkSpace(moveableContainer);

            rectangle = new Rectangle()
            {
                #region rectangle properties
                StrokeThickness = 5,
                Stroke = new SolidColorBrush(Colors.LawnGreen),
                StrokeDashArray = new DoubleCollection(),
                StrokeDashOffset = 0.2
                #endregion
            };
            rectangle.StrokeDashArray.Add(2.0);
            Canvas.SetTop(rectangle, p.Position.Y);
            Canvas.SetLeft(rectangle, p.Position.X);

            AssignedWorkspace.Children.Add(rectangle);
            startPoint = p;
            endPoint = p;

            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }


        /// <summary>
        /// <para> The `continue drawing` event called when user moves the pointer (and when the drawing was started). </para>
        /// <para> Updates the selection of the specified text box space. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with moving the pointer.</param>
        protected override Task Updated(PointerPoint p)
        {
            double x = (p.Position.X < startPoint.Position.X) ? p.Position.X : startPoint.Position.X;
            double y = (p.Position.Y < startPoint.Position.Y) ? p.Position.Y : startPoint.Position.Y;

            var settings = IoC.Get<Settings>() as Settings;
            int halfOfThumbSize = (int)(settings.MinThumbSize / 2);

            if (checkRectangleBounds(x + halfOfThumbSize, y + halfOfThumbSize, Math.Abs(p.Position.X - startPoint.Position.X), Math.Abs(p.Position.Y - startPoint.Position.Y)))
            {
                Canvas.SetLeft(rectangle, x);
                Canvas.SetTop(rectangle, y);

                var height = p.Position.Y - startPoint.Position.Y;
                var width = p.Position.X - startPoint.Position.X;

                if ((height > 0 && width < 0) || (height < 0 && width > 0)) { height = 0; width = 0; }

                rectangle.Width = Math.Abs(width);
                rectangle.Height = Math.Abs(height);

                endPoint = p;
            }

            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }


        /// <summary>
        /// <para> The `end drawing` event called when user stops holding the tap/mouse click. </para>
        /// <para> Setups the moveable container on the workspace in the selected space. </para>
        /// </summary>
        /// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
        protected override Task Finished(PointerPoint p)
        {
            PointerPoint bPoint = (endPoint.Position.X < startPoint.Position.X || endPoint.Position.Y < startPoint.Position.Y ? endPoint : startPoint);
            basePoint = new Point(bPoint.Position.X, bPoint.Position.Y);

            moveableContainer = new Grid();

            var settings = IoC.Get<Settings>() as Settings;

            Thumb thumbRightBottom = createThumb(settings.MinThumbSize, HorizontalAlignment.Right, VerticalAlignment.Bottom);
            thumbRightBottom.DragDelta += thumbRightBottom_ManipulationDelta;

            #region bindings creation
            Binding bindTextBoxWidth = new Binding() { Source = this, Path = new PropertyPath("EditTextBoxWidth") };
            Binding bindTextBoxHeight = new Binding() { Source = this, Path = new PropertyPath("EditTextBoxHeight") };
            Binding bindTextBoxFont = new Binding() { Source = this, Path = new PropertyPath("SelectedFont") };
            Binding bindTextBoxFontSize = new Binding() { Source = this, Path = new PropertyPath("FontSize") };
            Binding bindTextBoxColor = new Binding() { Source = AssignedWorkspace, Path = new PropertyPath("SelectedColor"), Converter = new ColorToBrushConverter() };
            Binding bindTextBoxFontStyle = new Binding() { Source = this, Path = new PropertyPath("SelectedFontStyle") };
            Binding bindTextBoxFontWeight = new Binding() { Source = this, Path = new PropertyPath("SelectedFontWeight") };
            Binding bindTextBoxText = new Binding() { Source = this, Path = new PropertyPath("EditTextBoxText"), Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged };
            #endregion

            customTextBox = new TextBox()
            {
                #region customTextBox properties
                BorderThickness = new Thickness(settings.MinTextToolBorderThickness),
                Background = new SolidColorBrush(Colors.Transparent),
                Margin = new Thickness( (int)(settings.MinThumbSize/2)),
                Padding = new Thickness(0),
                AcceptsReturn = true,
                BorderBrush = new SolidColorBrush(Colors.LawnGreen),
                ManipulationMode = ManipulationModes.All,
                MinWidth = (rectangle.ActualWidth < settings.MinTextBoxWidth ? settings.MinTextBoxWidth : rectangle.ActualWidth),
                MinHeight = (rectangle.ActualHeight < settings.MinTextBoxHeight ? settings.MinTextBoxHeight : rectangle.ActualHeight)
                #endregion
            };

            EditTextBoxWidth = Double.NaN;
            EditTextBoxHeight = Double.NaN;

            #region bindings setup
            customTextBox.SetBinding(TextBox.WidthProperty, bindTextBoxWidth);
            customTextBox.SetBinding(TextBox.HeightProperty, bindTextBoxHeight);
            customTextBox.SetBinding(TextBox.FontFamilyProperty, bindTextBoxFont);
            customTextBox.SetBinding(TextBox.FontSizeProperty, bindTextBoxFontSize);
            customTextBox.SetBinding(TextBox.ForegroundProperty, bindTextBoxColor);
            customTextBox.SetBinding(TextBox.FontStyleProperty, bindTextBoxFontStyle);
            customTextBox.SetBinding(TextBox.FontWeightProperty, bindTextBoxFontWeight);
            customTextBox.SetBinding(TextBox.TextProperty, bindTextBoxText);
            #endregion

            moveableContainer.Children.Add(customTextBox);
            moveableContainer.Children.Add(thumbRightBottom);

            StackPanel stackPanel = createButtons(settings);
            moveableContainer.Children.Add(stackPanel);

            moveableContainer.RenderTransform = new TranslateTransform();

            #region tb movement away from the edge if necessary
            if (basePoint.X + settings.MinTextBoxWidth >= AssignedWorkspace.ActualWidth)
                basePoint = new Point(basePoint.X - (basePoint.X + settings.MinTextBoxWidth - AssignedWorkspace.ActualWidth + 15), basePoint.Y);

            if (basePoint.Y + settings.MinTextBoxHeight >= AssignedWorkspace.ActualHeight)
                basePoint = new Point(basePoint.X, basePoint.Y - (basePoint.Y + settings.MinTextBoxHeight - AssignedWorkspace.ActualHeight + 15));
            #endregion

            Canvas.SetLeft(moveableContainer, basePoint.X);
            Canvas.SetTop(moveableContainer, basePoint.Y);

            AssignedWorkspace.Children.Remove(rectangle);

            AssignedWorkspace.Children.Add(moveableContainer);
            EditTextBoxText = String.Empty;

            AssignedWorkspace.IsClipToBoundsEnabled = false;

            // Since this implementation deos not need to await,
            // then returning this, since task with predefined result creates no scheduling overhead
            return Task.FromResult(0); 
        }

        #region EVENTS

        /// <summary>
        /// Handles the ManipulationDelta event of the moveable container control. Responsible for moving textbox through the workspace.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The instance containing the event data.</param>
        private void thumbRightBottom_ManipulationDelta(object sender, DragDeltaEventArgs e)
        {
            if (checkTextBoxBounds(e.HorizontalChange, e.VerticalChange))
            {
                (moveableContainer.RenderTransform as TranslateTransform).X += e.HorizontalChange;
                (moveableContainer.RenderTransform as TranslateTransform).Y += e.VerticalChange;

                UpdateBasePoint(e.HorizontalChange, e.VerticalChange);
            }
        }

        /// <summary>
        /// Handles the Click event of the pasteButton control. It is responsible for pasting accepted text on the canvas.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The instance containing the event data.</param>
        async void pasteButton_Click(object sender, RoutedEventArgs e)
        {
            AssignedWorkspace.IsClipToBoundsEnabled = true;

            var textWriter = new TextWriter(AssignedWorkspace);
            textWriter.WriteText(
                #region arguments
                basePoint, 
                EditTextBoxText, 
                FontSize, 
                SelectedFont, 
                new SolidColorBrush(AssignedWorkspace.SelectedColor), 
                SelectedFontStyle, 
                SelectedFontWeight, 
                customTextBox.Margin, 
                customTextBox.BorderThickness, 
                moveableContainer.Children[0].RenderSize.Width, 
                moveableContainer.Children[0].RenderSize.Height
                #endregion
            );

            RemoveFromWorkSpace(moveableContainer);

            await AssignedWorkspace.ImageProcessing.MergeWorkspaceAsync(false);
        }

        /// <summary>
        /// Handles the Click event of the closeButton control. Closes moveable container with no changes on the workspace.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The instance containing the event data.</param>
        void closeButton_Click(object sender, RoutedEventArgs e)
        {
            AssignedWorkspace.IsClipToBoundsEnabled = true;
            RemoveFromWorkSpace(moveableContainer);
        }

        /// <summary>
        /// Increases font size.
        /// </summary>
        public void FontSizeIncrease_Clicked()
        {
            ++FontSize;
        }

        /// <summary>
        /// Decreases font size.
        /// </summary>
        public void FontSizeDecrease_Clicked()
        {
            --FontSize;
        }

        /// <summary>
        /// Checks the Bold attribute.
        /// </summary>
        public void Bold_Checked()
        {
            SelectedFontWeight = FontWeights.Bold;
        }

        /// <summary>
        /// Unchecks the Bold attribute.
        /// </summary>
        public void Bold_Unchecked()
        {
            SelectedFontWeight = FontWeights.Normal;
        }

        /// <summary>
        /// Checks the Italic attribute.
        /// </summary>
        public void Italic_Checked()
        {
            SelectedFontStyle = FontStyle.Italic;
        }

        /// <summary>
        /// Unchecks the Italic attribute.
        /// </summary>
        public void Italic_Unchecked()
        {
            SelectedFontStyle = FontStyle.Normal;
        }
        #endregion

        #region HELPFUL METHODS

        /// <summary>
        /// Updates the base point with specified coordinates.
        /// </summary>
        /// <param name="p1">X - coordinate</param>
        /// <param name="p2">Y - coordinate</param>
        private void UpdateBasePoint(double p1, double p2)
        {
            basePoint = new Point(basePoint.X + p1, basePoint.Y + p2);
        }

        /// <summary>
        /// Checks the text box bounds. The maximum permissible transgression is closely associated with the actual height and the actual width of the moveable container.
        /// </summary>
        /// <param name="hor">The horizontal position change.</param>
        /// <param name="ver">The vertival position change.</param>
        /// <returns></returns>
        private bool checkTextBoxBounds(double hor, double ver)
        {
            double horPos = basePoint.X + hor;
            double verPos = basePoint.Y + ver;

            if (horPos >= -customTextBox.ActualWidth &&
                horPos + moveableContainer.ActualWidth <= AssignedWorkspace.ActualWidth + customTextBox.ActualWidth &&
                verPos >= -customTextBox.ActualHeight &&
                verPos + moveableContainer.ActualHeight <= AssignedWorkspace.ActualHeight + customTextBox.ActualHeight)
                return true;
            return false;
        }

        /// <summary>
        /// Checks the rectangle bounds of the selection.
        /// </summary>
        /// <param name="hor">The horizontal position change.</param>
        /// <param name="ver">The vertival position change.</param>
        /// <param name="range1">The horizontal range.</param>
        /// <param name="range2">The vertical range.</param>
        /// <returns></returns>
        private bool checkRectangleBounds(double hor, double ver, double range1, double range2)
        {
            if (hor >= 0 && hor + range1 < AssignedWorkspace.ActualWidth && ver >= 0 && ver + range2 < AssignedWorkspace.ActualHeight)
                return true;
            return false;
        }

        /// <summary>
        /// Creates the buttons - paste and close buttons.
        /// </summary>
        /// <returns></returns>
        private StackPanel createButtons( Settings settings)
        {
            StackPanel stackPanel = new StackPanel()
            {
                #region stackPanel properties
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
                Orientation = Orientation.Horizontal,
                Margin = new Thickness(0)
                #endregion
            };

            Button closeButton = new Button()
            {
                #region close button properties
                Width = settings.MinThumbSize,
                Height = settings.MinThumbSize,
                Content = "\uE10A",
                Foreground = new SolidColorBrush(Colors.Black),
                Background = new SolidColorBrush(Colors.GreenYellow),
                BorderBrush = new SolidColorBrush(Colors.Black),
                BorderThickness = new Thickness(settings.MinTextToolBorderThickness),
                FontFamily = new FontFamily("Segoe UI Symbol"),
                FontSize = settings.MinThumbFontSize,
                Margin = new Thickness(0),
                Padding = new Thickness(0)
                #endregion
            };

            closeButton.Click += closeButton_Click;

            Button pasteButton = new Button()
            {
                #region paste button properties
                Width = settings.MinThumbSize,
                Height = settings.MinThumbSize,
                Content = "\uE10B",
                Foreground = new SolidColorBrush(Colors.Black),
                Background = new SolidColorBrush(Colors.GreenYellow),
                BorderBrush = new SolidColorBrush(Colors.Black),
                BorderThickness = new Thickness(settings.MinTextToolBorderThickness),
                FontFamily = new FontFamily("Segoe UI Symbol"),
                FontSize = settings.MinThumbFontSize,
                Margin = new Thickness(0),
                Padding = new Thickness(0)
                #endregion
            };

            pasteButton.Click += pasteButton_Click;

            stackPanel.Children.Add(closeButton);
            stackPanel.Children.Add(pasteButton);

            return stackPanel;
        }

        /// <summary>
        /// Removes specified element from the workspace.
        /// </summary>
        /// <param name="element">The element.</param>
        private void RemoveFromWorkSpace(UIElement element)
        {
            AssignedWorkspace.Children.Remove(element);
        }

        /// <summary>
        /// Creates the thumb.
        /// </summary>
        /// <param name="size">The size.</param>
        /// <param name="ha">The horizontal alignment attribute.</param>
        /// <param name="va">The vertical alignment attribute.</param>
        /// <returns></returns>
        private Thumb createThumb(int size, HorizontalAlignment ha, VerticalAlignment va)
        {
            var settings = IoC.Get<Settings>() as Settings;

            return new Thumb()
            {
                #region thumb properties
                Width = size,
                Height = size,
                HorizontalAlignment = ha,
                VerticalAlignment = va,
                Background = new SolidColorBrush(Colors.LawnGreen),
                BorderBrush = new SolidColorBrush(Colors.Black),
                BorderThickness = new Thickness(settings.MinTextToolBorderThickness),
                Padding = new Thickness(0),
                Margin = new Thickness(0)
                #endregion
            };
        }
        #endregion

        #region PROPERTIES
        /// <summary>
        /// The custom text box text.The default text is the empty string.
        /// </summary>
        private String _EditTextBoxText = String.Empty;

        /// <summary>
        /// Gets or sets the edit text box text.
        /// </summary>
        /// <value>
        /// The edit text box text.
        /// </value>
        public String EditTextBoxText
        {
            get { return _EditTextBoxText; }

            set
            {
                _EditTextBoxText = value;
                NotifyOfPropertyChange(() => EditTextBoxText);
            }
        }

        /// <summary>
        /// The font size. Default = 20.
        /// </summary>
        private int _FontSize;

        /// <summary>
        /// Gets or sets the size of the font. Checks bounds beetween 1-1000.
        /// </summary>
        /// <value>
        /// The size of the font.
        /// </value>
        public int FontSize
        {
            get { return _FontSize; }
            set
            {
                var settings = IoC.Get<Settings>() as Settings;

                _FontSize = Math.Max(1, value);
                _FontSize = Math.Min(_FontSize, settings.AbsoluteMaxFontSize);
                NotifyOfPropertyChange(() => FontSize);
            }
        }

        /// <summary>
        /// The available collection of fonts.
        /// </summary>
        public ObservableCollection<FontFamily> _Fonts;

        /// <summary>
        /// Gets or sets the collection of fonts.
        /// </summary>
        /// <value>
        /// The fonts.
        /// </value>
        public ObservableCollection<FontFamily> Fonts
        {
            get { return _Fonts; }
            set
            {
                _Fonts = value;
                NotifyOfPropertyChange(() => Fonts);
            }
        }

        /// <summary>
        /// The selected font. Default = Arial.
        /// </summary>
        private FontFamily _SelectedFont;

        /// <summary>
        /// Gets or sets the selected font.
        /// </summary>
        /// <value>
        /// The selected font.
        /// </value>
        public FontFamily SelectedFont
        {
            get { return _SelectedFont; }
            set
            {
                _SelectedFont = value;
                NotifyOfPropertyChange(() => SelectedFont);
            }
        }

        /// <summary>
        /// The currect text box width. Min = MinTextBoxWidth.
        /// </summary>
        private double _EditTextBoxWidth;

        /// <summary>
        /// Gets or sets the width of the text box. Checks minimum width of the text box.
        /// </summary>
        /// <value>
        /// The width of the text box.
        /// </value>
        public double EditTextBoxWidth
        {
            get { return _EditTextBoxWidth; }

            set
            {
                if (value != _EditTextBoxWidth)
                {
                    var settings = IoC.Get<Settings>() as Settings;

                    _EditTextBoxWidth = Math.Max(settings.MinTextBoxWidth, value);
                    NotifyOfPropertyChange(() => EditTextBoxWidth);
                }
            }
        }

        /// <summary>
        /// The current text box height. Default = MinTextBoxHeight.
        /// </summary>
        private double _EditTextBoxHeight;

        /// <summary>
        /// Gets or sets the height of the text box. Checks minimum height of the text box.
        /// </summary>
        /// <value>
        /// The height of the text box.
        /// </value>
        public double EditTextBoxHeight
        {
            get { return _EditTextBoxHeight; }

            set
            {
                if (value != _EditTextBoxHeight)
                {
                    var settings = IoC.Get<Settings>() as Settings;

                    _EditTextBoxHeight = Math.Max(settings.MinTextBoxHeight, value);
                    NotifyOfPropertyChange(() => EditTextBoxHeight);
                }
            }
        }

        /// <summary>
        /// The selected font style. Default = Normal.
        /// </summary>
        private FontStyle _SelectedFontStyle = FontStyle.Normal;

        /// <summary>
        /// Gets or sets the selected font style.
        /// </summary>
        /// <value>
        /// The selected font style.
        /// </value>
        public FontStyle SelectedFontStyle
        {
            get { return _SelectedFontStyle; }

            set
            {
                _SelectedFontStyle = value;
                NotifyOfPropertyChange(() => SelectedFontStyle);
            }
        }

        /// <summary>
        /// The selected font weight. Default = Normal.
        /// </summary>
        private FontWeight _SelectedFontWeight = FontWeights.Normal;

        /// <summary>
        /// Gets or sets the selected font weight.
        /// </summary>
        /// <value>
        /// The selected font weight.
        /// </value>
        public FontWeight SelectedFontWeight
        {
            get { return _SelectedFontWeight; }

            set
            {
                _SelectedFontWeight = value;
                NotifyOfPropertyChange(() => SelectedFontWeight);
            }
        }

        /// <summary>
        /// The selected font index. Default = -1 => None.
        /// </summary>
        private int _SelectedFontIndex = -1;

        /// <summary>
        /// Gets or sets the index of the selected font.
        /// </summary>
        /// <value>
        /// The index of the selected font.
        /// </value>
        public int SelectedFontIndex
        {
            get { return _SelectedFontIndex; }

            set
            {
                _SelectedFontIndex = value;
                NotifyOfPropertyChange(() => SelectedFontIndex);
            }
        }
        #endregion

    }
}
