﻿using Caliburn.Micro;
using RGraphics.Common;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Media.Capture;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;
using WinRTXamlToolkit.Imaging;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// The NewImageViewModel associated with NewImageView.
    /// </summary>
    public class NewImageViewModel : Screen, IDialog<WriteableBitmap>
    {
        private readonly Settings settings;
        /// <summary>
        /// Initializes a new instance of the <see cref="NewImageViewModel"/> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public NewImageViewModel(Settings settings)
        {
            this.settings = settings;
        }
        /// <summary>
        /// Returns whether "Apply" button is enabled
        /// Width and Height properties are notifying UI when they changes, to refresh this property
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is new image apply enabled; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsNewImageApplyEnabled
        {
            get { return !String.IsNullOrEmpty(NewImageWidth) && !String.IsNullOrEmpty(NewImageHeight); }
        }


        private Boolean _IsOpen;

        /// <summary>
        /// Determines whether the new image dialog is opened
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is open; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsOpen
        {
            get { return _IsOpen; }
            set
            {
                _IsOpen = value;
                NotifyOfPropertyChange(() => IsOpen);
            }
        }


        private WriteableBitmap _Result;

        /// <summary>
        /// Stores image result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public WriteableBitmap Result
        {
            get { return _Result; }
            set
            {
                _Result = value;
                NotifyOfPropertyChange(() => Result);
            }
        }

        private String _NewImageWidth;

        /// <summary>
        /// <para> Width of the new image which is passed by the user when writing in a textbox. </para>
        /// <para> It is bound to the UI. </para>
        /// <para> Notifies IsNewImageApplyEnabled to refresh its state. </para>
        /// </summary>
        /// <value>
        /// The new width of the image.
        /// </value>
        public String NewImageWidth
        {
            get { return _NewImageWidth; }
            set
            {
                _NewImageWidth = value;
                NotifyOfPropertyChange(() => NewImageWidth);
                NotifyOfPropertyChange(() => IsNewImageApplyEnabled);
            }
        }

        private String _NewImageHeight;

        /// <summary>
        /// <para> Height of the new image which is passed by the user when writing in a textbox. </para>
        /// <para> It is bound to the UI. </para>
        /// <para> Notifies IsNewImageApplyEnabled to refresh its state. </para>
        /// </summary>
        public String NewImageHeight
        {
            get { return _NewImageHeight; }
            set
            {
                _NewImageHeight = value;
                NotifyOfPropertyChange(() => NewImageHeight);
                NotifyOfPropertyChange(() => IsNewImageApplyEnabled);
            }
        }

        /// <summary>
        /// <para> Validates width and height passed by the user. </para>
        /// <para> This action is an attached behavior to the textboxes like this: </para>
        /// <para> caliburn:Message.Attach="[Event LostFocus] = [Action ValidateSize]" </para>
        /// </summary>
        public void ValidateSize()  // TODO : this is also in ScalingViewModel
        {
            int maxWidth = settings.MaxImageWidth;
            int maxHeight = settings.MaxImageHeight;

            int s;

            if (int.TryParse(NewImageWidth, out s) && s > maxWidth)
                NewImageWidth = maxWidth.ToString();

            if (int.TryParse(NewImageHeight, out s) && s > maxHeight)
                NewImageHeight = maxHeight.ToString();
        }

        /// <summary>
        /// <para> Creates empty image of user specified size. </para>
        /// <para> It is attached to apply button through Caliburn.Micro. </para>
        /// </summary>
        public void NewImageApply()
        {
            int w = int.Parse(NewImageWidth);
            int h = int.Parse(NewImageHeight);

			IoC.Get<ShellViewModel>().UndoRedoStack.Clear();

            FillWhiteImgByDimensions(w, h);

			IoC.Get<ShellViewModel>().UndoRedoStack.Push(Result);
        }

        /// <summary>
        /// Creates 800x800 empty image on application startup.
        /// </summary>
        public void ImageOnStartup()
        {
            int w = settings.ImageWidthOnStartup;
            int h = settings.ImageHeightOnStartup;

            settings.UpdateTextBoxSize(w,h);

            FillWhiteImgByDimensions(w, h);
        }

        /// <summary>
        /// Sets empty (white) width x height WriteableBitmap as Result
        /// Assumes that NewImage dialog is closing (IsOpen = false)
        /// </summary>
        /// <param name="width">new empty image width</param>
        /// <param name="height">new empty image height</param>
        private void FillWhiteImgByDimensions(int width, int height)
        {
            WriteableBitmap image = new WriteableBitmap(width, height);
            image.FillRectangle(0, 0, width, height, Colors.White);

            Result = image;
            IsOpen = false;
        }

        /// <summary>
        /// Loads image picked through FileOpenPicker.
        /// This method can't be unit tested because FileOpenPicker can't be mocked.
        /// </summary>
        public async void FromFile()
        {
            FileOpenPicker fop = new FileOpenPicker();
            fop.ViewMode = PickerViewMode.Thumbnail;
            fop.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            fop.FileTypeFilter.Add(".jpg");
            fop.FileTypeFilter.Add(".png");
            fop.FileTypeFilter.Add(".bmp");
            fop.FileTypeFilter.Add(".jpeg");

            StorageFile file = await fop.PickSingleFileAsync();

            if (file == null)
                return;

            Result = await Extensions.CreateWriteableBitmapFromFile(file);

			IoC.Get<ShellViewModel>().UndoRedoStack.Clear();
			IoC.Get<ShellViewModel>().UndoRedoStack.Push(Result);

            IsOpen = false;
        }

        /// <summary>
        /// Loads camera captured image.
        /// This method can't be unit tested because FileOpenPicker can't be mocked.
        /// </summary>
        public async void FromCamera()
        {
            CameraCaptureUI dialog = new CameraCaptureUI();
            Size aspectRatio = new Size(16, 9);
            dialog.PhotoSettings.CroppedAspectRatio = aspectRatio;
            StorageFile file = await dialog.CaptureFileAsync(CameraCaptureUIMode.Photo);

            if (file == null)
                return;

            Result = await Extensions.CreateWriteableBitmapFromFile(file);

			IoC.Get<ShellViewModel>().UndoRedoStack.Clear();
			IoC.Get<ShellViewModel>().UndoRedoStack.Push(Result);

            IsOpen = false;
        }
    }
}
