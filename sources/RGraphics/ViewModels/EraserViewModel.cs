﻿using Caliburn.Micro;
using RGraphics.Models;
using RGraphics.Models.DrawingTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace RGraphics.ViewModels
{
	public class EraserViewModel : DrawingToolBase
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EraserViewModel"/> class.
		/// </summary>
		/// <param name="workspace">The workspace.</param>
		public EraserViewModel(IWorkspace workspace)
			: base(workspace)
		{
			Icon = new BitmapIcon()
			{
				UriSource = new Uri("ms-appx:///Assets/eraser_icon.png"),
				Foreground = new SolidColorBrush(Colors.White)
			};

			ScaleX = 1.0;
			ScaleY = 1.0;

			HasAttributes = true;

			var settings = IoC.Get<Settings>();

			ThicknessIndex = settings.DefaultThickness;
		}

		/// <summary>
		/// Gets or sets the temporary data.
		/// </summary>
		/// <value>
		/// The temporary data.
		/// </value>
		private List<UIElement> tmpData { get; set; }

		private int _ThicknessIndex;
		/// <summary>
		/// Gets or sets the index of the thickness.
		/// </summary>
		/// <value>
		/// The index of the thickness.
		/// </value>
		public int ThicknessIndex
		{
			get { return _ThicknessIndex; }
			set
			{
				_ThicknessIndex = value;
				NotifyOfPropertyChange(() => ThicknessIndex);
			}
		}

		/// <summary>
		/// The thicknesses
		/// </summary>
		private readonly int[] Thicknesses = { 5, 8, 12, 20 };
		/// <summary>
		/// Gets or sets the polyline segments.
		/// </summary>
		/// <value>
		/// The polyline segments.
		/// </value>
		private PointCollection PolylineSegments { get; set; }

		/// <summary>
		/// The `start drawing` event called when the user clicks/taps on the workspace.
		/// </summary>
		/// <param name="p">The PointerPoint associated with tap/click.</param>
		protected override Task Started(PointerPoint p)
		{
			tmpData = new List<UIElement>();

			Polyline poly = new Polyline();
			poly.Fill = null;
			poly.Stroke = new SolidColorBrush(Colors.White);
			poly.StrokeThickness = Thicknesses[ThicknessIndex];
			PolylineSegments = new PointCollection();
			poly.Points = PolylineSegments;

			tmpData.Add(poly);
			AssignedWorkspace.Children.Add(poly);

			// Since this implementation deos not need to await,
			// then returning this, since task with predefined result creates no scheduling overhead
			return Task.FromResult(0);
		}

		/// <summary>
		/// The `continue drawing` event called when user moves the pointer (and when the drawing was started).
		/// </summary>
		/// <param name="p">The PointerPoint associated with moving the pointer.</param>
		protected override Task Updated(PointerPoint p)
		{
			PolylineSegments.Add(p.Position);

			// Since this implementation deos not need to await,
			// then returning this, since task with predefined result creates no scheduling overhead
			return Task.FromResult(0);
		}

		/// <summary>
		/// The `end drawing` event called when user stops holding the tap/mouse click.
		/// </summary>
		/// <param name="p">The PointerPoint associated with the position on which user stopped holding tap/click.</param>
		protected override async Task Finished(PointerPoint p)
		{
			if (PolylineSegments.Count > 1)
			{
				var path = new Path();
				PathGeometry geometry = new PathGeometry();
				path.Data = geometry;
				geometry.Figures = new PathFigureCollection();
				var figure = new PathFigure();
				figure.StartPoint = PolylineSegments.First();
				path.StrokeThickness = Thicknesses[ThicknessIndex];

				for (int i = 0; i < PolylineSegments.Count - 1; i += 2)
					figure.Segments.Add(
						new BezierSegment()
						{
							Point1 = PolylineSegments[i],
							Point2 = new Point(
								0.5 * (PolylineSegments[i].X + PolylineSegments[i + 1].X),
								0.5 * (PolylineSegments[i].Y + PolylineSegments[i + 1].Y)),
							Point3 = PolylineSegments[i + 1]
						});

				geometry.Figures.Add(figure);
				path.Stroke = new SolidColorBrush(Colors.White);
				path.StrokeThickness = Thicknesses[ThicknessIndex];

				AssignedWorkspace.Children.Add(path);
			}
			else
			{
				var path = new Path();
				PathGeometry geometry = new PathGeometry();
				path.Data = geometry;
				geometry.Figures = new PathFigureCollection();
				var figure = new PathFigure();
				figure.StartPoint = new Point(p.Position.X - 5, p.Position.Y);

				figure.Segments.Add(new LineSegment() { Point = new Point(p.Position.X + 5, p.Position.Y) });

				geometry.Figures.Add(figure);
				path.Stroke = new SolidColorBrush(Colors.White);
				path.StrokeThickness = Thicknesses[ThicknessIndex];

				AssignedWorkspace.Children.Add(path);

			}
			foreach (var t in tmpData)
				AssignedWorkspace.Children.Remove(t);

			await AssignedWorkspace.ImageProcessing.MergeWorkspaceAsync();

		}
	}
}
