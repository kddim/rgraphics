﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// The ProgressReportDialogViewModel associated with ProgressReportDialogView.
    /// </summary>
    public class ProgressReportDialogViewModel : Screen, IProgressReporter
    {
        /// <summary>
        /// Gets or sets the navigation service.
        /// </summary>
        /// <value>
        /// The navigation service.
        /// </value>
        private INavigationService NavigationService { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressReportDialogViewModel"/> class.
        /// </summary>
        /// <param name="navigationService">The navigation service.</param>
        public ProgressReportDialogViewModel(INavigationService navigationService)
        {
            this.NavigationService = navigationService;
        }



        private object _Content = "Applying effect..."; // TODO localization
        /// <summary>
        /// Gets or sets the content shown on the ProgressReporterDialogView
        /// </summary>
        public object Content
        {
            get { return _Content; }
            set
            {
                _Content = value;
                NotifyOfPropertyChange(() => Content);

            }
        }

        private bool _CanUserCancel = true;
        /// <summary>
        /// Gets or sets a value indicating whether operation can be cancelled
        /// </summary>
        public bool CanUserCancel
        {
            get { return _CanUserCancel; }
            set
            {
                _CanUserCancel = value;
                NotifyOfPropertyChange(() => CanUserCancel);
            }
        }

        private double _Progress = 0;
        /// <summary>
        /// Gets or sets current progress of the operation.
        /// </summary>
        /// <value>
        /// The progress.
        /// </value>
        public double Progress
        {
            get { return _Progress; }
            set
            {
                _Progress = value;
                NotifyOfPropertyChange(() => Progress);
            }
        }


        /// <summary>
        /// Cancels the operation.
        /// </summary>
        public void Cancel()
        {
            IsOperationCanceled = true;
            Close();
        }

        private async void Close()
        {
            VisualStateManager.GoToState(GetView() as Control, "Finished", true);   // TODO localization
            await Task.Delay(500);
            NavigationService.GoBack();
        }


        private bool _IsIndeterminate;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is indeterminate.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is indeterminate; otherwise, <c>false</c>.
        /// </value>
        public bool IsIndeterminate
        {
            get { return _IsIndeterminate; }
            set
            {
                _IsIndeterminate = value;
                NotifyOfPropertyChange(() => IsIndeterminate);
            }
        }


        private bool _IsOperationCanceled;
        /// <summary>
        /// Gets a value indicating whether operation was canceled.
        /// </summary>
        public bool IsOperationCanceled
        {
            get { return _IsOperationCanceled; }
            private set
            {
                _IsOperationCanceled = value;
                NotifyOfPropertyChange(() => IsOperationCanceled);
            }
        }

        /// <summary>
        /// Shows progress reporter dialog
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Show()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Closes the reporter dialog.
        /// </summary>
        public void Completed()
        {
            IsCompleted = true;

            Close();
        }


        private bool _IsCompleted;
        /// <summary>
        /// Gets a value indicating whether operation is completed.
        /// </summary>
        public bool IsCompleted
        {
            get { return _IsCompleted; }
            private set
            {
                _IsCompleted = value;
                NotifyOfPropertyChange(() => IsCompleted);
            }
        }



        /// <summary>
        /// Resets whole progress reporter (called before reuse).
        /// </summary>
        public void Reset()
        {
            IsCompleted = false;
            IsOperationCanceled = false;
            IsIndeterminate = false;
            Content = null;
            Progress = 0;
        }
    }
}
