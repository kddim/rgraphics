﻿using Caliburn.Micro;
using RGraphics.Common;
using RGraphics.Models;
using System;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml.Media.Imaging;
using RGraphics.Models.DrawingTools;
using Windows.ApplicationModel.Resources;
using Windows.UI.ApplicationSettings;
using Windows.ApplicationModel;
using Windows.UI.Popups;
using System.Threading.Tasks;
using Windows.UI.ViewManagement;

namespace RGraphics.ViewModels
{
	/// <summary>
	/// <para> The ShellViewModel associated with ShellView. </para>
	/// <para> This is application's main view model. </para>
	/// </summary>
    public class ShellViewModel : Screen, IHandle<SaveNotififaction>
    {
        private INavigationService NavigationService;
        private IWorkspace _Workspace;

        /// <summary>
        /// Gets or sets the workspace.
        /// </summary>
        /// <value>
        /// The workspace.
        /// </value>
        public IWorkspace Workspace
        {
            get { return _Workspace; }
            set
            {
                _Workspace = value;
                NotifyOfPropertyChange(() => Workspace);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellViewModel"/> class.
        /// </summary>
        /// <param name="navigationService">The navigation service.</param>
        /// <param name="eventAggregator">The event aggregator.</param>
        /// <param name="workspace">The workspace.</param>
        public ShellViewModel(INavigationService navigationService, IEventAggregator eventAggregator, IWorkspace workspace)
        {
            // Subscribing to  Caliburn.Micro's EventAggregator, 
            // so we will handle SaveNotififaction published events
            eventAggregator.Subscribe(this);

            this.NavigationService = navigationService;
            this.Workspace = workspace;

			UndoRedoStack = new LimitedStack<WriteableBitmap>();

            GenerateEmptyImageOnStartUp();

            DrawingTools = new BindableCollection<DrawingToolBase>();
            DrawingTools.Add(new PointerTool(Workspace));
            DrawingTools.Add(new PencilViewModel(Workspace));
            DrawingTools.Add(new TextToolViewModel(Workspace));
            DrawingTools.Add(new FloodFillViewModel(Workspace));
            DrawingTools.Add(new ShapesViewModel(Workspace));
            DrawingTools.Add(new ScalingViewModel(Workspace, eventAggregator));
            DrawingTools.Add(new RotationViewModel(Workspace));
			DrawingTools.Add(new EraserViewModel(Workspace));

            ImageSaveMode = SaveNotififaction.Mode.DoNotSave;
        }

		/// <summary>
		/// Undo-redo stack instance.
		/// </summary>
		/// <value>
		/// The images levels.
		/// </value>
		public LimitedStack<WriteableBitmap> UndoRedoStack { get; private set; }

        /// <summary>
        /// Called when a view is attached.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <param name="context">The context in which the view appears.</param>
        protected override void OnViewAttached(object view, object context)
        {
            base.OnViewAttached(view, context);
        }

        /// <summary>
        /// Called when an attached view's Loaded event fires.
        /// </summary>
        /// <param name="view"></param>
        protected override void OnViewLoaded(object view)
        {
            Workspace.RefreshUI();
        }

        /// <summary>
        /// Launches image effects menu.
        /// </summary>
        public async void EffectsMenu()   // TODO
        {
          //  IsDrawingToolsVisible = false;
          //  NavigationService.NavigateToViewModel<EffectsViewModel>();
            await Workspace.ImageProcessing.MergeWorkspaceAsync();
            await IoC.Get<EffectsDialogViewModel>().ShowDialogAsync();
        }

        /// <summary>
        /// Launches settings menu.
        /// </summary>
        public void Settings()
        {
            SettingsPane.Show();
        }

        /// <summary>
        /// <para> Goes into Histogram page. </para>
        /// <para> Side effect: closes drawing tools (not closing them creates a visual bug after returning back to main page). </para>
        /// </summary>
        public void HistogramPage()
        {
            var wb = Workspace.Image;
            if (wb == null)
                return;

            IsDrawingToolsVisible = false;
            NavigationService.NavigateToViewModel<HistogramViewModel>();
        }

        /// <summary>
        /// Generates the empty image on start up.
        /// </summary>
        private void GenerateEmptyImageOnStartUp()
        {
            var NewImage = IoC.Get<NewImageViewModel>();
            NewImage.ImageOnStartup();

            Workspace.Image = NewImage.Result as WriteableBitmap;

            Workspace.UpdateActualSize(Workspace.Image.PixelWidth, Workspace.Image.PixelHeight);
            Workspace.ZoomToFitWorkspace();

			UndoRedoStack.Push(Workspace.Image);
        }


        private Boolean _IsDrawingToolsVisible = false;

        /// <summary>
        /// Determines whether drawing tools are visible for the user.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is drawing tools visible; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsDrawingToolsVisible
        {
            get { return _IsDrawingToolsVisible; }
            set
            {
                _IsDrawingToolsVisible = value;
                NotifyOfPropertyChange(() => IsDrawingToolsVisible);
            }
        }

        private Boolean _IsMainMenuOpen;

        /// <summary>
        /// Determines whether main menu is visible for the user.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is main menu open; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsMainMenuOpen
        {
            get { return _IsMainMenuOpen; }
            set
            {
                _IsMainMenuOpen = value;
                NotifyOfPropertyChange(() => IsMainMenuOpen);
            }
        }

        private BindableCollection<DrawingToolBase> _DrawingTools;
        
        /// <summary>
        /// Collection of drawing tools.
        /// </summary>
        public BindableCollection<DrawingToolBase> DrawingTools
        {
            get { return _DrawingTools; }
            set
            {
                _DrawingTools = value;
                NotifyOfPropertyChange(() => DrawingTools);
            }
        }

        public void Clear_Clicked()
        {
            var cleared = Workspace.Image.Clone();
            cleared.Clear(Windows.UI.Colors.White);
            UndoRedoStack.Push(cleared);

            Workspace.Image = cleared;
        }

		public void Undo_Clicked()
		{
			Workspace.Image = UndoRedoStack.Back();

			Workspace.UpdateActualSize(Workspace.Image.PixelWidth, Workspace.Image.PixelHeight);
			Workspace.ZoomToFitWorkspace();
		}

		public void Redo_Clicked()
		{
			Workspace.Image = UndoRedoStack.Next();

			Workspace.UpdateActualSize(Workspace.Image.PixelWidth, Workspace.Image.PixelHeight);
			Workspace.ZoomToFitWorkspace();
		}

        /// <summary>
        /// Saves image into a file picked by the user.
        /// </summary>
        public async void Save()
        {
            var picker = new FileSavePicker();
            picker.FileTypeChoices.Add("PNG Image", new string[] { ".png" });
            picker.FileTypeChoices.Add("JPEG Image", new string[] { ".jpg" });
            StorageFile file = await picker.PickSaveFileAsync();
            if (file != null)
            {
                await Workspace.Image.Save(file);
                ImageSaveMode = SaveNotififaction.Mode.DoNotSave;
            }

        }

        /// <summary>
        /// Creates new image with size specified by the user.
        /// </summary>
        public async void NewImage()
        {
            if (ImageSaveMode == SaveNotififaction.Mode.Save)   // If image was modified, show save prompt
            {
                await ShowSavePrompt();
                if (SavePrompt == Result.Cancel)                // If user pressed cancel button, cancel action
                    return;
            }

            IsMainMenuOpen = false;

            NewImageViewModel newImage = IoC.Get<NewImageViewModel>();
            await newImage.ShowDialogAsync();

            if (newImage.Result != null)
            {
                Workspace.Image = newImage.Result as WriteableBitmap;
                Workspace.UpdateActualSize(Workspace.Image.PixelWidth, Workspace.Image.PixelHeight);
                Workspace.ZoomToFitWorkspace();

                ImageSaveMode = SaveNotififaction.Mode.DoNotSave;
            }

        }

        /// <summary>
        /// Navigates to CropView page.
        /// </summary>
        public void Transform()
        {
            IsDrawingToolsVisible = false;
            NavigationService.NavigateToViewModel<CropViewModel>();
        }

        #region IHandle<SaveNotification> implementation

        /// <summary>
        /// Handles SaveNotification message sent by Caliburn's Micro EventAggregator
        /// </summary>
        /// <param name="message">SaveNotification image containing save data</param>
        public void Handle(SaveNotififaction message)
        {
            ImageSaveMode = message.SaveMode;
        }

        private enum Result { Yes, No, Cancel }
        private Result SavePrompt;
        private async Task ShowSavePrompt()
        {
	        var res = new Settings().Res_UI;

			var dlg = new MessageDialog(res.GetString("MessageDialog_WarningUnsaved"), res.GetString("MessageDialog_TitleUnsaved"));
            
            dlg.Commands.Add(new UICommand(res.GetString("MessageDialog_Yes"), new UICommandInvokedHandler((x) => SavePrompt = Result.Yes)));
            dlg.Commands.Add(new UICommand(res.GetString("MessageDialog_No"), new UICommandInvokedHandler((x) => SavePrompt = Result.No)));
            dlg.Commands.Add(new UICommand(res.GetString("MessageDialog_Cancel"), new UICommandInvokedHandler((x) => SavePrompt = Result.Cancel)));
            await dlg.ShowAsync();

            if (SavePrompt == Result.Yes)
                Save();
        }

        private SaveNotififaction.Mode ImageSaveMode;
        #endregion
    }
}
