﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// Manages text tool settings.
    /// </summary>
    public class TextToolSettingsViewModel : Screen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextToolSettingsViewModel"/> class.
        /// </summary>
        public TextToolSettingsViewModel() {
            var settings = IoC.Get<Settings>() as Settings;

            Fonts = settings.AvailableFonts;
            FontSize = settings.DefaultFontSize.ToString();
            SelectedFont = settings.DefaultFont;
        }

        /// <summary>
        /// The available fonts
        /// </summary>
        private ObservableCollection<FontFamily> _Fonts;
        /// <summary>
        /// Gets or sets the fonts.
        /// </summary>
        /// <value>
        /// The fonts.
        /// </value>
        public ObservableCollection<FontFamily> Fonts
        {
            get { return _Fonts; }
            set
            {
                _Fonts = value;
                NotifyOfPropertyChange(() => Fonts);
            }
        }

        /// <summary>
        /// The font size
        /// </summary>
        private String _FontSize;
        /// <summary>
        /// Gets or sets the size of the font.
        /// </summary>
        /// <value>
        /// The size of the font.
        /// </value>
        public String FontSize
        {
            get { return _FontSize; }
            set
            {
                if (value != String.Empty)
                {
                    _FontSize = value;

                    var set = IoC.Get<Settings>();
                    set.DefaultFontSize = int.Parse(_FontSize);

                    NotifyOfPropertyChange(() => FontSize);
                }
            }
        }

        /// <summary>
        /// The selected font
        /// </summary>
        private int _SelectedFont;
        /// <summary>
        /// Gets or sets the selected font.
        /// </summary>
        /// <value>
        /// The selected font.
        /// </value>
        public int SelectedFont
        {
            get { return _SelectedFont; }
            set
            {
                _SelectedFont = value;

                var set = IoC.Get<Settings>();
                set.DefaultFont = _SelectedFont;

                NotifyOfPropertyChange(() => SelectedFont);
            }
        }

    }
}
