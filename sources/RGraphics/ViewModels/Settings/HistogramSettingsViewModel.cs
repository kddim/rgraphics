﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// Manages histogram settings.
    /// </summary>
    public class HistogramSettingsViewModel : Screen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HistogramSettingsViewModel"/> class.
        /// </summary>
        public HistogramSettingsViewModel() {
            var settings = IoC.Get<Settings>() as Settings;

            TextColor = settings.HistogramTextColorIndex;
            TicklineColor = settings.HistogramTicklineColorIndex;
            AxislineColor = settings.HistogramAxislineColorIndex;
        }

        /// <summary>
        /// The histogram text color
        /// </summary>
        private int _TextColor;
        /// <summary>
        /// Gets or sets the color of the histogram text.
        /// </summary>
        /// <value>
        /// The color of the text.
        /// </value>
        public int TextColor
        {
            get { return _TextColor; }
            set
            {
                _TextColor = value;

                var set = IoC.Get<Settings>();
                set.HistogramTextColorIndex = _TextColor;

                NotifyOfPropertyChange(() => TextColor);
            }
        }

        /// <summary>
        /// The histogram tickline color
        /// </summary>
        private int _TicklineColor;
        /// <summary>
        /// Gets or sets the color of the histogram tickline.
        /// </summary>
        /// <value>
        /// The color of the tickline.
        /// </value>
        public int TicklineColor
        {
            get { return _TicklineColor; }
            set
            {
                _TicklineColor = value;

                var set = IoC.Get<Settings>();
                set.HistogramTicklineColorIndex = _TicklineColor;

                NotifyOfPropertyChange(() => TicklineColor);
            }
        }

        /// <summary>
        /// The histogram axisline color
        /// </summary>
        private int _AxislineColor;
        /// <summary>
        /// Gets or sets the color of the histogram axisline.
        /// </summary>
        /// <value>
        /// The color of the axisline.
        /// </value>
        public int AxislineColor
        {
            get { return _AxislineColor; }
            set
            {
                _AxislineColor = value;

                var set = IoC.Get<Settings>();
                set.HistogramAxislineColorIndex = _AxislineColor;

                NotifyOfPropertyChange(() => AxislineColor);
            }
        }
    }
}
