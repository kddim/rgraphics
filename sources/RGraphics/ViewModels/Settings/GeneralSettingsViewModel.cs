﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// Manages general settings
    /// </summary>
    public class GeneralSettingsViewModel : Screen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GeneralSettingsViewModel"/> class.
        /// </summary>
        public GeneralSettingsViewModel() {
            var settings = IoC.Get<Settings>() as Settings;

            RefreshProperties(settings);
        }

        /// <summary>
        /// The image width on startup
        /// </summary>
        private String _ImageWidthOnStartup;
        /// <summary>
        /// Gets or sets the image width on startup.
        /// </summary>
        /// <value>
        /// The image width on startup.
        /// </value>
        public String ImageWidthOnStartup
        {
            get { return _ImageWidthOnStartup; }
            set
            {
                if (value != String.Empty)
                {
                    var set = IoC.Get<Settings>() as Settings;
                    _ImageWidthOnStartup = validateSize(value, set.MaxImageWidth);

                    set.ImageWidthOnStartup = int.Parse(_ImageWidthOnStartup);

                    NotifyOfPropertyChange(() => ImageWidthOnStartup);
                }
            }
        }

        /// <summary>
        /// The image height on startup
        /// </summary>
        private String _ImageHeightOnStartup;
        /// <summary>
        /// Gets or sets the image height on startup.
        /// </summary>
        /// <value>
        /// The image height on startup.
        /// </value>
        public String ImageHeightOnStartup
        {
            get { return _ImageHeightOnStartup; }
            set
            {
                if (value != String.Empty)
                {
                    var set = IoC.Get<Settings>() as Settings;
                    _ImageHeightOnStartup = validateSize(value, set.MaxImageHeight);
                    
                    set.ImageHeightOnStartup = int.Parse(_ImageHeightOnStartup);

                    NotifyOfPropertyChange(() => ImageHeightOnStartup);
                }
            }
        }

        /// <summary>
        /// Validates the size whether it is smaller than the range.
        /// </summary>
        /// <param name="size">The size.</param>
        /// <param name="limit">The limit.</param>
        /// <returns>Size which is beetween the bounds.</returns>
        private String validateSize(String size, int limit)
        {
            int isize = int.Parse(size);
            int ilimit = limit;

            if (isize > ilimit)
                return limit.ToString();
            return size;
        }

        /// <summary>
        /// Restores all default settings
        /// </summary>
        private void RestoreDefault()
        {
            var set = IoC.Get<Settings>() as Settings;
            set.RestoreDefaultSettings();

            RefreshProperties(set);
        }

        /// <summary>
        /// Refreshes the properties from general settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        private void RefreshProperties( Settings settings)
        {
            ImageWidthOnStartup = settings.ImageWidthOnStartup.ToString();
            ImageHeightOnStartup = settings.ImageHeightOnStartup.ToString();
        }
    }
}
