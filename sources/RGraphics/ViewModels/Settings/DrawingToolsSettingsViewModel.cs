﻿using Caliburn.Micro;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace RGraphics.ViewModels
{
    /// <summary>
    /// Manages drawing tools settings
    /// </summary>
    public class DrawingToolsSettingsViewModel : Screen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrawingToolsSettingsViewModel"/> class.
        /// </summary>
        public DrawingToolsSettingsViewModel() {
            var settings = IoC.Get<Settings>() as Settings;

            Thickness = settings.DefaultThickness;
            Color = settings.DefaultColorIndex;
        }

        /// <summary>
        /// The drawing tool color setting
        /// </summary>
        private int _Color;
        /// <summary>
        /// Gets or sets the drawing tool color setting.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public int Color
        {
            get { return _Color; }
            set
            {
                _Color = value;

                var set = IoC.Get<Settings>();
                set.DefaultColorIndex = _Color;

                NotifyOfPropertyChange(() => Color);
            }
        }

        /// <summary>
        /// The drawing tool thickness setting
        /// </summary>
        private int _Thickness;
        /// <summary>
        /// Gets or sets the drawing tool thickness setting.
        /// </summary>
        /// <value>
        /// The thickness.
        /// </value>
        public int Thickness
        {
            get { return _Thickness; }
            set
            {
                _Thickness = value;

                var set = IoC.Get<Settings>();
                set.DefaultThickness = _Thickness;

                NotifyOfPropertyChange(() => Thickness);
            }
        }
    }
}
