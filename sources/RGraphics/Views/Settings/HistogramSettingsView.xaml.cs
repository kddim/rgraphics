﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RGraphics.Views
{
    /// <summary>
    /// View for Histogram's Settings.
    /// </summary>
    public sealed partial class HistogramSettingsView : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HistogramSettingsView"/> class.
        /// </summary>
        public HistogramSettingsView()
        {
            this.InitializeComponent();
        }
    }
}
