﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace RGraphics.Common.Converters
{
	/// <summary>
	/// Class resposible for converting color to brush property.
	/// </summary>
	public class ColorToBrushConverter : IValueConverter
	{
		/// <summary>
		/// Converts color to brush property.
		/// </summary>
		/// <param name="value">Color value</param>
		/// <param name="targetType"> Target type</param>
		/// <param name="parameter"> Converter parameter</param>
		/// <param name="language"></param>
		/// <returns></returns>
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			return new SolidColorBrush((Color)value);
		}

		/// <summary>
		/// Converts back brush to color property.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="targetType"></param>
		/// <param name="parameter"></param>
		/// <param name="language"></param>
		/// <returns></returns>
		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotImplementedException();
		}
	}
}
