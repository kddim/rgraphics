﻿using Caliburn.Micro;
using Microsoft.Xaml.Interactivity;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using WinRTXamlToolkit.Interactivity;

namespace RGraphics.Common
{
    /// <summary>
    /// Extends ScrollViewer by adding Dependency Property which allows to bind actual Zoom Factor's value to the viewmodel.
    /// </summary>
    public class ScrollViewerBehavior : DependencyObject, IBehavior
    {

        /// <summary>
        /// Gets or sets the zoom factor.
        /// </summary>
        /// <value>
        /// The zoom factor.
        /// </value>
        public double ZoomFactor
        {
            get { return (double)GetValue(ZoomFactorProperty); }
            set { SetValue(ZoomFactorProperty, value); }
        }


        /// <summary>
        /// The zoom factor property
        /// </summary>
        public static readonly DependencyProperty ZoomFactorProperty =
            DependencyProperty.Register("ZoomFactor", typeof(double), typeof(ScrollViewerBehavior), new PropertyMetadata(null, ZoomFactorChanged));
        /// <summary>
        /// Zooms the factor changed.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ZoomFactorChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            ScrollViewerBehavior svb = source as ScrollViewerBehavior;
                double? h = null;
                double? v = null;
                float? z = (float)svb.ZoomFactor;
                svb.AssociatedScrollViewerObject.ChangeView(h, v, z);
        }



        /// <summary>
        /// Gets or sets the workspace.
        /// </summary>
        /// <value>
        /// The workspace.
        /// </value>
        public IWorkspace Workspace
        {
            get { return (IWorkspace)GetValue(WorkspaceProperty); }
            set { SetValue(WorkspaceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Workspace.  This enables animation, styling, binding, etc...
        /// <summary>
        /// The workspace property
        /// </summary>
        public static readonly DependencyProperty WorkspaceProperty =
            DependencyProperty.Register("Workspace", typeof(object), typeof(ScrollViewerBehavior), new PropertyMetadata(null, WorkspaceChanged));


        /// <summary>
        /// Workspaces the changed.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void WorkspaceChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            ScrollViewerBehavior svb = source as ScrollViewerBehavior;
            if (e.OldValue != null)
            {
                (e.OldValue as IWorkspace).ZoomChanged -= svb.ZoomChanged;
                (e.OldValue as IWorkspace).ActualSizeChanged -= svb.WorkspaceContentSizeChanged;
                (e.OldValue as IWorkspace).UIRefreshed -= svb.UIRefreshed;
            }
            if (e.NewValue != null)
            {
                (e.NewValue as IWorkspace).ZoomChanged += svb.ZoomChanged;
                (e.NewValue as IWorkspace).ActualSizeChanged += svb.WorkspaceContentSizeChanged;
                (e.NewValue as IWorkspace).UIRefreshed += svb.UIRefreshed;
            }

        }

        /// <summary>
        /// UIs the refreshed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void UIRefreshed(object sender, EventArgs e)
        {
            AssociatedScrollViewerObject.InvalidateArrange();
            AssociatedScrollViewerObject.InvalidateMeasure();
            AssociatedScrollViewerObject.InvalidateScrollInfo();
        }

        /// <summary>
        /// Workspaces the content size changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Models.SizeChangedEventArgs"/> instance containing the event data.</param>
        private void WorkspaceContentSizeChanged(object sender, Models.SizeChangedEventArgs e)
        {
            AssociatedScrollViewerObject.InvalidateArrange();
            AssociatedScrollViewerObject.InvalidateMeasure();
            AssociatedScrollViewerObject.InvalidateScrollInfo();
        }

        /// <summary>
        /// Zooms the changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ZoomChangedEventArgs"/> instance containing the event data.</param>
        private void ZoomChanged(object sender, ZoomChangedEventArgs e)
        {
            float? zoom = (float)e.ZoomFactor;
            AssociatedScrollViewerObject.ChangeView(e.HorizontalOffset, e.VerticalOffset, zoom);
        }

        /// <summary>
        /// The _ associated object
        /// </summary>
        private DependencyObject _AssociatedObject;
        /// <summary>
        /// Gets or sets the associated object.
        /// </summary>
        /// <value>
        /// The associated object.
        /// </value>
        public DependencyObject AssociatedObject
        {
            get { return _AssociatedObject; }
            set
            {
                _AssociatedObject = value;
            } 
        }

        /// <summary>
        /// Gets or sets the associated scroll viewer object.
        /// </summary>
        /// <value>
        /// The associated scroll viewer object.
        /// </value>
        private ScrollViewer AssociatedScrollViewerObject { get; set; }


        /// <summary>
        /// Attaches the specified associated object.
        /// </summary>
        /// <param name="associatedObject">The associated object.</param>
        /// <exception cref="System.InvalidOperationException">ScrollViewerViewBehavior can be attached only to objects of type ScrollViewer</exception>
        public void Attach(DependencyObject associatedObject)
        {
            if (associatedObject as ScrollViewer == null)
                throw new InvalidOperationException("ScrollViewerViewBehavior can be attached only to objects of type ScrollViewer");
            AssociatedObject = associatedObject;
            AssociatedScrollViewerObject = associatedObject as ScrollViewer;
            AssociatedScrollViewerObject.SizeChanged += WorkspaceSizeChanged;
        }

        /// <summary>
        /// Workspaces the size changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.SizeChangedEventArgs"/> instance containing the event data.</param>
        void WorkspaceSizeChanged(object sender, Windows.UI.Xaml.SizeChangedEventArgs e)
        {
            if (Workspace != null)
            {
                Workspace.UpdateWorkspaceSize(e.NewSize.Width, e.NewSize.Height);
            }
        }

        /// <summary>
        /// Detaches this instance.
        /// </summary>
        public void Detach()
        {
            if (Workspace != null)
                Workspace.ZoomChanged -= ZoomChanged;
            AssociatedScrollViewerObject.SizeChanged -= WorkspaceSizeChanged;
            AssociatedScrollViewerObject = null;
            AssociatedObject = null;
        }
    }
}
