﻿using Caliburn.Micro;
using Callisto.Controls;
using RGraphics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.Threading;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using System.IO;
using WinRTXamlToolkit.Imaging;

namespace RGraphics.Common
{
    /// <summary>
    /// This class contains method extensions.
    /// </summary>
    public static class Extensions
    {

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <exception cref="System.NotSupportedException"></exception>
        public static void ShowDialog(this IDialog viewModel)
        {
            var view = ViewLocator.LocateForModel(viewModel, null, null);

            if (view as CustomDialog == null)
                throw new NotSupportedException(String.Format("{0} must inherit from Callisto.CustomDialog", view.GetType().Name));

            ViewModelBinder.Bind(viewModel, view, null);


            var activator = viewModel as IActivate;
            if (activator != null)
                activator.Activate();

            viewModel.IsOpen = true;
        }

        /// <summary>
        /// Shows the dialog asynchronous.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        public static async Task ShowDialogAsync(this IDialog viewModel)
        {
            var view = ViewLocator.LocateForModel(viewModel, null, null);

            if (view as CustomDialog == null)
                throw new NotSupportedException(String.Format("{0} must inherit from Callisto.CustomDialog", view.GetType().Name));

            ViewModelBinder.Bind(viewModel, view, null);

            var activator = viewModel as IActivate;
            if (activator != null)
                activator.Activate();

            viewModel.IsOpen = true;

            await ThreadPool.RunAsync(new WorkItemHandler((IAsyncAction action) =>
            {
                while (viewModel.IsOpen)
                    Task.Delay(1000);
            }));
        }

        /// <summary>
        /// Creates the writeable bitmap from file.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        public static async Task<WriteableBitmap> CreateWriteableBitmapFromFile(StorageFile file)
        {
            IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.Read);
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.SetSource(fileStream);
            Guid decoderId;
            switch (file.FileType.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                    decoderId = BitmapDecoder.JpegDecoderId;
                    break;
                case ".bmp":
                    decoderId = BitmapDecoder.BmpDecoderId;
                    break;
                default:
                    decoderId = BitmapDecoder.PngDecoderId;
                    break;
            }
            BitmapDecoder decoder = await BitmapDecoder.CreateAsync(decoderId, fileStream);
            PixelDataProvider pixelData = await decoder.GetPixelDataAsync(
                BitmapPixelFormat.Bgra8,    // byte array format: Blue Green Red Alpha [8 bit values: 0-255] // the int format will still be ARGB
                BitmapAlphaMode.Straight,   // How to code Alpha channel
                new BitmapTransform(),
                ExifOrientationMode.IgnoreExifOrientation,
                ColorManagementMode.DoNotColorManage);

            return new WriteableBitmap((int)decoder.PixelWidth, (int)decoder.PixelHeight).FromByteArray(pixelData.DetachPixelData());
        }

        /// <summary>
        /// Saves the specified writeable bitmap.
        /// </summary>
        /// <param name="writeableBitmap">The writeable bitmap.</param>
        /// <param name="outputFile">The output file.</param>
        /// <param name="encoderId">The encoder identifier.</param>
        /// <returns></returns>
        public static async Task Save(this WriteableBitmap writeableBitmap, StorageFile outputFile, Guid encoderId)
        {
            Stream stream = writeableBitmap.PixelBuffer.AsStream();
            byte[] pixels = new byte[(uint)stream.Length];
            await stream.ReadAsync(pixels, 0, pixels.Length);

            using (var writeStream = await outputFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                var encoder = await BitmapEncoder.CreateAsync(encoderId, writeStream);
                encoder.SetPixelData(
                    BitmapPixelFormat.Bgra8,
                    BitmapAlphaMode.Premultiplied,
                    (uint)writeableBitmap.PixelWidth,
                    (uint)writeableBitmap.PixelHeight,
                    96,                 // TODO: Shouldnt we get this from the image?
                    96,
                    pixels);
                await encoder.FlushAsync();

                using (var outputStream = writeStream.GetOutputStreamAt(0))
                {
                    await outputStream.FlushAsync();
                }
            }
        }

        /// <summary>
        /// Saves the specified writeable bitmap.
        /// </summary>
        /// <param name="writeableBitmap">The writeable bitmap.</param>
        /// <param name="outputFile">The output file.</param>
        /// <returns></returns>
        public static async Task Save(this WriteableBitmap writeableBitmap, StorageFile outputFile)
        {
            Guid encoderId;

            var ext = "." + outputFile.FileType.ToLower();

            if (new[] { ".bmp", ".dib" }.Contains(ext))
            {
                encoderId = BitmapEncoder.BmpEncoderId;
            }
            else if (new[] { ".tiff", ".tif" }.Contains(ext))
            {
                encoderId = BitmapEncoder.TiffEncoderId;
            }
            else if (new[] { ".gif" }.Contains(ext))
            {
                encoderId = BitmapEncoder.GifEncoderId;
            }
            else if (new[] { ".jpg", ".jpeg", ".jpe", ".jfif", ".jif" }.Contains(ext))
            {
                encoderId = BitmapEncoder.JpegEncoderId;
            }
            else if (new[] { ".hdp", ".jxr", ".wdp" }.Contains(ext))
            {
                encoderId = BitmapEncoder.JpegXREncoderId;
            }
            else
            {
                encoderId = BitmapEncoder.PngEncoderId;
            }

            await writeableBitmap.Save(outputFile, encoderId);
        }

        /// <summary>
        /// Converts value above 255 to 255, value under 0 to 0.
        /// </summary>
        /// <param name="val">Value to be cut</param>
        /// <returns>val > 255 ? 255 : val < 0 ? 0 : val</returns>
        public static byte CutToByte(this int val)
        {
            return val > 255 ? (byte)255 : val < 0 ? (byte)0 : (byte)val;
        }

        /// <summary>
        /// Converts value above 255 to 255, value under 0 to 0.
        /// </summary>
        /// <param name="val">Value to be cut</param>
        /// <returns>val > 255 ? 255 : val < 0 ? 0 : val</returns>
        public static byte CutToByte(this long val)
        {
            return val > 255 ? (byte)255 : val < 0 ? (byte)0 : (byte)val;
        }

        /// <summary>
        /// <para> Returns pixel index in source.ToByteArray() </para>
        /// <para> Used by "low-level" functions </para>
        /// </summary>
        /// <param name="source">Input WriteableBitmap</param>
        /// <param name="x">x image coordinate</param>
        /// <param name="y">y image coordinate</param>
        /// <param name="channel">Color channel, A=0, R=1, G=2, B=3</param>
        /// <returns>Index in byte[]</returns>
        public static int BAIndex(this WriteableBitmap source, int x, int y, int channel = 0)
        {
            const int channels = 4;
            return y * source.PixelWidth * channels + x * channels + channel;
        }

        /// <summary>
        /// Returns length of WritableBitmap.
        /// </summary>
        /// <param name="wb">Input WriteableBitmap</param>
        /// <returns>Length of WrtiableBitmap</returns>
        public static int Length(this WriteableBitmap wb)
        {
            return wb.PixelHeight * wb.PixelWidth * 4;
        }


        /// <summary>
        /// <para> Point structure with 32-bit Integer coordinates. </para>
        /// <para> Used by CustomFloodFill implementation </para>
        /// </summary>
        internal struct Pnt
        {
            internal int X, Y;
        }

        /// <summary>
        /// <para> This extension method was copied from: http://winrtxamltoolkit.codeplex.com/discussions/538898 </para>
        /// <para> Due to WinRTXamlToolkit bug. </para>
        /// </summary>
        public static void CustomFloodFillScanlineReplace(
            this WriteableBitmap target, int x, int y, int oldColor, int fillColor, byte maxDiff)
        {
            using (var context = target.GetBitmapContext())
            {
                var pixels = context.Pixels;

                var width = target.PixelWidth;
                var height = target.PixelHeight;

                if (pixels[x + width * y] == fillColor)
                {
                    return;
                }

                var stack = new Stack<Pnt>();
                var scannedPoints = new List<Pnt>(); // Added this line

                stack.Push(new Pnt { X = x, Y = y });

                while (stack.Count > 0)
                {
                    var pnt = stack.Pop();
                    x = pnt.X;
                    y = pnt.Y;

                    if (scannedPoints.Contains(pnt))    // Added this line
                        continue;
                    scannedPoints.Add(pnt);             // Added this line

                    var y1 = y;

                    // Find first unfilled point in line
                    while (
                        y1 >= 0 &&
                        pixels[x + width * y1].MaxDiff(oldColor) < maxDiff)
                    //pixels[x + width * y1] == oldColor)
                    {
                        y1--;
                    }

                    y1++;
                    var spanLeft = false;
                    var spanRight = false;


                    while (
                        y1 < height &&
                        pixels[x + width * y1].MaxDiff(oldColor) < maxDiff)
                    {
                        pixels[x + width * y1] = fillColor;

                        if (!spanLeft &&
                            x > 0 &&
                            pixels[x - 1 + width * y1].MaxDiff(oldColor) < maxDiff)
                        {
                            stack.Push(new Pnt { X = x - 1, Y = y1 });

                            spanLeft = true;
                        }
                        else if (
                            spanLeft &&
                            x > 0 &&
                            pixels[x - 1 + width * y1].MaxDiff(oldColor) >= maxDiff)
                        {
                            spanLeft = false;
                        }

                        if (!spanRight &&
                            x < width - 1 &&
                            pixels[x + 1 + width * y1].MaxDiff(oldColor) < maxDiff)
                        {
                            stack.Push(new Pnt { X = x + 1, Y = y1 });
                            spanRight = true;
                        }
                        else if (
                            spanRight &&
                            x < width - 1 &&
                            pixels[x + 1 + width * y1].MaxDiff(oldColor) >= maxDiff)
                        {
                            spanRight = false;
                        }

                        y1++;
                    }
                }
            }
        }
    }
}
