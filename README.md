# README #

Welcome to the RGraphics project repository.

## What is RGraphics?

Rgraphics is a free raster image editor providing operations such as finger drawing, shapes, text, cropping, resizing, rotating, flood fill, etc. Additionally it allows using a series of editing filters.

[Check Windows Store for more (screenshots or download).](http://apps.microsoft.com/windows/pl-pl/app/6e925622-2d86-4d07-979b-7c9bc8a35af3)

The project has been written for Software Engineering university course at AGH University of Science and Technology in Poland.

It has been written in C# as Windows Store app and is covered with unit and UI (automatic!) tests.

After the release it hasn't been developped anymore.


## Old stuff - used to be here for the team ;)

### Some handy guides ###

* [Our wiki](https://bitbucket.org/kddim/rgraphics/wiki/Home)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Tasks ###

* **Read about [localization](https://bitbucket.org/kddim/rgraphics/wiki/Localization) and apply it whenever writing new code** (it is already applied for the old code)

* **Do the TODOs** (search project for TODO phrase)

* **Check for issues**